import "@testing-library/jest-native/extend-expect";

export function getTextContent(node: { children: any[] }) {
  return node.children.join("");
}

export const initialMetrics = {
  frame: { height: 0, width: 0, x: 0, y: 0 },
  insets: { bottom: 0, left: 0, right: 0, top: 0 },
};

export function pauseTest(timeout = 1000) {
  return new Promise((done) => {
    setTimeout(done, timeout);
  });
}
