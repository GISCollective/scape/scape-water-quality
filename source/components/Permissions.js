"use strict";

import React, { Component } from "react";
import { Platform, Text, TouchableOpacity, View } from "react-native";
import { check, request, PERMISSIONS, RESULTS } from "react-native-permissions";

import Section from "./Section";

const ReadySetGo = ({ isReady, navigation }) => {
  if (isReady) {
    return (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate("Place");
        }}
      >
        <Section title="Ready. Set. Go!" desc="Click here to file a report!" />
      </TouchableOpacity>
    );
  }

  return <Text>&nbsp;</Text>;
};

class Permissions extends Component {
  state = {
    locationAllowed: false,
  };

  getLocationPermission = async () => {
    let permission =
      Platform.OS === "ios"
        ? PERMISSIONS.IOS.LOCATION_WHEN_IN_USE
        : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION;

    const res = await check(permission);

    if (res === RESULTS.GRANTED) {
      this.setState({
        locationAllowed: true,
      });
    } else {
      request(permission).then((result) => {
        if (result === RESULTS.GRANTED) {
          this.setState({
            locationAllowed: true,
          });
        }
      });
    }
  };

  render() {
    return (
      <View>
        <TouchableOpacity onPress={this.getLocationPermission}>
          <Section
            title="Step One"
            success={this.state.locationAllowed}
            desc="This app uses location data to help report data accurately. Tap
              here to allow this app to access your location."
          />
        </TouchableOpacity>
        <ReadySetGo
          isReady={this.state.locationAllowed}
          navigation={this.props.navigation}
        />
      </View>
    );
  }
}

export default Permissions;
