jest.mock("react-native-keyboard-aware-scroll-view", () => {
  const KeyboardAwareScrollView = require("react-native").ScrollView;
  return { KeyboardAwareScrollView };
});

import "react-native";
import React from "react";
import Macro from "./Macro";

import { render, fireEvent, act } from "@testing-library/react-native";

describe("Macro", function () {
  let document;

  describe("when there is no category", () => {
    beforeEach(() => {
      document = render(<Macro />);
    });

    it("no section is rendered", () => {
      const section = document.queryByTestId("sensitivity-section");
      expect(section).toBeFalsy();
    });
  });

  describe("when there is a category with 2 items", () => {
    beforeEach(() => {
      const records = {
        "Sensitive.|PTI taxa Group I": [
          {
            "Common Name": "Caddisfly",
            "Taxonomic Rank": "Order Trichoptera",
            "Descrip. of Juvenile":
              "Three pairs of legs (6 total); segmented grub-like body; some kinds may have gills along lower and upper portions of the abdomen; small hair-like tails or hooks. Case builders may be enclosed in a case (retreat) that they construct using stream bottom\nmaterials such as pebbles, sand grains, woody debris,\npieces of plant material or some combination; others\nconstruct a net, which consists of materials held\ntogether by a silk-like thread.",
            "Description of Adult":
              "General features. Adult caddisflies are commonly 3 to 15 millimetres (0.118 to 0.590 inch) in length. Their anterior wings usually range from 4 to 20 millimetres in length, providing wing spans of 8 to 40 millimetres.",
            Tolerance: "Low",
            Distribution:
              "Worldwide and throughout North America. Species most diverse in well-aerated streams, but also occur in lakes, ponds, and marshes. Adults rest on nearby vegetation during the day; flight activity begins at dusk. Adults are attracted - sometimes in great numbers - to artificial light.\n",
            Sources:
              "https://www.seagrant.wisc.edu/wp-content/uploads/2020/06/caddisfly-1.jpg",
            Media: "https://www.youtube.com/watch?v=w3T9P7qOgE4",
          },
          {
            "Common Name": "Dobsonfly\n(Hellgrammites)",
            "Taxonomic Rank": "Order Megaloptera",
            "Descrip. of Juvenile":
              "Larval corydalids, commonly known as hellgrammites, can be quite large (3–8 cm long), long-lived (2–5 years), and are primarily found in streams.",
            "Description of Adult":
              "Dobsonflies are a subfamily of insects, Corydalinae, part of the Megalopteran family Corydalidae. The larvae (commonly called hellgrammites) are aquatic, living in streams, and the adults are often found along streams as well. ",
            Tolerance: "Low",
            Distribution:
              "The nine genera of dobsonflies are distributed in the Americas, Asia, and South Africa.",
            Sources:
              "https://www.sciencedirect.com/topics/agricultural-and-biological-sciences/corydalidae",
            Media: "https://www.youtube.com/watch?v=82J0VlXo6-0",
          },
        ],
      };
      document = render(<Macro records={records} />);
    });

    it("a section with title and subtitle are rendered", () => {
      const section = document.queryByTestId("sensitivity-section");
      const title = document.queryByTestId(
        "txt-section-title-sensitivity-section",
      );
      const desc = document.queryByTestId(
        "txt-section-desc-sensitivity-section",
      );

      expect(section).toBeTruthy();
      expect(title).toHaveTextContent("Sensitive.");
      expect(desc).toHaveTextContent("PTI taxa Group I");
    });

    it("renders two rows", () => {
      const rows = document.queryAllByTestId("sensitivity-section-row");

      expect(rows.length).toEqual(2);
    });
  });
});
