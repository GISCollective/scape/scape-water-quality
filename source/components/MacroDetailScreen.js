import React from "react";

import {
  StyleSheet,
  SafeAreaView,
  ScrollView,
  View,
  Image,
  TouchableOpacity,
} from "react-native";
import { H1, H2, P } from "../giscollective/components/html";
import { useTheme } from "../giscollective/hooks/useTheme";
import Section from "./atoms/Section";

const MacroDetailScreen = ({ navigation, route }) => {
  const theme = useTheme();

  const styles = StyleSheet.create({
    linkText: {
      color: theme.toColor("blue"),
    },
    row: {
      flexDirection: "row",
      justifyContent: "flex-start",
      alignItems: "flex-start",
      width: "60%",
      margin: 20,
    },
    header: {
      fontSize: 24,
      fontWeight: "bold",
    },
    subheader: {
      fontSize: 18,
    },
    textCenter: {
      textAlign: "center",
    },
    image: {
      flex: 1,
      aspectRatio: 1,
      alignSelf: "center",
      opacity: 1,
      resizeMode: "contain",
      width: "100%",
      height: 250,
      borderRadius: 10,
    },
  });

  const record = route.params.record;
  const onNavigate = (screenName, options) => {
    navigation.navigate(screenName, options);
  };

  return (
    <SafeAreaView style={theme.toStyle([], { backgroundColor: "gray-100" })}>
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View>
          <Image
            style={[
              styles.image,
              theme.toStyle(["mb-5", "mt-5"]),
              theme.iconTint,
            ]}
            source={record.icon}
          />
          {record["Common Name"] && (
            <H1 style={theme.toTextStyle(["text-center", "me-3", "ms-3"])}>
              {record["Common Name"]}
            </H1>
          )}
          {record["Taxonomic Rank"] && (
            <H2
              style={theme.toTextStyle(["text-center", "me-3", "ms-3", "mb-5"])}
            >
              {record["Taxonomic Rank"]}
            </H2>
          )}

          {record.drawing1 && (
            <Section
              style={theme.toStyle(["ms-3", "me-3"])}
              title="Drawing"
              center={true}
            >
              <Image style={styles.image} source={record.drawing1} />
            </Section>
          )}

          {record.photo_immature && (
            <Section
              style={theme.toStyle(["ms-3", "me-3"])}
              title="Juvenile"
              center={true}
            >
              <Image style={styles.image} source={record.photo_immature} />
            </Section>
          )}

          {record.photo_adult && (
            <Section
              style={theme.toStyle(["ms-3", "me-3"])}
              title="Adult"
              center={true}
            >
              <Image style={styles.image} source={record.photo_adult} />
            </Section>
          )}

          {record.Tolerance && (
            <Section style={theme.toStyle(["ms-3", "me-3"])} title="Tolerance">
              <P>{record.Tolerance}</P>
            </Section>
          )}

          {record["Descrip. of Juvenile"] && (
            <Section
              style={theme.toStyle(["ms-3", "me-3"])}
              title="Description of Juvenile"
            >
              <P>{record["Descrip. of Juvenile"]}</P>
            </Section>
          )}

          {record["Description of Adult"] && (
            <Section
              style={theme.toStyle(["ms-3", "me-3"])}
              title="Description of Adult"
            >
              <P>{record["Description of Adult"]}</P>
            </Section>
          )}

          {record.Distribution && (
            <Section
              style={theme.toStyle(["ms-3", "me-3"])}
              title="Distribution"
            >
              <P>{record.Distribution}</P>
            </Section>
          )}

          {record.Sources && (
            <Section style={theme.toStyle(["ms-3", "me-3"])} title="Sources">
              <TouchableOpacity
                onPress={() => {
                  onNavigate("Browser", { uri: record.Sources });
                }}
              >
                <P style={styles.linkText}>{record.Sources}</P>
              </TouchableOpacity>
            </Section>
          )}

          {record.Media && (
            <Section style={theme.toStyle(["ms-3", "me-3"])} title="Media">
              <TouchableOpacity
                onPress={() => {
                  onNavigate("Browser", { uri: record.Media });
                }}
              >
                <P style={styles.linkText}>{record.Media}</P>
              </TouchableOpacity>
            </Section>
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default MacroDetailScreen;
