import React, { useEffect, useState } from "react";
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import { useIsMounted } from "usehooks-ts";
import { Snackbar } from "react-native-paper";
import { SurveyId } from "./SurveyScreen";
import { useNavigationState } from "@react-navigation/native";
import { InputButton } from "../giscollective/components/input/InputButton";
import { useTheme } from "../giscollective/hooks/useTheme";
import { P } from "../giscollective/components/html";
import { hasPendingAnswer } from "../giscollective/lib/surveyAnswer";

const componentStyles = StyleSheet.create({
  container: {
    flex: 1,
  },
  col1: {
    flex: 2,
  },
  colLogo: {
    flex: 6,
    justifyContent: "center",
    alignItems: "center",
  },
  logo: {
    resizeMode: "contain",
    width: 150,
    height: 150,
  },
  headerImage: {
    flex: 1,
    height: 100,
    resizeMode: "stretch",
  },
});

const HomeScreen = ({
  onStart,
  onSend,
  onAbout,
  onSensors,
  storedAnswerCount,
}) => {
  const theme = useTheme();
  const isDarkMode = theme.name === "dark";

  const [snackState, setSnackState] = useState({});
  const [isSending, setIsSending] = useState(false);
  const [startLabel, setStartLabel] = useState("Start");
  const navigation = useNavigationState((state) => state);

  const handleStart = () => {
    onStart?.((message) => {
      setSnackState({ ...snackState, message, visible: true });
    });
  };

  const handleSensors = () => {
    onSensors?.();
  };

  const handleAbout = () => {
    onAbout?.();
  };

  const handleSend = async () => {
    let message = "Observations were successfully submitted.";
    setIsSending(true);
    try {
      await onSend?.();
    } catch (err) {
      message = "Some issues were encountered.";
    }

    setIsSending(false);
    setSnackState({ ...snackState, message, visible: true });
  };

  const onLoad = async () => {
    let shouldContinue = await hasPendingAnswer(SurveyId);

    if (shouldContinue) {
      setStartLabel("Continue");
    } else {
      setStartLabel("Start");
    }
  };

  const isMounted = useIsMounted();

  useEffect(() => {
    onLoad();
  }, [isMounted, navigation]);

  const sendMessage = `Send ${storedAnswerCount} observation${
    storedAnswerCount !== 1 ? "s" : ""
  }`;

  const headerImage = isDarkMode
    ? require("../../assets/home_top_dark.png")
    : require("../../assets/home_top_light.png");

  const containerStyle = theme.toStyle(["d-flex"], {
    ...componentStyles.container,
    backgroundColor: "blue-400",
  });

  const aboutTextStyle = theme.toTextStyle(
    ["text-center", "fw-bold", "mt-2", "text-decoration-underline", "w-100"],
    {},
  );

  const rowStyle = theme.toStyle(["mt-3", "mb-3", "ms-3", "me-3"]);

  return (
    <View style={containerStyle}>
      <View style={componentStyles.col1}>
        <RepeatImage
          style={[componentStyles.headerImage]}
          source={headerImage}
        />
      </View>
      <View style={componentStyles.colLogo}>
        <TouchableOpacity onPress={handleAbout}>
          <Image
            style={componentStyles.logo}
            source={require("../../assets/scape_logo.png")}
          />
        </TouchableOpacity>
      </View>

      <View
        style={[
          componentStyles.col1,
          { alignItems: "flex-end", flexDirection: "row" },
        ]}
      >
        <TouchableOpacity onPress={handleAbout} style={{ flex: 1 }}>
          <P style={aboutTextStyle}>About</P>
        </TouchableOpacity>
      </View>

      {storedAnswerCount > 0 && (
        <View style={[componentStyles.col1]}>
          <View style={rowStyle}>
            <InputButton
              classes={["btn-primary"]}
              testID="btn-send"
              onPress={handleSend}
            >
              {sendMessage}
            </InputButton>
          </View>
        </View>
      )}

      <View style={componentStyles.col1}>
        <View style={rowStyle}>
          <InputButton
            classes={["btn-primary"]}
            testID="btn-sensors"
            onPress={handleSensors}
          >
            Sensors
          </InputButton>
        </View>
      </View>

      <View style={componentStyles.col1}>
        <View style={rowStyle}>
          <InputButton
            classes={["btn-primary"]}
            testID="btn-start"
            onPress={handleStart}
          >
            {startLabel}
          </InputButton>
        </View>
      </View>
      <Snackbar
        testID="snack-message"
        visible={snackState.visible}
        onDismiss={() => setSnackState({ ...snackState, visible: false })}
        action={{
          label: "ok",
          onPress: () => {
            setSnackState({ ...snackState, visible: false });
          },
        }}
      >
        {snackState.message}
      </Snackbar>
    </View>
  );
};

export default HomeScreen;

const RepeatImage = ({ source, style }) => {
  let images = [],
    imgWidth = 100,
    winWidth = Dimensions.get("window").width;

  for (var i = 0; i < Math.ceil(winWidth / imgWidth); i++) {
    images.push(
      <Image key={i} source={source} style={[style, { width: imgWidth }]} />,
    );
  }

  return (
    <View style={{ flex: 1, flexDirection: "row" }}>
      {images.map(function (img, i) {
        return img;
      })}
    </View>
  );
};
