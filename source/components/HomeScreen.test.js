jest.mock("@react-native-community/netinfo", () => {
  return {
    useNetInfo: () => ({}),
  };
});

import "react-native";
import React from "react";
import HomeScreen from "./HomeScreen";
import { render, fireEvent, act, waitFor } from "@testing-library/react-native";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { NavigationContainer } from "@react-navigation/native";
import { initialMetrics, pauseTest } from "../testHelper";
import {
  LocalStorage,
  LocalStorageMemory,
} from "../giscollective/lib/localStorage";

describe("HomeScreen", function () {
  let document;
  let onSend;
  let onStart;

  beforeEach(() => {
    onSend = jest.fn();
    onStart = jest.fn();

    LocalStorage.instance = new LocalStorageMemory();
  });

  describe("when there are 2 stored answers", () => {
    beforeEach(async () => {
      onSend.mockReturnValue("");

      document = render(
        <NavigationContainer>
          <SafeAreaProvider initialMetrics={initialMetrics}>
            <HomeScreen
              onSend={onSend}
              onStart={onStart}
              storedAnswerCount={2}
            />
          </SafeAreaProvider>
        </NavigationContainer>,
      );

      await pauseTest(100);
    });

    describe("the start button", () => {
      it("is rendered", () => {
        const btn = document.queryByTestId("btn-start");
        expect(btn).toBeTruthy();
      });

      it("triggers onStart on press", async () => {
        const btn = document.queryByTestId("btn-start");

        await act(() => fireEvent.press(btn));

        expect(onStart).toBeCalled();
      });
    });

    describe("the send button", () => {
      it("is rendered", () => {
        const btn = document.queryByTestId("btn-send");

        expect(btn).toBeTruthy();
        expect(btn).not.toBeDisabled();
        expect(btn).toHaveTextContent("Send 2 observations");
      });

      it("triggers onSend on press", async () => {
        const btn = document.queryByTestId("btn-send");

        act(() => {
          fireEvent.press(btn);
        });

        await waitFor(() => {
          expect(onSend).toBeCalled();
        });
      });

      it("shows a Snackbar on successful sent", async () => {
        const btn = document.queryByTestId("btn-send");

        await act(async () => {
          await fireEvent.press(btn);
        });

        const snack = document.queryByTestId("snack-message");
        expect(snack).toBeTruthy();
        expect(snack).toHaveTextContent(
          "Observations were successfully submitted.ok",
        );
      });

      it("shows a Snackbar on fail send", async () => {
        onSend.mockReturnValue(Promise.reject(""));
        const btn = document.queryByTestId("btn-send");

        await act(async () => {
          await fireEvent.press(btn);
        });

        const snack = document.queryByTestId("snack-message");
        expect(snack).toBeTruthy();
        expect(snack).toHaveTextContent("Some issues were encountered.ok");
      });
    });
  });

  describe("when there is 1 stored answer", () => {
    beforeEach(async () => {
      document = render(
        <NavigationContainer>
          <SafeAreaProvider initialMetrics={initialMetrics}>
            <HomeScreen
              onSend={onSend}
              onStart={onStart}
              storedAnswerCount={1}
            />
          </SafeAreaProvider>
        </NavigationContainer>,
      );

      await pauseTest(100);
    });

    it("renders the send button", () => {
      const btn = document.queryByTestId("btn-send");

      expect(btn).toBeTruthy();
      expect(btn).not.toBeDisabled();
      expect(btn).toHaveTextContent("Send 1 observation");
    });
  });

  describe("when there are no stored answers", () => {
    beforeEach(async () => {
      document = render(
        <NavigationContainer>
          <SafeAreaProvider initialMetrics={initialMetrics}>
            <HomeScreen
              onSend={onSend}
              onStart={onStart}
              storedAnswerCount={0}
            />
          </SafeAreaProvider>
        </NavigationContainer>,
      );

      await pauseTest(100);
    });

    it("renders the send button", async () => {
      const btn = await document.queryByTestId("btn-send");
      expect(btn).toBeFalsy();
    });
  });
});
