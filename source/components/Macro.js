import React from "react";
import { StyleSheet, View, Image } from "react-native";
import { DataTable } from "react-native-paper";
import { useTheme } from "../giscollective/hooks/useTheme";
import { Dimensions } from "react-native";
import Section from "./atoms/Section";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { H6, P } from "../giscollective/components/html";

const cellHeight = 70;

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
  },

  row: {
    height: 70,
    margin: 0,
    padding: 0,
    justifyContent: "flex-start",
    alignItems: "center",
  },

  inputRow: {
    height: 160,
  },

  cell: {
    height: cellHeight,
    flex: 1,
  },

  inputCell: {
    height: 160,
  },

  nameRecord: {
    justifyContent: "flex-start",
    flex: 1,
    height: 80,
    flexDirection: "row",
    alignItems: "center",
  },

  imgContainer: {
    flexDirection: "row",
  },

  textTaxonomy: {
    fontSize: 12,
  },

  textName: {
    fontSize: 16,
  },

  image: {
    aspectRatio: 1,
    opacity: 1,
    resizeMode: "contain",
    height: 40,
    width: 40,
    marginRight: 10,
  },
});

const MacroName = ({ record }) => {
  const theme = useTheme();
  const width = Dimensions.get("window").width;

  const withEllipsis = (value) => {
    const maxWidth = width / 13;

    if (!value) {
      return "";
    }

    if (value.length > maxWidth) {
      return value.substring(0, maxWidth) + "...";
    }

    return value;
  };

  return (
    <View style={styles.nameRecord}>
      <View style={theme.toStyle(["me-3"], { width: 40, height: 40 })}>
        <Image style={[theme.iconTint, styles.image]} source={record.icon} />
      </View>

      <View
        style={{
          height: 40,
          justifyContent: "center",
        }}
      >
        <H6 numberOfLines={1} testID="common-name">
          {withEllipsis(record["Common Name"])}
        </H6>
        <P numberOfLines={1} testID="taxonomic-rank">
          {withEllipsis(record["Taxonomic Rank"])}
        </P>
      </View>
    </View>
  );
};

const MacroRow = ({ record, navigation }) => {
  return (
    <DataTable.Row style={styles.row} testID="sensitivity-section-row">
      <DataTable.Cell
        style={[styles.cell]}
        onPress={() =>
          navigation.navigate("MacroinvertebratesDetails", {
            record,
          })
        }
      >
        <MacroName record={record} />
      </DataTable.Cell>
    </DataTable.Row>
  );
};

const MacroCategory = ({ navigation, withCounters, title, records }) => {
  const theme = useTheme();
  const pieces = title.split("|");

  const getRow = (record) => {
    return (
      <MacroRow
        key={record["Common Name"]}
        record={record}
        navigation={navigation}
      />
    );
  };

  return (
    <Section
      testID="sensitivity-section"
      title={pieces[0]}
      desc={pieces[1]}
      titleStyle={theme.toStyle(["me-3", "ms-3"])}
    >
      <DataTable>{records.map(getRow)}</DataTable>
    </Section>
  );
};

const Macro = ({ navigation, route, records }) => {
  const theme = useTheme();

  return (
    <KeyboardAwareScrollView>
      <View style={theme.toStyle(["mt-4"])}>
        {Object.keys(records ?? {}).map((title) => (
          <MacroCategory
            key={title}
            navigation={navigation}
            route={route}
            title={title}
            records={records[title]}
            withCounters={route?.params?.count}
          />
        ))}
      </View>
    </KeyboardAwareScrollView>
  );
};

export default Macro;
