import React from "react";
import { Text, View, ScrollView, Image, TouchableOpacity } from "react-native";
import { P } from "../giscollective/components/html";
import { useTheme } from "../giscollective/hooks/useTheme";

const AboutScreen = ({ navigation }) => {
  const theme = useTheme();
  const isDarkMode = theme.name === "dark";

  const openURI = (uri) => () => {
    navigation.navigate("Browser", { uri });
  };

  const darkColor = theme.toColor("dark");

  const componentStyles = {
    TLIlogo: {
      height: 70,
      width: 200,
      tintColor: darkColor,
      alignSelf: "center",
    },

    EPAlogo: {
      height: 120,
      width: 120,
      alignSelf: "center",
    },

    LLlogo: {
      height: 50,
      width: 180,
      alignSelf: "center",
    },

    GISClogo: {
      height: 50,
      width: 200,
      alignSelf: "center",
    },

    FLBSlogo: {
      height: 80,
      width: 200,
      alignSelf: "center",
    },

    OGMlogo: {
      height: 110,
      width: 200,
      alignSelf: "center",
    },
  };

  const logoStyle = {
    resizeMode: "contain",
  };
  const appLogoStyle = theme.toStyle(["mt-5", "mb-3"], {
    ...logoStyle,
    height: 50,
    width: 50,
    tintColor: darkColor,
    alignSelf: "center",
  });
  const importantTextStyle = theme.toTextStyle(["fw-bold"], { color: "info" });
  const logoSpacerStyle = theme.toStyle(["mt-2"]);
  const pStyle = theme.toStyle(["mt-4", "ms-4", "me-4"]);

  return (
    <View style={theme.toStyle([], { backgroundColor: "gray-100" })}>
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <Image
          style={appLogoStyle}
          source={require("../../assets/scape_logo.png")}
        />

        <P style={pStyle}>
          The<Text> </Text>
          <Text style={importantTextStyle}>SCAPE Water Quality App</Text>
          <Text> </Text>
          has been developed for educators, students, citizen scientists, and
          researchers engaged in water-monitoring and data-collection efforts.
          It aids in calculating streamflow, water chemistry sampling, and
          “keying out” biological water quality indicators such as benthic
          macroinvertebrates. User location, date, and time of day is
          automatically recorded.
        </P>

        <P style={pStyle}>
          The SCAPE Water Quality App was developed by educators, scientists,
          designers, and programmers at<Text> </Text>
          <Text style={importantTextStyle}>
            Arizona State University in Tempe
          </Text>
          , Arizona and<Text> </Text>
          <Text style={importantTextStyle}>GISCollective</Text> in Berlin,
          Germany.
        </P>

        <View style={{ marginLeft: 100, paddingRight: 100 }}>
          <TouchableOpacity onPress={openURI("https://tellurideinstitute.org")}>
            <Image
              style={[logoStyle, logoSpacerStyle, componentStyles.TLIlogo]}
              source={require("../../assets/logos/TLI.png")}
            />
          </TouchableOpacity>

          <TouchableOpacity onPress={openURI("https://www.epa.gov/")}>
            <Image
              style={[logoStyle, logoSpacerStyle, componentStyles.EPAlogo]}
              source={require("../../assets/logos/EPA.png")}
            />
          </TouchableOpacity>

          <TouchableOpacity onPress={openURI("https://theluminositylab.com/")}>
            {!isDarkMode && (
              <Image
                style={[logoStyle, logoSpacerStyle, componentStyles.LLlogo]}
                source={require("../../assets/logos/LL_white.png")}
              />
            )}
            {isDarkMode && (
              <Image
                style={[logoStyle, logoSpacerStyle, componentStyles.LLlogo]}
                source={require("../../assets/logos/LL.png")}
              />
            )}
          </TouchableOpacity>

          <TouchableOpacity onPress={openURI("https://giscollective.com/")}>
            {!isDarkMode && (
              <Image
                style={[logoStyle, logoSpacerStyle, componentStyles.GISClogo]}
                source={require("../../assets/logos/giscollective_white.png")}
              />
            )}
            {isDarkMode && (
              <Image
                style={[logoStyle, logoSpacerStyle, componentStyles.GISClogo]}
                source={require("../../assets/logos/giscollective.png")}
              />
            )}
          </TouchableOpacity>

          <TouchableOpacity onPress={openURI("https://flbs.umt.edu/newflbs/")}>
            {!isDarkMode && (
              <Image
                style={[logoStyle, logoSpacerStyle, componentStyles.FLBSlogo]}
                source={require("../../assets/logos/flbs_white.png")}
              />
            )}
            {isDarkMode && (
              <Image
                style={[logoStyle, logoSpacerStyle, componentStyles.FLBSlogo]}
                source={require("../../assets/logos/flbs.png")}
              />
            )}
          </TouchableOpacity>

          <TouchableOpacity onPress={openURI("https://www.greenmap.org/")}>
            <Image
              style={[logoStyle, logoSpacerStyle, componentStyles.OGMlogo]}
              source={require("../../assets/logos/ogm.png")}
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default AboutScreen;
