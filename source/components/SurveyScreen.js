import React, { useState, useEffect } from "react";
import { SafeAreaView, View } from "react-native";
import { useIsMounted } from "usehooks-ts";
import { Store } from "../giscollective/models/store";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { CampaignForm } from "../giscollective/components/view/CampaignForm";
import { FetchStatus } from "../giscollective/components/view/FetchStatus";
import { useTheme } from "../giscollective/hooks/useTheme";

export const SurveyId = "61e179d0f04ae50100e433bc";

export const SurveyScreen = ({ onSubmit, onNavigate }) => {
  const theme = useTheme();
  const [model, setModel] = useState();
  const [value, setValue] = useState();
  const [error, setError] = useState();

  const containerStyle = theme.toStyle(["mt-3", "ms-3", "me-3", "mb-3"], {
    flex: 1,
    minHeight: "100%",
  });

  const isMounted = useIsMounted();

  useEffect(() => {
    if (!isMounted()) {
      return;
    }

    const loadData = async () => {
      try {
        const survey = await Store.instance.findCampaign(SurveyId);

        setModel({ survey });
        setValue({ modelKey: "survey", data: {} });
      } catch (err) {
        setError(err);
      }
    };

    loadData();
  }, [isMounted]);

  if (!model || !value) {
    return <FetchStatus error={error} />;
  }

  return (
    <SafeAreaView style={{ height: "100%" }}>
      <KeyboardAwareScrollView contentInsetAdjustmentBehavior="automatic">
        <View style={containerStyle}>
          <CampaignForm model={model} value={value} />
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};
