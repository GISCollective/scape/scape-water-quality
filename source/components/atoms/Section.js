import React from "react";
import { useTheme } from "../../giscollective/hooks/useTheme";
import { View } from "react-native";
import { H1, H2 } from "../../giscollective/components/html";

const Section = ({
  children,
  title,
  desc,
  testID,
  center,
  style,
  titleStyle,
}) => {
  const styles = {};
  const theme = useTheme();
  const viewStyle = {
    borderColor: "primary",
    borderBottomWidth: 1,
  };

  if (center) {
    viewStyle.alignItems = "center";
  }

  return (
    <View
      style={[theme.toStyle(["mb-5", "pb-5"], viewStyle), style]}
      testID={testID}
    >
      <View style={titleStyle}>
        {title && <H1 testID={`txt-section-title-${testID}`}>{title}</H1>}
        {desc && <H2 testID={`txt-section-desc-${testID}`}>{desc}</H2>}
      </View>
      {children}
    </View>
  );
};

export default Section;
