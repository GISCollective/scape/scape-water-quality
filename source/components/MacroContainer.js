import React from "react";
import useMacroInvertebrates from "../hooks/useMacroInvertebrates";

import Macro from "./Macro";

const MacroContainer = ({ navigation, route }) => {
  const MacroInvertebrates = useMacroInvertebrates();

  return (
    <Macro navigation={navigation} route={route} records={MacroInvertebrates} />
  );
};

export default MacroContainer;
