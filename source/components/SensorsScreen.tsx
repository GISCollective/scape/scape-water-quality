import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  ViewStyle,
} from "react-native";
import { DataTable } from "react-native-paper";
import { Peripheral } from "react-native-ble-manager";
import { Measurement } from "../lib/sensor";
import { P } from "../giscollective/components/html";
import { useSensors } from "../giscollective/hooks/useSensors";
import { useTheme } from "../giscollective/hooks/useTheme";

const componentStyles = StyleSheet.create({
  topContainer: {
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 20,
    paddingRight: 20,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 1,
  },
  spinner: {
    paddingRight: 10,
  },
});

const SensorValue = ({ sensor }) => {
  const theme = useTheme();
  const [measurements, setMeasurements] = useState<Measurement[]>([]);

  useEffect(() => {
    sensor.on("value-changed", () => {
      setMeasurements(
        sensor?.lastMeasurements.filter((a) => a.isReliable) ?? [],
      );
    });
  }, [sensor]);

  let primaryMeasurement = measurements.find((a) => a.isPrimary);

  const measurement = primaryMeasurement ?? measurements.find((a) => a);

  if (!measurement) {
    return null;
  }

  return (
    <View>
      <P style={theme.toStyle(["text-size-11", "fw-bold"])}>
        {measurement.name} {measurement.value?.toFixed(2) ?? ""}
        {measurement.unit ?? ""}
      </P>
    </View>
  );
};

const SensorValues = ({ device }) => {
  return device.sensors.map((s, i) => <SensorValue key={i} sensor={s} />);
};

const RenderItem = ({
  item,
  onToggleConnection,
}: {
  item: Peripheral;
  onToggleConnection: (Peripheral) => void;
}) => {
  const theme = useTheme();
  const [device, setDevice] = useState();

  useEffect(() => {
    setDevice(item.device);
  }, [item.device, item.connected]);

  const listRowStyle: ViewStyle = {
    height: 70,
    margin: 0,
    padding: 0,
    justifyContent: "flex-start",
    alignItems: "center",
  };

  return (
    <DataTable.Row style={listRowStyle} testID="sensor-section-row">
      <DataTable.Cell
        style={theme.toStyle([
          "ps-0",
          "pe-0",
          "pt-0",
          "pb-0",
          "ms-0",
          "me-0",
          "mt-0",
          "mb-0",
        ])}
        onPress={() => onToggleConnection?.(item)}
      >
        <View>
          <P style={theme.toTextStyle(["text-size-12"], { color: "primary" })}>
            {item?.name}
            {item.connecting && " - Connecting..."}
            {item.connected && " - Connected"}
          </P>

          {item.connected && device && <SensorValues device={device} />}
        </View>
      </DataTable.Cell>
    </DataTable.Row>
  );
};

const SensorsScreen = ({}) => {
  const theme = useTheme();
  const sensors = useSensors(true);

  return (
    <View>
      {sensors.isScanning && (
        <View
          style={theme.toStyle(["pt-4", "pb-4"], {
            borderBottomWidth: 1,
            borderColor: "gray-200",
          })}
        >
          <ActivityIndicator
            animating={true}
            style={theme.toStyle(["mb-2"], componentStyles.spinner)}
            color={theme.toColor("primary")}
          />
          <P style={theme.toStyle(["text-center"])}>
            Searching for wireless sensors
          </P>
        </View>
      )}
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <DataTable>
          {sensors.peripheralList.map((a) => (
            <RenderItem
              item={a}
              onToggleConnection={sensors.toggleConnection}
              key={`${a.id}${a.changes}`}
            />
          ))}
        </DataTable>
      </ScrollView>
    </View>
  );
};

export default SensorsScreen;
