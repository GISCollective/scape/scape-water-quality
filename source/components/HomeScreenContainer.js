import React, { useEffect } from "react";
import HomeScreen from "./HomeScreen";
import { usePendingAnswers } from "../giscollective/hooks/usePendingAnswers";
import { useNavigationState } from "@react-navigation/native";
import { SurveyAnswerUpload } from "../giscollective/lib/submitSurveyAnswer";

export const HomeScreenContainer = ({ navigation }) => {
  const [storedAnswers, reloadAnswers] = usePendingAnswers();
  const navigationState = useNavigationState((state) => state);

  useEffect(() => {
    reloadAnswers();
  }, [navigationState]);

  const handleStart = (setSnack) => {
    navigation.navigate("Place", {});
  };

  const handleAbout = () => {
    navigation.navigate("About");
  };

  const handleSensors = () => {
    navigation.navigate("Sensors");
  };

  const handleSend = async () => {
    await SurveyAnswerUpload.instance.upload(storedAnswers);
    await reloadAnswers();
  };

  return (
    <HomeScreen
      onStart={handleStart}
      onSend={handleSend}
      onAbout={handleAbout}
      onSensors={handleSensors}
      storedAnswerCount={storedAnswers.length}
    />
  );
};

export default HomeScreenContainer;
