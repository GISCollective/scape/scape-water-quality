import React from "react";
import MapView from "react-native-maps";

const LocationScreen = () => {
  return (
    <MapView
      style={{ height: "100%", width: "100%" }}
      userLocationUpdateInterval={1000}
      showsUserLocation={true}
      zoomControlEnabled={true}
      showsMyLocationButton={true}
      followsUserLocation={true}
    />
  );
};

export default LocationScreen;
