import React from "react";

import { WebView } from "react-native-webview";

const WebBrowserScreen = ({ navigation, route }) => {
  const uri = route.params.uri;
  return <WebView source={{ uri: uri }} />;
};

export default WebBrowserScreen;
