import React from "react";

import { SafeAreaView, ScrollView } from "react-native";

import MacroContainer from "./MacroContainer";

const MacroScreen = ({ navigation, route }) => {
  return (
    <SafeAreaView>
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <MacroContainer navigation={navigation} route={route} />
      </ScrollView>
    </SafeAreaView>
  );
};

export default MacroScreen;
