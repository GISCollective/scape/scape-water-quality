const macroInvertebratesData = {
  "Sensitive.|PTI taxa Group I": [
    {
      "Common Name": "Caddisfly",
      "Taxonomic Rank": "Order Trichoptera",
      icon: require("../../assets/photos/caddisfly-1.png"),
      drawing1: require("../../assets/photos/caddisfly-2.png"),
      drawing2: require("../../assets/photos/caddisfly-3.png"),
      photo_immature: require("../../assets/photos/caddisfly-4.png"),
      photo_adult: require("../../assets/photos/caddisfly-5.png"),
      "Descrip. of Juvenile":
        "Three pairs of legs (6 total); segmented grub-like body; some kinds may have gills along lower and upper portions of the abdomen; small hair-like tails or hooks. Case builders may be enclosed in a case (retreat) that they construct using stream bottom materials such as pebbles, sand grains, woody debris, pieces of plant material or some combination; others construct a net, which consists of materials held together by a silk-like thread.",
      "Description of Adult":
        "General features. Adult caddisflies are commonly 3 to 15 millimetres (0.118 to 0.590 inch) in length. Their anterior wings usually range from 4 to 20 millimetres in length, providing wing spans of 8 to 40 millimetres.",
      Tolerance: "Low",
      Distribution:
        "Worldwide and throughout North America. Species most diverse in well-aerated streams, but also occur in lakes, ponds, and marshes. Adults rest on nearby vegetation during the day; flight activity begins at dusk. Adults are attracted - sometimes in great numbers - to artificial light.",
      Sources:
        "https://www.seagrant.wisc.edu/wp-content/uploads/2020/06/caddisfly-1.jpg",
      Media: "https://www.youtube.com/watch?v=w3T9P7qOgE4",
    },
    {
      "Common Name": "Dobsonfly (Hellgrammites)",
      "Taxonomic Rank": "Order Megaloptera",
      icon: require("../../assets/photos/dobsonfly-hellgrammites-1.png"),
      drawing1: require("../../assets/photos/dobsonfly-hellgrammites-2.png"),
      photo_immature: require("../../assets/photos/dobsonfly-hellgrammites-4.png"),
      photo_adult: require("../../assets/photos/dobsonfly-hellgrammites-5.png"),
      "Descrip. of Juvenile":
        "Larval corydalids, commonly known as hellgrammites, can be quite large (3–8 cm long), long-lived (2–5 years), and are primarily found in streams.",
      "Description of Adult":
        "Dobsonflies are a subfamily of insects, Corydalinae, part of the Megalopteran family Corydalidae. The larvae (commonly called hellgrammites) are aquatic, living in streams, and the adults are often found along streams as well.",
      Tolerance: "Low",
      Distribution:
        "The nine genera of dobsonflies are distributed in the Americas, Asia, and South Africa.",
      Sources:
        "https://www.sciencedirect.com/topics/agricultural-and-biological-sciences/corydalidae",
      Media: "https://www.youtube.com/watch?v=82J0VlXo6-0",
    },
    {
      "Common Name": "Mayfly",
      "Taxonomic Rank": "Order Ephemeroptera",
      icon: require("../../assets/photos/mayfly-1.png"),
      drawing1: require("../../assets/photos/mayfly-2.png"),
      photo_immature: require("../../assets/photos/mayfly-4.png"),
      photo_adult: require("../../assets/photos/mayfly-5.png"),
      "Descrip. of Juvenile":
        "Three pairs (6 total) of legs; one hooked claw at the end of each leg end of each leg; gills on the abdomen (may be covered by plates); 2 or 3 tail filaments and 2 short antennae.",
      "Description of Adult":
        "The Mayfly varies in size, but most species have a 1¾ inch long body. Its color also ranges from pale white to brown to black. Some also have bright streaks of yellow or some other color. Mayflies are also called shadflies or lake flies. A mayfly typically has four wings and three long hair-like appendages that extend from the abdomen. When resting, adult flies hold their two pairs of the upper membranous wings folded vertically above their abdomen.",
      Tolerance: "Low",
      Distribution:
        "One species, the small brown minnow mayfly, has been collected from Nova Scotia (Peterson, 1989), south to South Carolina (McCafferty et al. 2010), and west to Colorado, Idaho (McCafferty et al. 2012) and Saskatchewan (McCafferty and Randolph, 1998).",
      Sources: "https://en.wikipedia.org/wiki/Mayfly",
      Media: "https://www.youtube.com/watch?v=yCtQa5ajmpU",
    },
    {
      "Common Name": "Right-handed Gilled Snails",
      "Taxonomic Rank": "Order Gastropoda Family Bithyniidae",
      icon: require("../../assets/photos/right-handed-gilled-snails-1.png"),
      drawing1: require("../../assets/photos/right-handed-gilled-snails-2.png"),
      photo_adult: require("../../assets/photos/right-handed-gilled-snails-5.png"),
      "Description of Adult":
        "The height of shell is usually no larger than 12–15 mm. The snail is sexually mature by the time the height of shell reaches 8 mm in size.",
      Tolerance: "Low",
      Distribution:
        "Members of this family are found native throughout Europe, Asia, and in Africa, Indonesia, the Philippines and Australia. This species was introduced a long time ago to North America and has spread widely. However, it has been reported in Pleistocene deposits in Chicago so it may already have been in North America at the time of European settlement (Burch 1989). It has recently (last 50 years) been reported in Montana from the Flathead Basin and Georgetown Lake.",
      Sources: "https://en.wikipedia.org/wiki/Freshwater_snail",
      Media: "https://www.youtube.com/watch?v=PeKFPDnOEaM",
    },
    {
      "Common Name": "Riffle Beetles",
      "Taxonomic Rank": "Order Coleoptera",
      icon: require("../../assets/photos/riffle-beetles-1.png"),
      drawing1: require("../../assets/photos/riffle-beetles-2.png"),
      drawing2: require("../../assets/photos/riffle-beetles-3.png"),
      photo_immature: require("../../assets/photos/riffle-beetles-4.png"),
      photo_adult: require("../../assets/photos/riffle-beetles-5.png"),
      "Descrip. of Juvenile":
        "Larvae may be distinctly larger than their adult forms--up to 10 mm long. They are segmented and equipped with a pair of antennae and three pairs of legs. Unlike adults, larvae have filamentous gills occurring at the tip of the abdomen. The gills can be expanded, contracted, or fully retracted depending on the need for oxygen or protection. These are fully lost in adulthood.",
      "Description of Adult":
        "Adults are notably dark-colored, elongated, and small. Most reach a length of just 5 millimeters, comparable to the length of house ants. They are distinguished by their clubbed or filiform antennae. Up close, these may appear segmented. When peering at them from above, you should be able to see 3 pairs of legs, each bent at 3 joints. These terminate in tiny yet powerful tarsal claws that allow them to maintain their position in a strong current. The head capsule may be distinctly segmented from the rest of the hard body and has several lateral ocelli (simple eyes).",
      Tolerance: "Low",
      Distribution:
        "Endangered in Montana. Generally, riffle beetles are more diverse in tropical compared to temperate regions. They prefer humid climates and mild to warm temperatures.",
      Sources: "https://en.wikipedia.org/wiki/Elmidae",
      Media: "https://www.youtube.com/watch?v=O0uNbzsceZE",
    },
    {
      "Common Name": "Stoneflies",
      "Taxonomic Rank": "Order Plecoptera",
      icon: require("../../assets/photos/stoneflies-1.png"),
      drawing1: require("../../assets/photos/stoneflies-2.png"),
      photo_immature: require("../../assets/photos/stoneflies-4.png"),
      photo_adult: require("../../assets/photos/stoneflies-5.png"),
      "Descrip. of Juvenile":
        "Three pairs of legs (6 total); 2 hooked claws at the end of each leg; no gills on most of the abdomen but may have gills on the legs, thorax and upper abdomen; 2 tail filaments and 2 long antennae.",
      "Description of Adult":
        "Any of about 2,000 species of insects, the adults of which have long antennae, weak, chewing mouthparts, and two pairs of membranous wings. The stonefly ranges in size from 6 to more than 60 mm (0.25 to 2.5 inches). The hindwings are generally larger and shorter than the forewings and fold like a fan when not in use. Even though its wings are well developed, the stonefly is a poor flier. Many species are gray, black, or brown and blend into their surroundings.",
      Tolerance: "Low",
      Distribution:
        "Some 3,500 species are described worldwide, with new species still being discovered. Stoneflies are found worldwide, except Antarctica.",
      Sources: "https://en.wikipedia.org/wiki/Plecoptera",
      Media: "https://www.youtube.com/watch?v=-sGyM2KUY6o",
    },
  ],
  "Facultative|(Somewhat Tolerant)|PTI Taxa Group 2": [
    {
      "Common Name": "Clams",
      "Taxonomic Rank":
        "Phylum Mollusca, Class Pelecypoda (also Class Bivalvia)",
      icon: require("../../assets/photos/clams-1.png"),
      photo_adult: require("../../assets/photos/clams-5.png"),
      "Description of Adult":
        "Clam is a common name for several kinds of bivalve molluscs. The word is often applied only to those that are edible and live as infauna, spending most of their lives halfway buried in the sand of the seafloor or riverbeds. Clams have two shells of equal size connected by two adductor muscles and have a powerful burrowing foot.[1] They live in both freshwater and marine environments; in salt water they prefer to burrow down into the mud and the turbidity of the water required varies with species and location; the greatest diversity of these is in North America.[2]",
      Tolerance: "Medium",
      Media: "https://www.youtube.com/watch?v=tDMzLKAR5rE",
      Sources: undefined,
    },
    {
      "Common Name": "Mussels",
      "Taxonomic Rank": "Class Bivalvia",
      icon: require("../../assets/photos/mussels-1.png"),
      drawing1: require("../../assets/photos/mussels-2.png"),
      photo_adult: require("../../assets/photos/mussels-5.png"),
      "Description of Adult":
        "Mussels refer to any of bivalve mollusks with a brown or purplish-black shell. They live in both freshwater and marine water. Most of the edible, marine mussels belong to the family Mytilidae. The shell of the mussels is asymmetric when compared to the shell of the clams. The outline of the shell is elongated. The shell is less oval or circular in shape as well. The color of the shell can be dark blue, brown or black. The inside is silver or grayish. Freshwater mussels species include freshwater pearl mussels.",
      Tolerance: "Medium",
      Sources: "https://www.inaturalist.org/observations/3720275",
      Media: "https://www.youtube.com/watch?v=K0JRTqDIXXI",
    },
    {
      "Common Name": "Cranefly",
      "Taxonomic Rank": "Family Tipulidae, Order Diptera",
      icon: require("../../assets/photos/cranefly-1.png"),
      drawing1: require("../../assets/photos/cranefly-2.png"),
      photo_immature: require("../../assets/photos/cranefly-4.png"),
      photo_adult: require("../../assets/photos/cranefly-5.png"),
      "Descrip. of Juvenile":
        "There are hundreds of species of crane flies in North America. The larvae are essentially tan, gray, or greenish grubs: plump, segmented caterpillars with a definite head and with tiny, fleshy projections at the hind end. They lack legs. Sometimes you can see the dark line of their digestive tract under the translucent body covering.",
      "Description of Adult":
        "Crane flies have a slender mosquito-like body and extremely long legs. Ranging in size from tiny to almost 3 cm (1.2 inches) long, these harmless slow-flying insects are usually found around water or among abundant vegetation.",
      Tolerance: "Medium",
      Sources: "https://en.wikipedia.org/wiki/Crane_fly",
      Media: "https://www.youtube.com/watch?v=FNnS_yzQ3tg",
    },
    {
      "Common Name": "Crayfish",
      "Taxonomic Rank": "Order Decapoda",
      icon: require("../../assets/photos/crayfish-1.png"),
      drawing1: require("../../assets/photos/crayfish-2.png"),
      photo_immature: require("../../assets/photos/crayfish-4.png"),
      photo_adult: require("../../assets/photos/crayfish-5.png"),
      "Description of Adult":
        "Freshwater crayfish can be anywhere from 4 to 16 cm in length. They are characterized by a joined head and thorax, or midsection, and a segmented body, which is sandy yellow, green, red, or dark brown in colour. The head has a sharp snout, and the compound eyes are on movable stalks. The exoskeleton, or body covering, is thin but tough. The front pair of the five pairs of legs have large, powerful pincers (chelae). There are five pairs of smaller appendages on the abdomen, used mostly for swimming and circulating water for respiration.",
      Tolerance: "Medium",
      Distribution:
        "Crayfish, common in streams and lakes, often conceal themselves under rocks or logs. They are most active at night, when they feed largely on snails, insect larvae, worms, and amphibian tadpoles; some eat vegetation. Crayfish mate in the autumn and lay eggs in the spring. The eggs, attached to the female’s abdomen, hatch in five to eight weeks. The larvae remain on the mother for several weeks. Sexual maturity is achieved in a few months to several years, and the life span ranges from 1 to 20 years, depending on the species.",
      Sources: "https://www.ecospark.ca/crayfish",
      Media: "https://www.youtube.com/watch?v=qExYxbZphWg",
    },
    {
      "Common Name": "Dragonfly",
      "Taxonomic Rank": "Order Odonata",
      icon: require("../../assets/photos/dragonfly-1.png"),
      drawing1: require("../../assets/photos/dragonfly-2.png"),
      drawing2: require("../../assets/photos/dragonfly-3.png"),
      photo_immature: require("../../assets/photos/dragonfly-4.png"),
      photo_adult: require("../../assets/photos/dragonfly-5.png"),
      "Descrip. of Juvenile":
        "Dragonfly larvae (nymphs) are aquatic, usually drab, with 6 legs, large eyes, and small wing buds on the back of the thorax.",
      "Description of Adult":
        'The Lake Darner is one particular kind of dragonfly. It is a member of the family Aeshnidae. Darners are among the largest and fastest-flying North American dragonflies, 2 1/4-4 3/4" long. These brilliant blue, green, or brown insects have large, clear wings spanning up to 5 7/8". Their compound eyes meet on top of the head. The female hovers above water usually attached or guarded by the male and, using a well-developed ovipositor for slicing into emergent plants, thrusts eggs one at a time in the stems.',
      Tolerance: "Medium",
      Distribution:
        "Dragonflies live on every continent except Antarctica. In contrast to the damselflies (Zygoptera), which tend to have restricted distributions, some genera and species are spread across continents. For example, the blue-eyed darner Rhionaeschna multicolor lives all across North America, and in Central America;[10] emperors Anax live throughout the Americas from as far north as Newfoundland to as far south as Bahia Blanca in Argentina,[11] across Europe to central Asia, North Africa, and the Middle East.[12] The globe skimmer Pantala flavescens is probably the most widespread dragonfly species in the world; it is cosmopolitan, occurring on all continents in the warmer regions. Most Anisoptera species are tropical, with far fewer species in temperate regions.[13]",
      Sources: "https://en.wikipedia.org/wiki/Dragonfly",
      Media: "https://www.youtube.com/watch?v=edW30jsCy6M",
    },
    {
      "Common Name": "Damselfly",
      "Taxonomic Rank": "Order Odonata",
      icon: require("../../assets/photos/damselfly-1.png"),
      drawing1: require("../../assets/photos/damselfly-2.png"),
      drawing2: require("../../assets/photos/damselfly-3.png"),
      photo_immature: require("../../assets/photos/damselfly-4.png"),
      photo_adult: require("../../assets/photos/damselfly-5.png"),
      "Descrip. of Juvenile":
        "Damselfly larvae (nymphs) are aquatic, slender, usually drab insects, with 6 thin legs, large eyes, and small wing buds on the back of the thorax. The 3 gills are leaflike or paddlelike and positioned in a tripod configuration at the tip of the abdomen (unlike the gills of the related dragonflies, which are hidden within the tip of the abdomen).",
      "Description of Adult":
        "Damselflies are similar to dragonflies, but the adults can be distinguished by the fact that the wings of most damselflies are held along, and parallel to, the body when at rest. Furthermore, the hindwing of the damselfly is essentially similar to the forewing, while the hindwing of the dragonfly broadens near the base. Damselflies are also usually smaller than dragonflies and weaker fliers in comparison",
      Tolerance: "Medium",
      Media: "https://www.youtube.com/watch?v=X_MERp2yDIY",
      Sources: undefined,
    },
    {
      "Common Name": "Scuds (Side-swimmer)",
      "Taxonomic Rank": "Order Amphipoda",
      icon: require("../../assets/photos/scuds-side-swimmer-1.png"),
      drawing1: require("../../assets/photos/scuds-side-swimmer-2.png"),
      photo_adult: require("../../assets/photos/scuds-side-swimmer-5.png"),
      "Description of Adult":
        "Scuds are about ¼ to ¾ inch long (varies with species). You could describe scuds (members of the order Amphipoda) as “shrimplike sowbugs.” Like sowbugs (in the order Isopoda), they have two pairs of antennae; they lack a carapace (a covering “back” like a crayfish has); their eyes are not on stalks; and they have several body segments with legs, gills, and other appendages. But unlike sowbugs, their arched bodies are flattened sideways, like shrimp, and the gills arise on the thorax segments (not on the abdomen). The various appendages have different purposes: armlike gnathopods at the front for feeding, followed by leglike pleopods for swimming, waving water across the gills, and other types of locomotion.",
      Tolerance: "Medium",
      Sources: "https://en.wikipedia.org/wiki/Amphipoda",
      Media: "https://www.youtube.com/watch?v=DeVug1u0Zk0",
    },
    {
      "Common Name": "Sow bugs",
      "Taxonomic Rank": "Order Isopoda",
      icon: require("../../assets/photos/sow-bugs-1.png"),
      drawing1: require("../../assets/photos/sow-bugs-2.png"),
      photo_adult: require("../../assets/photos/sow-bugs-5.png"),
      "Description of Adult":
        "Sowbugs are flat, oval creatures that are about 1 cm long. Their body has several segments, including seven pair of legs and two pair of antennae. Despite their name, sowbugs, sometimes called a woodlouse, are not really bugs. They are land-living crustaceans. Another crustacean, the pillbug, resembles the sowbug. Sowbugs cannot roll up when they are disturbed like pillbugs do. Sowbugs have two appendages that look like tails. Pillbugs do not have the appendages.",
      Tolerance: "Medium",
      Sources: "https://en.wikipedia.org/wiki/Trichoniscidae",
      Media: "https://www.youtube.com/watch?v=OVtxS5RKFC4",
    },
    {
      "Common Name": "Fishfly",
      "Taxonomic Rank": "Order Megaloptera",
      icon: require("../../assets/photos/fishfly-1.png"),
      drawing1: require("../../assets/photos/fishfly-2.png"),
      photo_immature: require("../../assets/photos/fishfly-4.png"),
      photo_adult: require("../../assets/photos/fishfly-5.png"),
      "Descrip. of Juvenile":
        "Fishfly larvae look a lot like hellgrammites but usually do not grow quite so large. Unlike hellgrammites, they lack gill tufts below the abdomen, and the abdomen tip is forked, with 2 short, fleshy tails, and each tail has a pair of hooks. They are generally fairly light-colored; often tan with a reddish cast. Like the larvae of dobsonflies and alderflies, fishflies have 3 pairs of jointed legs in the upper part of the body, with each leg tipped with a tiny, 2-parted pincer; they have several fleshy filaments extending from the sides of the abdomen; and the mouthparts are large pincers.",
      "Description of Adult":
        "Fishflies are quite large, with a wingspan of 2.5 to 3 inches (6 to 8 cm). They will eat aquatic plants as well as small animals including vertebrates like minnows and tadpoles, and may live up to seven days as adults. Their entire lifespan is several years, but most of this time is spent as larvae.",
      Tolerance: "Medium",
      Sources: "https://en.wikipedia.org/wiki/Chauliodinae",
      Media: "https://www.youtube.com/watch?v=XLtUPWxr-Co",
    },
    {
      "Common Name": "Alderfly",
      "Taxonomic Rank": "Order Odonata",
      icon: require("../../assets/photos/alderfly-1.png"),
      drawing1: require("../../assets/photos/alderfly-2.png"),
      photo_immature: require("../../assets/photos/alderfly-4.png"),
      photo_adult: require("../../assets/photos/alderfly-5.png"),
      "Descrip. of Juvenile":
        "Alderfly larvae look a lot like their cousins the fishflies, but instead of having pairs of fleshy tails, they have only a single tail pointing straight back.",
      "Description of Adult":
        "Any insect of the megalopteran family Sialidae, characterized by long, filamentous antennae and two pairs of large wings (anterior wing length 20 to 50 mm [ 3/4 inch to 2 inches]), membranous and well-developed, with part of the hind wing folding like a fan. The adult alderfly is dark-coloured, 15 to 30 mm (3/5 inch to 1 1/5 inches) long, sluggish, and a weak flier. It usually inhabits shore vegetation, especially alder trees.",
      Tolerance: "Medium",
      Media: "https://www.youtube.com/watch?v=M-xXQhTFGBo",
      Sources: undefined,
    },
    {
      "Common Name": "Watersnipe Fly",
      "Taxonomic Rank": "Order Diptera",
      icon: require("../../assets/photos/watersnipe-fly-1.png"),
      drawing2: require("../../assets/photos/watersnipe-fly-3.png"),
      photo_immature: require("../../assets/photos/watersnipe-fly-4.png"),
      photo_adult: require("../../assets/photos/watersnipe-fly-5.png"),
      "Descrip. of Juvenile":
        "7 pairs of abdominal prolegs and a single proleg on the last segment",
      "Description of Adult": "lack true head",
      Tolerance: "long fleshy filaments extend from hind end",
      Distribution:
        "short filaments protrude from each side of each abdominal segment",
      Sources: undefined,
      Media: undefined,
      "": "Water snipe fly larva: https://www.youtube.com/watch?v=fh0OQP4t6rc",
    },
  ],
  "Tolerant|PTI Taxa Group 3": [
    {
      "Common Name": "Aquatic Worms",
      "Taxonomic Rank": "Subclass Oligochaeta",
      icon: require("../../assets/photos/aquatic-worms-1.png"),
      photo_adult: require("../../assets/photos/aquatic-worms-5.png"),
      "Description of Adult":
        "Aquatic earthworms closely resemble terrestrial earthworms. They have long, moderately muscular, cylindrical bodies composed of ring-like segments. Most aquatic earthworms range in length from 1-30mm long, although some may be as long as 150mm.",
      Tolerance: "High",
      Distribution:
        "The majority of aquatic oligochaetes are small, slender worms, whose organs can be seen through the transparent body wall. They burrow into the sediment or live among the vegetation mostly in shallow, freshwater environments. Some are transitional between terrestrial and aquatic habitats, inhabiting swamps, mud or the borders of water bodies. About two hundred species are marine, mostly in the families Enchytraeidae and Naididae; these are found largely in the tidal and shallow subtidal zones, but a few are found at abyssal depths.[3]",
      Sources: "https://en.wikipedia.org/wiki/Oligochaeta",
      Media: "https://www.youtube.com/watch?v=7UyJW4LfsWI",
    },
    {
      "Common Name": "Black Fly",
      "Taxonomic Rank": "Order Diptera",
      icon: require("../../assets/photos/black-fly-1.png"),
      drawing1: require("../../assets/photos/black-fly-2.png"),
      photo_immature: require("../../assets/photos/black-fly-4.png"),
      photo_adult: require("../../assets/photos/black-fly-5.png"),
      "Descrip. of Juvenile":
        "Elongated, worm-like body that is wider at one end than the other. Maximum size = ¼” (6.35 mm)",
      "Description of Adult":
        "Adult black flies are small insects that measure 1 to 5 mm in length, and possess a shiny thorax (middle of the fly) that ranges in color from black to various shades of gray or yellow.",
      Tolerance: "High",
      Sources: "https://en.wikipedia.org/wiki/Black_fly",
      Media: "https://www.youtube.com/watch?v=42v75SbNquM",
    },
    {
      "Common Name": "Leeches",
      "Taxonomic Rank": "subclass Hirudinea within the phylum Annelida.",
      icon: require("../../assets/photos/leeches-1.png"),
      drawing1: require("../../assets/photos/leeches-2.png"),
      photo_adult: require("../../assets/photos/leeches-5.png"),
      "Description of Adult":
        "The majority of freshwater leeches are found in the shallow, vegetated areas on the edges of ponds, lakes and slow-moving streams; very few species tolerate fast-flowing water. In their preferred habitats, they may occur in very high densities; in a favourable environment with water high in organic pollutants",
      Tolerance: "High",
      Distribution:
        "The majority of leeches live in freshwater habitats, while some species can be found in terrestrial or marine environments. The best-known species, such as the medicinal leech, Hirudo medicinalis, are hematophagous, attaching themselves to a host with a sucker and feeding on blood, having first secreted the peptide hirudin to prevent the blood from clotting. The jaws used to pierce the skin are replaced in other species by a proboscis which is pushed into the skin. A minority of leech species are predatory, mostly preying on small invertebrates.",
      Sources: "https://en.wikipedia.org/wiki/Leech",
      Media: "https://www.youtube.com/watch?v=O-0SFWPLaII",
    },
    {
      "Common Name": "Midge",
      "Taxonomic Rank": "Order Diptera",
      icon: require("../../assets/photos/midge-1.png"),
      drawing1: require("../../assets/photos/midge-2.png"),
      photo_immature: require("../../assets/photos/midge-4.png"),
      photo_adult: require("../../assets/photos/midge-5.png"),
      "Descrip. of Juvenile":
        "The tiny wormlike aquatic larvae, soft-bodied and often bloodred, are commonly known as bloodworms.The larvae tend to be most active at night in the dark, which provides some protection from predators. Midge larvae can range anywhere from 2 mm to 30 mm in length. They are important food for aquatic animals, especially trout and young salmon.",
      "Description of Adult":
        "A midge is any small fly, including species in several families of non-mosquito Nematoceran Diptera. Midges are found (seasonally or otherwise) on practically every land area outside permanently arid deserts and the frigid zones.",
      Tolerance: "High",
      Distribution:
        "Midge Flies, chironomids are commonly found in man made lakes, wastewater facilities and streams with high nutrient content. These flies are similar in appearance to mosquitos, yet do not bite. The midge fly larvae (known as blood worms) feed on the organic debris at the bottom of the body of water and provide a food source for fish and predatory insects.",
      Sources: "https://en.wikipedia.org/wiki/Midge",
      Media: "https://www.youtube.com/watch?v=yaCnnKRrO-g",
    },
    {
      "Common Name": "Pouch or Lung Snails (left-handed)",
      "Taxonomic Rank": "Subclass Pulmonata",
      icon: require("../../assets/photos/pouch-or-lung-snails-left-handed-1.png"),
      drawing1: require("../../assets/photos/pouch-or-lung-snails-left-handed-2.png"),
      photo_adult: require("../../assets/photos/pouch-or-lung-snails-left-handed-5.png"),
      "Description of Adult":
        "Lunged snails (subclass: Pulmonata) rise to the surface and breathe air through a “pouch” membrane that acts as a lung in their shells. They are also known as pouch snails and are identified as “left-handed' snails because their shell opening points to the left when held with the tip up. Shell length: 1/8 to 1 inch",
      Tolerance: "most are less than 1/2 inch (varies with species).",
      Distribution: "High",
      Sources: undefined,
      Media: undefined,
    },
  ],
};

module.exports = { macroInvertebratesData };
