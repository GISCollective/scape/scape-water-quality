import { useState } from "react";

class ObservationData {
  static instance = new ObservationData();
  values = {};
}

export default function useObservationData() {
  const [values, setValues] = useState(ObservationData.instance.values);

  const storeValues = (key, count) => {
    ObservationData.instance.values[key] = { count };

    if (count == 0) {
      delete ObservationData.instance.values[key];
    }

    setValues(ObservationData.instance.values);
  };

  const clearValues = () => {
    ObservationData.instance.values = {};
    setValues(ObservationData.instance.values);
  };

  return [values, storeValues, clearValues];
}
