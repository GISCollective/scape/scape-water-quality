import { macroInvertebratesData } from "./data";

export default function useMacroInvertebrates() {
  return macroInvertebratesData;
}
