import "react-native";
import "@testing-library/jest-dom";
import React from "react";

import { render } from "@testing-library/react-native";
import { SensorValueLabel } from "./SensorRead";
import { Measurement } from "../../lib/sensor";
import { VernierSensor } from "../lib/vernier/Sensor";

describe("SensorValueLabel", function () {
  it("renders nothing when there is no sensor", () => {
    const document = render(<SensorValueLabel />);

    expect(document.queryByTestId("sensor-label")).not.toBeInTheDocument();
  });

  it("renders nothing when there is no value", () => {
    const sensor = new VernierSensor({
      name: "name",
      unit: "unit",
    });

    const document = render(<SensorValueLabel sensor={sensor} />);

    expect(document.queryByTestId("sensor-label")).not.toBeInTheDocument();
  });

  it("renders a sensor value label", () => {
    const measurement: Measurement = {
      name: "name",
      unit: "unit",
      id: "",
      value: 3.2222,
      isReliable: false,
    };

    const document = render(<SensorValueLabel measurement={measurement} />);

    expect(
      document.queryByTestId("sensor-label")?.props.children.join(""),
    ).toEqual("name 3.22unit");
  });

  it("renders a temperature value with F and C labels", () => {
    const measurement: Measurement = {
      name: "Temperature",
      unit: "C",
      id: "",
      value: 28.22222,
      isReliable: false,
    };

    const document = render(<SensorValueLabel measurement={measurement} />);

    expect(
      document.queryByTestId("sensor-label")?.props.children.join(""),
    ).toEqual("Temperature 82.80ºF/28.22°C");
  });
});
