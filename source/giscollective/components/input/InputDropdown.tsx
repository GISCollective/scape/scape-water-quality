import React, { useState } from "react";
import { View, Text, Button, ViewStyle, TextStyle } from "react-native";
import { Theme } from "../../lib/theme";
import { InputButton } from "./InputButton";
import { useTheme } from "../../hooks/useTheme";

function toString<T>(value: T) {
  if (!value) {
    return "";
  }

  if (typeof value === "string") {
    return value;
  }

  if (value && typeof value == "object" && typeof value["name"] == "string") {
    return value["name"];
  }

  return "";
}

interface InputDropdownProps<T> {
  testID?: string;
  options: T[];
  onChange?: (value: T) => void;
  value?: T | undefined;
  classes?: string[];
}

export const InputDropdown = <T extends unknown>(
  props: InputDropdownProps<T>,
) => {
  const theme = useTheme();
  const [showOptions, setShowOptions] = useState(false);
  const containerStyle: ViewStyle = {
    flex: 1,
    justifyContent: "space-between",
    flexDirection: "row",
    alignItems: "center",
  };

  const inputStyle: TextStyle = {
    ...theme.toFormControlStyle(props.classes, ""),
    paddingStart: 0,
    paddingEnd: 0,
    paddingBottom: 0,
    paddingTop: 0,
  };

  const labelStyle: TextStyle = theme.toFormControlStyle([], "", {
    color: "black",
    flexShrink: 1,
    borderWidth: 0,
    backgroundColor: "transparent",
  });

  const selectOption = (value: T) => {
    props.onChange?.(value);
    setShowOptions(false);
  };

  const showChange =
    props.options.length > 1 ||
    (props.options.length == 1 &&
      toString(props.value) != toString(props.options?.[0]));
  let btnLabel = "change";

  if (showOptions) {
    btnLabel = "hide options";
  }

  return (
    <>
      {!props.value && !showOptions && (
        <InputButton
          onPress={() => setShowOptions(!showOptions)}
          classes={["btn-outline-primary"]}
          testID={`${props.testID}-change`}
          align="start"
        >
          Select a value from the list
        </InputButton>
      )}

      {(props.value || showOptions) && (
        <View testID={props.testID} style={inputStyle}>
          <View style={containerStyle}>
            {(props.value || showOptions) && (
              <Text testID={`${props.testID}-value`} style={labelStyle}>
                {toString(props.value)}
              </Text>
            )}
            {showChange && (
              <InputButton
                onPress={() => setShowOptions(!showOptions)}
                classes={["btn-link"]}
                testID={`${props.testID}-change`}
              >
                {btnLabel}
              </InputButton>
            )}
          </View>

          {showOptions && (
            <View testID={`${props.testID}-option-list`}>
              {props.options.map((a, i) => (
                <InputButton
                  key={i}
                  testID={`${props.testID}-option-${i}`}
                  classes={["btn-link", "ps-2", "pe-2"]}
                  onPress={() => selectOption(a)}
                >
                  {toString(a)}
                </InputButton>
              ))}
            </View>
          )}
        </View>
      )}
    </>
  );
};
