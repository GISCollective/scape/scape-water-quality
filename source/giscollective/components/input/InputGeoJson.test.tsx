import {
  render,
  screen,
  fireEvent,
  act,
  waitFor,
} from "@testing-library/react-native";
import React from "react";
import { InputGeoJson } from "./InputGeoJson";
import { GeolocationResponse } from "@react-native-community/geolocation";
import { LocationService } from "../../lib/locationService";
import { PositionDetailsValue } from "../../lib/positionDetails";

class LocationMock extends LocationService {
  lastValue?: GeolocationResponse | undefined = {
    coords: {
      accuracy: 1,
      altitude: 2,
      altitudeAccuracy: 3,
      heading: 4,
      latitude: 5,
      longitude: 6,
      speed: 7,
    },
    timestamp: 1,
  };

  getCurrentPosition(): Promise<GeolocationResponse> {
    return Promise.resolve(this.lastValue as GeolocationResponse);
  }
}

describe("InputGeoJson", () => {
  beforeEach(() => {
    LocationService.instance = new LocationMock();

    LocationService.instance.lastValue = {
      coords: {
        accuracy: 1,
        altitude: 2,
        altitudeAccuracy: 3,
        heading: 4,
        latitude: 5,
        longitude: 6,
        speed: 7,
      },
      timestamp: 1,
    };
  });

  it("renders all options when they are enabled", async () => {
    const options = {
      allowGps: true,
      allowManual: true,
      allowAddress: true,
      allowExistingFeature: true,
    };

    const component = render(<InputGeoJson testID="input" options={options} />);

    expect(component.queryByTestId("input-btn-gps")).toBeDefined();
    expect(component.queryByTestId("input-btn-manual")).toBeDefined();
    expect(component.queryByTestId("input-btn-address")).toBeDefined();
    expect(component.queryByTestId("input-btn-existing-feature")).toBeDefined();
  });

  it("does not render the map by default", async () => {
    const value = undefined;
    const options = {
      allowGps: true,
      allowManual: true,
      allowAddress: true,
      allowExistingFeature: true,
    };

    const component = render(
      <InputGeoJson testID="input" value={value} options={options} />,
    );

    expect(component.queryByTestId("input-map-view")).toBeNull();
  });

  describe("gps", () => {
    it("renders the map view when the gps option is pressed", async () => {
      const value = {};
      const options = {
        allowGps: true,
        allowManual: true,
        allowAddress: true,
        allowExistingFeature: true,
      };
      const change = jest.fn();

      const component = render(
        <InputGeoJson testID="input" options={options} onChange={change} />,
      );

      let button = await component.findByTestId("input-gps");

      fireEvent.press(button);

      const view = await component.findByTestId("input-map-view");
      expect(view).not.toBeNull();

      button = await component.findByTestId("input-gps-bg");
      expect(button).toHaveStyle({
        backgroundColor: "rgb(57,97,208)",
      });

      await waitFor(
        () =>
          expect(change.mock.calls[0][0]).toEqual({
            coordinates: [6, 5],
            type: "Point",
          }),
        { timeout: 5000 },
      );

      await waitFor(() =>
        expect(change.mock.calls[0][1].toJSON()).toEqual({
          altitude: 2,
          accuracy: 1,
          altitudeAccuracy: 3,
          capturedUsingGPS: false,
          type: "gps",
          searchTerm: null,
          address: null,
          id: null,
        }),
      );
    }, 6000);

    it("triggers gps updates when the gps option is selected", async () => {
      const options = {
        allowGps: true,
        allowManual: true,
        allowAddress: true,
        allowExistingFeature: true,
      };

      const positionDetails: PositionDetailsValue = {
        capturedUsingGPS: false,
        type: "gps",
        altitude: 0,
        accuracy: 0,
        altitudeAccuracy: 0,
        searchTerm: null,
        address: null,
        id: null,
        toJSON: function (): PositionDetailsValue {
          return this;
        },
      };

      const change = jest.fn();

      const component = render(
        <InputGeoJson
          testID="input"
          value={undefined}
          positionDetails={positionDetails}
          options={options}
          onChange={change}
        />,
      );

      const view = await component.findByTestId("input-map-view");
      expect(view).not.toBeNull();
      LocationService.instance.lastValue = {
        coords: {
          accuracy: 11,
          altitude: 12,
          altitudeAccuracy: 13,
          heading: 14,
          latitude: 15,
          longitude: 16,
          speed: 17,
        },
        timestamp: 11,
      };

      await waitFor(
        () =>
          expect(change.mock.calls[2][0]).toEqual({
            coordinates: [16, 15],
            type: "Point",
          }),
        { timeout: 5000 },
      );

      await waitFor(() =>
        expect(change.mock.calls[2][1].toJSON()).toEqual({
          altitude: 12,
          accuracy: 11,
          altitudeAccuracy: 13,
          capturedUsingGPS: false,
          type: "gps",
          searchTerm: null,
          address: null,
          id: null,
        }),
      );
    }, 6000);
  });

  describe("manual", () => {
    it("opens a location sheet when the manual option is pressed", async () => {
      const value = {};
      const options = {
        allowGps: true,
        allowManual: true,
        allowAddress: true,
        allowExistingFeature: true,
      };
      const change = jest.fn();

      const component = render(
        <InputGeoJson testID="input" options={options} onChange={change} />,
      );

      let button = await component.findByTestId("input-manual");

      fireEvent.press(button);

      const sheet = await component.findByTestId("location-picker-modal");
      expect(sheet).not.toBeNull();
    });
  });

  describe("by address", () => {
    it("shows an address search field when the address button is selected", async () => {
      const value = {};
      const options = {
        allowGps: true,
        allowManual: true,
        allowAddress: true,
        allowExistingFeature: true,
      };
      const change = jest.fn();

      const component = render(
        <InputGeoJson testID="input" options={options} onChange={change} />,
      );

      let button = await component.findByTestId("input-address");
      fireEvent.press(button);

      let textSearch = await component.findByTestId("input-address");
      expect(textSearch).not.toBeNull();
    });
  });
});
