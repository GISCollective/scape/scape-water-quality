import React, { useEffect, useState } from "react";
import { View } from "react-native";
import { IconModel } from "../../models/Icon";
import { InputButton } from "./InputButton";
import { useIsMounted } from "usehooks-ts";
import { Store } from "../../models/store";
import { IconPicker } from "../IconListPicker";
import { IconSheetPicker } from "../IconSheetPicker";

export interface InputIconSelectorProps {
  testID?: string;
  value?: IconModel[];
  icons: string[];
  onChange?: (value: IconModel[]) => void;
}

export const InputIconSelector = (props: InputIconSelectorProps) => {
  const [showSelector, setShowSelector] = useState(false);
  const [value, setValue] = useState<IconModel[]>(props.value ?? []);
  const [icons, setIcons] = useState<IconModel[]>([]);
  const [isLoading, setIsLoading] = useState(true);
  const isMounted = useIsMounted();
  const store = Store.instance;

  useEffect(() => {
    if (!isMounted()) {
      return;
    }

    const promiseList = Promise.all(
      props.icons?.map?.((a) => store.findIcon(a)) ?? [],
    );

    promiseList
      .then((records) => {
        setIcons(records ?? []);
        setIsLoading(false);
      })
      .catch((err) => {
        console.error(err);
        setIsLoading(false);
      });
  }, [isMounted]);

  const selectIcons = (icons: IconModel[]) => {
    setValue(icons);
    setShowSelector(false);
    props.onChange?.(icons);
  };

  const handleIconRemove = (icon: IconModel) => {
    const newIcons = value.filter((a) => a._id != icon._id);
    setValue(newIcons);
    props.onChange?.(newIcons);
  };

  return (
    <View>
      <View>
        {value.map((a, i) => (
          <IconPicker
            icon={a}
            showTopLine={i != 0}
            key={a._id}
            testID="input-selected-icon"
            onRemove={handleIconRemove}
          />
        ))}
      </View>
      {showSelector && (
        <IconSheetPicker
          icons={icons}
          value={value}
          onSelectedIcons={selectIcons}
          testID={`${props.testID}-icon-picker`}
        />
      )}
      {!isLoading && (
        <InputButton
          onPress={() => setShowSelector(true)}
          testID={`${props.testID}-select-icons`}
          classes={["btn-primary", "mt-3", "mb-4"]}
        >
          Select icons
        </InputButton>
      )}
    </View>
  );
};
