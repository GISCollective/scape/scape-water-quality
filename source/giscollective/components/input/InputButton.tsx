import React, { ReactNode, useState } from "react";
import {
  ActivityIndicator,
  Pressable,
  Text,
  TextStyle,
  View,
  ViewStyle,
} from "react-native";
import { GestureResponderEvent } from "react-native";
import { Theme } from "../../lib/theme";

export type ButtonStates = "" | "active" | "disabled" | "waiting";
export type ButtonAlign = "start" | "end" | "default";

export interface InputButtonProps {
  onPress?:
    | ((event: GestureResponderEvent) => void | Promise<void>)
    | undefined;
  onStateChange?: (state: ButtonStates) => void;
  align?: ButtonAlign;
  children: string | ReactNode;
  testID?: string;
  classes?: string[];
  isLoading?: boolean;
  disabled?: boolean;
  style?: ViewStyle;
}

export function InputButton(props: InputButtonProps) {
  const [state, setState] = useState<ButtonStates>(
    props.disabled ? "disabled" : "",
  );

  const [isLoading, setIsLoading] = useState<boolean>(false);

  let content: ReactNode;

  const buttonState = isLoading || props.isLoading ? "waiting" : state;

  const style: ViewStyle = Theme.instance.toButtonStyle(
    props.classes,
    buttonState,
    {
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center",
      ...(props.style ?? {}),
    },
  );
  const textStyle: TextStyle = Theme.instance.toButtonTextStyle(
    props.classes,
    buttonState,
  );
  const spinnerStyle = Theme.instance.toStyle(["me-2"], { height: 5 });

  if (props.align == "start") {
    textStyle.textAlign = "left";
  }

  if (props.align == "end") {
    textStyle.textAlign = "right";
  }

  if (typeof props.children == "string") {
    content = <Text style={textStyle}>{props.children}</Text>;
  } else {
    content = props.children;
  }

  const handlePress = async (event: GestureResponderEvent) => {
    setIsLoading(true);

    try {
      await props.onPress?.(event);
    } catch (err) {
      console.error(err);
    }

    setIsLoading(false);
  };

  const handleStateChange = async (newState: ButtonStates) => {
    setState(newState);
    await props.onStateChange?.(newState);
  };

  if (["waiting", "disabled"].includes(buttonState)) {
    return (
      <View style={style} testID={`${props.testID}-bg`}>
        {buttonState == "waiting" && (
          <ActivityIndicator
            testID={`${props.testID}-loading`}
            style={spinnerStyle}
            color={textStyle.color}
          />
        )}
        {content}
      </View>
    );
  }

  return (
    <Pressable
      onPress={handlePress}
      onTouchStart={() => handleStateChange("active")}
      onTouchEnd={() => handleStateChange("")}
      testID={props.testID}
    >
      <View style={style} testID={`${props.testID}-bg`}>
        {content}
      </View>
    </Pressable>
  );
}
