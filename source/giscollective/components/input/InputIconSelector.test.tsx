import React from "react";
import { InputIconSelector } from "./InputIconSelector";
import { fireEvent, render, waitFor } from "@testing-library/react-native";
import { IconModel } from "../../models/Icon";
import { Store } from "../../models/store";
import { MemoryCache } from "../../lib/cache";
import { StaticAuthenticator } from "../../lib/authenticator";

jest.mock("../RemoteImage", () => {
  const { Text } = require("react-native");

  return {
    RemoteImage: (props) => (
      <>
        <Text testID={`${props.testID}-url`}>{props.url}</Text>
        <Text testID={`${props.testID}-width`}>{props.width}</Text>
        <Text testID={`${props.testID}-height`}>{props.height}</Text>
      </>
    ),
  };
});

const icon1Data = {
  _id: "1",
  name: "test name",
  localName: "test local name",
  image: {
    value: "https://example.com/image.jpg",
  },
  attributes: [],
  iconSet: "",
};

describe("InputIconSelector", () => {
  let cache;

  beforeAll(() => {
    cache = new MemoryCache();

    Store.instance = new Store(
      "",
      jest.fn(),
      cache,
      new StaticAuthenticator(""),
    );

    cache.upsert("1", "Icon", icon1Data);
  });

  it("only renders the select button", async () => {
    const component = render(<InputIconSelector testID="input" icons={[]} />);

    await waitFor(() => {
      expect(component.queryAllByTestId("input-select-icons")).toHaveLength(1);
    });

    expect(component.queryAllByTestId("input-selected-icon")).toHaveLength(0);
    expect(component.queryByTestId("input-select-icons")).not.toBeNull();
  });

  it("renders one icon", async () => {
    const icons: string[] = ["1"];

    const value = [new IconModel(icon1Data)];

    const component = render(
      <InputIconSelector testID="input" icons={icons} value={value} />,
    );

    await waitFor(() => {
      expect(component.queryAllByTestId("input-selected-icon")).toHaveLength(1);
      expect(component.queryByTestId("input-select-icons")).not.toBeNull();
    });
  });

  it("can remove one icon", async () => {
    const change = jest.fn();
    const value: IconModel[] = [
      new IconModel({
        _id: "1",
        name: "test name",
        localName: "test local name",
        image: {
          value: "https://example.com/image.jpg",
        },
        attributes: [],
        iconSet: "",
      }),
    ];

    const component = render(
      <InputIconSelector
        testID="input"
        icons={["1"]}
        value={value}
        onChange={change}
      />,
    );

    const removeBtn = await component.findByTestId(
      "input-selected-icon-remove",
    );

    await waitFor(() => {
      expect(component.queryAllByTestId("input-selected-icon")).toHaveLength(1);
    });

    await fireEvent.press(removeBtn);

    expect(change).toHaveBeenCalledWith([]);
    expect(component.queryAllByTestId("input-selected-icon")).toHaveLength(0);
    expect(component.queryByTestId("input-select-icons")).not.toBeNull();
  });
});
