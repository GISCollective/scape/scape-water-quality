import React, { useState } from "react";
import { ViewStyle } from "react-native";
import { View } from "react-native";
import { InputButton } from "./InputButton";

interface InputBooleanProps {
  testID?: string;
  required?: boolean;
  onChange: (value?: boolean) => void;
  value?: boolean;
}

export function InputBoolean(props: InputBooleanProps) {
  const [selectedValue, setSelectedValue] = useState(props.value);

  const style: ViewStyle = {
    flexDirection: "row",
    flexWrap: "wrap",
  };

  const middleButtonStyle = {
    marginHorizontal: 10,
  };

  const change = (value: boolean | undefined) => {
    setSelectedValue(value);
    props.onChange(value);
  };

  return (
    <View style={style}>
      <InputButton
        onPress={() => change(true)}
        testID={`${props.testID}-true`}
        classes={[
          selectedValue === true ? "btn-primary" : "btn-outline-secondary",
        ]}
      >
        Yes
      </InputButton>

      <View style={middleButtonStyle}>
        <InputButton
          onPress={() => change(false)}
          testID={`${props.testID}-false`}
          classes={[
            selectedValue === false ? "btn-primary" : "btn-outline-secondary",
          ]}
        >
          No
        </InputButton>
      </View>

      {!props.required && (
        <InputButton
          testID={`${props.testID}-undefined`}
          classes={[
            typeof selectedValue != "boolean"
              ? "btn-primary"
              : "btn-outline-secondary",
          ]}
          onPress={() => change(undefined)}
        >
          Unknown
        </InputButton>
      )}
    </View>
  );
}
