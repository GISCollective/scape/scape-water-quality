import { render, fireEvent } from "@testing-library/react-native";
import React from "react";
import { InputStreamDischarge, StreamDischarge } from "./InputStreamDischarge";

describe("InputStreamDischarge", () => {
  it("renders a value", async () => {
    const change = jest.fn();
    let value: StreamDischarge = {
      type: "stream discharge",
      data: {
        velocity: 20,
        width: 30,
        depth: 40,
        result: 0,
      },
    };

    const component = render(
      <InputStreamDischarge
        testID="input"
        onChange={change}
        value={value}
        system={"metric"}
      />,
    );

    const inputVelocity = await component.findByTestId("input-velocity");
    expect(inputVelocity.props.value).toBe("20");

    const inputWidth = await component.findByTestId("input-width");
    expect(inputWidth.props.value).toBe("30");

    const inputDepth = await component.findByTestId("input-depth");
    expect(inputDepth.props.value).toBe("40");

    const inputResult = await component.findByTestId("input-result");
    expect(inputResult.props.value).toBe("24000");
  });

  it("can update a value using a metric system", async () => {
    const change = jest.fn();
    let value: StreamDischarge = {
      type: "stream discharge",
      data: {
        velocity: 20,
        width: 30,
        depth: 40,
        result: 0,
      },
    };

    const component = render(
      <InputStreamDischarge
        testID="input"
        onChange={change}
        value={value}
        system={"metric"}
      />,
    );

    const inputVelocity = await component.findByTestId("input-velocity");
    fireEvent.changeText(inputVelocity, "2");

    const inputWidth = await component.findByTestId("input-width");
    fireEvent.changeText(inputWidth, "3");

    const inputDepth = await component.findByTestId("input-depth");
    fireEvent.changeText(inputDepth, "4");

    const inputResult = await component.findByTestId("input-result");
    expect(inputResult.props.value).toBe("24");
  });

  it("can trigger a change event when the input is changed", async () => {
    const change = jest.fn();
    let value: StreamDischarge = {
      type: "stream discharge",
      data: {
        velocity: 20,
        width: 30,
        depth: 40,
        result: 0,
      },
    };

    const component = render(
      <InputStreamDischarge
        testID="input"
        onChange={change}
        value={value}
        system={"metric"}
      />,
    );

    const inputVelocity = await component.findByTestId("input-velocity");
    fireEvent.changeText(inputVelocity, "2");

    const inputWidth = await component.findByTestId("input-width");
    fireEvent.changeText(inputWidth, "3");

    const inputDepth = await component.findByTestId("input-depth");
    fireEvent.changeText(inputDepth, "4");

    expect(change).toHaveBeenCalledWith({
      type: "stream discharge",
      data: {
        velocity: 2,
        width: 3,
        depth: 4,
        result: 24,
      },
    });
  });
});
