import React, { useState } from "react";
import { View, Image, PermissionsAndroid, Platform } from "react-native";
import { useWindowDimensions, TouchableHighlight } from "react-native";
import { Menu } from "react-native-paper";
import {
  ImageLibraryOptions,
  ImagePickerResponse,
  launchCamera,
  launchImageLibrary,
} from "react-native-image-picker";
import { InputButton } from "./InputButton";
import { Theme } from "../../lib/theme";
import { PictureModel } from "../../models/Picture";
import { uuidv4 } from "../../lib/uuid";
import { Store } from "../../models/store";

const requestCameraPermission = async () => {
  if (Platform.OS === "ios") {
    return true;
  }

  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
    );

    return granted === PermissionsAndroid.RESULTS.GRANTED;
  } catch (err) {
    console.warn(err);
    return false;
  }
};

interface InputPicturesProps {
  testID?: string;
  onChange: (value: PictureModel[]) => void;
  value?: PictureModel[];
}

export function InputPictures(props: InputPicturesProps) {
  const [isMenuVisible, setIsMenuVisible] = useState(false);
  const [value, setValue] = useState<PictureModel[]>(props.value ?? []);
  const [visiblePictureMenu, setVisiblePictureMenu] = useState(-1);

  const { width } = useWindowDimensions();
  const imageWidth = width / 3 - Theme.instance.spacers[1] / 1.5;

  const options: ImageLibraryOptions = {
    mediaType: "photo",
    quality: 0.9,
    includeBase64: true,
    selectionLimit: 0,
    presentationStyle: "pageSheet",
    maxWidth: 3000,
    maxHeight: 3000,
  };

  const onChange = (newValue: PictureModel[]) => {
    setValue(newValue);
    props.onChange(newValue);
  };

  const takePhotoUsingCamera = async () => {
    setIsMenuVisible(false);

    await requestCameraPermission();

    let result;

    try {
      result = await launchCamera(options);

      if (!result || result.errorCode == "camera_unavailable") {
        result = await launchImageLibrary(options);
      }
    } catch (err) {
      console.log("Can't launch camera.", err);
    }

    if (!result || result.errorCode == "camera_unavailable") {
      return alert("The camera is unavailable.");
    }

    await handlePhotoResult(result);
  };

  const takePhotoFromLibrary = async () => {
    setIsMenuVisible(false);

    const result = await launchImageLibrary(options);

    if (result.errorCode == "camera_unavailable") {
      return alert("The camera is unavailable.");
    }

    await handlePhotoResult(result);
  };

  const handlePhotoResult = async (result: ImagePickerResponse) => {
    if (!result?.assets?.length) {
      return;
    }

    const newPictures = result.assets
      .map((a) => "data:" + a.type + ";base64," + a.base64)
      .map(
        (picture) =>
          new PictureModel({
            _id: `pending-${uuidv4()}`,
            name: "",
            picture,
          }),
      );

    for (const picture of newPictures) {
      Store.instance.cache.upsert(picture._id, "Picture", picture.toJSON());
    }

    await onChange?.([...value, ...newPictures]);
  };

  const removePicture = (index: number) => {
    setVisiblePictureMenu(-1);
    onChange?.([...value.slice(0, index), ...value.slice(index + 1)]);
  };

  return (
    <>
      <View
        style={{
          display: "flex",
          flexDirection: "row",
          columnGap: Theme.instance.spacers[1],
          flexWrap: "wrap",
        }}
      >
        {value.map((item, index) => (
          <Menu
            visible={visiblePictureMenu === index}
            key={`photo-${index}`}
            onDismiss={() => setVisiblePictureMenu(-1)}
            anchor={
              <TouchableHighlight
                onPress={() => {
                  setVisiblePictureMenu(index);
                }}
              >
                <Image
                  style={{
                    width: imageWidth,
                    height: imageWidth,
                    marginBottom: Theme.instance.spacers[1],
                    resizeMode: "cover",
                  }}
                  source={{ uri: item.picture }}
                />
              </TouchableHighlight>
            }
          >
            <Menu.Item onPress={() => removePicture(index)} title="Remove" />
          </Menu>
        ))}
      </View>
      <Menu
        visible={isMenuVisible}
        onDismiss={() => setIsMenuVisible(false)}
        anchor={
          <InputButton
            testID="btn-take-photo"
            onPress={() => setIsMenuVisible(true)}
            classes={["btn-primary"]}
          >
            Take Photo
          </InputButton>
        }
      >
        <Menu.Item onPress={takePhotoUsingCamera} title="Using Camera" />
        <Menu.Item onPress={takePhotoFromLibrary} title="From Photo Library" />
      </Menu>
    </>
  );
}
