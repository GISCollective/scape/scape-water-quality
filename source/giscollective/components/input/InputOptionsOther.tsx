import React, { useState } from "react";
import { InputDropdown } from "./InputDropdown";
import { InputShortText } from "./InputShortText";

interface InputOptionsProps {
  testID?: string;
  onChange: (string: string) => void;
  value?: string;
  placeholder?: string;
  options: string;
}

export function InputOptionsOther(props: InputOptionsProps) {
  const options = props.options.split(",").map((a) => a.trim());
  const [isOther, setIsOther] = useState(false);

  const handleChange = (value: string) => {
    if (!isOther && value == "other") {
      setIsOther(true);
      return props.onChange?.("");
    }

    if (isOther) {
      setIsOther(false);
    }

    props.onChange?.(value);
  };

  return (
    <>
      <InputDropdown
        testID={props.testID}
        onChange={handleChange}
        value={props.value}
        options={[...options, "other"]}
      />

      {isOther && (
        <InputShortText
          classes={["mt-2"]}
          onChange={props.onChange}
          testID={`${props.testID}-input`}
        />
      )}
    </>
  );
}
