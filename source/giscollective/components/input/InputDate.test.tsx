import { render, fireEvent, waitFor } from "@testing-library/react-native";
import React, { useState } from "react";
import { InputDate } from "./InputDate";

jest.mock("react-native-paper-dates", () => {
  return {
    DatePickerModal: () => <></>,
  };
});

describe("InputDate", () => {
  it("renders the value when set", async () => {
    const change = jest.fn();

    const component = render(
      <InputDate testID="input" onChange={change} value="2024-12-12" />,
    );

    const btn = await component.findByTestId("input-open");

    expect(btn).toHaveTextContent("December 12, 2024");
  });

  it("renders a message when the value is not set", async () => {
    const change = jest.fn();

    const component = render(
      <InputDate testID="input" onChange={change} value="" />,
    );

    const btn = await component.findByTestId("input-open");

    expect(btn).toHaveTextContent("Tap to select the date");
  });
});
