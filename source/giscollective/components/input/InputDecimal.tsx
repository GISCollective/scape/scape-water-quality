import React, { useEffect, useRef, useState } from "react";
import {
  KeyboardTypeOptions,
  TextInput,
  TextStyle,
  Text,
  View,
  ViewStyle,
} from "react-native";
import { Theme } from "../../lib/theme";

export const metricUnits: Record<string, string> = {
  Temperature: "°C",
  "Dissolved Oxygen": "mg/L",
  Turbidity: "NTU",
  Conductivity: "μS/cm",
  Salinity: "ppt",
  Distance: "m",
};

export const imperialUnits: Record<string, string> = {
  Temperature: "°F",
  "Dissolved Oxygen": "mg/L",
  Turbidity: "NTU",
  Conductivity: "μS/cm",
  Salinity: "ppt",
  Distance: "ft",
};

export const units = {
  metric: metricUnits,
  imperial: imperialUnits,
};

const transform: Record<string, Record<string, (a: string) => string>> = {
  Temperature: {
    toSI: (value: string) => {
      const result = (parseFloat(value) - 32) * (5 / 9);

      if (isNaN(result)) {
        return value;
      }

      return `${Math.round(result * 10000) / 10000}`;
    },
    fromSI: (value: string) => {
      const result = (parseFloat(value) * 9) / 5 + 32;

      if (isNaN(result)) {
        return value;
      }

      return `${result}`;
    },
  },
  Distance: {
    toSI: (value: string) => {
      const result = parseFloat(value) / 3.28;

      if (isNaN(result)) {
        return value;
      }

      return `${Math.round(result * 10000) / 10000}`;
    },
    fromSI: (value: string) => {
      const result = parseFloat(value) * 3.28;

      if (isNaN(result)) {
        return value;
      }

      return `${result}`;
    },
  },
};

interface InputDecimalProps {
  testID?: string;
  onChange?: (string: number) => void;
  value?: string;
  placeholder?: string;
  keyboardType?: KeyboardTypeOptions;
  options?: Record<string, string>;
  system: "metric" | "imperial";
}

export function getPostfix(props: InputDecimalProps) {
  if (!props.system) {
    return null;
  }

  const measurement = props.options?.measurement ?? "";

  return units?.[props.system]?.[measurement];
}

export function transformToSi(value: string, props: InputDecimalProps) {
  if (props.system == "metric") {
    return value;
  }

  const measurement = props.options?.measurement ?? "";

  return transform?.[measurement]?.toSI(value) ?? value;
}

export function transformFromSi(value: string, props: InputDecimalProps) {
  if (props.system == "metric") {
    return value;
  }

  const measurement = props.options?.measurement ?? "";

  return transform?.[measurement]?.fromSI(value) ?? value;
}

export function TransformedValue(props: InputDecimalProps) {
  if (props.system == "metric") {
    return null;
  }

  const measurement = props.options?.measurement ?? "";

  if (!transform?.[measurement]?.toSI) {
    return null;
  }

  const unit = units.metric[measurement];
  const value = transformToSi(props.value ?? "", props) || "";

  const padding: number =
    (Theme.instance.style?.formControl?.paddingHorizontal as number) ?? 0;
  const border: number =
    (Theme.instance.style?.formControl?.borderWidth as number) ?? 0;

  const textStyle = Theme.instance.toTextStyle(["pt-2"], {
    textAlign: "right",
    color: Theme.instance.toColor("secondary"),
    fontWeight: "bold",
    paddingEnd: padding + border,
  });

  return (
    <Text testID={`${props.testID}-transformed`} style={textStyle}>
      {value}
      {unit}
    </Text>
  );
}

interface Measurement {
  x: number;
  y: number;
  width: number;
  height: number;
}

export function InputDecimal(props: InputDecimalProps) {
  const [selectedValue, setSelectedValue] = useState(
    transformFromSi(props.value ?? "", props),
  );
  const [isValid, setIsValid] = useState(true);
  const [inputMeasurements, setInputMeasurements] = useState<Measurement>({
    x: 0,
    y: 0,
    width: 0,
    height: 0,
  });
  const [placeholderMeasurements, setPlaceholderMeasurements] =
    useState<Measurement>({ x: 0, y: 0, width: 0, height: 0 });
  const [relativeStyle, setRelativeStyle] = useState<TextStyle | null>(null);

  let state: "" | "focus" | "disabled" = "";

  const containerRef = useRef<View>(null);
  const inputPlaceholderRef = useRef<Text>(null);
  const inputRef = useRef<TextInput>(null);

  useEffect(() => {
    if (!inputRef.current || !inputPlaceholderRef.current) {
      return;
    }

    inputRef.current.measureInWindow((x, y, width, height) => {
      setInputMeasurements({ x, y, width, height });
    });

    inputPlaceholderRef.current.measureInWindow((x, y, width, height) => {
      setPlaceholderMeasurements({ x, y, width, height });
    });
  }, [inputRef.current, inputPlaceholderRef.current]);

  useEffect(() => {
    if (
      placeholderMeasurements.width == inputMeasurements.width &&
      placeholderMeasurements.x == inputMeasurements.x
    ) {
      return;
    }

    const value: ViewStyle = {
      position: "relative",
      width: placeholderMeasurements.width,
      left: placeholderMeasurements.x - inputMeasurements.x,
      top: placeholderMeasurements.y - inputMeasurements.y,
      backgroundColor: "transparent",
    };

    setRelativeStyle(value);
  }, [placeholderMeasurements, inputMeasurements]);

  const updateFocus = (hasFocus: boolean) => {
    let style: TextStyle = Theme.instance.toFormControlStyle(
      [],
      hasFocus ? "focus" : "",
      {},
    );

    containerRef.current?.setNativeProps({
      style,
    });
  };

  const change = (value: string) => {
    setSelectedValue(value);

    const newValue = transformToSi(value, props);

    if (`${parseFloat(newValue)}` != newValue) {
      setIsValid(false);
      return;
    }

    setIsValid(true);
    props.onChange?.(parseFloat(newValue));
  };

  useEffect(() => {
    if (props.value === selectedValue) {
      return;
    }

    if (props.value === undefined) {
      return;
    }

    change(transformFromSi(props.value, props));
  }, [props.value]);

  if (!props.onChange) {
    state = "disabled";
  }

  let style: TextStyle = Theme.instance.toFormControlStyle([], state, {});

  const inputStyle: TextStyle = {
    ...style,
    paddingBottom: 0,
    paddingTop: 0,
    paddingLeft: 0,
    paddingEnd: 0,
    borderWidth: 0,
    borderRadius: 0,
    flexGrow: 1,
  };

  const postfixStyle = {
    ...style,
    paddingBottom: 0,
    paddingTop: 0,
    paddingLeft: 0,
    paddingEnd: 0,
    borderWidth: 0,
    flexGrow: 0,
    color: Theme.instance.toColor("secondary"),
  };

  if (!isValid) {
    style.borderColor = Theme.instance.toColor("danger");
  }

  const postfix = getPostfix(props);

  return (
    <>
      <View
        ref={containerRef}
        style={{ display: "flex", flexDirection: "row", ...style }}
      >
        <Text ref={inputPlaceholderRef} style={inputStyle} />
        {postfix && (
          <Text style={postfixStyle} testID={`${props.testID}-postfix`}>
            {postfix}
          </Text>
        )}
      </View>

      <TextInput
        testID={props.testID}
        style={{ ...inputStyle, ...relativeStyle }}
        onFocus={() => updateFocus(true)}
        onBlur={() => updateFocus(false)}
        onChangeText={change}
        value={`${selectedValue}`}
        placeholder={props.placeholder}
        keyboardType={"numbers-and-punctuation"}
        editable={props.onChange ? true : false}
        ref={inputRef}
      />

      <TransformedValue {...props} value={selectedValue} />
    </>
  );
}
