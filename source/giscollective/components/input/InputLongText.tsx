import React from "react";
import { InputShortText } from "./InputShortText";

interface InputLongTextProps {
  testID?: string;
  onChange: (string: string) => void;
  value?: string;
  placeholder?: string;
}

export function InputLongText(props: InputLongTextProps) {
  return (
    <InputShortText
      testID={props.testID}
      onChange={props.onChange}
      value={props.value}
      placeholder={props.placeholder}
      numberOfLines={6}
    />
  );
}
