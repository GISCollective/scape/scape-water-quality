import React from "react";
import { InputOptionsOther } from "./InputOptionsOther";
import {
  render,
  fireEvent,
  act,
  waitFor,
  userEvent,
} from "@testing-library/react-native";

describe("InputOptionsOther", () => {
  it("renders an other option", async () => {
    const component = render(
      <InputOptionsOther
        onChange={jest.fn()}
        options="a,b,c"
        testID="options"
      />,
    );

    await fireEvent.press(component.queryByTestId("options-change")!);

    const element = await component.findByTestId("options-option-3");

    expect(element).toHaveTextContent("other");
  });

  it("can set an option", async () => {
    const changed = jest.fn();
    const component = render(
      <InputOptionsOther onChange={changed} options="a,b,c" testID="options" />,
    );

    await fireEvent.press(component.queryByTestId("options-change")!);

    const button = await component.findByTestId("options-option-0");

    await fireEvent.press(button);

    await waitFor(() => {
      expect(changed).toHaveBeenCalledWith("a");
    });
  });

  it("can set a custom value", async () => {
    const changed = jest.fn();
    const component = render(
      <InputOptionsOther onChange={changed} options="a,b,c" testID="options" />,
    );

    await fireEvent.press(component.queryByTestId("options-change")!);

    const button = await component.findByTestId("options-option-3");
    await fireEvent.press(button);

    const input = await component.findByTestId("options-input");
    fireEvent.changeText(input, "some new value");

    await waitFor(() => {
      expect(changed).toHaveBeenCalledWith("some new value");
    });
  });
});
