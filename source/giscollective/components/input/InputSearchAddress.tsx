import React from "react";
import { GeoJsonGeometry } from "../../models/GeoJsonGeometry";
import { InputShortText } from "./InputShortText";
import { View } from "react-native";
import { Theme } from "../../lib/theme";
import { useState } from "react";
import { useDebounceCallback } from "usehooks-ts";
import { Store } from "../../models/store";
import { GeocodingModel } from "../../models/Geocoding";
import { InputDropdown } from "./InputDropdown";
import { Label } from "../Label";
import { GeoJson } from "../../lib/geoJson";
import { PositionDetailsValue } from "../../lib/positionDetails";

interface InputSearchAddressProps {
  testID?: string;
  classes?: string[];
  searchTerm?: string;
  onChange?: (
    position: GeoJsonGeometry,
    details: PositionDetailsValue | undefined,
  ) => void;
}

export const InputSearchAddress = (props: InputSearchAddressProps) => {
  const [isLoading, setIsLoading] = useState(false);
  const [result, setResult] = useState<GeocodingModel[]>([]);
  const [searchTerm, setSearchTerm] = useState<string>(props.searchTerm ?? "");
  const [value, setValue] = useState<GeocodingModel | undefined>();

  const selectAddress = (value: GeocodingModel | undefined) => {
    setValue(value);

    if (!value) {
      return;
    }

    const center = new GeoJson(value?.geometry).center;

    const position = center.toJSON();
    const details: PositionDetailsValue = {
      capturedUsingGPS: false,
      type: "",
      altitude: 0,
      accuracy: 0,
      altitudeAccuracy: 0,
      searchTerm,
      address: value.name,
      id: null,
    };

    props.onChange?.(position, details);
  };

  const debounced = useDebounceCallback(async (query) => {
    setIsLoading(true);
    setSearchTerm(query);

    try {
      const response = await Store.instance.queryGeocodings({ query });
      setResult(response);
      selectAddress(response[0]);
    } catch (err) {
      console.error(err);
    }

    setIsLoading(false);
  }, 500);

  return (
    <InputSearchAddressView
      onChange={selectAddress}
      value={value}
      searchTerm={props.searchTerm}
      testID={props.testID}
      classes={props.classes}
      isLoading={isLoading}
      list={result}
      handleSearch={debounced}
    />
  );
};

interface InputSearchAddressViewProps {
  list: GeocodingModel[];
  value: GeocodingModel | undefined;
  searchTerm?: string;
  testID?: string;
  onChange?: (value: GeocodingModel) => void;
  classes?: string[];
  isLoading: boolean;
  handleSearch: (value: string) => void;
}

export const InputSearchAddressView = (props: InputSearchAddressViewProps) => {
  const style = Theme.instance.toStyle(props.classes);
  const hasOptions = !!props.list?.length;

  return (
    <View style={style}>
      <Label isLoading={props.isLoading}>search an address</Label>
      <InputShortText
        value={props.searchTerm}
        testID={`${props.testID}-search`}
        onChange={props.handleSearch}
      />

      {hasOptions && (
        <InputDropdown
          classes={["mt-2"]}
          testID="input"
          value={props.value}
          options={props.list}
          onChange={props.onChange}
        />
      )}
    </View>
  );
};
