import React, { useState } from "react";
import { KeyboardTypeOptions, TextInput, TextStyle } from "react-native";
import { Theme } from "../../lib/theme";

interface InputShortTextProps {
  testID?: string;
  onChange: (string: string) => void;
  value?: string;
  placeholder?: string;
  keyboardType?: KeyboardTypeOptions;
  classes?: string[];
  numberOfLines?: number;
}

export function InputShortText(props: InputShortTextProps) {
  const [selectedValue, setSelectedValue] = useState(props.value ?? "");
  const [focus, setFocus] = useState(false);
  const numberOfLines = props.numberOfLines ?? 1;

  let style: TextStyle = Theme.instance.toStyle(props.classes, {});

  if (numberOfLines > 1) {
    style.textAlignVertical = "top";
  }

  style = Theme.instance.toFormControlStyle([], focus ? "focus" : "", style);

  const change = (value: string) => {
    setSelectedValue(value);
    props.onChange(value);
  };

  return (
    <TextInput
      testID={props.testID}
      style={style}
      onFocus={() => setFocus(true)}
      onBlur={() => setFocus(false)}
      onChangeText={change}
      value={selectedValue}
      placeholder={props.placeholder}
      keyboardType={props.keyboardType}
      numberOfLines={props.numberOfLines}
      multiline={numberOfLines > 1 ? true : false}
    />
  );
}
