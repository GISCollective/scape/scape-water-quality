import React, { useState } from "react";
import { KeyboardTypeOptions, TextInput, TextStyle } from "react-native";
import { Theme } from "../../lib/theme";

interface InputIntegerProps {
  testID?: string;
  onChange: (string: number) => void;
  value?: string;
  placeholder?: string;
  keyboardType?: KeyboardTypeOptions;
}

export function InputInteger(props: InputIntegerProps) {
  const [selectedValue, setSelectedValue] = useState(props.value ?? "");
  const [focus, setFocus] = useState(false);
  const [isValid, setIsValid] = useState(true);

  let style: TextStyle = {};

  style = Theme.instance.toFormControlStyle([], focus ? "focus" : "", style);

  if (!isValid) {
    style.borderColor = Theme.instance.toColor("danger");
  }

  const change = (value: string) => {
    setSelectedValue(value);

    if (`${parseInt(value)}` != value) {
      setIsValid(false);
      return;
    }

    setIsValid(true);
    props.onChange(parseInt(value));
  };

  return (
    <TextInput
      testID={props.testID}
      style={style}
      onFocus={() => setFocus(true)}
      onBlur={() => setFocus(false)}
      onChangeText={change}
      value={`${selectedValue}`}
      placeholder={props.placeholder}
      keyboardType={"numbers-and-punctuation"}
    />
  );
}
