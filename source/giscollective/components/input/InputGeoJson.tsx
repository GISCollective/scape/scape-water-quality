import { View, ViewStyle } from "react-native";
import { InputButton } from "./InputButton";
import React, { RefObject, useState } from "react";
import { Theme } from "../../lib/theme";
import { GeoJsonGeometry, Point } from "../../models/GeoJsonGeometry";
import { InputSearchAddress } from "./InputSearchAddress";
import { Store } from "../../models/store";
import { ActionSheetRef } from "react-native-actions-sheet";
import { FeaturePicker } from "../FeaturePicker";
import { GeoJsonView } from "../GeoJsonView";
import { H6 } from "../html";
import { LocationPicker } from "../LocationPicker";
import { LocationWatch } from "../LocationWatch";
import {
  PositionDetailsValue,
  PositionDetails,
} from "../../lib/positionDetails";
import { MapView } from "../MapView";

export interface InputGeoJsonProps {
  testID?: string;
  value?: GeoJsonGeometry;
  details?: PositionDetailsValue;
  mapId?: string;
  extent?: GeoJsonGeometry;
  positionDetails?: PositionDetailsValue;
  onChange?: (
    position: GeoJsonGeometry,
    details: PositionDetailsValue | undefined,
  ) => void;
  options: {
    allowGps: boolean;
    allowManual: boolean;
    allowAddress: boolean;
    allowExistingFeature: boolean;
  };
}

const defaultPosition: Point = {
  type: "Point",
  coordinates: [],
};

export function InputGeoJson(props: InputGeoJsonProps) {
  const buttonContainer: ViewStyle = {
    flexDirection: "row",
    flexWrap: "wrap",
  };

  const [position, setPosition] = useState<Point | undefined>(
    props.value as Point,
  );
  const [details, setDetails] = useState<PositionDetailsValue | undefined>(
    props.details,
  );
  const [sheetRef, setSheetRef] = useState<RefObject<ActionSheetRef> | null>(
    null,
  );

  const mapStyle = {
    ...Theme.instance.style.formControl,
    height: 300,
    paddingHorizontal: 0,
    paddingVertical: 0,
  };

  const detailsType = props.positionDetails?.type ?? "";

  const [initialType, setInitialType] = useState(
    props.value ? "manual" : detailsType,
  );
  const [type, setType] = useState(props.value ? "manual" : detailsType);

  const triggerChange = () => {
    props.onChange?.(position ?? defaultPosition, details);
  };

  const setGps = () => {
    setType("gps");
  };

  const setManually = () => {
    setType("manual");
    sheetRef?.current?.show();
  };

  const setExistingFeature = () => {
    setType("feature");
  };

  const setAddress = () => {
    setType("address");
  };

  const selectFeature = async (features: string[]) => {
    if (features.length == 0) {
      return setType("");
    }

    const feature = await Store.instance.findFeature(features[0]);

    setPosition(feature.position as Point);
    setDetails(new PositionDetails({ type: "feature", id: features[0] }));

    if (position) {
      return props.onChange?.(position, details);
    }
  };

  const selectCenter = (coordinates: number[] | undefined) => {
    if (!coordinates) {
      return;
    }

    if (coordinates) {
      setPosition({
        type: "Point",
        coordinates,
      });
    }

    triggerChange();
  };

  const handleChange = (
    position: GeoJsonGeometry,
    details: PositionDetailsValue | undefined,
  ) => {
    setPosition(position as Point);
    setDetails(details);
    return props.onChange?.(position, details);
  };

  const onGpsChange = (
    newValue: GeoJsonGeometry,
    details: PositionDetailsValue | undefined,
  ) => {
    console.log("GPS change", newValue, details);
    setPosition(newValue as Point);
    props.onChange?.(newValue, details);
  };

  const isFeaturePicker = type == "feature" && props.mapId;

  return (
    <>
      <View style={buttonContainer}>
        {props.options?.allowGps && (
          <InputButton
            onPress={() => setGps()}
            testID={`${props.testID}-gps`}
            classes={[
              type == "gps" ? "btn-primary" : "btn-outline-secondary",
              "mb-2",
              "me-2",
            ]}
          >
            use my current location
          </InputButton>
        )}

        {props.options?.allowManual && (
          <InputButton
            onPress={() => setManually()}
            testID={`${props.testID}-manual`}
            classes={[
              type == "manual" ? "btn-primary" : "btn-outline-secondary",
              "mb-2",
              "me-2",
            ]}
          >
            choose manually on the map
          </InputButton>
        )}

        {props.options?.allowAddress && (
          <InputButton
            onPress={() => setAddress()}
            testID={`${props.testID}-address`}
            classes={[
              type == "address" ? "btn-primary" : "btn-outline-secondary",
              "mb-2",
              "me-2",
            ]}
          >
            by address
          </InputButton>
        )}

        {props.options?.allowExistingFeature && (
          <InputButton
            onPress={() => setExistingFeature()}
            classes={[
              type == "feature" ? "btn-primary" : "btn-outline-secondary",
              "mb-2",
              "me-2",
            ]}
          >
            select an existing feature
          </InputButton>
        )}
      </View>

      {type == "address" && (
        <InputSearchAddress
          classes={["mb-3"]}
          testID={props.testID}
          onChange={handleChange}
        />
      )}

      {type && (
        <>
          <View style={mapStyle} testID={`${props.testID}-map-view`}>
            {type == "gps" && (
              <>
                <MapView
                  mode="user-location"
                  style={{
                    borderRadius: Theme.instance.style.formControl.borderRadius,
                  }}
                />
                <LocationWatch onChange={onGpsChange} />
              </>
            )}
            {type != "gps" && position && (
              <GeoJsonView
                style={{
                  borderRadius: Theme.instance.style?.formControl?.borderRadius,
                }}
                value={position}
              />
            )}
          </View>
          <View style={Theme.instance.resolveClasses(["mt-2"])}>
            <H6>Is there a better location?</H6>
            <InputButton
              onPress={() => setManually()}
              testID={`${props.testID}-set-manually`}
              classes={["btn-secondary", "mb-2"]}
            >
              Pick it on the map
            </InputButton>
          </View>
        </>
      )}

      {!isFeaturePicker && (
        <LocationPicker
          testID="location-picker"
          onSelectedLocation={selectCenter}
          extent={props.extent}
          center={position?.coordinates}
          initialType={initialType}
          onRefChange={(ref) => setSheetRef(ref)}
        />
      )}

      {isFeaturePicker && (
        <FeaturePicker
          testID="feature-picker"
          mapId={props.mapId}
          onSelect={selectFeature}
          extent={props.extent}
        />
      )}
    </>
  );
}
