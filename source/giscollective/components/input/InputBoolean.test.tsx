import { render, fireEvent } from "@testing-library/react-native";
import React from "react";
import { InputBoolean } from "./InputBoolean";

describe("InputBoolean", () => {
  it("triggers onChange true when the yes button is pressed", async () => {
    const change = jest.fn();

    const component = render(<InputBoolean testID="input" onChange={change} />);

    const button = await component.findByTestId("input-true");
    fireEvent.press(button);

    expect(change).toHaveBeenCalledWith(true);
  });

  it("triggers onChange false when the yes button is pressed", async () => {
    const change = jest.fn();

    const component = render(<InputBoolean testID="input" onChange={change} />);

    const button = await component.findByTestId("input-false");
    fireEvent.press(button);

    expect(change).toHaveBeenCalledWith(false);
  });

  it("triggers onChange undefined when the yes button is pressed", async () => {
    const change = jest.fn();

    const component = render(<InputBoolean testID="input" onChange={change} />);

    const button = await component.findByTestId("input-undefined");
    fireEvent.press(button);

    expect(change).toHaveBeenCalledWith(undefined);
  });

  it("renders the yes option when true", async () => {
    const change = jest.fn();

    const component = render(
      <InputBoolean testID="input" onChange={change} value={true} />,
    );
    const buttonYes = await component.findByTestId("input-true-bg");
    const buttonNo = await component.findByTestId("input-false-bg");
    const buttonUnknown = await component.findByTestId("input-undefined-bg");

    expect(buttonYes).toHaveStyle({
      backgroundColor: "rgb(57,97,208)",
    });
    expect(buttonNo).toHaveStyle({
      backgroundColor: "rgb(255,255,255)",
    });
    expect(buttonUnknown).toHaveStyle({
      backgroundColor: "rgb(255,255,255)",
    });
  });

  it("renders the no option when false", async () => {
    const change = jest.fn();

    const component = render(
      <InputBoolean testID="input" onChange={change} value={false} />,
    );
    const buttonYes = await component.findByTestId("input-true-bg");
    const buttonNo = await component.findByTestId("input-false-bg");
    const buttonUnknown = await component.findByTestId("input-undefined-bg");

    expect(buttonYes).toHaveStyle({
      backgroundColor: "rgb(255,255,255)",
    });
    expect(buttonNo).toHaveStyle({
      backgroundColor: "rgb(57,97,208)",
    });
    expect(buttonUnknown).toHaveStyle({
      backgroundColor: "rgb(255,255,255)",
    });
  });

  it("renders the no option when undefined", async () => {
    const change = jest.fn();

    const component = render(
      <InputBoolean testID="input" onChange={change} value={undefined} />,
    );
    const buttonYes = await component.findByTestId("input-true-bg");
    const buttonNo = await component.findByTestId("input-false-bg");
    const buttonUnknown = await component.findByTestId("input-undefined-bg");

    expect(buttonYes).toHaveStyle({
      backgroundColor: "rgb(255,255,255)",
    });
    expect(buttonNo).toHaveStyle({
      backgroundColor: "rgb(255,255,255)",
    });
    expect(buttonUnknown).toHaveStyle({
      backgroundColor: "rgb(57,97,208)",
    });
  });
});
