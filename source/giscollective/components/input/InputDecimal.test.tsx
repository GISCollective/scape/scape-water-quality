import { render, fireEvent } from "@testing-library/react-native";
import React from "react";
import { InputDecimal } from "./InputDecimal";

describe("InputDecimal", () => {
  it("triggers onChange when the input is filled", async () => {
    const change = jest.fn();

    const component = render(
      <InputDecimal testID="input" onChange={change} system={"imperial"} />,
    );

    const input = await component.findByTestId("input");
    fireEvent.changeText(input, "123.5");

    expect(change).toHaveBeenCalledWith(123.5);
    expect(input.props.value).toBe("123.5");
  });

  it("does not trigger onChange when the input is filled ith an invalid value", async () => {
    const change = jest.fn();

    const component = render(
      <InputDecimal testID="input" onChange={change} system={"imperial"} />,
    );

    const input = await component.findByTestId("input");
    fireEvent.changeText(input, "abc");

    expect(change).toHaveBeenCalledTimes(0);
    expect(input.props.value).toBe("abc");
  });

  it("renders the value when set", async () => {
    const change = jest.fn();

    const component = render(
      <InputDecimal
        testID="input"
        onChange={change}
        value="123"
        system={"imperial"}
      />,
    );

    const input = await component.findByTestId("input");

    expect(input.props.value).toBe("123");
    expect(component.queryByTestId("input-postfix")).toBeFalsy();
  });

  describe("reading temperature", () => {
    describe("using the metric system", () => {
      it("renders the postfix", async () => {
        const change = jest.fn();
        const options = { measurement: "Temperature" };

        const component = render(
          <InputDecimal
            testID="input"
            onChange={change}
            value="123"
            options={options}
            system="metric"
          />,
        );

        expect(component.queryByTestId("input-postfix")).toHaveTextContent(
          "°C",
        );
      });

      it("renders the value", async () => {
        const change = jest.fn();
        const options = { measurement: "Temperature" };

        const component = render(
          <InputDecimal
            testID="input"
            onChange={change}
            value="123"
            options={options}
            system={"metric"}
          />,
        );

        const input = await component.findByTestId("input");

        expect(input.props.value).toBe("123");
        expect(component.queryByTestId("input-transformed")).toBeFalsy();
      });

      it("triggers onChange when the input is filled", async () => {
        const change = jest.fn();
        const options = { measurement: "Temperature" };
        const component = render(
          <InputDecimal
            testID="input"
            options={options}
            onChange={change}
            system={"metric"}
          />,
        );

        const input = await component.findByTestId("input");
        fireEvent.changeText(input, "123.5");

        expect(change).toHaveBeenCalledWith(123.5);
        expect(input.props.value).toBe("123.5");
      });
    });

    describe("using the imperial system", () => {
      it("renders the postfix", async () => {
        const change = jest.fn();
        const options = { measurement: "Temperature" };

        const component = render(
          <InputDecimal
            testID="input"
            onChange={change}
            value="123"
            options={options}
            system="imperial"
          />,
        );

        expect(component.queryByTestId("input-postfix")).toHaveTextContent(
          "°F",
        );
      });

      it("renders renders the transformed value", async () => {
        const change = jest.fn();
        const options = { measurement: "Temperature" };

        const component = render(
          <InputDecimal
            testID="input"
            onChange={change}
            value="123"
            options={options}
            system={"imperial"}
          />,
        );

        const input = await component.findByTestId("input");

        expect(component.queryByTestId("input-transformed")).toHaveTextContent(
          "123°C",
        );
        expect(input.props.value).toBe("253.4");
      });

      it("triggers onChange when the input is filled", async () => {
        const change = jest.fn();
        const options = { measurement: "Temperature" };
        const component = render(
          <InputDecimal
            testID="input"
            options={options}
            onChange={change}
            system={"imperial"}
          />,
        );

        const input = await component.findByTestId("input");
        fireEvent.changeText(input, "254.3");

        expect(change).toHaveBeenCalledWith(123.5);
        expect(input.props.value).toBe("254.3");
      });
    });
  });
});
