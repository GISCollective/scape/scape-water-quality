import React, { useState } from "react";
import { KeyboardTypeOptions } from "react-native";
import { TimePickerModal } from "react-native-paper-dates";
import { InputButton } from "./InputButton";

interface InputTimeProps {
  testID?: string;
  onChange: (string: string) => void;
  value?: string;
  keyboardType?: KeyboardTypeOptions;
}

interface Time {
  hours: number;
  minutes: number;
  timezone: string;
}

function toTime(value: string | undefined): Time | null {
  if (!value) {
    return null;
  }

  const pieces = value.split(":").map((a) => parseInt(a));

  return {
    hours: pieces[0],
    minutes: pieces[1],
    timezone: "",
  };
}

function niceNumber(value: number | undefined | null) {
  return `${(value ?? 0) <= 9 ? "0" : ""}${value}`;
}

export function InputTime(props: InputTimeProps) {
  const [sampleTimeOpen, setSampleTimeOpen] = useState(false);
  const [value, setValue] = useState<Time | null>(toTime(props.value));

  const onSampleDateDismiss = React.useCallback(() => {
    setSampleTimeOpen(false);
  }, [setSampleTimeOpen]);

  const onSampleDateChange = React.useCallback(
    (params) => {
      setSampleTimeOpen(false);
      setValue({ hours: params.hours, minutes: params.minutes, ...params });
      props.onChange(
        `${niceNumber(params.hours)}:${niceNumber(params.minutes)}`,
      );
    },
    [setSampleTimeOpen, setValue],
  );

  const mm = niceNumber(value?.minutes);
  const hh = niceNumber(value?.hours);

  const label = value ? `${hh}:${mm}` : "Tap to select the time";

  return (
    <>
      <InputButton
        onPress={() => setSampleTimeOpen(true)}
        testID={`${props.testID}-open`}
        classes={["btn-outline-primary"]}
        align="start"
      >
        {label}
      </InputButton>

      <TimePickerModal
        locale="en"
        visible={sampleTimeOpen}
        hours={value?.hours ?? 0}
        minutes={value?.minutes ?? 0}
        onConfirm={onSampleDateChange}
        onDismiss={onSampleDateDismiss}
      />
    </>
  );
}
