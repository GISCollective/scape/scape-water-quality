import { render, fireEvent, waitFor } from "@testing-library/react-native";
import React, { useState } from "react";
import { InputTime } from "./InputTime";

jest.mock("react-native-paper-dates", () => {
  return {
    TimePickerModal: () => <></>,
  };
});

describe("InputTime", () => {
  it("renders the value when set", async () => {
    const change = jest.fn();

    const component = render(
      <InputTime testID="input" onChange={change} value="12:10" />,
    );

    const btn = await component.findByTestId("input-open");

    expect(btn).toHaveTextContent("12:10");
  });

  it("renders a message when the value is not set", async () => {
    const change = jest.fn();

    const component = render(
      <InputTime testID="input" onChange={change} value="" />,
    );

    const btn = await component.findByTestId("input-open");

    expect(btn).toHaveTextContent("Tap to select the time");
  });
});
