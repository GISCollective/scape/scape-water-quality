import React, { useState } from "react";
import { InputDecimal } from "./InputDecimal";
import { Label } from "../Label";

export interface StreamDischargeData {
  velocity?: number;
  width?: number;
  depth?: number;
  result?: number;
}

export interface StreamDischarge {
  type?: "stream discharge";
  data?: StreamDischargeData;
}

export interface InputStreamDischargeProps {
  value: StreamDischarge;
  testID?: string;
  onChange: (newValue: StreamDischarge) => void;
  classes?: string[];
  isRequired?: boolean;
  system: "metric" | "imperial";
}

export function InputStreamDischarge(props: InputStreamDischargeProps) {
  const [velocity, setVelocity] = useState(props.value?.data?.velocity ?? 0);
  const [width, setWidth] = useState(props.value?.data?.width ?? 0);
  const [depth, setDepth] = useState(props.value?.data?.depth ?? 0);

  const triggerChange = (
    newVelocity: number,
    newWidth: number,
    newDepth: number,
  ) => {
    if (velocity != newVelocity) {
      setVelocity(newVelocity);
    }

    if (width != newWidth) {
      setWidth(newWidth);
    }

    if (depth != newDepth) {
      setDepth(newDepth);
    }

    props.onChange({
      type: "stream discharge",
      data: {
        velocity: newVelocity,
        width: newWidth,
        depth: newDepth,
        result: newVelocity * newWidth * newDepth,
      },
    });
  };

  const options = {
    measurement: "Distance",
  };
  const result = velocity * width * depth;

  return (
    <>
      <Label
        testID={`${props.testID}-question-text`}
        isRequired={props.isRequired}
        classes={["mt-3"]}
        color="secondary"
      >
        Velocity
      </Label>
      <InputDecimal
        testID={`${props.testID}-velocity`}
        value={`${velocity}`}
        options={options}
        onChange={(newValue) => triggerChange(newValue, width, depth)}
        system={props.system}
      />

      <Label
        testID={`${props.testID}-question-text`}
        isRequired={props.isRequired}
        classes={["mt-3"]}
        color="secondary"
      >
        Width
      </Label>
      <InputDecimal
        testID={`${props.testID}-width`}
        options={options}
        value={`${width}`}
        onChange={(newValue) => triggerChange(velocity, newValue, depth)}
        system={props.system}
      />

      <Label
        testID={`${props.testID}-question-text`}
        isRequired={props.isRequired}
        classes={["mt-3"]}
        color="secondary"
      >
        Depth
      </Label>
      <InputDecimal
        testID={`${props.testID}-depth`}
        options={options}
        value={`${depth}`}
        onChange={(newValue) => triggerChange(velocity, width, newValue)}
        system={props.system}
      />

      <Label
        testID={`${props.testID}-question-text`}
        isRequired={props.isRequired}
        classes={["mt-3"]}
        color="secondary"
      >
        Stream Discharge
      </Label>
      <InputDecimal
        testID={`${props.testID}-result`}
        options={options}
        value={`${result}`}
        system={props.system}
      />
    </>
  );
}
