import React from "react";
import { InputDropdown } from "./InputDropdown";

interface InputOptionsProps {
  testID?: string;
  onChange: (string: string) => void;
  value?: string;
  placeholder?: string;
  options: string;
}

export function InputOptions(props: InputOptionsProps) {
  const options = props.options.split(",").map((a) => a.trim());

  const handleChange = (value: string) => {
    props.onChange?.(value);
  };

  return (
    <InputDropdown
      testID={props.testID}
      onChange={handleChange}
      value={props.value}
      options={options}
    />
  );
}
