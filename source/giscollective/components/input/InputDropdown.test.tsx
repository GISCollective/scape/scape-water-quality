import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react-native";
import { InputDropdown } from "./InputDropdown";

describe("InputDropdown", () => {
  it("renders an empty value when there is no value set", () => {
    const component = render(
      <InputDropdown testID="input" options={["a", "b"]} />,
    );

    expect(component.queryByTestId("input-option-list")).toBeNull();
    expect(component.queryByTestId("input-value")).toBeNull();
  });

  it("renders a string when the value is set as a string", () => {
    const component = render(
      <InputDropdown testID="input" value="some value" options={[]} />,
    );

    expect(component.queryByTestId("input-value")).toHaveTextContent(
      "some value",
    );
  });

  it("opens the options list when the change button is pressed", async () => {
    const component = render(
      <InputDropdown
        testID="input"
        value=""
        options={["option1", "option2"]}
      />,
    );

    await fireEvent.press(component.queryByTestId("input-change")!);

    expect(component.queryByTestId("input-option-list")).toBeDefined();

    await waitFor(() => component.queryAllByTestId("input-option").length != 0);

    const button1 = await component.findByTestId("input-option-0");
    const button2 = await component.findByTestId("input-option-1");

    expect(button1).toHaveTextContent("option1");
    expect(button2).toHaveTextContent("option2");
  });

  it("does not render a change button when there is only one option and it is selected", async () => {
    const component = render(
      <InputDropdown testID="input" value="option1" options={["option1"]} />,
    );

    expect(component.queryByTestId("input-change")).toBeNull();
  });

  it("triggers onChange when an option is selected", async () => {
    const onChange = jest.fn();
    const component = render(
      <InputDropdown
        testID="input"
        value=""
        options={["option1", "option2"]}
        onChange={onChange}
      />,
    );

    await fireEvent.press(component.queryByTestId("input-change")!);
    expect(component.queryByTestId("input-option-list")).toBeDefined();

    await waitFor(() => component.queryAllByTestId("input-option").length != 0);

    const button = await component.findByTestId("input-option-1");
    await fireEvent.press(button);

    expect(onChange).toHaveBeenCalledWith("option2");
  });

  it("renders the object name when an object is set as value", () => {
    const component = render(
      <InputDropdown
        testID="input"
        value={{ name: "some value" }}
        options={[]}
      />,
    );

    expect(component.queryByTestId("input-value")).toHaveTextContent(
      "some value",
    );
  });
});
