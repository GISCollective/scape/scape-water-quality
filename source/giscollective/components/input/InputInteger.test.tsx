import { render, fireEvent } from "@testing-library/react-native";
import React from "react";
import { InputInteger } from "./InputInteger";

describe("InputInteger", () => {
  it("triggers onChange when the input is filled", async () => {
    const change = jest.fn();

    const component = render(<InputInteger testID="input" onChange={change} />);

    const input = await component.findByTestId("input");
    fireEvent.changeText(input, "123");

    expect(change).toHaveBeenCalledWith(123);
    expect(input.props.value).toBe("123");
  });

  it("does not trigger onChange when the input is filled with an invalid value", async () => {
    const change = jest.fn();

    const component = render(<InputInteger testID="input" onChange={change} />);

    const input = await component.findByTestId("input");
    fireEvent.changeText(input, "abc");

    expect(change).toHaveBeenCalledTimes(0);
    expect(input.props.value).toBe("abc");
  });

  it("does not trigger onChange when the input is filled with a decimal value", async () => {
    const change = jest.fn();

    const component = render(<InputInteger testID="input" onChange={change} />);

    const input = await component.findByTestId("input");
    fireEvent.changeText(input, "12.3");

    expect(change).toHaveBeenCalledTimes(0);
    expect(input.props.value).toBe("12.3");
  });

  it("renders the value when set", async () => {
    const change = jest.fn();

    const component = render(
      <InputInteger testID="input" onChange={change} value="123" />,
    );

    const input = await component.findByTestId("input");

    expect(input.props.value).toBe("123");
  });
});
