import { render, fireEvent } from "@testing-library/react-native";
import React from "react";
import { InputShortText } from "./InputShortText";

describe("InputShortText", () => {
  it("triggers onChange when the input is filled", async () => {
    const change = jest.fn();

    const component = render(
      <InputShortText testID="input" onChange={change} />,
    );

    const input = await component.findByTestId("input");
    fireEvent.changeText(input, "some new value");

    expect(change).toHaveBeenCalledWith("some new value");
    expect(input.props.value).toBe("some new value");
  });

  it("renders the value when set", async () => {
    const change = jest.fn();

    const component = render(
      <InputShortText testID="input" onChange={change} value="some value" />,
    );

    const input = await component.findByTestId("input");

    expect(input.props.value).toBe("some value");
  });
});
