import { render, fireEvent, waitFor } from "@testing-library/react-native";
import React from "react";
import { InputSearchAddress } from "./InputSearchAddress";
import { Store } from "../../models/store";
import { GeocodingJson, GeocodingModel } from "../../models/Geocoding";

describe("InputSearchAddress", () => {
  let queryGeocodings: jest.Mock;

  beforeEach(() => {
    queryGeocodings = jest.fn();

    Store.instance = { queryGeocodings } as unknown as Store;
  });

  it("triggers onChange when the input is filled", async () => {
    const val: GeocodingJson = {
      _id: "",
      name: "",
      icons: [],
      score: 0,
      geometry: { type: "Point", coordinates: [1, 2] },
      license: {},
    };

    queryGeocodings.mockReturnValue([new GeocodingModel(val)]);
    const change = jest.fn();

    const component = render(
      <InputSearchAddress testID="input" onChange={change} />,
    );

    const input = await component.findByTestId("input-search");
    fireEvent.changeText(input, "some new value");

    await waitFor(() => expect(change).toHaveBeenCalled(), { timeout: 5000 });

    expect(change).toHaveBeenCalledWith(
      { coordinates: [1, 2], type: "Point" },
      {
        accuracy: 0,
        address: "",
        altitude: 0,
        altitudeAccuracy: 0,
        capturedUsingGPS: false,
        id: null,
        searchTerm: "",
        type: "",
      },
    );
    expect(input.props.value).toBe("some new value");
  });

  it("renders the value when set", async () => {
    const change = jest.fn();

    const component = render(
      <InputSearchAddress
        testID="input"
        searchTerm="some value"
        onChange={change}
      />,
    );

    const input = await component.findByTestId("input-search");

    expect(input.props.value).toBe("some value");
  });
});
