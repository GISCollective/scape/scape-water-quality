import React, { useState } from "react";
import { SoundModel } from "../../models/Sound";
import { InputButton } from "./InputButton";
import AudioRecorderPlayer from "react-native-audio-recorder-player";
import {
  ActivityIndicator,
  PermissionsAndroid,
  Platform,
  View,
  Text,
} from "react-native";
import { Theme } from "../../lib/theme";
import RNFS from "react-native-fs";
import { Sound } from "../view/Sound";
import { uuidv4 } from "../../lib/uuid";
import { Store } from "../../models/store";

export interface InputSoundsProps {
  testID?: string;
  onChange: (sounds: SoundModel[]) => void;
  value?: SoundModel[];
}

interface RecordingState {
  recordSecs: number;
  recordTime?: string;
}

export interface RecordingViewProps {
  recordingState: RecordingState;
}

export const RecordingView = (props: RecordingViewProps) => {
  const containerStyle = Theme.instance.toStyle(["mb-2", "mt-2"], {
    flexDirection: "row",
  });
  const loadingStyle = Theme.instance.toTextStyle(["ps-2", "me-2"]);
  const primaryColor = Theme.instance.toColor("danger");
  const labelStyle = Theme.instance.toTextStyle(["fw-bold"]);
  labelStyle.color = Theme.instance.toColor("danger");

  return (
    <View style={containerStyle}>
      <ActivityIndicator
        size={Theme.instance.textSize + Theme.instance.spacers[1]}
        style={loadingStyle}
        color={primaryColor}
      />
      <Text style={labelStyle}>{props.recordingState.recordTime}</Text>
    </View>
  );
};

export const InputSounds = (props: InputSoundsProps) => {
  const [audioRecorderPlayer] = useState(new AudioRecorderPlayer());
  const [recordingIndex, setRecordingIndex] = useState(0);
  const [recordingState, setRecordingState] = useState<RecordingState>({
    recordSecs: 0,
  });
  const [soundList, setSoundList] = useState<SoundModel[]>(props.value ?? []);

  const checkRights = async () => {
    if (Platform.OS === "android") {
      try {
        const grants = await PermissionsAndroid.requestMultiple([
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
        ]);

        console.log("write external storage", grants);

        if (
          grants["android.permission.WRITE_EXTERNAL_STORAGE"] ===
            PermissionsAndroid.RESULTS.GRANTED &&
          grants["android.permission.READ_EXTERNAL_STORAGE"] ===
            PermissionsAndroid.RESULTS.GRANTED &&
          grants["android.permission.RECORD_AUDIO"] ===
            PermissionsAndroid.RESULTS.GRANTED
        ) {
          console.log("Permissions granted");
        } else {
          console.log("All required permissions not granted");
          return;
        }
      } catch (err) {
        console.warn(err);
        return;
      }
    }
  };

  const onStartRecord = async () => {
    await checkRights();

    const result = await audioRecorderPlayer.startRecorder();
    audioRecorderPlayer.addRecordBackListener((e) => {
      setRecordingState({
        recordSecs: e.currentPosition,
        recordTime: audioRecorderPlayer.mmssss(Math.floor(e.currentPosition)),
      });
      return;
    });
  };

  const onStopRecord = async () => {
    const result = await audioRecorderPlayer.stopRecorder();
    audioRecorderPlayer.removeRecordBackListener();

    const content = await RNFS.readFile(result, "base64");
    const sound = `data:audio/mp4;base64,${content}`;
    const newSoundModel = new SoundModel({
      _id: `pending-${uuidv4()}`,
      name: `Recording ${recordingIndex + 1}`,
      visibility: { isPublic: false, team: "" },
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      sound,
    });

    Store.instance.cache.upsert(
      newSoundModel._id,
      "Sound",
      newSoundModel.toJSON(),
    );

    const newSoundList = [...soundList, newSoundModel];

    setSoundList(newSoundList);
    setRecordingIndex(recordingIndex + 1);
    setRecordingState({
      recordSecs: 0,
    });

    props.onChange(newSoundList);
  };

  const isRecording = !!recordingState.recordSecs;

  return (
    <>
      {soundList.map((sound, index) => (
        <Sound value={{ record: sound }} key={`sound-${index}`} />
      ))}

      {isRecording && <RecordingView recordingState={recordingState} />}

      {!isRecording && (
        <InputButton
          testID={`${props.testID}-start`}
          onPress={onStartRecord}
          classes={["btn-primary", "mb-2"]}
        >
          Record audio
        </InputButton>
      )}

      {isRecording && (
        <InputButton
          testID={`${props.testID}-stop`}
          onPress={onStopRecord}
          classes={["btn-danger", "mb-2"]}
        >
          Stop
        </InputButton>
      )}
    </>
  );
};
