import React, { useState } from "react";
import { KeyboardTypeOptions } from "react-native";
import { DatePickerModal } from "react-native-paper-dates";
import { DateTime } from "luxon";
import { InputButton } from "./InputButton";

interface InputDateProps {
  testID?: string;
  onChange: (newValue: string) => void;
  value?: string | Date;
  placeholder?: string;
  keyboardType?: KeyboardTypeOptions;
}

function toDateTime(
  value: string | Date | string | undefined,
): DateTime | null {
  let result: DateTime<true> | DateTime<false> | null = DateTime.now();

  if (value instanceof Date) {
    result = DateTime.fromJSDate(value);
  }

  if (typeof value == "string") {
    result = DateTime.fromISO(value);
  }

  if (!result.isValid) {
    result = null;
  }

  return result;
}

export function InputDate(props: InputDateProps) {
  const [sampleDateOpen, setSampleDateOpen] = useState(false);
  const [value, setValue] = useState<DateTime | null>(toDateTime(props.value));

  const onSampleDateDismiss = React.useCallback(() => {
    setSampleDateOpen(false);
  }, [setSampleDateOpen]);

  const onSampleDateChange = React.useCallback(
    (params) => {
      setSampleDateOpen(false);
      const newValue = toDateTime(params.date);
      setValue(newValue);

      props.onChange(newValue?.toISODate() ?? "");
    },
    [setSampleDateOpen, setValue],
  );

  const label = value
    ? value.toLocaleString(DateTime.DATE_FULL)
    : "Tap to select the date";

  return (
    <>
      <InputButton
        onPress={() => setSampleDateOpen(true)}
        testID={`${props.testID}-open`}
        classes={["btn-outline-primary"]}
        align="start"
      >
        {label}
      </InputButton>

      <DatePickerModal
        locale="en"
        visible={sampleDateOpen}
        mode="single"
        date={value?.toJSDate()}
        onConfirm={onSampleDateChange}
        onDismiss={onSampleDateDismiss}
      />
    </>
  );
}
