import ActionSheet, { ActionSheetRef } from "react-native-actions-sheet";
import React, { useEffect, useRef, useState } from "react";
import { View } from "react-native";
import { useIsMounted } from "usehooks-ts";
import { Dimensions } from "react-native";
import { IconModel } from "../../giscollective/models/Icon";
import { IconListPicker } from "./IconListPicker";
import { useTheme } from "../hooks/useTheme";

export interface IconSheetPickerProps {
  testID?: string;
  icons: IconModel[];
  value: IconModel[];
  onSelectedIcons: (value: IconModel[]) => void;
}

export const IconSheetPicker = (props: IconSheetPickerProps) => {
  const theme = useTheme();

  const actionSheetRef = useRef<ActionSheetRef>(null);
  const isMounted = useIsMounted();

  const [selectedIcons, setSelectedIcons] = useState(props.value);

  useEffect(() => {
    actionSheetRef.current?.show();
  }, [isMounted]);

  const triggerSelectedValue = () => {
    props.onSelectedIcons?.(selectedIcons);
  };

  const setup = () => {
    setSelectedIcons(props.value);
  };

  const windowHeight = Dimensions.get("window").height;

  return (
    <ActionSheet
      testIDs={{
        sheet: `${props.testID}-sheet`,
        modal: `${props.testID}-modal`,
        backdrop: `${props.testID}-backdrop`,
        root: `${props.testID}-root`,
      }}
      ref={actionSheetRef}
      closable={true}
      isModal={true}
      onClose={triggerSelectedValue}
      onBeforeShow={setup}
      useBottomSafeAreaPadding
    >
      <View
        style={{
          height: windowHeight * 0.75,
          backgroundColor: theme.toColor("gray-100"),
        }}
      >
        <IconListPicker
          icons={props.icons}
          value={selectedIcons}
          onSelectedIcons={(v) => {
            setSelectedIcons(v);
          }}
        />
      </View>
    </ActionSheet>
  );
};
