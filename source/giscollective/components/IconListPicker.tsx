import React, { useState } from "react";
import { ScrollView, Text, View, Pressable } from "react-native";
import { IconModel } from "../../giscollective/models/Icon";
import { RemoteImage } from "./RemoteImage";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faCheck, faXmark } from "@fortawesome/free-solid-svg-icons";
import { InputButton } from "../../giscollective/components/input/InputButton";
import { Theme } from "../lib/theme";
import { useTheme } from "../hooks/useTheme";

export interface IconPickerProps {
  testID?: string;
  icon: IconModel;
  showTopLine?: boolean;
  isSelected?: boolean;
  onTap?: (icon: IconModel, isSelected: boolean) => void;
  onRemove?: (icon: IconModel) => void;
}

export const IconPicker = (props: IconPickerProps) => {
  const theme = useTheme();
  const [state, setState] = useState("");
  const [removeState, setRemoveState] = useState("");

  const containerStyle = theme.toStyle(["p-4"], {
    height: 60,
    flex: 1,
    alignItems: "center",
    flexDirection: "row",
  });

  const selectionStyle = theme.toStyle(["me-4", "ms-2"], {});
  selectionStyle.backgroundColor = "transparent";
  selectionStyle.borderColor = "transparent";
  selectionStyle.shadowColor = "transparent";

  const green: string = String(theme.toColor("green"));
  const danger: string = String(theme.toColor("danger"));

  const removeIconColor = danger;

  const nameStyle = theme.toTextStyle(["ps-3"], { flexGrow: 1 });

  const handleSelection = () => {
    props.onTap?.(props.icon, !props.isSelected);
  };

  const handleTouch = () => {
    if (!props.onTap) {
      return;
    }

    setState("active");
  };

  if (props.showTopLine) {
    containerStyle.borderTopWidth = 1;
    containerStyle.borderTopColor = theme.toColor("gray-200");
  }

  if (state == "active") {
    containerStyle.backgroundColor = theme.toColor("gray-200");
  }

  return (
    <Pressable
      onPress={handleSelection}
      onTouchStart={() => handleTouch()}
      onTouchEnd={() => setState("")}
      testID={props.testID}
    >
      <View style={containerStyle}>
        {props.icon.image.value && (
          <RemoteImage
            width={60}
            height={60}
            url={`${props.icon.image.value}/png`}
            testID={`${props.testID}-image`}
            tintColor={theme.iconTint.tintColor}
          />
        )}
        <Text style={nameStyle} testID={`${props.testID}-name`}>
          {props.icon.localName}
        </Text>

        {props.isSelected && (
          <View testID={`${props.testID}-selection`}>
            <FontAwesomeIcon
              style={selectionStyle}
              icon={faCheck}
              size={25}
              color={green}
            />
          </View>
        )}
        {props.onRemove && (
          <InputButton
            testID={`${props.testID}-remove`}
            onPress={() => props.onRemove?.(props.icon)}
            onStateChange={setRemoveState}
            classes={["btn-outline-danger"]}
          >
            <FontAwesomeIcon icon={faXmark} size={25} color={removeIconColor} />
          </InputButton>
        )}
      </View>
    </Pressable>
  );
};

export interface IconListPicker {
  testID?: string;
  icons: IconModel[];
  value: IconModel[];
  onSelectedIcons: (value: IconModel[]) => void;
}

export const IconListPicker = (props: IconListPicker) => {
  const [selection, setSelection] = useState(props.value);

  const updateSelection = (icon: IconModel, isSelected: boolean) => {
    let newValue = [];

    if (isSelected) {
      newValue = [...selection, icon];
    } else {
      newValue = selection.filter((a) => a._id != icon._id);
    }

    setSelection(newValue);
    props.onSelectedIcons(newValue);
  };

  const containerStyle = Theme.instance.toStyle(["pb-5"]);
  const idList = selection.map((a) => a._id);

  return (
    <ScrollView>
      <View style={containerStyle}>
        {props.icons.map((a, i) => (
          <IconPicker
            icon={a}
            onTap={updateSelection}
            showTopLine={i != 0}
            isSelected={Boolean(idList.includes(a._id))}
            key={a._id}
          />
        ))}
      </View>
    </ScrollView>
  );
};
