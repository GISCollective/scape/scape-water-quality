import { useEffect, useState } from "react";
import { Platform, StyleProp, ViewStyle } from "react-native";
import WebView, { WebViewMessageEvent } from "react-native-webview";
import React from "react";
import useCurrentLocation from "../hooks/useCurrentLocation";
import { storeFetch, webviewFetch } from "../lib/embeddedFetch";

export interface MapViewProps {
  testID?: string;
  style?: StyleProp<ViewStyle>;
  value?: any;
  mode?:
    | "user-location"
    | "map-view"
    | "location-selector"
    | "geo-json"
    | "map-selectable";
  onSelectedLocation?: (point: number[]) => void;
  onSelectedFeatures?: (idList: string[]) => void;
}

export const MapView = (props: MapViewProps) => {
  const [webView, setWebView] = useState<WebView | null>(null);
  const location = useCurrentLocation();

  const doSetup = () => {
    webView?.injectJavaScript?.(`
      (function () {
        let setValueTimer = setInterval(() => {
          if(!window.isReady) {
            return;
          }

          console.log("is ready");
          window.hasEmbeddedStore = true;

          if(window.setPosition) {
            window.setPosition([parseFloat(${
              location.coords.longitude
            }), parseFloat(${location.coords.latitude})]);
          }

          if(window.setValue) {
            window.setValue('${JSON.stringify(props.value)?.replace(
              /\\'/g,
              "",
            )}');
          }

          clearInterval(setValueTimer);
        }, 100);

        let setConfigTimer = setInterval(() => {
          if(!window.setConfig) {
            return;
          }

          window.setConfig("apiUrl", "https://new.opengreenmap.org/api-v1");
          window.setConfig("route", "${props.mode || "map-view"}");

          clearInterval(setConfigTimer);
        }, 100);
      })();
    `);
  };

  useEffect(() => {
    webView?.injectJavaScript?.(`
      (function () {
        if(!window.setPosition) {
          return;
        }
        console.log("Update Location");

        window.setPosition([parseFloat(${location.coords.longitude}), parseFloat(${location.coords.latitude})]);
    })()`);
  }, [location.timestamp, webView]);

  useEffect(() => {
    webView?.injectJavaScript?.(`
      (function () {
        if(!window.setValue) {
          return;
        }
        console.log("Update Value", '${JSON.stringify(props.value)?.replace(
          /\\'/g,
          "",
        )}');

        window.setValue('${JSON.stringify(props.value)?.replace(/\\'/g, "")}');
    })()`);
  }, [props.value]);

  const resolveRequest = (index: number, message: string) => {
    const code = `
      (function () {
        const data = "${message}";

        setTimeout(() => {
          window.resolveRequest(${index}, atob(data));
        }, 1)
      })()`;

    webView?.injectJavaScript?.(code);
  };

  const rejectRequest = (index: number, message: string) =>
    webView?.injectJavaScript?.(`
    (function () {
      const data = "${message}";

      window.rejectRequest(${index}, atob(data));
    })()`);

  const handleMessage = (event: WebViewMessageEvent) => {
    const message = JSON.parse(event.nativeEvent.data);
    const messageIndex = message.messageIndex;

    if (message.name == "storeFetch") {
      const data = message.args[0];

      storeFetch(data.info, data.type, data.options)
        .then((response) => {
          resolveRequest(messageIndex, response);
        })
        .catch((err) => {
          rejectRequest(messageIndex, btoa(JSON.stringify(err)));
        });
    }

    if (message.name == "fetch") {
      const data = message.args[0];

      webviewFetch(data)
        .then((response) => {
          resolveRequest(messageIndex, response);
        })
        .catch((err) => {
          rejectRequest(messageIndex, btoa(JSON.stringify(err)));
        });
    }

    if (message.name == "selectedLocation") {
      props.onSelectedLocation?.(message.args[0]);
    }

    if (message.name == "selection") {
      props.onSelectedFeatures?.(message.args[0]);
    }
  };

  let source = {
    uri: "file:///android_asset/custom/index.html",
    baseUrl: "file:///android_asset/custom/",
  };

  if (Platform.OS === "ios") {
    source = {
      uri: "./index.html",
      baseUrl: ".",
    };
  }

  // source = {
  //   uri: "http://127.0.0.1:4100/",
  //   baseUrl: "http://127.0.0.1:4100/",
  // };

  return (
    <WebView
      testID={props.testID}
      originWhitelist={["*"]}
      source={source}
      javaScriptEnabled={true}
      scrollEnabled={false}
      bounces={false}
      cacheEnabled={true}
      cacheMode={"LOAD_CACHE_ELSE_NETWORK"}
      ref={setWebView}
      onLoadEnd={doSetup}
      webviewDebuggingEnabled={true}
      allowFileAccess={true}
      allowUniversalAccessFromFileURLs={true}
      allowFileAccessFromFileURLs={true}
      allowsBackForwardNavigationGestures={false}
      allowsLinkPreview={false}
      onMessage={handleMessage}
      mixedContentMode="always"
      style={props.style}
    />
  );
};
