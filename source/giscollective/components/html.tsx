import React from "react";
import { TextWithOptions } from "../../giscollective/components/view/TextWithOptions";
import { TextStyle, View } from "react-native";
import { useTheme } from "../hooks/useTheme";

interface HtmlProps {
  testID?: string;
  children?: string | string[];
  classes?: string[];
  style?: TextStyle;
  numberOfLines?: number;
}

export const H1 = (props: HtmlProps) => {
  const theme = useTheme();

  const textOptions = theme.toFontStyle(
    "h1",
    ["pb-2", ...(props.classes ?? [])],
    props.style,
  );

  return (
    <TextWithOptions style={textOptions} testID={props.testID}>
      {props.children}
    </TextWithOptions>
  );
};

export const H2 = (props: HtmlProps) => {
  const theme = useTheme();

  const textOptions = theme.toFontStyle(
    "h2",
    ["pb-2", ...(props.classes ?? [])],
    props.style,
  );

  return (
    <TextWithOptions style={textOptions} testID={props.testID}>
      {props.children}
    </TextWithOptions>
  );
};

export const H3 = (props: HtmlProps) => {
  const theme = useTheme();

  const textOptions = theme.toFontStyle(
    "h3",
    ["pb-2", ...(props.classes ?? [])],
    props.style,
  );

  return (
    <TextWithOptions style={textOptions} testID={props.testID}>
      {props.children}
    </TextWithOptions>
  );
};

export const H4 = (props: HtmlProps) => {
  const theme = useTheme();

  const textOptions = theme.toFontStyle(
    "h4",
    ["pb-2", ...(props.classes ?? [])],
    props.style,
  );

  return (
    <TextWithOptions style={textOptions} testID={props.testID}>
      {props.children}
    </TextWithOptions>
  );
};

export const H5 = (props: HtmlProps) => {
  const theme = useTheme();

  const textOptions = theme.toFontStyle(
    "h5",
    ["pb-2", ...(props.classes ?? [])],
    props.style,
  );

  return (
    <TextWithOptions style={textOptions} testID={props.testID}>
      {props.children}
    </TextWithOptions>
  );
};

export const H6 = (props: HtmlProps) => {
  const theme = useTheme();

  const textOptions = theme.toFontStyle(
    "h6",
    ["pb-2", ...(props.classes ?? [])],
    props.style,
  );

  return (
    <TextWithOptions style={textOptions} testID={props.testID}>
      {props.children}
    </TextWithOptions>
  );
};

interface HRProps extends HtmlProps {
  color?: string;
  height?: number;
}

export const HR = (props: HRProps) => {
  const theme = useTheme();

  const style = theme.toStyle(props.classes, {
    flexDirection: "row",
    alignItems: "center",
    width: "100%",
    borderBottomColor: theme.toColor(props.color || "gray-600"),
    borderBottomWidth: props.height || 1,
    ...(props.style ?? {}),
  });

  return <View style={style}></View>;
};

export const P = (props: HtmlProps) => {
  const theme = useTheme();

  const textOptions = theme.toFontStyle(
    "paragraph",
    ["pb-2", ...(props.classes ?? [])],
    props.style,
  );

  return (
    <TextWithOptions
      style={textOptions}
      testID={props.testID}
      numberOfLines={props.numberOfLines}
    >
      {props.children}
    </TextWithOptions>
  );
};
