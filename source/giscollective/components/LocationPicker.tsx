import ActionSheet, { ActionSheetRef } from "react-native-actions-sheet";
import React, { RefObject, useEffect, useRef, useState } from "react";
import { View } from "react-native";
import { useIsMounted } from "usehooks-ts";
import { MapView } from "./MapView";
import { Dimensions } from "react-native";
import { GeoJsonGeometry } from "../../giscollective/models/GeoJsonGeometry";

export interface LocationPickerProps {
  extent?: GeoJsonGeometry;
  center?: number[];
  testID?: string;
  initialType?: string;
  onSelectedLocation: (value: number[] | undefined) => void;
  onRefChange?: (ref: RefObject<ActionSheetRef>) => void;
}

export const LocationPicker = (props: LocationPickerProps) => {
  const actionSheetRef = useRef<ActionSheetRef>(null);
  const isMounted = useIsMounted();
  const [selectedValue, setSelectedValue] = useState<number[] | undefined>();

  useEffect(() => {
    if (props.initialType && props.initialType != "manual") {
      actionSheetRef.current?.show();
    }
  }, [isMounted]);

  useEffect(() => {
    props.onRefChange?.(actionSheetRef);
  }, [actionSheetRef]);

  const selectCenter = (value: number[]) => {
    setSelectedValue(value);

    setTimeout(() => {
      actionSheetRef.current?.hide();
    }, 1);
  };

  const triggerSelectedLocation = () => {
    props.onSelectedLocation?.(selectedValue);
  };

  const windowHeight = Dimensions.get("window").height;
  const value = { extent: props.extent, center: props.center };

  return (
    <ActionSheet
      testIDs={{
        sheet: `${props.testID}-sheet`,
        modal: `${props.testID}-modal`,
        backdrop: `${props.testID}-backdrop`,
        root: `${props.testID}-root`,
      }}
      ref={actionSheetRef}
      closable={true}
      isModal={true}
      onClose={triggerSelectedLocation}
    >
      <View style={{ height: windowHeight * 0.75 }}>
        <MapView
          value={value}
          testID="location-picker-map"
          mode="location-selector"
          onSelectedLocation={selectCenter}
        />
      </View>
    </ActionSheet>
  );
};
