import React, { FunctionComponent, useState } from "react";
import { Text, View } from "react-native";
import { useInterval } from "usehooks-ts";
import { useSensors } from "../hooks/useSensors";
import { InputButton } from "./input/InputButton";
import { Sensor, Measurement } from "../lib/sensor";
import { useTheme } from "../hooks/useTheme";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faEyeDropper } from "@fortawesome/free-solid-svg-icons";
import { metricUnits } from "./input/InputDecimal";

interface SensorReadProps {
  type: string;
  testID?: string;
  sensor?: Sensor;
  style?: any[];
  onSelect: (value: string) => void;
}

interface SensorValueLabelProps {
  measurement?: Measurement;
  style?: any[];
}

export const SensorValueLabel: FunctionComponent<SensorValueLabelProps> = ({
  measurement,
}) => {
  const theme = useTheme();

  const style = theme.toTextStyle(["mb-2", "mt-2", "ms-2"]);
  const tintColor = theme.toColor("black") as string;
  const containerStyle = theme.toStyle(["mt-3", "mb-3"], {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  });

  if (!measurement) {
    return null;
  }

  const value: number = measurement.value ?? NaN;

  if (isNaN(value)) {
    return null;
  }

  if (measurement.name == "Temperature") {
    const valueF = value * 1.8 + 32;

    return (
      <View style={containerStyle}>
        <FontAwesomeIcon icon={faEyeDropper} size={15} color={tintColor} />
        <Text style={style} testID="sensor-label">
          {measurement.name} {valueF.toFixed(2)}ºF/{value.toFixed(2)}
          {metricUnits["Temperature"]}
        </Text>
      </View>
    );
  }

  return (
    <View style={containerStyle}>
      <FontAwesomeIcon icon={faEyeDropper} size={15} color={tintColor} />
      <Text style={style} testID="sensor-label">
        {measurement.name} {value.toFixed(2)}
        {measurement.unit}
      </Text>
    </View>
  );
};

export const niceValue = (type: string, value?: number | null): string => {
  if (value === undefined || value === null || isNaN(value)) {
    return "0";
  }

  return value?.toFixed(2);
};

export const SensorRead: FunctionComponent<SensorReadProps> = ({
  type,
  onSelect,
  style,
  testID,
}) => {
  const sensors = useSensors(false);

  const getMeasurement = () =>
    sensors.enabled
      ?.filter((a) => a.lastMeasurements)
      ?.flatMap((sensor) =>
        sensor.lastMeasurements.map((measurement) => ({
          sensor,
          measurement,
        })),
      )
      ?.filter((a) => a.measurement.name)
      ?.filter(
        (a) => `${a.measurement.name}`.toLowerCase() == type?.toLowerCase(),
      )
      ?.find((a) => a);

  let [pair, setPair] = useState<Measurement | undefined>(getMeasurement());

  useInterval(() => {
    setPair(getMeasurement());
  }, 1000);

  if (pair === undefined) {
    return null;
  }

  const value = niceValue(pair.measurement.name, pair.measurement.value);

  const handleValueRead = () => {
    onSelect(value);
  };

  return (
    <View style={style}>
      <SensorValueLabel measurement={pair.measurement} />

      <InputButton
        classes={["btn-primary", "mb-4"]}
        testID={`${testID}-read-sensor`}
        onPress={handleValueRead}
      >
        Use sensor value
      </InputButton>
    </View>
  );
};
