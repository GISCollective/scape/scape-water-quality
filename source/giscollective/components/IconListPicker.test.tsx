import React from "react";
import { IconModel } from "../../giscollective/models/Icon";
import { fireEvent, render } from "@testing-library/react-native";
import { IconPicker } from "./IconListPicker";

jest.mock("./RemoteImage", () => {
  const { Text } = require("react-native");

  return {
    RemoteImage: (props) => (
      <>
        <Text testID={`${props.testID}-url`}>{props.url}</Text>
        <Text testID={`${props.testID}-width`}>{props.width}</Text>
        <Text testID={`${props.testID}-height`}>{props.height}</Text>
      </>
    ),
  };
});

export interface IconPickerProps {
  testID?: string;
  icon: IconModel;
  showTopLine?: boolean;
  onTap: (id: string, isSelected: boolean) => void;
}

describe("IconPicker", () => {
  it("renders an unselected icon name and and icon", async () => {
    const tap = jest.fn();
    const icon = new IconModel({
      _id: "1",
      name: "test name",
      localName: "test local name",
      image: {
        value: "https://example.com/image.jpg",
      },
      attributes: [],
      iconSet: "",
    });

    const component = render(
      <IconPicker testID="test" icon={icon} onTap={tap} />,
    );

    const name = await component.findByTestId("test-name");
    const url = await component.findByTestId("test-image-url");
    const selection = await component.queryByTestId("test-selection");

    expect(name).toHaveTextContent("test local name");
    expect(url).toHaveTextContent("https://example.com/image.jpg/png");
    expect(selection).toBeNull();
  });

  it("renders an selected icon name and and icon", async () => {
    const tap = jest.fn();
    const icon = new IconModel({
      _id: "1",
      name: "test name",
      localName: "test local name",
      image: {
        value: "https://example.com/image.jpg",
      },
      attributes: [],
      iconSet: "",
    });

    const component = render(
      <IconPicker testID="test" icon={icon} isSelected={true} onTap={tap} />,
    );

    const name = await component.findByTestId("test-name");
    const url = await component.findByTestId("test-image-url");
    const selection = component.queryByTestId("test-selection");

    expect(name).toHaveTextContent("test local name");
    expect(url).toHaveTextContent("https://example.com/image.jpg/png");
    expect(selection).not.toBeNull();
  });

  it("triggers on tap when the selected element is pressed", async () => {
    const tap = jest.fn();
    const icon = new IconModel({
      _id: "1",
      name: "test name",
      localName: "test local name",
      image: {
        value: "https://example.com/image.jpg",
      },
      attributes: [],
      iconSet: "",
    });

    const component = render(
      <IconPicker testID="test" icon={icon} isSelected={true} onTap={tap} />,
    );

    const button = await component.findByTestId("test");
    await fireEvent.press(button);

    expect(tap.mock.calls[0][0]._id).toBe("1");
    expect(tap.mock.calls[0][1]).toBe(false);
  });

  it("triggers on tap when the unselected element is pressed", async () => {
    const tap = jest.fn();
    const icon = new IconModel({
      _id: "1",
      name: "test name",
      localName: "test local name",
      image: {
        value: "https://example.com/image.jpg",
      },
      attributes: [],
      iconSet: "",
    });

    const component = render(
      <IconPicker testID="test" icon={icon} isSelected={false} onTap={tap} />,
    );

    const button = await component.findByTestId("test");
    await fireEvent.press(button);

    expect(tap.mock.calls[0][0]._id).toBe("1");
    expect(tap.mock.calls[0][1]).toBe(true);
  });

  it("triggers onRemove when the remove button is pressed", async () => {
    const remove = jest.fn();
    const icon = new IconModel({
      _id: "1",
      name: "test name",
      localName: "test local name",
      image: {
        value: "https://example.com/image.jpg",
      },
      attributes: [],
      iconSet: "",
    });

    const component = render(
      <IconPicker testID="test" icon={icon} onRemove={remove} />,
    );

    const button = await component.findByTestId("test-remove");
    await fireEvent.press(button);

    expect(remove.mock.calls[0][0]._id).toBe("1");
  });
});
