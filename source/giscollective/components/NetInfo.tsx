import React, { useEffect, useState } from "react";
import { useIsMounted } from "usehooks-ts";
import { refresh } from "@react-native-community/netinfo";
import { FetchPolicy } from "../lib/fetch";

export function NetInfo() {
  const [timer, setTimer] = useState<number | undefined>();

  const isMounted = useIsMounted();

  const loop = () => {
    refresh()
      .then((state) => {
        FetchPolicy.instance.netinfo = state;
        startLoop();
      })
      .catch((err) => {
        console.error(err);
        startLoop();
      });
  };

  const startLoop = () => {
    if (!isMounted()) {
      return;
    }

    const t = setTimeout(loop, 3000);
    setTimer(t as unknown as number);
  };

  useEffect(() => {
    startLoop();

    return () => clearTimeout(timer);
  }, [isMounted]);

  return <></>;
}
