import React, { Children } from "react";
import { ActivityIndicator, View } from "react-native";
import { TextWithOptions } from "../../giscollective/components/view/TextWithOptions";
import { Theme } from "../lib/theme";

interface LabelProps {
  testID?: string;
  isRequired?: boolean;
  isLoading?: boolean;
  classes?: string[];
  color?: string;
  children: string;
}

export const Label = (props: LabelProps) => {
  const containerStyle = Theme.instance.toTextStyle([
    "mb-1",
    ...(props.classes ?? []),
  ]);
  containerStyle.display = "flex";
  containerStyle.flexDirection = "row";

  const loadingStyle = Theme.instance.toTextStyle(["ps-2"]);
  loadingStyle.alignSelf = "flex-end";

  const primaryColor = Theme.instance.toColor("primary");
  const textOptions = Theme.instance.toFontStyle(
    "paragraph",
    ["pb-1", "text-dark", "fw-bold", "color"],
    { color: props.color },
  );

  return (
    <View style={containerStyle}>
      <TextWithOptions style={textOptions} testID={`${props.testID}-label`}>
        {props.children}
      </TextWithOptions>

      {props.isRequired && (
        <TextWithOptions
          style={textOptions}
          color="danger"
          classes={["ps-1"]}
          testID={`${props.testID}-required`}
        >
          *
        </TextWithOptions>
      )}

      {props.isLoading && (
        <ActivityIndicator
          size={Theme.instance.textSize + Theme.instance.spacers[1]}
          style={loadingStyle}
          color={primaryColor}
        />
      )}
    </View>
  );
};
