import React, { useEffect, useState } from "react";
import {
  DimensionValue,
  View,
  ViewStyle,
  Image,
  ColorValue,
} from "react-native";
import { SvgXml } from "react-native-svg";
import { useIsMounted } from "usehooks-ts";
import { genericBinaryFetch } from "../lib/fetch";

export interface RemoteImageProps {
  testID: string;
  url: string;
  width: DimensionValue;
  height: DimensionValue;
  tintColor?: ColorValue;
  style?: ViewStyle;
}

export const RemoteImage = (props: RemoteImageProps) => {
  const containerStyle = {
    ...(props.style ?? {}),
    width: props.width,
    height: props.height,
  };

  const [image, setImage] = useState<string>("");
  const [type, setType] = useState<string>("");
  const isMounted = useIsMounted();
  let width = 24;
  let height = 24;

  if (typeof props.width == "number") {
    width = props.width;
  }

  if (typeof props.height == "number") {
    height = props.height;
  }

  useEffect(() => {
    if (!isMounted()) {
      return;
    }

    genericBinaryFetch("GET", props.url, {}, undefined, 3)
      .then((result) => {
        setImage(result.data as string);
        setType(result.type);
      })
      .catch((err) => {
        console.error(err);
      });
  }, [isMounted, props.url]);

  if (!image) {
    return <View style={containerStyle}></View>;
  }

  return (
    <View style={containerStyle}>
      {type == "image/svg+xml" ? (
        <SvgXml xml={image} width="100%" height="100%" />
      ) : (
        <Image
          source={{ uri: `data:${type};base64,${image}` }}
          width={width}
          height={height}
          style={{ tintColor: props.tintColor }}
        />
      )}
    </View>
  );
};
