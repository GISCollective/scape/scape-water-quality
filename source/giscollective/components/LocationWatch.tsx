import {
  GeoJsonGeometry,
  Point,
} from "../../giscollective/models/GeoJsonGeometry";
import React, { useEffect } from "react";
import { useIsMounted } from "usehooks-ts";
import useCurrentLocation from "../hooks/useCurrentLocation";
import { PositionDetails, PositionDetailsGps } from "../lib/positionDetails";

export interface LocationWatchProps {
  onChange?: (position: GeoJsonGeometry, details: PositionDetails) => void;
}

export const LocationWatch = (props: LocationWatchProps) => {
  const location = useCurrentLocation();

  const triggerChange = () => {
    const details = new PositionDetailsGps();
    const position: Point = {
      type: "Point",
      coordinates: [location.coords.longitude, location.coords.latitude],
    };

    props.onChange?.(position, details);
  };

  useEffect(() => {
    triggerChange();
  }, [location.coords.longitude, location.coords.latitude]);

  const isMounted = useIsMounted();

  // simulate an api call and update state
  useEffect(() => {
    triggerChange();
  }, [isMounted]);

  return <></>;
};
