import ActionSheet, { ActionSheetRef } from "react-native-actions-sheet";
import React, { useEffect, useRef, useState } from "react";
import { View } from "react-native";
import { useIsMounted } from "usehooks-ts";
import { MapView } from "./MapView";
import { Dimensions } from "react-native";
import { GeoJsonGeometry } from "../../giscollective/models/GeoJsonGeometry";

export interface FeaturePickerProps {
  testID?: string;
  mapId: string;
  onSelect: (idList: string[]) => void;
  extent?: GeoJsonGeometry;
}

export const FeaturePicker = (props: FeaturePickerProps) => {
  const actionSheetRef = useRef<ActionSheetRef>(null);
  const isMounted = useIsMounted();

  useEffect(() => {
    actionSheetRef.current?.show();
  }, [isMounted]);

  const windowHeight = Dimensions.get("window").height;

  const value = { mapId: props.mapId, extent: props.extent };

  const handleSelectedFeatures = (featureList: string[]) => {
    if (featureList.length == 0) {
      return 0;
    }

    props.onSelect(featureList);
    actionSheetRef.current?.hide();
  };

  return (
    <ActionSheet
      testIDs={{
        sheet: `${props.testID}-sheet`,
        modal: `${props.testID}-modal`,
        backdrop: `${props.testID}-backdrop`,
        root: `${props.testID}-root`,
      }}
      ref={actionSheetRef}
      closable={true}
      isModal={true}
    >
      <View style={{ height: windowHeight * 0.75 }}>
        <MapView
          value={value}
          testID="location-picker-feature"
          mode="map-selectable"
          onSelectedFeatures={handleSelectedFeatures}
        />
      </View>
    </ActionSheet>
  );
};
