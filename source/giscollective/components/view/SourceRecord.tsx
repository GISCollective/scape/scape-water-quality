import React from "react";
import { AnyGISCollectiveModel, GISCollectiveModel } from "../../models/store";
import { Fetch } from "./Fetch";

interface SourceRecordProps {
  model: GISCollectiveModel;
  onLoad: (record: AnyGISCollectiveModel) => Promise<void>;
  value: {
    modelKey: string;
  };
}

export function SourceRecord(props: SourceRecordProps) {
  const handleLoadedData = (record: AnyGISCollectiveModel) => {
    return props.onLoad(record);
  };

  const loadData = async () => {
    const record = props.model[props.value.modelKey];

    return record;
  };

  return <Fetch fetch={loadData} onSuccess={handleLoadedData} />;
}
