import React from "react";
import { ActivityIndicator, View } from "react-native";
import { Theme } from "../../lib/theme";
import { useTheme } from "../../hooks/useTheme";

export interface LoadingProps {}

export function Loading(props: LoadingProps) {
  const theme = useTheme();
  const color = theme.toColor("primary");

  return (
    <View
      style={{
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
        flex: 1,
        justifyContent: "center",
        width: "100%",
      }}
    >
      <ActivityIndicator
        testID="loading-placeholder"
        animating={true}
        color={color}
      />
    </View>
  );
}
