import { render } from "@testing-library/react-native";
import React from "react";
import { TextWithOptions } from "./TextWithOptions";

describe("TextWithOptions", () => {
  it("renders the children", () => {
    const component = render(
      <TextWithOptions testID="test-text">Some text</TextWithOptions>,
    );

    const text = component.queryByTestId("test-text");

    expect(text).toHaveTextContent("Some text");
  });

  it("applies the styles for a paragraph", () => {
    const value = {
      color: "danger",
      classes: [
        "ff-anton",
        "lh-lg",
        "fw-bold",
        "text-center",
        "text-line-clamp-2",
        "pt-5",
        "pb-5",
        "text-size-17",
      ],
      minLines: { md: 0, lg: 0, sm: 2 },
    };

    const component = render(
      <TextWithOptions value={value} testID="test-text">
        Some text
      </TextWithOptions>,
    );

    const text = component.queryByTestId("test-text");

    expect(text).toHaveStyle({
      fontFamily: "anton",
      lineHeight: 28,
      fontWeight: "bold",
      textAlign: "center",
      fontSize: 19.833333333333336,
      paddingTop: 42,
      paddingBottom: 42,
      color: "#e50620",
    });
  });
});
