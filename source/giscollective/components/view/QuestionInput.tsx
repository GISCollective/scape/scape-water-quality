import React, { useState } from "react";
import { Question } from "../../models/Question";
import { InputBoolean } from "../input/InputBoolean";
import { InputShortText } from "../input/InputShortText";
import { Theme } from "../../lib/theme";
import { View } from "react-native";
import { InputLongText } from "../input/InputLongText";
import { InputDecimal } from "../input/InputDecimal";
import { InputGeoJson } from "../input/InputGeoJson";
import { InputOptions } from "../input/InputOptions";
import { InputOptionsOther } from "../input/InputOptionsOther";
import { InputPictures } from "../input/InputPictures";
import { InputSounds } from "../input/InputSounds";
import { GeoJsonGeometry } from "../../models/GeoJsonGeometry";
import { InputIconSelector } from "../input/InputIconSelector";
import { InputDate } from "../input/InputDate";
import { InputTime } from "../input/InputTime";
import { PictureModel } from "../../models/Picture";
import { SoundModel } from "../../models/Sound";
import { IconModel } from "../../models/Icon";
import { InputInteger } from "../input/InputInteger";
import { InputStreamDischarge } from "../input/InputStreamDischarge";
import { P } from "../html";
import { Label } from "../Label";
import { SensorRead } from "../SensorRead";

export interface QuestionInputProps {
  question: Question;
  testID?: string;
  mapId?: string;
  extent?: GeoJsonGeometry;
  values?: Record<string, any>;
  system?: "metric" | "imperial";
  onChange: (question: Question, value: any) => Promise<void>;
}

type AnyValue =
  | undefined
  | string
  | number
  | boolean
  | PictureModel[]
  | SoundModel[]
  | IconModel[];

export function QuestionInput(props: QuestionInputProps) {
  const mb3 = Theme.instance.toTextStyle(["mb-3"]);
  const key = props.question.name;
  const [value, setValue] = useState<AnyValue>(props?.values?.[key]);

  const triggerChange = (value: AnyValue) => {
    setValue(value);
    return props?.onChange(props?.question, value);
  };

  return (
    <View style={mb3}>
      <Label
        testID={`${props.testID}-question-text`}
        isRequired={props.question.isRequired}
      >
        {props.question.question}
      </Label>

      {props.question.help && (
        <P
          testID={`${props.testID}-help-text`}
          style={{ color: "info" }}
          classes={["mb-1"]}
        >
          {props.question.help}
        </P>
      )}

      {props.question.type == "boolean" && (
        <InputBoolean
          testID={`${props.testID}-boolean`}
          onChange={triggerChange}
          value={value}
        />
      )}

      {props.question.type == "boolean-required" && (
        <InputBoolean
          required={true}
          testID={`${props.testID}-boolean-required`}
          value={value}
          onChange={triggerChange}
        />
      )}

      {props.question.type == "short text" && (
        <InputShortText
          testID={`${props.testID}-short-text`}
          value={value}
          onChange={triggerChange}
        />
      )}

      {props.question.type == "email" && (
        <InputShortText
          testID={`${props.testID}-email`}
          value={value}
          keyboardType="email-address"
          onChange={triggerChange}
        />
      )}

      {props.question.type == "decimal" && (
        <>
          <InputDecimal
            testID={`${props.testID}-decimal`}
            value={isNaN(value as number) ? undefined : `${value}`}
            options={props.question.options}
            system={props.system ?? "metric"}
            onChange={triggerChange}
          />

          {props.question.options?.measurement && (
            <SensorRead
              testID={`${props.testID}-decimal-sensor`}
              type={props.question.options?.measurement}
              onSelect={(a) => triggerChange(parseFloat(a))}
            />
          )}
        </>
      )}

      {props.question.type == "integer" && (
        <>
          <InputInteger
            testID={`${props.testID}-integer`}
            value={value}
            onChange={triggerChange}
          />
        </>
      )}

      {props.question.type == "long text" && (
        <InputLongText
          testID={`${props.testID}-long-text`}
          value={value}
          onChange={triggerChange}
        />
      )}

      {props.question.type == "geo json" && (
        <InputGeoJson
          testID={`${props.testID}-geo-json`}
          mapId={props.mapId}
          extent={props.extent}
          value={value}
          details={props?.values?.["position details"]}
          onChange={triggerChange}
          options={props.question?.options}
        />
      )}

      {props.question.type == "options" && (
        <InputOptions
          testID={`${props.testID}-options`}
          onChange={triggerChange}
          value={value}
          options={props.question?.options}
        />
      )}

      {props.question.type == "options with other" && (
        <InputOptionsOther
          testID={`${props.testID}-options-other`}
          onChange={triggerChange}
          value={value}
          options={props.question?.options}
        />
      )}

      {props.question.type == "pictures" && (
        <InputPictures
          testID={`${props.testID}-pictures`}
          value={value}
          onChange={triggerChange}
        />
      )}

      {props.question.type == "sounds" && (
        <InputSounds onChange={triggerChange} value={props?.values?.[key]} />
      )}

      {props.question.type == "icons" && (
        <InputIconSelector
          value={value}
          icons={props.question.options ?? []}
          onChange={triggerChange}
          testID={`${props.testID}-icons`}
        />
      )}

      {props.question.type == "date" && (
        <InputDate
          onChange={triggerChange}
          value={value}
          testID={`${props.testID}-date`}
        />
      )}

      {props.question.type == "time" && (
        <InputTime
          onChange={triggerChange}
          value={value}
          testID={`${props.testID}-time`}
        />
      )}

      {props.question.type == "stream discharge" && (
        <InputStreamDischarge
          onChange={triggerChange}
          value={value}
          testID={`${props.testID}-stream-discharge`}
          system={props.system ?? "metric"}
        />
      )}
    </View>
  );
}
