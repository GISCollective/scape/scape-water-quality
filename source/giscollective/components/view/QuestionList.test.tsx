import React from "react";
import { QuestionList } from "./QuestionList";
import {
  render,
  fireEvent,
  waitFor,
  userEvent,
} from "@testing-library/react-native";
import { Store } from "../../models/store";
import { MemoryCache } from "../../lib/cache";
import { IconModel } from "../../models/Icon";
import { StaticAuthenticator } from "../../lib/authenticator";

const icon1Data = {
  _id: "1",
  name: "test name",
  localName: "test local name",
  image: {
    value: "https://example.com/image.jpg",
  },
  attributes: [],
  iconSet: "",
};

const icon2Data = {
  _id: "2",
  name: "test name",
  localName: "test local name",
  image: {
    value: "https://example.com/image.jpg",
  },
  attributes: [],
  iconSet: "",
};

const icon3Data = {
  _id: "3",
  name: "test name",
  localName: "test local name",
  image: {
    value: "https://example.com/image.jpg",
  },
  attributes: [],
  iconSet: "",
};

jest.mock("../RemoteImage", () => {
  const { Text } = require("react-native");

  return {
    RemoteImage: (props) => (
      <>
        <Text testID={`${props.testID}-url`}>{props.url}</Text>
        <Text testID={`${props.testID}-width`}>{props.width}</Text>
        <Text testID={`${props.testID}-height`}>{props.height}</Text>
      </>
    ),
  };
});

jest.mock("../IconSheetPicker", () => {
  const { Text } = require("react-native");
  const { IconModel } = require("../../models/Icon");
  const { useEffect } = require("react");

  return {
    IconSheetPicker: (props) => {
      useEffect(() => {
        props.onSelectedIcons?.([new IconModel(icon1Data)]);
      }, []);

      return (
        <Text testID={`${props.testID}`}>
          {props.value?.map?.((a) => a._id).join(" ")}
        </Text>
      );
    },
  };
});

jest.mock("../../hooks/useSensors", () => ({
  useSensors: () => ({
    enabled: [
      {
        lastMeasurements: [
          {
            id: 1,
            value: 25.5,
            name: "Temperature",
            unit: "C",
            isReliable: true,
          },
        ],
      },
    ],
  }),
}));

describe("QuestionList", () => {
  let cache;

  beforeAll(() => {
    cache = new MemoryCache();

    Store.instance = new Store(
      "",
      jest.fn(),
      cache,
      new StaticAuthenticator(""),
    );

    cache.upsert("1", "Icon", icon1Data);
    cache.upsert("2", "Icon", icon2Data);
    cache.upsert("3", "Icon", icon3Data);
  });

  describe("a short text answer", () => {
    const fields = [
      {
        help: "some help message",
        isRequired: true,
        isVisible: true,
        name: "some name",
        options: {},
        question: "question",
        type: "short text",
      },
    ];

    it("can be rendered", () => {
      const component = render(
        <QuestionList fields={fields} onChange={jest.fn()} />,
      );

      expect(
        component.queryByTestId("question-0-question-text-label"),
      ).toHaveTextContent("question");
      expect(component.queryByTestId("question-0-help-text")).toHaveTextContent(
        "some help message",
      );
      expect(
        component.queryByTestId("question-0-question-text-required"),
      ).toHaveTextContent("*");
    });

    it("can fill in a value", async () => {
      const change = jest.fn();
      const component = render(
        <QuestionList fields={fields} onChange={change} />,
      );

      const input = await component.findByTestId("question-0-short-text");
      fireEvent.changeText(input, "some new value");

      expect(change).toHaveBeenCalledWith({
        "some name": "some new value",
      });
    });
  });

  it("can fill in a long text answer", async () => {
    const fields = [
      {
        help: "some help message",
        isRequired: true,
        isVisible: true,
        name: "some name",
        options: {},
        question: "question",
        type: "long text",
      },
    ];

    const change = jest.fn();
    const component = render(
      <QuestionList fields={fields} onChange={change} />,
    );

    const input = await component.findByTestId("question-0-long-text");
    fireEvent.changeText(input, "some new value");

    expect(change).toHaveBeenCalledWith({
      "some name": "some new value",
    });
  });

  it("can fill in a boolean value", async () => {
    const fields = [
      {
        help: "some help message",
        isRequired: true,
        isVisible: true,
        name: "some name",
        options: {},
        question: "question",
        type: "boolean",
      },
    ];

    const change = jest.fn();
    const component = render(
      <QuestionList fields={fields} onChange={change} />,
    );

    const input = await component.findByTestId("question-0-boolean-false");
    fireEvent.press(input);

    expect(change).toHaveBeenCalledWith({
      "some name": false,
    });
  });

  it("can fill in a boolean-required value", async () => {
    const fields = [
      {
        help: "some help message",
        isRequired: true,
        isVisible: true,
        name: "some name",
        options: {},
        question: "question",
        type: "boolean-required",
      },
    ];

    const change = jest.fn();
    const component = render(
      <QuestionList fields={fields} onChange={change} />,
    );

    const input = await component.findByTestId(
      "question-0-boolean-required-false",
    );
    fireEvent.press(input);
    expect(
      component.queryAllByTestId("question-0-boolean-required-undefined"),
    ).toHaveLength(0);

    expect(change).toHaveBeenCalledWith({
      "some name": false,
    });
  });

  it("can fill in a geo json value", async () => {
    const fields = [
      {
        help: "some help message",
        isRequired: true,
        isVisible: true,
        name: "some name",
        options: {
          allowGps: true,
        },
        question: "question",
        type: "geo json",
      },
    ];

    const change = jest.fn();
    const component = render(
      <QuestionList fields={fields} onChange={change} />,
    );

    await waitFor(() =>
      expect(component.queryByTestId("question-0-geo-json-gps")).toBeTruthy(),
    );

    const input = await component.findByTestId("question-0-geo-json-gps");
    fireEvent.press(input);

    await waitFor(() => {
      expect(change).toHaveBeenCalledWith({
        "some name": {
          coordinates: [0, 0],
          type: "Point",
        },
      });
    });
  });

  it("can fill in a options value", async () => {
    const fields = [
      {
        help: "some help message",
        isRequired: true,
        isVisible: true,
        name: "some name",
        options: "a,b,c",
        question: "question",
        type: "options",
      },
    ];

    const change = jest.fn();
    const component = render(
      <QuestionList fields={fields} onChange={change} />,
    );

    await fireEvent.press(
      component.queryByTestId("question-0-options-change")!,
    );
    expect(
      component.queryByTestId("question-0-options-option-list"),
    ).toBeDefined();

    const button = await component.findByTestId("question-0-options-option-0");

    await fireEvent.press(button);

    await waitFor(() => {
      expect(change).toHaveBeenCalledWith({
        "some name": "a",
      });
    });
  });

  it("can fill in a decimal value", async () => {
    const fields = [
      {
        help: "some help message",
        isRequired: true,
        isVisible: true,
        name: "some name",
        options: "a,b,c",
        question: "question",
        type: "decimal",
      },
    ];

    const change = jest.fn();
    const component = render(
      <QuestionList fields={fields} onChange={change} />,
    );

    const input = await component.findByTestId("question-0-decimal");
    fireEvent.changeText(input, "34.5");

    await waitFor(() => {
      expect(change).toHaveBeenCalledWith({
        "some name": 34.5,
      });
    });
  });

  it("can read a measurement", async () => {
    const fields = [
      {
        help: "some help message",
        isRequired: true,
        isVisible: true,
        name: "some name",
        question: "question",
        type: "decimal",
        options: { measurement: "Temperature" },
      },
    ];

    const change = jest.fn();
    const component = render(
      <QuestionList fields={fields} onChange={change} />,
    );

    const button = await component.findByTestId(
      "question-0-decimal-sensor-read-sensor",
    );
    await userEvent.press(button);

    await waitFor(() => {
      expect(change).toHaveBeenCalledWith({
        "some name": 25.5,
      });
    });
  });

  it("can select a value to options with other value", async () => {
    const fields = [
      {
        help: "some help message",
        isRequired: true,
        isVisible: true,
        name: "some name",
        options: "a,b,c",
        question: "question",
        type: "options with other",
      },
    ];

    const change = jest.fn();
    const component = render(
      <QuestionList fields={fields} onChange={change} />,
    );

    await fireEvent.press(
      component.queryByTestId("question-0-options-other-change")!,
    );
    expect(
      component.queryByTestId("question-0-options-other-option-list"),
    ).toBeDefined();

    const button = await component.findByTestId(
      "question-0-options-other-option-3",
    );
    await fireEvent.press(button);

    const input = await component.findByTestId(
      "question-0-options-other-input",
    );
    fireEvent.changeText(input, "some new value");

    await waitFor(() => {
      expect(change).toHaveBeenCalledWith({
        "some name": "some new value",
      });
    });
  });

  it("can fill in a custom value to options with other value", async () => {
    const fields = [
      {
        help: "some help message",
        isRequired: true,
        isVisible: true,
        name: "some name",
        options: "a,b,c",
        question: "question",
        type: "options with other",
      },
    ];

    const change = jest.fn();
    const component = render(
      <QuestionList fields={fields} onChange={change} />,
    );

    await fireEvent.press(
      component.queryByTestId("question-0-options-other-change")!,
    );
    expect(
      component.queryByTestId("question-0-options-other-option-list"),
    ).toBeDefined();

    const button = await component.findByTestId(
      "question-0-options-other-option-0",
    );

    await fireEvent.press(button);

    await waitFor(() => {
      expect(change).toHaveBeenCalledWith({
        "some name": "a",
      });
    });
  });

  it("can select an icon", async () => {
    const fields = [
      {
        help: "",
        isVisible: true,
        name: "icons",
        question: "select an icon",
        isRequired: false,
        type: "icons",
        options: ["1", "2"],
      },
    ];

    const change = jest.fn();
    const component = render(
      <QuestionList fields={fields} onChange={change} />,
    );

    await waitFor(() => {
      expect(
        component.queryAllByTestId("question-0-icons-select-icons"),
      ).toHaveLength(1);
    });

    const selectBtn = await component.findByTestId(
      "question-0-icons-select-icons",
    );
    fireEvent.press(selectBtn);

    await waitFor(() => {
      expect(change).toHaveBeenCalledTimes(1);
    });

    expect(change).toHaveBeenCalledWith({ icons: [new IconModel(icon1Data)] });
  });

  it("renders the current values on first render", async () => {
    const fields = [
      {
        help: "",
        isVisible: true,
        name: "name",
        question: "name",
        isRequired: false,
        type: "short text",
        options: "",
      },
    ];

    const values: Record<string, unknown> = {
      name: "text value",
    };

    const component = render(
      <QuestionList fields={fields} value={values} onChange={jest.fn()} />,
    );

    const nameInput = await component.findByTestId("question-0-short-text");

    expect(nameInput.props.value).toEqual("text value");
  });
});
