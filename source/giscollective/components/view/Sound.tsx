import React, { useState } from "react";
import { GISCollectiveModel } from "../../models/store";
import { SoundModel } from "../../models/Sound";
import { View, Text } from "react-native";
import { Theme } from "../../lib/theme";
import RNFS from "react-native-fs";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faPlay, faStop } from "@fortawesome/free-solid-svg-icons";
import { InputButton } from "../input/InputButton";
import { useInterval } from "usehooks-ts";
import { AudioService } from "../../lib/audioService";

function makeid(length: number) {
  let result = "";
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  const charactersLength = characters.length;
  let counter = 0;
  while (counter < length) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
    counter += 1;
  }
  return result;
}

export interface SoundData {}

interface SoundModelProps {
  value: { record: SoundModel };
}

interface SoundProps {
  model: GISCollectiveModel;
  value: {
    modelKey: string;
    data: SoundData;
  };
}

export interface SoundViewProps {
  record: SoundModel;
  file: string;
}

export interface SoundLoaderProps {
  record: SoundModel;
  onLoad: (file: string) => void;
}

export function SoundView(props: SoundViewProps) {
  const [playTime, setPlayTime] = useState("");
  const [duration, setDuration] = useState("");
  const [isPlaying, setIsPlaying] = useState(false);
  const [file, setFile] = useState("");

  useInterval(async () => {
    const e = AudioService.instance.playBackState;
    setIsPlaying(e.isPlaying && file != "" && e.file == file);

    if (!isPlaying) {
      return;
    }

    setPlayTime(e.playTime);
    setDuration(e.duration);
  }, 500);

  const labelStyle = Theme.instance.toTextStyle(["fw-bold"]);
  labelStyle.color = Theme.instance.toColor("gray-900");
  const labelContainerStyle = Theme.instance.toStyle(["mt-1", "mb-1"], {
    justifyContent: "center",
    flex: 1,
  });

  const startPlay = () => {
    setFile("file://" + props.file);
    AudioService.instance.play("file://" + props.file);
  };

  const stopPlay = () => {
    setIsPlaying(false);
    AudioService.instance.stop();
  };

  const hasPlaybackInfo = isPlaying && duration != "";

  return (
    <View style={{ flexDirection: "row", flex: 1 }}>
      {!isPlaying && (
        <InputButton
          testID="btn-take-photo"
          onPress={startPlay}
          classes={["btn-primary", "me-2"]}
        >
          <FontAwesomeIcon icon={faPlay} size={25} color={"white"} />
        </InputButton>
      )}

      {isPlaying && (
        <InputButton
          testID="btn-take-photo"
          onPress={stopPlay}
          classes={["btn-primary", "me-2"]}
        >
          <FontAwesomeIcon icon={faStop} size={25} color={"white"} />
        </InputButton>
      )}

      <View style={labelContainerStyle}>
        <Text style={labelStyle}>{props.record.name}</Text>

        {hasPlaybackInfo && (
          <Text>
            {playTime}/{duration}
          </Text>
        )}
      </View>
    </View>
  );
}

export function SoundLoader(props: SoundLoaderProps) {
  var path = `${RNFS.DocumentDirectoryPath}/${makeid(12)}.mp4`;

  const index = props.record.sound.indexOf(";base64,");
  const content = props.record.sound.slice(index + ";base64,".length);

  RNFS.writeFile(path, content, "base64")
    .then((success) => {
      props.onLoad(path);
    })
    .catch((err) => {
      console.log(err.message);
    });

  return <></>;
}

export function Sound(props: SoundProps | SoundModelProps) {
  const [file, setFile] = useState<string>();
  const containerStyle = Theme.instance.toStyle(["mb-2", "mt-2"], {
    flexDirection: "row",
  });

  let record: SoundModel | undefined;

  if ("record" in props.value) {
    record = props.value.record;
  }

  if (record && !file) {
    return <SoundLoader record={record} onLoad={setFile} />;
  }

  if (!record) {
    return <></>;
  }

  return (
    <View style={containerStyle}>
      <SoundView record={record} file={file ?? ""} />
    </View>
  );
}
