import React from "react";
import { Text, TextStyle } from "react-native";
import { Theme } from "../../lib/theme";

export interface TextWithOptionsValue {
  color?: string;
  classes?: string[];
  minLines?: { md: number; lg: number; sm: number };
}

export interface TextWithOptionsProps {
  children: string | React.ReactNode;
  testID?: string;
  color?: string;
  value?: TextWithOptionsValue;
  style?: TextStyle;
  classes?: string[];
  numberOfLines?: number;
}

export function TextWithOptions(props: TextWithOptionsProps) {
  const style = Theme.instance.toTextStyle(
    props.value?.classes || props?.classes,
    props.style,
  );

  if (props.value?.color || props.color) {
    style.color = Theme.instance.toColor(props.value?.color || props.color);
  }

  return (
    <Text
      style={style}
      testID={props.testID}
      numberOfLines={props.numberOfLines}
    >
      {props.children}
    </Text>
  );
}
