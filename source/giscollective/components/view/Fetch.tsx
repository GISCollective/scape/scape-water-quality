import { useEffect, useState } from "react";
import { FetchStatus } from "./FetchStatus";
import React from "react";
import { useNetInfo } from "@react-native-community/netinfo";

export interface FetchProps<T> {
  fetch: () => Promise<T>;
  onSuccess: (result: T) => void;
  fakeLoading?: boolean;
}

export function Fetch<T>(props: FetchProps<T>) {
  const netInfo = useNetInfo();
  const [error, setError] = useState<string | undefined>();
  const [isReady, setIsReady] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  const handleError = (message: string) => {
    setTimeout(() => {
      setError(message);
      setIsLoading(false);
    }, 1000);
  };

  const runFetch = async () => {
    if (props.fakeLoading) {
      return;
    }

    let result: T | undefined;
    setIsLoading(true);
    setError(undefined);

    try {
      result = await props.fetch();
    } catch (err) {
      console.error(err);
      return handleError(`${err}`);
    }

    if (!result) {
      return handleError(``);
    }

    try {
      await props.onSuccess(result);
    } catch (err) {
      console.error(err);
      return handleError(`${err}`);
    }

    setIsReady(true);
    setIsLoading(false);
  };

  useEffect(() => {
    runFetch();
  }, []);

  if (isReady) {
    return null;
  }

  return (
    <FetchStatus
      isLoading={isLoading}
      error={error}
      isConnected={netInfo.isInternetReachable}
      onTryAgain={runFetch}
    />
  );
}
