import { fireEvent, render } from "@testing-library/react-native";
import React from "react";
import { ErrorMessage } from "./FetchStatus";
import { act } from "react-test-renderer";

describe("ErrorMessage", () => {
  it("shows a generic message when there is no message", () => {
    const component = render(
      <ErrorMessage message="" onTryAgain={jest.fn()} isConnected={true} />,
    );

    expect(component.queryByTestId("error-message")).toHaveTextContent(
      "There was an error fetching your data.\nPlease try again later.",
    );
  });

  it("shows a message when set", () => {
    const component = render(
      <ErrorMessage
        message="Some message"
        onTryAgain={jest.fn()}
        isConnected={true}
      />,
    );

    expect(component.queryByTestId("error-message")).toHaveTextContent(
      "Some message",
    );
  });

  it("removes 'Error:' from the provided message", () => {
    const component = render(
      <ErrorMessage
        message="Error: Some message"
        onTryAgain={jest.fn()}
        isConnected={true}
      />,
    );

    expect(component.queryByTestId("error-message")).toHaveTextContent(
      "Some message",
    );
  });

  it("shows a disconnected message when the user is not connected to the internet", () => {
    const component = render(
      <ErrorMessage
        message="Error: Some message"
        onTryAgain={jest.fn()}
        isConnected={false}
      />,
    );

    expect(component.queryByTestId("error-message")).toHaveTextContent(
      "You are offline.\nConnect to the Internet and try again.",
    );
  });

  it("raises an event when the button is pressed", async () => {
    const func = jest.fn();

    const component = render(
      <ErrorMessage
        message="Error: Some message"
        onTryAgain={func}
        isConnected={true}
      />,
    );

    await fireEvent.press(component.queryByTestId("btn-try-again")!);

    expect(func).toHaveBeenCalledTimes(1);
  });
});
