import { Loading } from "./Loading";
import React from "react";
import { View, ViewStyle } from "react-native";
import { useTheme } from "../../hooks/useTheme";
import { P } from "../html";
import { InputButton } from "../input/InputButton";
import {
  faWifi,
  faTriangleExclamation,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";

export interface FetchStatusProps {
  error?: string;
  style?: ViewStyle;
  onTryAgain: () => void;
  isConnected?: boolean | null;
  isLoading: boolean;
}

export interface ErrorMessageProps {
  message: string | undefined;
  onTryAgain: () => void;
  isConnected?: boolean | null;
}

export function ErrorMessage(props: ErrorMessageProps) {
  const theme = useTheme();

  let icon = faTriangleExclamation;
  let message =
    props.message ||
    "There was an error fetching your data.\nPlease try again later.";
  message = message.replace("Error:", "").trim();

  if (!props.isConnected) {
    message = "You are offline.\nConnect to the Internet and try again.";
    icon = faWifi;
  }

  return (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        alignSelf: "center",
        width: "100%",
      }}
    >
      <FontAwesomeIcon
        icon={icon}
        size={50}
        style={theme.toStyle(["mb-3"], { alignSelf: "center" })}
        color={theme.toColor("gray-400") as string}
      />

      <P
        style={theme.toTextStyle(["fw-bold", "mb-3", "text-center"], {
          color: "red-900-rgb",
        })}
        testID="error-message"
      >
        {message}
      </P>
      <InputButton
        classes={["btn-primary", "me-5", "ms-5"]}
        testID="btn-try-again"
        onPress={props.onTryAgain}
      >
        Try again
      </InputButton>
    </View>
  );
}

export function FetchStatus(props: FetchStatusProps) {
  return (
    <View
      style={{
        display: "flex",
        flexWrap: "wrap",
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
      }}
    >
      {!props.isLoading && (
        <ErrorMessage
          message={props.error}
          isConnected={props.isConnected}
          onTryAgain={props.onTryAgain}
        />
      )}
      {props.isLoading && <Loading />}
    </View>
  );
}
