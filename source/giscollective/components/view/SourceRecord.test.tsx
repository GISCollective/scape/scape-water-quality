jest.mock("@react-native-community/netinfo", () => {
  return {
    useNetInfo: () => ({}),
  };
});

import { render, waitFor } from "@testing-library/react-native";
import { SourceRecord } from "./SourceRecord";
import React from "react";
import { CampaignModel } from "../../models/Campaign";

describe("SourceRecord", () => {
  it("does nothing when there is no model", () => {
    const loaded = jest.fn();
    const model = {};

    render(
      <SourceRecord
        model={model}
        onLoad={loaded}
        value={{
          modelKey: "",
        }}
      />,
    );

    expect(loaded).not.toHaveBeenCalled();
  });

  it("calls the loaded event when there is a model", async () => {
    const loaded = jest.fn();
    const model = {
      "some-key": new CampaignModel(null),
    };

    render(
      <SourceRecord
        model={model}
        onLoad={loaded}
        value={{
          modelKey: "some-key",
        }}
      />,
    );

    await waitFor(() => expect(loaded).toHaveBeenCalled());

    expect(loaded).toHaveBeenCalledWith(model["some-key"]);
  });
});
