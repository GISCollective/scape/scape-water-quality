import { render, fireEvent, waitFor } from "@testing-library/react-native";
import React from "react";
import { QuestionIconList } from "./QuestionIconList";
import { IconModel } from "../../models/Icon";

describe("QuestionIconList", () => {
  it("does not render an icon group when there are no icons", () => {
    const component = render(
      <QuestionIconList
        testID="test"
        onChange={jest.fn()}
        value={{}}
        icons={[]}
      />,
    );

    expect(component.queryByTestId("test-group-0")).toBeNull();
  });

  it("renders one group when there is an icon with attributes", () => {
    let icon = new IconModel({
      _id: "1",
      name: "2",
      image: {},
      attributes: [
        {
          name: "test",
          type: "short text",
          displayName: "display name",
          help: "help message",
        },
      ],
      iconSet: "3",
    });
    let icons = [icon];

    const component = render(
      <QuestionIconList
        testID="test"
        onChange={jest.fn()}
        value={{}}
        icons={icons}
      />,
    );

    expect(component.queryByTestId("test-group-0")).not.toBeNull();
    expect(
      component.queryByTestId("test-group-0-question-0-short-text"),
    ).not.toBeNull();
  });

  it("renders nothing when there is an icon without attributes", () => {
    let icon = new IconModel({
      _id: "1",
      name: "2",
      image: {},
      attributes: [],
      iconSet: "3",
    });
    let icons = [icon];

    const component = render(
      <QuestionIconList
        testID="test"
        onChange={jest.fn()}
        value={{}}
        icons={icons}
      />,
    );

    expect(component.queryByTestId("test-group-0")).toBeNull();
  });

  describe("when there is an icon with attributes", () => {
    let icon: IconModel;
    let icons: IconModel[];

    beforeEach(() => {
      icon = new IconModel({
        _id: "1",
        name: "name",
        localName: "local name",
        image: {},
        attributes: [
          {
            name: "test",
            type: "short text",
            displayName: "display name",
            help: "help message",
          },
        ],
        iconSet: "3",
      });
      icons = [icon];
    });

    it("renders the input group", () => {
      const component = render(
        <QuestionIconList
          testID="test"
          onChange={jest.fn()}
          value={{}}
          icons={icons}
        />,
      );

      expect(component.queryByTestId("test-group-0")).not.toBeNull();
      expect(
        component.queryByTestId("test-group-0-question-0-short-text"),
      ).not.toBeNull();
    });

    it("renders the group title", () => {
      const component = render(
        <QuestionIconList
          testID="test"
          onChange={jest.fn()}
          value={{}}
          icons={icons}
        />,
      );

      expect(
        component.queryByTestId("test-group-0-title-text"),
      ).toHaveTextContent("local name");
    });

    it("triggers onChange when a value is changed", async () => {
      const changed = jest.fn();
      const component = render(
        <QuestionIconList
          testID="test"
          onChange={changed}
          value={{}}
          icons={icons}
        />,
      );

      const textInput = await component.findByTestId(
        "test-group-0-question-0-short-text",
      );
      fireEvent.changeText(textInput, "some new value");

      expect(changed).toHaveBeenCalledWith({
        name: { test: "some new value" },
      });
    });
  });

  describe("when there is an icon with attributes and allow many = true", () => {
    let icon: IconModel;
    let icons: IconModel[];

    beforeEach(() => {
      icon = new IconModel({
        _id: "1",
        name: "name",
        localName: "local name",
        image: {},
        attributes: [
          {
            name: "test",
            type: "short text",
            displayName: "display name",
            help: "help message",
          },
        ],
        iconSet: "3",
        allowMany: true,
      });
      icons = [icon];
    });

    it("renders the input group", () => {
      const component = render(
        <QuestionIconList
          testID="test"
          onChange={jest.fn()}
          value={{}}
          icons={icons}
        />,
      );

      expect(component.queryByTestId("test-group-0")).not.toBeNull();
      expect(
        component.queryByTestId("test-group-0-0-title-text"),
      ).toHaveTextContent("1. local name");
      expect(
        component.queryByTestId("test-group-0-0-question-0-short-text"),
      ).not.toBeNull();
    });

    it("triggers onChange when a value is changed", async () => {
      const changed = jest.fn();
      const component = render(
        <QuestionIconList
          testID="test"
          onChange={changed}
          value={{}}
          icons={icons}
        />,
      );

      const textInput = await component.findByTestId(
        "test-group-0-0-question-0-short-text",
      );
      fireEvent.changeText(textInput, "some new value");

      expect(changed).toHaveBeenCalledWith({
        name: [{ test: "some new value" }],
      });
    });

    it("can add and set a new set of values", async () => {
      const changed = jest.fn();
      const component = render(
        <QuestionIconList
          testID="test"
          onChange={changed}
          value={{}}
          icons={icons}
        />,
      );

      const btnAdd = await component.findByTestId("test-add");
      await fireEvent.press(btnAdd);

      await waitFor(() =>
        expect(
          component.queryAllByTestId("test-group-0-1-question-0-short-text"),
        ).toHaveLength(1),
      );

      const textInput = await component.findByTestId(
        "test-group-0-1-question-0-short-text",
      );
      fireEvent.changeText(textInput, "some new value");

      expect(changed).toHaveBeenCalledWith({
        name: [{}, { test: "some new value" }],
      });
    });

    it("can remove a set of values", async () => {
      const changed = jest.fn();
      const component = render(
        <QuestionIconList
          testID="test"
          onChange={changed}
          value={{ name: [{ test: "1" }, { test: "2" }] }}
          icons={icons}
        />,
      );

      const btnRemove = await component.findByTestId(
        "test-group-0-1-title-remove",
      );
      await fireEvent.press(btnRemove);

      expect(changed).toHaveBeenCalledWith({ name: [{ test: "1" }] });
    });
  });
});
