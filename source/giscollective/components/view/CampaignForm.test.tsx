jest.mock("@react-native-community/netinfo", () => {
  return {
    useNetInfo: () => ({}),
  };
});

jest.mock("../../lib/uuid", () => ({ uuidv4: () => "123456789" }));

import {
  CampaignAnswerJson,
  CampaignAnswerModel,
} from "../../models/CampaignAnswer";
import {
  CampaignForm,
  CampaignFormProps,
  extractGenericAnswers,
  fillAnswer,
} from "./CampaignForm";
import { CampaignModel } from "../../models/Campaign";
import { GISCollectiveModel, Store } from "../../models/store";
import { PictureModel } from "../../models/Picture";
import {
  fireEvent,
  render,
  waitFor,
  act,
  userEvent,
} from "@testing-library/react-native";
import React from "react";
import { MemoryCache } from "../../lib/cache";
import { IconModel } from "../../models/Icon";
import * as submitSurveyAnswerAPI from "../../lib/submitSurveyAnswer";
import { StaticAuthenticator } from "../../lib/authenticator";
import { LocalStorage, LocalStorageMemory } from "../../lib/localStorage";

jest.mock("../input/InputPictures", () => {
  const { Button } = require("react-native");
  const { PictureModel } = require("../../models/Picture");

  return {
    InputPictures: (props) => {
      const change = () => {
        const value = [
          new PictureModel({ _id: "pending-1", name: "", picture: "1" }),
          new PictureModel({ _id: "pending-2", name: "", picture: "2" }),
        ];

        props.onChange(value);
      };

      return (
        <>
          <Button testID="btn-take-photo" title="press" onPress={change} />
        </>
      );
    },
  };
});

jest.mock("@react-navigation/native", () => {
  const { Button } = require("react-native");
  const { PictureModel } = require("../../models/Picture");

  return {
    useNavigation: jest.fn(),
  };
});

describe("CampaignForm", () => {
  let cache: MemoryCache;

  beforeEach(function () {
    cache = new MemoryCache();
    const mockFetch = jest.fn();

    Store.instance = new Store(
      "",
      mockFetch,
      cache,
      new StaticAuthenticator(""),
    );

    cache.upsert("test-id", "Campaign", {
      _id: "test-id",
    });
  });

  it("does nothing when there is no model", () => {
    const loaded = jest.fn();
    const model = {};

    render(
      <CampaignForm
        model={model}
        value={{
          modelKey: "",
          data: {},
        }}
        onChange={jest.fn()}
      />,
    );

    expect(loaded).not.toHaveBeenCalled();
  });

  describe("a survey with required pictures", () => {
    let campaign;
    let model: GISCollectiveModel;
    let value: CampaignFormProps["value"];

    beforeEach(async function () {
      LocalStorage.instance = new LocalStorageMemory();

      campaign = new CampaignModel({
        _id: "",
        article: { blocks: [] },
        cover: "",
        icons: [],
        info: {
          author: "",
          changeIndex: 0,
          createdOn: "",
          lastChangeOn: "",
          originalAuthor: "",
        },
        name: "some name",
        visibility: { isPublic: true, team: "" },
        questions: [
          {
            help: "some help message",
            isRequired: true,
            isVisible: true,
            name: "some name",
            options: {},
            question: "pictures questions",
            type: "pictures",
          },
        ],
      });

      model = { campaign };
      value = {
        modelKey: "campaign",
        data: {},
      };

      cache = new MemoryCache();
      await cache.upsert(campaign._id, "Campaign", campaign.toJSON());

      Store.instance = new Store(
        "",
        jest.fn(),
        cache,
        new StaticAuthenticator(""),
      );
    });

    it("renders the field", async function () {
      const component = render(
        <CampaignForm
          model={model}
          value={value}
          onChange={function (answer: CampaignAnswerModel): Promise<void> {
            throw new Error("Function not implemented.");
          }}
        />,
      );

      await waitFor(() =>
        expect(component.queryByTestId("loading-placeholder")).not.toBeFalsy(),
      );

      await waitFor(() =>
        expect(component.queryByTestId("loading-placeholder")).toBeFalsy(),
      );

      expect(
        component.queryByTestId("question-0-question-text-label"),
      ).toHaveTextContent("pictures questions");
      expect(component.queryByTestId("question-0-help-text")).toHaveTextContent(
        "some help message",
      );
      expect(
        component.queryByTestId("question-0-question-text-required"),
      ).toHaveTextContent("*");
    });

    it("does not show reset section", async function () {
      const component = render(
        <CampaignForm
          model={model}
          value={value}
          onChange={function (answer: CampaignAnswerModel): Promise<void> {
            throw new Error("Function not implemented.");
          }}
        />,
      );

      await waitFor(() =>
        expect(component.queryAllByTestId("loading-placeholder")).toHaveLength(
          0,
        ),
      );

      expect(component.queryByTestId("btn-reset")).toBeNull();
    });
  });

  describe("a survey with mandatory icons", () => {
    let campaign;
    let model: GISCollectiveModel;
    let value: CampaignFormProps["value"];
    let icon: IconModel;
    let cache: MemoryCache;

    beforeEach(async function () {
      icon = new IconModel({
        _id: "1",
        name: "name",
        localName: "local name",
        image: {},
        attributes: [
          {
            name: "test",
            type: "short text",
            displayName: "display name",
            help: "help message",
          },
        ],
        iconSet: "3",
        allowMany: true,
      });

      campaign = new CampaignModel({
        _id: "1",
        article: { blocks: [] },
        cover: "",
        icons: ["1"],
        info: {
          author: "",
          changeIndex: 0,
          createdOn: "",
          lastChangeOn: "",
          originalAuthor: "",
        },
        name: "some name",
        visibility: { isPublic: true, team: "" },
        questions: [
          {
            help: "some help message",
            isRequired: true,
            isVisible: true,
            name: "some name",
            options: {},
            question: "pictures questions",
            type: "pictures",
          },
        ],
      });

      model = { campaign };
      value = {
        modelKey: "campaign",
        data: {},
      };

      cache = new MemoryCache();
      await cache.upsert(campaign._id, "Campaign", campaign.toJSON());

      Store.instance = new Store(
        "",
        jest.fn(),
        cache,
        new StaticAuthenticator(""),
      );

      cache.upsert("1", "Icon", icon.toJSON());
      LocalStorage.instance = new LocalStorageMemory();
    });

    it("stores attribute changes in the tmp store", async function () {
      const component = render(
        <CampaignForm model={model} value={value} onChange={jest.fn()} />,
      );

      await waitFor(() => component.findByTestId("question-icon-list-group-0"));

      const textInput = await component.findByTestId(
        "question-icon-list-group-0-0-question-0-short-text",
      );
      fireEvent.changeText(textInput, "some new value");

      await waitFor(
        async () =>
          (await LocalStorage.instance.getItem("pending-answers")) != ``,
      );

      const valuePending =
        await LocalStorage.instance.getItem("pending-answers");
      const storedRecord = await cache.get(
        `pending-123456789`,
        "CampaignAnswer",
      );

      expect(valuePending).toBe(JSON.stringify({ "1": "pending-123456789" }));
      expect(storedRecord).toBe(
        JSON.stringify({
          _id: "pending-123456789",
          attributes: {
            about: { name: "", description: "" },
            "position details": {},
            name: [{ test: "some new value" }],
          },
          campaign: "1",
          info: { author: "", changeIndex: 0, createdOn: "", lastChangeOn: "" },
          icons: ["1"],
        }),
      );
    });
  });

  describe("a survey with contributor questions", () => {
    let campaign;
    let model: GISCollectiveModel;
    let value: CampaignFormProps["value"];
    let icon: IconModel;
    let cache: MemoryCache;

    beforeEach(async function () {
      icon = new IconModel({
        _id: "1",
        name: "name",
        localName: "local name",
        image: {},
        attributes: [
          {
            name: "test",
            type: "short text",
            displayName: "display name",
            help: "help message",
          },
        ],
        iconSet: "3",
        allowMany: true,
      });

      campaign = new CampaignModel({
        _id: "1",
        article: { blocks: [] },
        cover: "",
        icons: ["1"],
        info: {
          author: "",
          changeIndex: 0,
          createdOn: "",
          lastChangeOn: "",
          originalAuthor: "",
        },
        name: "some name",
        visibility: { isPublic: true, team: "" },
        questions: [
          {
            help: "some help message",
            isRequired: false,
            isVisible: true,
            name: "some name",
            options: {},
            question: "pictures questions",
            type: "pictures",
          },
        ],
        contributorQuestions: [
          {
            help: "some help message",
            isRequired: true,
            isVisible: true,
            name: "some email",
            options: {},
            question: "email?",
            type: "email",
          },
        ],
      });

      model = { campaign };
      value = {
        modelKey: "campaign",
        data: {},
      };

      cache = new MemoryCache();
      await cache.upsert(campaign._id, "Campaign", campaign.toJSON());

      Store.instance = new Store(
        "",
        jest.fn(),
        cache,
        new StaticAuthenticator(""),
      );

      cache.upsert("1", "Icon", icon.toJSON());
      LocalStorage.instance = new LocalStorageMemory();
    });

    it("stores contributor attribute changes in the tmp store", async function () {
      const component = render(
        <CampaignForm model={model} value={value} onChange={jest.fn()} />,
      );

      await waitFor(() => component.findByTestId("question-icon-list-group-0"));

      const emailInput = await component.findByTestId("question-0-email");
      fireEvent.changeText(emailInput, "a@b.c");

      await waitFor(
        async () =>
          (await LocalStorage.instance.getItem("pending-answers")) != ``,
      );

      const valuePending =
        await LocalStorage.instance.getItem("pending-answers");
      const storedRecord = await cache.get(
        `pending-123456789`,
        "CampaignAnswer",
      );

      expect(valuePending).toBe(JSON.stringify({ "1": "pending-123456789" }));
      expect(storedRecord).toBe(
        JSON.stringify({
          _id: "pending-123456789",
          attributes: {
            about: { name: "", description: "" },
            "position details": {},
          },
          campaign: "1",
          info: { author: "", changeIndex: 0, createdOn: "", lastChangeOn: "" },
          icons: ["1"],
          contributor: { "some email": "a@b.c" },
        }),
      );
    });
  });

  describe("a survey with a temperature question", () => {
    let campaign;
    let model: GISCollectiveModel;
    let value: CampaignFormProps["value"];
    let icon: IconModel;
    let cache: MemoryCache;

    beforeEach(async function () {
      icon = new IconModel({
        _id: "1",
        name: "name",
        localName: "local name",
        image: {},
        attributes: [
          {
            name: "test",
            type: "short text",
            displayName: "display name",
            help: "help message",
          },
        ],
        iconSet: "3",
        allowMany: true,
      });

      campaign = new CampaignModel({
        _id: "1",
        article: { blocks: [] },
        cover: "",
        icons: [],
        info: {
          author: "",
          changeIndex: 0,
          createdOn: "",
          lastChangeOn: "",
          originalAuthor: "",
        },
        name: "some name",
        visibility: { isPublic: true, team: "" },
        questions: [
          {
            help: "some help message",
            isRequired: false,
            isVisible: true,
            name: "some name",
            options: { measurement: "Temperature" },
            question: "temperature",
            type: "decimal",
          },
        ],
        contributorQuestions: [],
      });

      model = { campaign };
      value = {
        modelKey: "campaign",
        data: {},
      };

      cache = new MemoryCache();
      await cache.upsert(campaign._id, "Campaign", campaign.toJSON());

      Store.instance = new Store(
        "",
        jest.fn(),
        cache,
        new StaticAuthenticator(""),
      );

      LocalStorage.instance = new LocalStorageMemory();
    });

    it("shows the measurement system option", async function () {
      const component = render(
        <CampaignForm model={model} value={value} onChange={jest.fn()} />,
      );

      await waitFor(() =>
        expect(component.queryAllByTestId("loading-placeholder")).toHaveLength(
          0,
        ),
      );

      expect(
        component.queryByTestId("question-0-decimal-postfix"),
      ).toHaveTextContent("°C");

      expect(component.queryByTestId("system-select-options")).toBeTruthy();
      expect(
        component.queryByTestId("system-select-options-value"),
      ).toHaveTextContent("metric");

      await act(async () => {
        await fireEvent.press(
          component.queryByTestId("system-select-options-change")!,
        );
      });

      const button = await component.findByTestId(
        "system-select-options-option-1",
      );
      await fireEvent.press(button);
      expect(
        component.queryByTestId("system-select-options-value"),
      ).toHaveTextContent("imperial");

      expect(
        component.queryByTestId("question-0-decimal-postfix"),
      ).toHaveTextContent("°F");
    });
  });

  describe("submitting an answer with pictures", () => {
    let campaign;
    let model: GISCollectiveModel;
    let value: CampaignFormProps["value"];
    let icon: IconModel;
    let cache: MemoryCache;
    let submitSurveyAnswerMock: jest.MockInstance<any, any, any>;

    beforeEach(async function () {
      submitSurveyAnswerMock = jest
        .spyOn(submitSurveyAnswerAPI, "submitSurveyAnswer")
        .mockReturnValue(Promise.resolve());

      icon = new IconModel({
        _id: "1",
        name: "name",
        localName: "local name",
        image: {},
        attributes: [
          {
            name: "test",
            type: "short text",
            displayName: "display name",
            help: "help message",
          },
        ],
        iconSet: "3",
      });

      const picture1 = new PictureModel({
        _id: "pending-1",
        name: "",
        picture: "1",
      });
      const picture2 = new PictureModel({
        _id: "pending-2",
        name: "",
        picture: "2",
      });

      campaign = new CampaignModel({
        _id: "1",
        article: { blocks: [] },
        cover: "",
        icons: ["1"],
        info: {
          author: "",
          changeIndex: 0,
          createdOn: "",
          lastChangeOn: "",
          originalAuthor: "",
        },
        name: "some name",
        visibility: { isPublic: true, team: "" },
        questions: [
          {
            help: "some help message",
            isRequired: true,
            isVisible: true,
            name: "pictures",
            options: {},
            question: "pictures questions",
            type: "pictures",
          },
        ],
      });

      model = { campaign };
      value = {
        modelKey: "campaign",
        data: {},
      };

      cache = new MemoryCache();
      await cache.upsert(campaign._id, "Campaign", campaign.toJSON());

      Store.instance = new Store(
        "",
        jest.fn(),
        cache,
        new StaticAuthenticator(""),
      );

      cache.upsert("1", "Icon", icon.toJSON());
      cache.upsert("pending-1", "Picture", picture1.toJSON());
      cache.upsert("pending-2", "Picture", picture2.toJSON());
      LocalStorage.instance = new LocalStorageMemory();
    });

    it("tries to upload the answer", async () => {
      const component = render(
        <CampaignForm model={model} value={value} onChange={jest.fn()} />,
      );

      await waitFor(() =>
        expect(component.queryAllByTestId("loading-placeholder")).toHaveLength(
          0,
        ),
      );

      await waitFor(() => component.findByTestId("question-icon-list-group-0"));

      const textInput = await component.findByTestId(
        "question-icon-list-group-0-question-0-short-text",
      );

      await act(() => {
        fireEvent.changeText(textInput, "some new value");
      });

      await act(() => {
        fireEvent.press(component.queryByTestId("btn-take-photo")!);
      });

      await act(() => {
        fireEvent.press(component.queryByTestId("btn-submit")!);
      });

      const sentAnswer = submitSurveyAnswerMock.mock.calls[0][0].toJSON();

      expect(submitSurveyAnswerMock).toHaveBeenCalled();
      expect(sentAnswer).toMatchObject({
        _id: expect.any(String),
        attributes: {
          name: { test: "some new value" },
          about: {
            description: "",
            name: "",
          },
          other: {},
          "position details": {},
        },
        campaign: "1",
        info: { author: "", changeIndex: 0, createdOn: "", lastChangeOn: "" },
        pictures: ["pending-1", "pending-2"],
        sounds: [],
        position: {},
        icons: ["1"],
      });
    });
  });

  describe("when there is a stored survey", () => {
    let campaign;
    let model: GISCollectiveModel;
    let value: CampaignFormProps["value"];

    beforeEach(async function () {
      LocalStorage.instance = new LocalStorageMemory();

      campaign = new CampaignModel({
        _id: "test-id",
        article: { blocks: [] },
        cover: "",
        icons: [],
        info: {
          author: "",
          changeIndex: 0,
          createdOn: "",
          lastChangeOn: "",
          originalAuthor: "",
        },
        name: "some name",
        visibility: { isPublic: true, team: "" },
        questions: [
          {
            help: "some help message",
            isRequired: true,
            isVisible: true,
            name: "some name",
            options: {},
            question: "pictures questions",
            type: "pictures",
          },
        ],
      });

      model = { campaign };
      value = {
        modelKey: "campaign",
        data: {},
      };

      const answer: CampaignAnswerJson = {
        _id: "pending-test-id",
        info: {
          changeIndex: 0,
          author: "",
          createdOn: "",
          lastChangeOn: "",
        },
        attributes: {},
        campaign: "test-id",
      };

      await cache.upsert("pending-test-id", "CampaignAnswer", answer);

      LocalStorage.instance.setItem(
        `pending-answers`,
        `{ "test-id": "pending-test-id" }`,
      );
    });

    it("renders the reset section", async function () {
      const component = render(
        <CampaignForm
          model={model}
          value={value}
          onChange={function (answer: CampaignAnswerModel): Promise<void> {
            throw new Error("Function not implemented.");
          }}
        />,
      );

      await waitFor(() =>
        expect(component.queryAllByTestId("loading-placeholder")).toHaveLength(
          0,
        ),
      );

      expect(component.queryByTestId("btn-reset")).toBeDefined();
    });

    it("can reset the form", async function () {
      const component = render(
        <CampaignForm
          model={model}
          value={value}
          onChange={function (answer: CampaignAnswerModel): Promise<void> {
            return Promise.resolve();
          }}
        />,
      );

      await waitFor(() =>
        expect(component.queryAllByTestId("loading-placeholder")).toHaveLength(
          0,
        ),
      );

      await userEvent.press(await component.findByTestId("btn-reset"));

      expect(component.queryByTestId("btn-reset")).toBeFalsy();
    });
  });
});
