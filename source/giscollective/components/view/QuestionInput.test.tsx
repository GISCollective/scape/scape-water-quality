import { render, fireEvent, waitFor } from "@testing-library/react-native";
import React from "react";
import { QuestionInput } from "./QuestionInput";
import { Question } from "../../models/Question";

describe("QuestionInput", () => {
  describe("when the type is boolean", () => {
    const question: Question = {
      type: "boolean",
      question: "",
      help: "",
      isRequired: true,
      isVisible: true,
      name: "some-name",
      options: {},
    };

    it("renders a boolean input", () => {
      const component = render(
        <QuestionInput
          question={question}
          testID="input-value"
          onChange={jest.fn()}
        />,
      );

      expect(
        component.queryByTestId("input-value-boolean-true"),
      ).toHaveTextContent("Yes");
      expect(
        component.queryByTestId("input-value-boolean-false"),
      ).toHaveTextContent("No");
      expect(
        component.queryByTestId("input-value-boolean-undefined"),
      ).toHaveTextContent("Unknown");
    });

    it("can select a value", async () => {
      const change = jest.fn();

      const component = render(
        <QuestionInput
          question={question}
          testID="input-value"
          onChange={change}
        />,
      );

      const button = await component.findByTestId("input-value-boolean-true");

      fireEvent.press(button);

      expect(change).toHaveBeenCalledWith(question, true);
    });
  });

  describe("when the type is short text", () => {
    const question: Question = {
      type: "short text",
      question: "",
      help: "",
      isRequired: true,
      isVisible: true,
      name: "some-name",
      options: {},
    };

    it("renders a short text input", () => {
      const component = render(
        <QuestionInput
          question={question}
          testID="input-value"
          onChange={jest.fn()}
        />,
      );

      expect(component.queryByTestId("input-value-short-text")).toBeTruthy();
    });

    it("can type a value", async () => {
      const change = jest.fn();

      const component = render(
        <QuestionInput
          question={question}
          testID="input-value"
          onChange={change}
        />,
      );

      const textInput = await component.findByTestId("input-value-short-text");
      fireEvent.changeText(textInput, "some new value");

      expect(change).toHaveBeenCalledWith(question, "some new value");
    });
  });

  describe("when the type is email", () => {
    const question: Question = {
      type: "email",
      question: "",
      help: "",
      isRequired: true,
      isVisible: true,
      name: "some-name",
      options: {},
    };

    it("renders a short email input", () => {
      const component = render(
        <QuestionInput
          question={question}
          testID="input-value"
          onChange={jest.fn()}
        />,
      );

      expect(component.queryByTestId("input-value-email")).toBeTruthy();
    });

    it("can type a value", async () => {
      const change = jest.fn();

      const component = render(
        <QuestionInput
          question={question}
          testID="input-value"
          onChange={change}
        />,
      );

      const textInput = await component.findByTestId("input-value-email");
      fireEvent.changeText(textInput, "a@b.c");

      expect(change).toHaveBeenCalledWith(question, "a@b.c");
    });
  });

  describe("when the type is long text", () => {
    const question: Question = {
      type: "long text",
      question: "",
      help: "",
      isRequired: true,
      isVisible: true,
      name: "some-name",
      options: {},
    };

    it("renders a long text input", () => {
      const component = render(
        <QuestionInput
          question={question}
          testID="input-value"
          onChange={jest.fn()}
        />,
      );

      expect(component.queryByTestId("input-value-long-text")).toBeTruthy();
    });

    it("can type a value", async () => {
      const change = jest.fn();

      const component = render(
        <QuestionInput
          question={question}
          testID="input-value"
          onChange={change}
        />,
      );

      const textInput = await component.findByTestId("input-value-long-text");
      fireEvent.changeText(textInput, "some new value");

      expect(change).toHaveBeenCalledWith(question, "some new value");
    });
  });

  describe("when the type is options", () => {
    const question: Question = {
      type: "options",
      question: "",
      help: "",
      isRequired: true,
      isVisible: true,
      name: "some-name",
      options: "value1,value2,value3",
    };

    it("renders an options input", () => {
      const component = render(
        <QuestionInput
          question={question}
          testID="input-value"
          onChange={jest.fn()}
        />,
      );

      expect(
        component.queryByTestId("input-value-options-change"),
      ).toBeTruthy();
    });

    it("can select a value", async () => {
      const change = jest.fn();

      const component = render(
        <QuestionInput
          question={question}
          testID="input-value"
          onChange={change}
        />,
      );

      const btnChange = await component.findByTestId(
        "input-value-options-change",
      );
      await fireEvent.press(btnChange);

      expect(component.queryByTestId("input-value-options-list")).toBeDefined();

      await waitFor(
        () => component.queryAllByTestId("input-option").length != 0,
      );

      const button = await component.findByTestId(
        "input-value-options-option-1",
      );
      await fireEvent.press(button);

      expect(change).toHaveBeenCalledWith(question, "value2");
    });
  });
});
