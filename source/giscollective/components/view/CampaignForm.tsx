import {
  AnyGISCollectiveModel,
  GISCollectiveModel,
  Store,
} from "../../models/store";
import { View, ViewStyle, Text } from "react-native";
import { CampaignAnswerModel } from "../../models/CampaignAnswer";
import { CampaignModel } from "../../models/Campaign";
import { GeoJsonGeometry } from "../../models/GeoJsonGeometry";
import { QuestionList } from "./QuestionList";
import { QuestionIconList } from "./QuestionIconList";
import { SourceRecord } from "./SourceRecord";
import { TextWithOptionsValue } from "./TextWithOptions";
import { useState } from "react";
import React from "react";
import { IconModel } from "../../models/Icon";
import { InputButton } from "../input/InputButton";
import { submitSurveyAnswer } from "../../lib/submitSurveyAnswer";
import { useNavigation } from "@react-navigation/native";
import { Theme } from "../../lib/theme";
import {
  createNewAnswer,
  extractGenericAnswers,
  fillAnswer,
  getPendingAnswers,
  needsUnitsQuestion,
  setPendingAnswer,
} from "../../lib/surveyAnswer";
import { QuestionInput } from "./QuestionInput";
import { Question } from "../../models/Question";
import { H1, P, HR, H4 } from "../html";
import { LocalStorage } from "../../lib/localStorage";

export interface CampaignFormData {
  labels?: TextWithOptionsValue;
  help?: TextWithOptionsValue;
}

export interface CampaignFormProps {
  model: GISCollectiveModel;
  value: {
    modelKey: string;
    data: CampaignFormData;
  };
  onChange: (answer: CampaignAnswerModel) => Promise<void>;
}

export interface SuccessMessageProps {
  onReset: () => Promise<void>;
}

export function SuccessMessage(props: SuccessMessageProps) {
  const containerStyle: ViewStyle = {
    display: "flex",
    flexDirection: "row",
    flexWrap: "nowrap",
    width: "100%",
  };

  const navigation = useNavigation();

  const handleGoHome = () => {
    navigation.goBack();
  };

  return (
    <>
      <H1 testID="title">Your answer was saved</H1>
      <P testID="message">Your answer was saved and it's waiting a review.</P>

      <View style={containerStyle}>
        <InputButton
          classes={["btn-success", "me-2", "mb-2"]}
          onPress={props.onReset}
        >
          Submit another answer
        </InputButton>
        <InputButton classes={["btn-success", "mb-2"]} onPress={handleGoHome}>
          Go to homepage
        </InputButton>
      </View>
    </>
  );
}

interface RestoreMessageProps {
  onReset: () => Promise<void>;
}

export function RestoreMessage(props: RestoreMessageProps) {
  const containerStyle = Theme.instance.toStyle(["mb-4", "mt-3"], {
    display: "flex",
    alignItems: "flex-start",
  });
  const textStyle = Theme.instance.toTextStyle(["mb-1"], { color: "warning" });

  return (
    <View style={containerStyle} testID="restore-message">
      <P style={textStyle}>
        You are currently finalizing a previous unsent answer.
      </P>

      <InputButton
        classes={["btn-outline-warning"]}
        testID="btn-reset"
        onPress={props.onReset}
      >
        Reset the form
      </InputButton>

      <HR classes={["mt-3"]} />
    </View>
  );
}

export interface SystemSelectorProps {
  value: "metric" | "imperial";
  onChange: (value: "metric" | "imperial") => void;
}

export function SystemSelector(props: SystemSelectorProps) {
  const question: Question = {
    question: "Which measurement system you want to use?",
    help: "",
    type: "options",
    name: "system",
    isRequired: false,
    isVisible: true,
    options: "metric,imperial",
  };

  return (
    <QuestionInput
      testID="system-select"
      question={question}
      values={{ system: props.value }}
      onChange={(_, value) => {
        props.onChange?.(value);

        return Promise.resolve();
      }}
    />
  );
}

export interface CampaignQuestionsProps {
  defaultIcons: IconModel[];
  genericAnswers: Record<string, any>;
  survey: CampaignModel;
  answer: CampaignAnswerModel;
  area: GeoJsonGeometry;
  showSystemSelector?: boolean;
  onChange: (answer: CampaignAnswerModel) => Promise<void>;
}

export function CampaignQuestions(props: CampaignQuestionsProps) {
  const { defaultIcons, answer, area, survey } = props;
  const [genericAnswers, setGenericAnswers] = useState<Record<string, any>>(
    props.genericAnswers,
  );
  const [attributes, setAttributes] = useState<Record<string, any>>(
    props.answer.attributes,
  );
  const [contributorAnswers, setContributorAnswers] = useState<
    Record<string, any>
  >({});
  const [selectedIcons, setSelectedIcons] = useState<IconModel[]>([
    ...defaultIcons,
    ...(props.genericAnswers.icons ?? []),
  ]);
  const [system, setSystem] = useState<"metric" | "imperial">("metric");

  const onGenericAnswerChange = async (newValues: Record<string, any>) => {
    if (typeof answer.attributes != "object" || !answer.attributes) {
      answer.attributes = {};
    }

    await fillAnswer(answer, newValues, defaultIcons);
    setAttributes(answer.attributes);
    setSelectedIcons([...defaultIcons, ...(newValues?.["icons"] ?? [])]);

    return props.onChange(answer);
  };

  const onChangeAttributes = async (newValues: Record<string, any>) => {
    for (let key in newValues) {
      answer.attributes[key] = newValues[key];
    }

    setAttributes(answer.attributes);

    return props.onChange(answer);
  };

  const onChangeContributor = async (newValues: Record<string, any>) => {
    answer.contributor = newValues;

    return props.onChange(answer);
  };

  return (
    <>
      {props.showSystemSelector && (
        <SystemSelector value={system} onChange={setSystem} />
      )}
      <QuestionList
        system={system}
        mapId={survey.map.map}
        extent={area}
        fields={survey.questions}
        value={genericAnswers}
        onChange={onGenericAnswerChange}
      />

      <QuestionIconList
        system={system}
        testID="question-icon-list"
        value={attributes}
        icons={selectedIcons}
        onChange={onChangeAttributes}
      />

      {survey.contributorQuestions?.length ? (
        <>
          <HR classes={["mb-4", "mt-4"]} />
          <H4>About you</H4>

          <P>
            None of your personal information will be published. We will only
            use this information to keep in touch and to monitor who is
            contributing ideas to our project.
          </P>

          <QuestionList
            system={system}
            mapId={survey.map.map}
            extent={area}
            fields={survey.contributorQuestions}
            value={contributorAnswers}
            onChange={onChangeContributor}
          />
        </>
      ) : null}
    </>
  );
}

export function CampaignForm(props: CampaignFormProps) {
  const [survey, setSurvey] = useState<CampaignModel | undefined>(undefined);
  const [area, setExtent] = useState<GeoJsonGeometry | undefined>(undefined);
  const [answer, setAnswer] = useState<CampaignAnswerModel | undefined>();
  const [genericAnswers, setGenericAnswers] = useState<
    Record<string, any> | undefined
  >(undefined);
  const [defaultIcons, setDefaultIcons] = useState<IconModel[]>([]);
  const [isSaved, setIsSaved] = useState<boolean>(false);
  const [isRestored, setIsRestored] = useState<boolean>(false);
  const [showSystemSelector, setShowSystemSelector] = useState(false);

  const surveyLoaded = async (record: AnyGISCollectiveModel) => {
    const { defaultIcons, answer, area, isRestored } = await createNewAnswer(
      record as CampaignModel,
    );

    setGenericAnswers(await extractGenericAnswers(answer));

    setIsRestored(isRestored);
    setDefaultIcons(defaultIcons);
    setExtent(area);
    setShowSystemSelector(await needsUnitsQuestion(record as CampaignModel));
    setAnswer(answer);
    setSurvey(record as CampaignModel);
  };

  const onChange = async (answer: CampaignAnswerModel) => {
    setAnswer(answer);

    await Store.instance.cache.upsert(
      answer._id,
      "CampaignAnswer",
      answer.toJSON(),
    );
    await setPendingAnswer(survey?._id ?? "", answer._id);
  };

  const onReset = async () => {
    if (!survey) {
      return;
    }

    const pendingAnswers = await getPendingAnswers();
    const answerId = pendingAnswers[survey._id];

    const tmp = survey;
    setSurvey(undefined);

    await Store.instance.cache.delete(answerId, "CampaignAnswer");
    await setPendingAnswer(survey._id, "");

    await surveyLoaded(tmp);
  };

  const onSubmit = async () => {
    setIsSaved(true);

    await submitSurveyAnswer(answer!);
    await setPendingAnswer(survey?._id ?? "", "");
  };

  if (isSaved) {
    return <SuccessMessage onReset={onReset} />;
  }

  const isReady = !!survey && !!answer && !!area && !!genericAnswers;

  return (
    <>
      <SourceRecord
        model={props.model}
        value={props.value}
        onLoad={surveyLoaded}
      />

      {isReady && (
        <>
          {isRestored && <RestoreMessage onReset={onReset} />}

          <CampaignQuestions
            showSystemSelector={showSystemSelector}
            defaultIcons={defaultIcons}
            genericAnswers={genericAnswers}
            survey={survey}
            answer={answer}
            area={area}
            onChange={onChange}
          />

          <InputButton
            classes={["btn-primary"]}
            testID="btn-submit"
            onPress={onSubmit}
          >
            Submit
          </InputButton>
        </>
      )}
    </>
  );
}
