import React, { useState } from "react";
import { Question } from "../../models/Question";
import { QuestionInput } from "./QuestionInput";
import { GeoJsonGeometry } from "../../models/GeoJsonGeometry";

export interface QuestionListProps {
  fields: Question[];
  mapId?: string;
  extent?: GeoJsonGeometry;
  onChange: (values: Record<string, any>) => Promise<void>;
  value?: Record<string, unknown>;
  testID?: string;
  system?: "metric" | "imperial";
}

export function QuestionList(props: QuestionListProps) {
  const fields = props.fields?.filter((a) => a.isVisible) ?? [];
  const [values, setValues] = useState(props.value ?? {});

  const handleChange = (key: string, value: any) => {
    const newValues = {
      ...values,
      [key]: value,
    };
    setValues(newValues);

    if (!props.onChange) {
      return console.error("QuestionList 'onChange' is not set");
    }

    return props.onChange(newValues);
  };

  return (
    <>
      {fields.map((a, i) => (
        <QuestionInput
          testID={`${props.testID ? `${props.testID}-` : ""}question-${i}`}
          key={a.name}
          question={a}
          mapId={props.mapId}
          extent={props.extent}
          system={props.system}
          values={values}
          onChange={async (question, value) =>
            handleChange(question.name, value)
          }
        />
      ))}
    </>
  );
}
