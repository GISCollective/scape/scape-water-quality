import React, { useState } from "react";
import { Question, QuestionModel } from "../../models/Question";
import { IconModel } from "../../models/Icon";
import { QuestionList } from "./QuestionList";
import { View } from "react-native";
import { IconAttributeModel } from "../../models/IconAttribute";
import { InputButton } from "../input/InputButton";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import { useTheme } from "../../hooks/useTheme";
import { H2 } from "../html";
import { RemoteImage } from "../RemoteImage";

export interface QuestionIconListProps {
  testID?: string;
  onChange: (values: Record<string, any>) => Promise<void>;
  value: Record<string, unknown>;
  system?: "metric" | "imperial";
  icons: IconModel[];
}

export function toQuestions(attributes: IconAttributeModel[]): Question[] {
  return attributes.map(
    (a) =>
      new QuestionModel({
        question: a.displayName || a.name,
        help: a.help ?? "",
        type: a.type,
        name: a.name,
        isRequired: a.isRequired ?? false,
        isVisible: true,
        options: a.options,
      }),
  );
}

interface IconTitleProps {
  icon: IconModel;
  testID: string;
  prefix?: string;
  onRemove?: () => void;
}

export function IconTitle(props: IconTitleProps) {
  const theme = useTheme();
  const [removeState, setRemoveState] = useState("");

  const containerStyle = theme.toStyle(["mb-2"], {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  });
  const iconStyle = theme.toStyle(["me-3", "ps-1", "pe-1", "pt-1", "pb-1"]);
  const headingStyle = theme.toTextStyle(["pt-3", "ps-1"]);

  const gray: string = String(theme.toColor("gray-600"));
  const white: string = String(theme.toColor("white"));

  const removeIconColor = removeState == "active" ? white : gray;

  return (
    <View style={containerStyle}>
      {props.icon.image.value && (
        <RemoteImage
          style={iconStyle}
          testID={`${props.testID}-image`}
          url={`${props.icon.image.value}/png`}
          width={40}
          height={40}
          tintColor={theme.iconTint.tintColor}
        />
      )}
      <H2 testID={`${props.testID}-text`} style={headingStyle}>
        {props.prefix ?? ""}
        {props.icon.localName || props.icon.name}
      </H2>

      {props.onRemove && (
        <InputButton
          classes={["btn-danger"]}
          onPress={props.onRemove}
          onStateChange={setRemoveState}
          testID={`${props.testID}-remove`}
        >
          <FontAwesomeIcon icon={faXmark} size={25} color={removeIconColor} />
        </InputButton>
      )}
    </View>
  );
}

export interface QuestionGroupProps {
  icon: IconModel;
  testID?: string;
  index: number;
  onChange: (key: string, values: Record<string, any>) => Promise<void>;
  value: Record<string, any>;
  system?: "metric" | "imperial";
}

export function QuestionGroup(props: QuestionGroupProps) {
  return (
    <>
      <IconTitle
        icon={props.icon}
        testID={`${props.testID}-group-${props.index}-title`}
      />
      <QuestionList
        system={props.system}
        fields={toQuestions(props.icon.attributes)}
        onChange={(values) => props.onChange(props.icon.name, values)}
        value={props.value}
        testID={`${props.testID}-group-${props.index}`}
      />
    </>
  );
}

export interface QuestionListGroupProps {
  icon: IconModel;
  testID?: string;
  index: number;
  onChange: (key: string, values: Record<string, any>) => Promise<void>;
  value: Record<string, any>;
  system: "metric" | "imperial";
}

export function QuestionListGroup(props: QuestionListGroupProps) {
  let initialValues: Record<string, any>[] = Array.isArray(props.value)
    ? props.value
    : [];

  if (
    initialValues.length == 0 &&
    !Array.isArray(props.value) &&
    typeof props.value == "object"
  ) {
    initialValues.push(props.value);
  }

  if (initialValues.length == 0) {
    initialValues.push({});
  }

  const [attributes, setAttributes] =
    useState<Record<string, any>[]>(initialValues);

  const addAnother = () => {
    attributes.push({});
    setAttributes(attributes);

    return props.onChange(props.icon.name, attributes);
  };

  const change = (index: number, values: Record<string, any>) => {
    const newAttributes = attributes;
    newAttributes[index] = values;

    setAttributes(newAttributes);

    return props.onChange(props.icon.name, newAttributes);
  };

  const remove = (index: number) => {
    const newAttributes = [...attributes];
    newAttributes.splice(index, 1);

    setAttributes(newAttributes);

    return props.onChange(props.icon.name, newAttributes);
  };

  return (
    <>
      {attributes.map((a, index) => (
        <View key={index}>
          <IconTitle
            prefix={`${index + 1}. `}
            icon={props.icon}
            testID={`${props.testID}-group-${props.index}-${index}-title`}
            onRemove={() => remove(index)}
          />
          <QuestionList
            fields={toQuestions(props.icon.attributes)}
            onChange={(values) => change(index, values)}
            value={a}
            system={props.system}
            testID={`${props.testID}-group-${props.index}-${index}`}
          />
        </View>
      ))}
      <InputButton testID={`${props.testID}-add`} onPress={addAnother}>
        Add another {props.icon.localName}
      </InputButton>
    </>
  );
}

export function QuestionIconList(props: QuestionIconListProps) {
  const [attributes, setAttributes] = useState(props.value ?? {});
  const iconsWithAttributes =
    props.icons?.filter((a) => a.attributes.length) ?? [];

  const change = (key: string, values: Record<string, any>) => {
    const newAttributes = {
      ...attributes,
      [key]: values,
    };

    setAttributes(newAttributes);

    return props.onChange(newAttributes);
  };

  return (
    <>
      {iconsWithAttributes.map((a, index) => (
        <View key={index} testID={`${props.testID}-group-${index}`}>
          {!a.allowMany && (
            <QuestionGroup
              system={props.system}
              icon={a}
              testID={props.testID}
              index={index}
              onChange={change}
              value={attributes[a.name] ?? {}}
            />
          )}

          {a.allowMany && (
            <QuestionListGroup
              system={props.system}
              icon={a}
              testID={props.testID}
              index={index}
              onChange={change}
              value={attributes[a.name] ?? []}
            />
          )}
        </View>
      ))}
    </>
  );
}
