import React from "react";
import { View, ViewStyle } from "react-native";
import { MapView } from "./MapView";
import { GeoJsonGeometry } from "../../giscollective/models/GeoJsonGeometry";

export interface GeoJsonViewProps {
  testID?: string;
  style?: ViewStyle;
  value?: GeoJsonGeometry;
}

export const GeoJsonView = (props: GeoJsonViewProps) => {
  return (
    <MapView
      testID={props.testID}
      value={props.value}
      style={props.style}
      mode="geo-json"
    />
  );
};
