import { registerTranslation, en } from "react-native-paper-dates";
import { SQLiteCache } from "./lib/cache";
import SQLite from "react-native-sqlite-storage";
import React, { useEffect, useState } from "react";
import { Loading } from "./components/view/Loading";
import { StaticAuthenticator } from "./lib/authenticator";
import { extractCssVariables } from "./lib/css";
import { genericFetch, genericBinaryFetch } from "./lib/fetch";
import { LocalStorage } from "./lib/localStorage";
import { RequestStore } from "./lib/requestStore";
import { Theme } from "./lib/theme";
import { Store } from "./models/store";
import { FetchStatus } from "./components/view/FetchStatus";
import { Dimensions, View } from "react-native";
import { Fetch } from "./components/view/Fetch";

export interface AppContainerProps {
  children: string | React.ReactNode;
  spaceId: string;
  apiUrl?: string;
  token?: string;
}

async function init(spaceId: string, apiUrl: string, token: string) {
  registerTranslation("en", en);

  const db = await SQLite.openDatabase({
    name: `appdata.sqlite`,
    location: "default",
  });

  const localStorageDb = await SQLite.openDatabase({
    name: `localStorage.sqlite`,
    location: "default",
  });

  const localStorage = new LocalStorage(localStorageDb);
  await localStorage.setup();
  LocalStorage.instance = localStorage;

  const cache = new SQLiteCache(db);
  await cache.setup();

  RequestStore.instance = new RequestStore(cache);
  Store.instance = new Store(
    apiUrl,
    genericFetch,
    cache,
    new StaticAuthenticator(token),
  );
  Theme.instance.space = await Store.instance.findSpace(spaceId);

  const spaceStyle = await genericBinaryFetch(
    "GET",
    `${apiUrl}/spaces/${spaceId}/style`,
    {},
  );

  const textStyle = atob(spaceStyle.data);
  const style = extractCssVariables(textStyle);

  Theme.instance.loadColors("light", style[`light-${spaceId}`]);
  Theme.instance.loadColors("dark", style[`dark-${spaceId}`]);

  return Store.instance;
}

export function AppContainer(props: AppContainerProps) {
  const [store, setStore] = useState<Store | null>(null);
  const [error, setError] = useState<string | undefined>();
  const ScreenHeight = Dimensions.get("window").height;

  const setup = () => {
    return init(
      props.spaceId,
      props.apiUrl ?? "https://new.opengreenmap.org/api-v1",
      props.token ?? "",
    );
  };

  const onReady = (newStore: Store) => {
    setStore(newStore);
  };

  if (!store) {
    return (
      <View
        style={{
          height: ScreenHeight,
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Fetch fetch={setup} onSuccess={onReady} />
      </View>
    );
  }

  return <>{props.children}</>;
}
