import { Store } from "./store";

export interface Model {
  _id: string;
  name: string;
  itemCount: number;
}

export interface ModelResponse {
  model: ModelJson;
}

export interface ModelsResponse {
  models: ModelJson[];
}

export interface ModelJson {
  _id: string;
  name: string;
  itemCount: number;
}

export class ModelModel {
  private json: ModelJson;
  private _isDirty: boolean = false;

  constructor(json: ModelJson | null | undefined) {
    this.json = json ?? { _id: "", name: "", itemCount: 0 };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: ulong
  get itemCount(): number {
    return this.json.itemCount;
  }

  set itemCount(newValue: number) {
    this._isDirty = true;
    this.json.itemCount = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<ModelModel> {
    let result: ModelModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createModel(this);
    } else {
      result = await Store.instance.saveModel(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): ModelJson {
    return this.json;
  }
}
