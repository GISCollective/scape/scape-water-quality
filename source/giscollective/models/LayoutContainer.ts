import { LayoutRow, LayoutRowJson, LayoutRowModel } from "./LayoutRow";
import {
  LayoutLayers,
  LayoutLayersJson,
  LayoutLayersModel,
} from "./LayoutLayers";
import { Store } from "./store";

export interface LayoutContainer {
  options?: string[];
  gid?: string;
  visibility?: string;
  data?: any;
  anchor?: string;
  height?: string;
  rows?: LayoutRow[];
  layers?: LayoutLayers;
}

export interface LayoutContainerJson {
  options?: string[];
  gid?: string;
  visibility?: string;
  data?: any;
  anchor?: string;
  height?: string;
  rows?: LayoutRowJson[];
  layers?: LayoutLayersJson;
}

export class LayoutContainerModel {
  private json: LayoutContainerJson;
  private _isDirty: boolean = false;

  constructor(json: LayoutContainerJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: string
  get options(): string[] | undefined {
    return this.json.options;
  }

  set options(newValue: string[] | undefined) {
    this._isDirty = true;
    this.json.options = newValue;
  }

  /// field: string
  get gid(): string | undefined {
    return this.json.gid;
  }

  set gid(newValue: string | undefined) {
    this._isDirty = true;
    this.json.gid = newValue;
  }

  /// field: string
  get visibility(): string | undefined {
    return this.json.visibility;
  }

  set visibility(newValue: string | undefined) {
    this._isDirty = true;
    this.json.visibility = newValue;
  }

  /// field: Json
  get data(): any | undefined {
    return this.json.data;
  }

  set data(newValue: any | undefined) {
    this._isDirty = true;
    this.json.data = newValue;
  }

  /// field: string
  get anchor(): string | undefined {
    return this.json.anchor;
  }

  set anchor(newValue: string | undefined) {
    this._isDirty = true;
    this.json.anchor = newValue;
  }

  /// field: string
  get height(): string | undefined {
    return this.json.height;
  }

  set height(newValue: string | undefined) {
    this._isDirty = true;
    this.json.height = newValue;
  }

  /// object: LayoutRow
  get rows(): LayoutRowModel[] {
    return this.json.rows?.map((a) => new LayoutRowModel(a)) ?? [];
  }

  set rows(newValue: LayoutRowModel[] | undefined) {
    this._isDirty = true;

    this.json.rows = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// object: LayoutLayers
  get layers(): LayoutLayersModel {
    return new LayoutLayersModel(this.json.layers);
  }

  set layers(newValue: LayoutLayersModel | undefined) {
    this._isDirty = true;

    this.json.layers = newValue?.toJSON();
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    for (let level0 of this.rows) {
      if (level0?.isDirty) {
        return true;
      }
    }

    if (this.layers?.isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): LayoutContainerJson {
    return this.json;
  }
}
