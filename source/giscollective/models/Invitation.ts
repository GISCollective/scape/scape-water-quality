import { Store } from "./store";

export interface Invitation {
  email: string;
  role: string;
}

export interface InvitationJson {
  email: string;
  role: string;
}

export class InvitationModel {
  private json: InvitationJson;
  private _isDirty: boolean = false;

  constructor(json: InvitationJson | null | undefined) {
    this.json = json ?? { email: "", role: "" };
  }

  /// field: string
  get email(): string {
    return this.json.email;
  }

  set email(newValue: string) {
    this._isDirty = true;
    this.json.email = newValue;
  }

  /// field: string
  get role(): string {
    return this.json.role;
  }

  set role(newValue: string) {
    this._isDirty = true;
    this.json.role = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): InvitationJson {
    return this.json;
  }
}
