import { Store } from "./store";

export interface Question {
  question: string;
  help: string;
  type: string;
  name: string;
  isRequired: boolean;
  isVisible: boolean;
  options: any;
}

export interface QuestionJson {
  question: string;
  help: string;
  type: string;
  name: string;
  isRequired: boolean;
  isVisible: boolean;
  options: any;
}

export class QuestionModel {
  private json: QuestionJson;
  private _isDirty: boolean = false;

  constructor(json: QuestionJson | null | undefined) {
    this.json = json ?? {
      question: "",
      help: "",
      type: "",
      name: "",
      isRequired: false,
      isVisible: false,
      options: null,
    };
  }

  /// field: string
  get question(): string {
    return this.json.question;
  }

  set question(newValue: string) {
    this._isDirty = true;
    this.json.question = newValue;
  }

  /// field: string
  get help(): string {
    return this.json.help;
  }

  set help(newValue: string) {
    this._isDirty = true;
    this.json.help = newValue;
  }

  /// field: string
  get type(): string {
    return this.json.type;
  }

  set type(newValue: string) {
    this._isDirty = true;
    this.json.type = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: bool
  get isRequired(): boolean {
    return this.json.isRequired;
  }

  set isRequired(newValue: boolean) {
    this._isDirty = true;
    this.json.isRequired = newValue;
  }

  /// field: bool
  get isVisible(): boolean {
    return this.json.isVisible;
  }

  set isVisible(newValue: boolean) {
    this._isDirty = true;
    this.json.isVisible = newValue;
  }

  /// field: Json
  get options(): any {
    return this.json.options;
  }

  set options(newValue: any) {
    this._isDirty = true;
    this.json.options = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): QuestionJson {
    return this.json;
  }
}
