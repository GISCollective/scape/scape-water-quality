import { Visibility, VisibilityJson, VisibilityModel } from "./Visibility";
import { IconSets, IconSetsJson, IconSetsModel } from "./IconSets";
import {
  IconAttribute,
  IconAttributeJson,
  IconAttributeModel,
} from "./IconAttribute";
import { Store } from "./store";
import { ModelInfo, ModelInfoJson, ModelInfoModel } from "./ModelInfo";
import { ArticleBody } from "./ArticleBody";
import { OptionalMap, OptionalMapJson, OptionalMapModel } from "./OptionalMap";
import { Picture, PictureModel } from "./Picture";

export interface Calendar {
  _id: string;
  name: string;
  article: ArticleBody;
  type: string;
  recordName?: string;
  visibility: Visibility;
  info: ModelInfo;
  iconSets: IconSets;
  map: OptionalMap;
  attributes?: IconAttribute[];
  cover?: Picture;
}

export interface CalendarResponse {
  calendar: CalendarJson;
}

export interface CalendarsResponse {
  calendars: CalendarJson[];
}

export interface CalendarJson {
  _id: string;
  name: string;
  article: ArticleBody;
  type: string;
  recordName?: string;
  visibility: VisibilityJson;
  info: ModelInfoJson;
  iconSets: IconSetsJson;
  map: OptionalMapJson;
  attributes?: IconAttributeJson[];
  cover?: string;
}

export class CalendarModel {
  private json: CalendarJson;
  private _isDirty: boolean = false;

  constructor(json: CalendarJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      name: "",
      article: { blocks: [] },
      type: "",
      visibility: { isPublic: false, team: "" },
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      iconSets: { useCustomList: false, list: [] },
      map: { isEnabled: false, map: "" },
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: Json
  get article(): ArticleBody {
    return this.json.article;
  }

  set article(newValue: ArticleBody) {
    this._isDirty = true;
    this.json.article = newValue;
  }

  /// field: string
  get type(): string {
    return this.json.type;
  }

  set type(newValue: string) {
    this._isDirty = true;
    this.json.type = newValue;
  }

  /// field: string
  get recordName(): string | undefined {
    return this.json.recordName;
  }

  set recordName(newValue: string | undefined) {
    this._isDirty = true;
    this.json.recordName = newValue;
  }

  /// object: Visibility
  get visibility(): VisibilityModel {
    return new VisibilityModel(this.json.visibility);
  }

  set visibility(newValue: VisibilityModel) {
    this._isDirty = true;

    this.json.visibility = newValue?.toJSON();
  }

  /// object: ModelInfo
  get info(): ModelInfoModel {
    return new ModelInfoModel(this.json.info);
  }

  set info(newValue: ModelInfoModel) {
    this._isDirty = true;

    this.json.info = newValue?.toJSON();
  }

  /// object: IconSets
  get iconSets(): IconSetsModel {
    return new IconSetsModel(this.json.iconSets);
  }

  set iconSets(newValue: IconSetsModel) {
    this._isDirty = true;

    this.json.iconSets = newValue?.toJSON();
  }

  /// object: OptionalMap
  get map(): OptionalMapModel {
    return new OptionalMapModel(this.json.map);
  }

  set map(newValue: OptionalMapModel) {
    this._isDirty = true;

    this.json.map = newValue?.toJSON();
  }

  /// object: IconAttribute
  get attributes(): IconAttributeModel[] {
    return this.json.attributes?.map((a) => new IconAttributeModel(a)) ?? [];
  }

  set attributes(newValue: IconAttributeModel[] | undefined) {
    this._isDirty = true;

    this.json.attributes = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// model: Picture
  get cover(): Promise<PictureModel | undefined> {
    if (!this.json.cover) {
      return Promise.resolve(undefined);
    }
    return Store.instance.findPicture(this.json.cover);
  }

  set cover(newValue: PictureModel | undefined) {
    this._isDirty = true;
    this.json.cover = newValue?._id;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.visibility?.isDirty) {
      return true;
    }

    if (this.info?.isDirty) {
      return true;
    }

    if (this.iconSets?.isDirty) {
      return true;
    }

    if (this.map?.isDirty) {
      return true;
    }

    for (let level0 of this.attributes) {
      if (level0?.isDirty) {
        return true;
      }
    }

    return false;
  }

  async save(): Promise<CalendarModel> {
    let result: CalendarModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createCalendar(this);
    } else {
      result = await Store.instance.saveCalendar(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): CalendarJson {
    return this.json;
  }
}
