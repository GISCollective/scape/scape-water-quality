import { DateTime } from "luxon";
import { Store } from "./store";

export interface Source {
  type?: string;
  modelId?: string;
  remoteId?: string;
  syncAt?: DateTime;
}

export interface SourceJson {
  type?: string;
  modelId?: string;
  remoteId?: string;
  syncAt?: string;
}

export class SourceModel {
  private json: SourceJson;
  private _isDirty: boolean = false;

  constructor(json: SourceJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: string
  get type(): string | undefined {
    return this.json.type;
  }

  set type(newValue: string | undefined) {
    this._isDirty = true;
    this.json.type = newValue;
  }

  /// field: string
  get modelId(): string | undefined {
    return this.json.modelId;
  }

  set modelId(newValue: string | undefined) {
    this._isDirty = true;
    this.json.modelId = newValue;
  }

  /// field: string
  get remoteId(): string | undefined {
    return this.json.remoteId;
  }

  set remoteId(newValue: string | undefined) {
    this._isDirty = true;
    this.json.remoteId = newValue;
  }

  /// object: SysTime
  get syncAt(): DateTime | undefined {
    if (!this.json.syncAt) {
      return undefined;
    }
    return DateTime.fromISO(this.json.syncAt);
  }

  set syncAt(newValue: DateTime | undefined) {
    this._isDirty = true;
    this.json.syncAt = newValue?.toISO() ?? undefined;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): SourceJson {
    return this.json;
  }
}
