import { Store } from "./store";

export interface SpaceSearchOptions {
  features: boolean;
  maps: boolean;
  campaigns: boolean;
  iconSets: boolean;
  teams: boolean;
  icons: boolean;
  geocodings: boolean;
  events: boolean;
  articles: boolean;
}

export interface SpaceSearchOptionsJson {
  features: boolean;
  maps: boolean;
  campaigns: boolean;
  iconSets: boolean;
  teams: boolean;
  icons: boolean;
  geocodings: boolean;
  events: boolean;
  articles: boolean;
}

export class SpaceSearchOptionsModel {
  private json: SpaceSearchOptionsJson;
  private _isDirty: boolean = false;

  constructor(json: SpaceSearchOptionsJson | null | undefined) {
    this.json = json ?? {
      features: false,
      maps: false,
      campaigns: false,
      iconSets: false,
      teams: false,
      icons: false,
      geocodings: false,
      events: false,
      articles: false,
    };
  }

  /// field: bool
  get features(): boolean {
    return this.json.features;
  }

  set features(newValue: boolean) {
    this._isDirty = true;
    this.json.features = newValue;
  }

  /// field: bool
  get maps(): boolean {
    return this.json.maps;
  }

  set maps(newValue: boolean) {
    this._isDirty = true;
    this.json.maps = newValue;
  }

  /// field: bool
  get campaigns(): boolean {
    return this.json.campaigns;
  }

  set campaigns(newValue: boolean) {
    this._isDirty = true;
    this.json.campaigns = newValue;
  }

  /// field: bool
  get iconSets(): boolean {
    return this.json.iconSets;
  }

  set iconSets(newValue: boolean) {
    this._isDirty = true;
    this.json.iconSets = newValue;
  }

  /// field: bool
  get teams(): boolean {
    return this.json.teams;
  }

  set teams(newValue: boolean) {
    this._isDirty = true;
    this.json.teams = newValue;
  }

  /// field: bool
  get icons(): boolean {
    return this.json.icons;
  }

  set icons(newValue: boolean) {
    this._isDirty = true;
    this.json.icons = newValue;
  }

  /// field: bool
  get geocodings(): boolean {
    return this.json.geocodings;
  }

  set geocodings(newValue: boolean) {
    this._isDirty = true;
    this.json.geocodings = newValue;
  }

  /// field: bool
  get events(): boolean {
    return this.json.events;
  }

  set events(newValue: boolean) {
    this._isDirty = true;
    this.json.events = newValue;
  }

  /// field: bool
  get articles(): boolean {
    return this.json.articles;
  }

  set articles(newValue: boolean) {
    this._isDirty = true;
    this.json.articles = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): SpaceSearchOptionsJson {
    return this.json;
  }
}
