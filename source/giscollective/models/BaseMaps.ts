import { BaseMap, BaseMapModel } from "./BaseMap";
import { Store } from "./store";

export interface BaseMaps {
  useCustomList: boolean;
  list: BaseMap[];
}

export interface BaseMapsJson {
  useCustomList: boolean;
  list: string[];
}

export class BaseMapsModel {
  private json: BaseMapsJson;
  private _isDirty: boolean = false;

  constructor(json: BaseMapsJson | null | undefined) {
    this.json = json ?? { useCustomList: false, list: [] };
  }

  /// field: bool
  get useCustomList(): boolean {
    return this.json.useCustomList;
  }

  set useCustomList(newValue: boolean) {
    this._isDirty = true;
    this.json.useCustomList = newValue;
  }

  /// model: BaseMap
  get list(): Promise<BaseMapModel[]> {
    const promises =
      this.json.list?.map((a) => Store.instance.findBaseMap(a)) ?? [];

    return Promise.all(promises);
  }

  set list(newValue: BaseMapModel[]) {
    this._isDirty = true;
    this.json.list = newValue?.map((a) => a._id) ?? [];
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): BaseMapsJson {
    return this.json;
  }
}
