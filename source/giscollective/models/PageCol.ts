import { Store } from "./store";

export interface PageCol {
  container?: number;
  col?: number;
  row?: number;
  data?: any;
  type?: string;
  gid?: string;
  name?: string;
}

export interface PageColJson {
  container?: number;
  col?: number;
  row?: number;
  data?: any;
  type?: string;
  gid?: string;
  name?: string;
}

export class PageColModel {
  private json: PageColJson;
  private _isDirty: boolean = false;

  constructor(json: PageColJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: int
  get container(): number | undefined {
    return this.json.container;
  }

  set container(newValue: number | undefined) {
    this._isDirty = true;
    this.json.container = newValue;
  }

  /// field: int
  get col(): number | undefined {
    return this.json.col;
  }

  set col(newValue: number | undefined) {
    this._isDirty = true;
    this.json.col = newValue;
  }

  /// field: int
  get row(): number | undefined {
    return this.json.row;
  }

  set row(newValue: number | undefined) {
    this._isDirty = true;
    this.json.row = newValue;
  }

  /// field: Json
  get data(): any | undefined {
    return this.json.data;
  }

  set data(newValue: any | undefined) {
    this._isDirty = true;
    this.json.data = newValue;
  }

  /// field: string
  get type(): string | undefined {
    return this.json.type;
  }

  set type(newValue: string | undefined) {
    this._isDirty = true;
    this.json.type = newValue;
  }

  /// field: string
  get gid(): string | undefined {
    return this.json.gid;
  }

  set gid(newValue: string | undefined) {
    this._isDirty = true;
    this.json.gid = newValue;
  }

  /// field: string
  get name(): string | undefined {
    return this.json.name;
  }

  set name(newValue: string | undefined) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): PageColJson {
    return this.json;
  }
}
