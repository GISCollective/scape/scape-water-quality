import { Store } from "./store";

export interface FontStyles {
  h1: string[];
  h2: string[];
  h3: string[];
  h4: string[];
  h5: string[];
  h6: string[];
  paragraph: string[];
}

export interface FontStylesJson {
  h1: string[];
  h2: string[];
  h3: string[];
  h4: string[];
  h5: string[];
  h6: string[];
  paragraph: string[];
}

export class FontStylesModel {
  private json: FontStylesJson;
  private _isDirty: boolean = false;

  constructor(json: FontStylesJson | null | undefined) {
    this.json = json ?? {
      h1: [],
      h2: [],
      h3: [],
      h4: [],
      h5: [],
      h6: [],
      paragraph: [],
    };
  }

  /// field: string
  get h1(): string[] {
    return this.json.h1;
  }

  set h1(newValue: string[]) {
    this._isDirty = true;
    this.json.h1 = newValue;
  }

  /// field: string
  get h2(): string[] {
    return this.json.h2;
  }

  set h2(newValue: string[]) {
    this._isDirty = true;
    this.json.h2 = newValue;
  }

  /// field: string
  get h3(): string[] {
    return this.json.h3;
  }

  set h3(newValue: string[]) {
    this._isDirty = true;
    this.json.h3 = newValue;
  }

  /// field: string
  get h4(): string[] {
    return this.json.h4;
  }

  set h4(newValue: string[]) {
    this._isDirty = true;
    this.json.h4 = newValue;
  }

  /// field: string
  get h5(): string[] {
    return this.json.h5;
  }

  set h5(newValue: string[]) {
    this._isDirty = true;
    this.json.h5 = newValue;
  }

  /// field: string
  get h6(): string[] {
    return this.json.h6;
  }

  set h6(newValue: string[]) {
    this._isDirty = true;
    this.json.h6 = newValue;
  }

  /// field: string
  get paragraph(): string[] {
    return this.json.paragraph;
  }

  set paragraph(newValue: string[]) {
    this._isDirty = true;
    this.json.paragraph = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): FontStylesJson {
    return this.json;
  }
}
