import { Store } from "./store";

export interface PictureMeta {
  attributions?: string;
  renderMode?: string;
  link?: any;
  data?: any;
  width?: number;
  height?: number;
  format?: string;
  mime?: string;
  properties?: Record<string, string>;
  disableOptimization?: boolean;
  disableOptimizationReason?: string;
}

export interface PictureMetaJson {
  attributions?: string;
  renderMode?: string;
  link?: any;
  data?: any;
  width?: number;
  height?: number;
  format?: string;
  mime?: string;
  properties?: Record<string, string>;
  disableOptimization?: boolean;
  disableOptimizationReason?: string;
}

export class PictureMetaModel {
  private json: PictureMetaJson;
  private _isDirty: boolean = false;

  constructor(json: PictureMetaJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: string
  get attributions(): string | undefined {
    return this.json.attributions;
  }

  set attributions(newValue: string | undefined) {
    this._isDirty = true;
    this.json.attributions = newValue;
  }

  /// field: string
  get renderMode(): string | undefined {
    return this.json.renderMode;
  }

  set renderMode(newValue: string | undefined) {
    this._isDirty = true;
    this.json.renderMode = newValue;
  }

  /// field: Json
  get link(): any | undefined {
    return this.json.link;
  }

  set link(newValue: any | undefined) {
    this._isDirty = true;
    this.json.link = newValue;
  }

  /// field: Json
  get data(): any | undefined {
    return this.json.data;
  }

  set data(newValue: any | undefined) {
    this._isDirty = true;
    this.json.data = newValue;
  }

  /// field: ulong
  get width(): number | undefined {
    return this.json.width;
  }

  set width(newValue: number | undefined) {
    this._isDirty = true;
    this.json.width = newValue;
  }

  /// field: ulong
  get height(): number | undefined {
    return this.json.height;
  }

  set height(newValue: number | undefined) {
    this._isDirty = true;
    this.json.height = newValue;
  }

  /// field: string
  get format(): string | undefined {
    return this.json.format;
  }

  set format(newValue: string | undefined) {
    this._isDirty = true;
    this.json.format = newValue;
  }

  /// field: string
  get mime(): string | undefined {
    return this.json.mime;
  }

  set mime(newValue: string | undefined) {
    this._isDirty = true;
    this.json.mime = newValue;
  }

  /// field: string
  get properties(): Record<string, string> | undefined {
    return this.json.properties;
  }

  set properties(newValue: Record<string, string> | undefined) {
    this._isDirty = true;
    this.json.properties = newValue;
  }

  /// field: bool
  get disableOptimization(): boolean | undefined {
    return this.json.disableOptimization;
  }

  set disableOptimization(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.disableOptimization = newValue;
  }

  /// field: string
  get disableOptimizationReason(): string | undefined {
    return this.json.disableOptimizationReason;
  }

  set disableOptimizationReason(newValue: string | undefined) {
    this._isDirty = true;
    this.json.disableOptimizationReason = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): PictureMetaJson {
    return this.json;
  }
}
