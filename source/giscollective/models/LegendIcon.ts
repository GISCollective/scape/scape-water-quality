import { Icon, IconModel } from "./Icon";
import { Store } from "./store";

export interface LegendIcon {
  uid: string;
  count: number;
  icon: Icon;
}

export interface LegendIconJson {
  uid: string;
  count: number;
  icon: string;
}

export class LegendIconModel {
  private json: LegendIconJson;
  private _isDirty: boolean = false;

  constructor(json: LegendIconJson | null | undefined) {
    this.json = json ?? { uid: "", count: 0, icon: "" };
  }

  /// field: string
  get uid(): string {
    return this.json.uid;
  }

  set uid(newValue: string) {
    this._isDirty = true;
    this.json.uid = newValue;
  }

  /// field: ulong
  get count(): number {
    return this.json.count;
  }

  set count(newValue: number) {
    this._isDirty = true;
    this.json.count = newValue;
  }

  /// model: Icon
  get icon(): Promise<IconModel> {
    return Store.instance.findIcon(this.json.icon);
  }

  set icon(newValue: IconModel) {
    this._isDirty = true;
    this.json.icon = newValue?._id;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): LegendIconJson {
    return this.json;
  }
}
