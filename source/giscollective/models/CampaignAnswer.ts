import { Sound, SoundModel } from "./Sound";
import { Icon, IconModel } from "./Icon";
import { Review, ReviewJson, ReviewModel } from "./Review";
import { Store } from "./store";
import { Campaign, CampaignModel } from "./Campaign";
import { ModelInfo, ModelInfoJson, ModelInfoModel } from "./ModelInfo";
import { GeoJsonGeometry } from "./GeoJsonGeometry";
import { Picture, PictureModel } from "./Picture";

export interface CampaignAnswer {
  _id: string;
  attributes: Record<string, any>;
  status?: number;
  featureId?: string;
  contributor?: Record<string, string>;
  info: ModelInfo;
  position?: GeoJsonGeometry;
  review?: Review;
  campaign: Campaign;
  icons?: Icon[];
  pictures?: Picture[];
  sounds?: Sound[];
}

export interface CampaignAnswerResponse {
  campaignAnswer: CampaignAnswerJson;
}

export interface CampaignAnswersResponse {
  campaignAnswers: CampaignAnswerJson[];
}

export interface CampaignAnswerJson {
  _id: string;
  attributes: Record<string, any>;
  status?: number;
  featureId?: string;
  contributor?: Record<string, string>;
  info: ModelInfoJson;
  position?: GeoJsonGeometry;
  review?: ReviewJson;
  campaign: string;
  icons?: string[];
  pictures?: string[];
  sounds?: string[];
}

export class CampaignAnswerModel {
  private json: CampaignAnswerJson;
  private _isDirty: boolean = false;

  constructor(json: CampaignAnswerJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      attributes: [],
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      campaign: "",
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: Json
  get attributes(): Record<string, any> {
    return this.json.attributes;
  }

  set attributes(newValue: Record<string, any>) {
    this._isDirty = true;
    this.json.attributes = newValue;
  }

  /// field: int
  get status(): number | undefined {
    return this.json.status;
  }

  set status(newValue: number | undefined) {
    this._isDirty = true;
    this.json.status = newValue;
  }

  /// field: string
  get featureId(): string | undefined {
    return this.json.featureId;
  }

  set featureId(newValue: string | undefined) {
    this._isDirty = true;
    this.json.featureId = newValue;
  }

  /// field: string
  get contributor(): Record<string, string> | undefined {
    return this.json.contributor;
  }

  set contributor(newValue: Record<string, string> | undefined) {
    this._isDirty = true;
    this.json.contributor = newValue;
  }

  /// object: ModelInfo
  get info(): ModelInfoModel {
    return new ModelInfoModel(this.json.info);
  }

  set info(newValue: ModelInfoModel) {
    this._isDirty = true;

    this.json.info = newValue?.toJSON();
  }

  /// object: GeoJsonGeometry
  get position(): GeoJsonGeometry {
    return this.json.position;
  }

  set position(newValue: GeoJsonGeometry | undefined) {
    this._isDirty = true;

    this.json.position = newValue;
  }

  /// object: Review
  get review(): ReviewModel {
    return new ReviewModel(this.json.review);
  }

  set review(newValue: ReviewModel | undefined) {
    this._isDirty = true;

    this.json.review = newValue?.toJSON();
  }

  /// model: Campaign
  get campaign(): Promise<CampaignModel> {
    return Store.instance.findCampaign(this.json.campaign);
  }

  set campaign(newValue: CampaignModel) {
    this._isDirty = true;
    this.json.campaign = newValue?._id;
  }

  /// model: Icon
  get icons(): Promise<IconModel[] | undefined> {
    const promises =
      this.json.icons?.map((a) => Store.instance.findIcon(a)) ?? [];

    return Promise.all(promises);
  }

  set icons(newValue: IconModel[] | undefined) {
    this._isDirty = true;
    this.json.icons = newValue?.map((a) => a._id) ?? [];
  }

  /// model: Picture
  get pictures(): Promise<PictureModel[] | undefined> {
    const promises =
      this.json.pictures?.map((a) => Store.instance.findPicture(a)) ?? [];

    return Promise.all(promises);
  }

  set pictures(newValue: PictureModel[] | undefined) {
    this._isDirty = true;
    this.json.pictures = newValue?.map((a) => a._id) ?? [];
  }

  /// model: Sound
  get sounds(): Promise<SoundModel[] | undefined> {
    const promises =
      this.json.sounds?.map((a) => Store.instance.findSound(a)) ?? [];

    return Promise.all(promises);
  }

  set sounds(newValue: SoundModel[] | undefined) {
    this._isDirty = true;
    this.json.sounds = newValue?.map((a) => a._id) ?? [];
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.info?.isDirty) {
      return true;
    }

    if (this.review?.isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<CampaignAnswerModel> {
    let result: CampaignAnswerModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createCampaignAnswer(this);
    } else {
      result = await Store.instance.saveCampaignAnswer(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): CampaignAnswerJson {
    return this.json;
  }
}
