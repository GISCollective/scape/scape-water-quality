import {
  OptionalIconImage,
  OptionalIconImageJson,
  OptionalIconImageModel,
} from "./OptionalIconImage";
import { IconSet, IconSetModel } from "./IconSet";
import { Visibility, VisibilityJson, VisibilityModel } from "./Visibility";
import {
  IconMeasurements,
  IconMeasurementsJson,
  IconMeasurementsModel,
} from "./IconMeasurements";
import {
  IconAttribute,
  IconAttributeJson,
  IconAttributeModel,
} from "./IconAttribute";
import {
  InheritedStyle,
  InheritedStyleJson,
  InheritedStyleModel,
} from "./InheritedStyle";
import { Store } from "./store";
import { ArticleBody } from "./ArticleBody";

export interface Icon {
  _id: string;
  category?: string;
  subcategory?: string;
  name: string;
  description?: ArticleBody;
  verticalIndex?: number;
  minZoom?: number;
  maxZoom?: number;
  parent?: string;
  allowMany?: boolean;
  keepWhenSmall?: boolean;
  otherNames?: string[];
  localName?: string;
  enforceSvg?: boolean;
  order?: number;
  image: OptionalIconImage;
  attributes: IconAttribute[];
  styles?: InheritedStyle;
  measurements?: IconMeasurements;
  visibility?: Visibility;
  iconSet: IconSet;
}

export interface IconResponse {
  icon: IconJson;
}

export interface IconsResponse {
  icons: IconJson[];
}

export interface IconJson {
  _id: string;
  category?: string;
  subcategory?: string;
  name: string;
  description?: ArticleBody;
  verticalIndex?: number;
  minZoom?: number;
  maxZoom?: number;
  parent?: string;
  allowMany?: boolean;
  keepWhenSmall?: boolean;
  otherNames?: string[];
  localName?: string;
  enforceSvg?: boolean;
  order?: number;
  image: OptionalIconImageJson;
  attributes: IconAttributeJson[];
  styles?: InheritedStyleJson;
  measurements?: IconMeasurementsJson;
  visibility?: VisibilityJson;
  iconSet: string;
}

export class IconModel {
  private json: IconJson;
  private _isDirty: boolean = false;

  constructor(json: IconJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      name: "",
      image: {},
      attributes: [],
      iconSet: "",
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get category(): string | undefined {
    return this.json.category;
  }

  set category(newValue: string | undefined) {
    this._isDirty = true;
    this.json.category = newValue;
  }

  /// field: string
  get subcategory(): string | undefined {
    return this.json.subcategory;
  }

  set subcategory(newValue: string | undefined) {
    this._isDirty = true;
    this.json.subcategory = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: Json
  get description(): ArticleBody | undefined {
    return this.json.description;
  }

  set description(newValue: ArticleBody | undefined) {
    this._isDirty = true;
    this.json.description = newValue;
  }

  /// field: int
  get verticalIndex(): number | undefined {
    return this.json.verticalIndex;
  }

  set verticalIndex(newValue: number | undefined) {
    this._isDirty = true;
    this.json.verticalIndex = newValue;
  }

  /// field: uint
  get minZoom(): number | undefined {
    return this.json.minZoom;
  }

  set minZoom(newValue: number | undefined) {
    this._isDirty = true;
    this.json.minZoom = newValue;
  }

  /// field: uint
  get maxZoom(): number | undefined {
    return this.json.maxZoom;
  }

  set maxZoom(newValue: number | undefined) {
    this._isDirty = true;
    this.json.maxZoom = newValue;
  }

  /// field: string
  get parent(): string | undefined {
    return this.json.parent;
  }

  set parent(newValue: string | undefined) {
    this._isDirty = true;
    this.json.parent = newValue;
  }

  /// field: bool
  get allowMany(): boolean | undefined {
    return this.json.allowMany;
  }

  set allowMany(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.allowMany = newValue;
  }

  /// field: bool
  get keepWhenSmall(): boolean | undefined {
    return this.json.keepWhenSmall;
  }

  set keepWhenSmall(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.keepWhenSmall = newValue;
  }

  /// field: string
  get otherNames(): string[] | undefined {
    return this.json.otherNames;
  }

  set otherNames(newValue: string[] | undefined) {
    this._isDirty = true;
    this.json.otherNames = newValue;
  }

  /// field: string
  get localName(): string | undefined {
    return this.json.localName;
  }

  set localName(newValue: string | undefined) {
    this._isDirty = true;
    this.json.localName = newValue;
  }

  /// field: bool
  get enforceSvg(): boolean | undefined {
    return this.json.enforceSvg;
  }

  set enforceSvg(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.enforceSvg = newValue;
  }

  /// field: long
  get order(): number | undefined {
    return this.json.order;
  }

  set order(newValue: number | undefined) {
    this._isDirty = true;
    this.json.order = newValue;
  }

  /// object: OptionalIconImage
  get image(): OptionalIconImageModel {
    return new OptionalIconImageModel(this.json.image);
  }

  set image(newValue: OptionalIconImageModel) {
    this._isDirty = true;

    this.json.image = newValue?.toJSON();
  }

  /// object: IconAttribute
  get attributes(): IconAttributeModel[] {
    return this.json.attributes?.map((a) => new IconAttributeModel(a)) ?? [];
  }

  set attributes(newValue: IconAttributeModel[]) {
    this._isDirty = true;

    this.json.attributes = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// object: InheritedStyle
  get styles(): InheritedStyleModel {
    return new InheritedStyleModel(this.json.styles);
  }

  set styles(newValue: InheritedStyleModel | undefined) {
    this._isDirty = true;

    this.json.styles = newValue?.toJSON();
  }

  /// object: IconMeasurements
  get measurements(): IconMeasurementsModel {
    return new IconMeasurementsModel(this.json.measurements);
  }

  set measurements(newValue: IconMeasurementsModel | undefined) {
    this._isDirty = true;

    this.json.measurements = newValue?.toJSON();
  }

  /// object: Visibility
  get visibility(): VisibilityModel {
    return new VisibilityModel(this.json.visibility);
  }

  set visibility(newValue: VisibilityModel | undefined) {
    this._isDirty = true;

    this.json.visibility = newValue?.toJSON();
  }

  /// model: IconSet
  get iconSet(): Promise<IconSetModel> {
    return Store.instance.findIconSet(this.json.iconSet);
  }

  set iconSet(newValue: IconSetModel) {
    this._isDirty = true;
    this.json.iconSet = newValue?._id;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.image?.isDirty) {
      return true;
    }

    for (let level0 of this.attributes) {
      if (level0?.isDirty) {
        return true;
      }
    }

    if (this.styles?.isDirty) {
      return true;
    }

    if (this.measurements?.isDirty) {
      return true;
    }

    if (this.visibility?.isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<IconModel> {
    let result: IconModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createIcon(this);
    } else {
      result = await Store.instance.saveIcon(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): IconJson {
    return this.json;
  }
}
