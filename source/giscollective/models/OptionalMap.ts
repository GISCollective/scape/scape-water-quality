import { Store } from "./store";

export interface OptionalMap {
  isEnabled: boolean;
  map: string;
}

export interface OptionalMapJson {
  isEnabled: boolean;
  map: string;
}

export class OptionalMapModel {
  private json: OptionalMapJson;
  private _isDirty: boolean = false;

  constructor(json: OptionalMapJson | null | undefined) {
    this.json = json ?? { isEnabled: false, map: "" };
  }

  /// field: bool
  get isEnabled(): boolean {
    return this.json.isEnabled;
  }

  set isEnabled(newValue: boolean) {
    this._isDirty = true;
    this.json.isEnabled = newValue;
  }

  /// field: string
  get map(): string {
    return this.json.map;
  }

  set map(newValue: string) {
    this._isDirty = true;
    this.json.map = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): OptionalMapJson {
    return this.json;
  }
}
