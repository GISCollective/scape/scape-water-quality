import { Store } from "./store";

export interface Translation {
  _id: string;
  name: string;
  locale: string;
  customTranslations?: Record<string, string>;
  file?: string;
}

export interface TranslationResponse {
  translation: TranslationJson;
}

export interface TranslationsResponse {
  translations: TranslationJson[];
}

export interface TranslationJson {
  _id: string;
  name: string;
  locale: string;
  customTranslations?: Record<string, string>;
  file?: string;
}

export class TranslationModel {
  private json: TranslationJson;
  private _isDirty: boolean = false;

  constructor(json: TranslationJson | null | undefined) {
    this.json = json ?? { _id: "", name: "", locale: "" };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: string
  get locale(): string {
    return this.json.locale;
  }

  set locale(newValue: string) {
    this._isDirty = true;
    this.json.locale = newValue;
  }

  /// field: string
  get customTranslations(): Record<string, string> | undefined {
    return this.json.customTranslations;
  }

  set customTranslations(newValue: Record<string, string> | undefined) {
    this._isDirty = true;
    this.json.customTranslations = newValue;
  }

  /// object: string
  get file(): string | undefined {
    return this.json.file;
  }

  set file(newValue: string | undefined) {
    this._isDirty = true;
    this.json.file = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.file?.isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<TranslationModel> {
    let result: TranslationModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createTranslation(this);
    } else {
      result = await Store.instance.saveTranslation(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): TranslationJson {
    return this.json;
  }
}
