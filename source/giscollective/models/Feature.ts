import {
  FeatureDecorators,
  FeatureDecoratorsJson,
  FeatureDecoratorsModel,
} from "./FeatureDecorators";
import { Sound, SoundModel } from "./Sound";
import { Source, SourceJson, SourceModel } from "./Source";
import { Visibility, VisibilityJson, VisibilityModel } from "./Visibility";
import { Icon, IconModel } from "./Icon";
import { Store } from "./store";
import { ModelInfo, ModelInfoJson, ModelInfoModel } from "./ModelInfo";
import { ArticleBody } from "./ArticleBody";
import { GeoJsonGeometry } from "./GeoJsonGeometry";
import { Map, MapModel } from "./Map";
import { Picture, PictureModel } from "./Picture";

export interface Feature {
  _id: string;
  name: string;
  description: ArticleBody;
  visibility?: number;
  contributors?: string[];
  attributes?: Record<string, any>;
  position: GeoJsonGeometry;
  info?: ModelInfo;
  decorators?: FeatureDecorators;
  source?: Source;
  computedVisibility?: Visibility;
  maps: Map[];
  pictures?: Picture[];
  sounds?: Sound[];
  icons?: Icon[];
}

export interface FeatureResponse {
  feature: FeatureJson;
}

export interface FeaturesResponse {
  features: FeatureJson[];
}

export interface FeatureJson {
  _id: string;
  name: string;
  description: ArticleBody;
  visibility?: number;
  contributors?: string[];
  attributes?: Record<string, any>;
  position: GeoJsonGeometry;
  info?: ModelInfoJson;
  decorators?: FeatureDecoratorsJson;
  source?: SourceJson;
  computedVisibility?: VisibilityJson;
  maps: string[];
  pictures?: string[];
  sounds?: string[];
  icons?: string[];
}

export class FeatureModel {
  private json: FeatureJson;
  private _isDirty: boolean = false;

  constructor(json: FeatureJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      name: "",
      description: { blocks: [] },
      position: { type: "Point", coordinates: [0, 0] },
      maps: [],
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: Json
  get description(): ArticleBody {
    return this.json.description;
  }

  set description(newValue: ArticleBody) {
    this._isDirty = true;
    this.json.description = newValue;
  }

  /// field: int
  get visibility(): number | undefined {
    return this.json.visibility;
  }

  set visibility(newValue: number | undefined) {
    this._isDirty = true;
    this.json.visibility = newValue;
  }

  /// field: string
  get contributors(): string[] | undefined {
    return this.json.contributors;
  }

  set contributors(newValue: string[] | undefined) {
    this._isDirty = true;
    this.json.contributors = newValue;
  }

  /// field: Json
  get attributes(): Record<string, any> | undefined {
    return this.json.attributes;
  }

  set attributes(newValue: Record<string, any> | undefined) {
    this._isDirty = true;
    this.json.attributes = newValue;
  }

  /// object: GeoJsonGeometry
  get position(): GeoJsonGeometry {
    return this.json.position;
  }

  set position(newValue: GeoJsonGeometry) {
    this._isDirty = true;

    this.json.position = newValue;
  }

  /// object: ModelInfo
  get info(): ModelInfoModel {
    return new ModelInfoModel(this.json.info);
  }

  set info(newValue: ModelInfoModel | undefined) {
    this._isDirty = true;

    this.json.info = newValue?.toJSON();
  }

  /// object: FeatureDecorators
  get decorators(): FeatureDecoratorsModel {
    return new FeatureDecoratorsModel(this.json.decorators);
  }

  set decorators(newValue: FeatureDecoratorsModel | undefined) {
    this._isDirty = true;

    this.json.decorators = newValue?.toJSON();
  }

  /// object: Source
  get source(): SourceModel {
    return new SourceModel(this.json.source);
  }

  set source(newValue: SourceModel | undefined) {
    this._isDirty = true;

    this.json.source = newValue?.toJSON();
  }

  /// object: Visibility
  get computedVisibility(): VisibilityModel {
    return new VisibilityModel(this.json.computedVisibility);
  }

  set computedVisibility(newValue: VisibilityModel | undefined) {
    this._isDirty = true;

    this.json.computedVisibility = newValue?.toJSON();
  }

  /// model: Map
  get maps(): Promise<MapModel[]> {
    const promises =
      this.json.maps?.map((a) => Store.instance.findMap(a)) ?? [];

    return Promise.all(promises);
  }

  set maps(newValue: MapModel[]) {
    this._isDirty = true;
    this.json.maps = newValue?.map((a) => a._id) ?? [];
  }

  /// model: Picture
  get pictures(): Promise<PictureModel[] | undefined> {
    const promises =
      this.json.pictures?.map((a) => Store.instance.findPicture(a)) ?? [];

    return Promise.all(promises);
  }

  set pictures(newValue: PictureModel[] | undefined) {
    this._isDirty = true;
    this.json.pictures = newValue?.map((a) => a._id) ?? [];
  }

  /// model: Sound
  get sounds(): Promise<SoundModel[] | undefined> {
    const promises =
      this.json.sounds?.map((a) => Store.instance.findSound(a)) ?? [];

    return Promise.all(promises);
  }

  set sounds(newValue: SoundModel[] | undefined) {
    this._isDirty = true;
    this.json.sounds = newValue?.map((a) => a._id) ?? [];
  }

  /// model: Icon
  get icons(): Promise<IconModel[] | undefined> {
    const promises =
      this.json.icons?.map((a) => Store.instance.findIcon(a)) ?? [];

    return Promise.all(promises);
  }

  set icons(newValue: IconModel[] | undefined) {
    this._isDirty = true;
    this.json.icons = newValue?.map((a) => a._id) ?? [];
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.info?.isDirty) {
      return true;
    }

    if (this.decorators?.isDirty) {
      return true;
    }

    if (this.source?.isDirty) {
      return true;
    }

    if (this.computedVisibility?.isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<FeatureModel> {
    let result: FeatureModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createFeature(this);
    } else {
      result = await Store.instance.saveFeature(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): FeatureJson {
    return this.json;
  }
}
