import { DateTime } from "luxon";
import { Store } from "./store";

export interface Issue {
  _id: string;
  feature: string;
  author: string;
  title: string;
  description?: string;
  attributions?: string;
  other?: string;
  assignee?: string;
  processed?: boolean;
  status?: string;
  type?: string;
  creationDate: DateTime;
  file?: string;
  resolveDate?: DateTime;
}

export interface IssueResponse {
  issue: IssueJson;
}

export interface IssuesResponse {
  issues: IssueJson[];
}

export interface IssueJson {
  _id: string;
  feature: string;
  author: string;
  title: string;
  description?: string;
  attributions?: string;
  other?: string;
  assignee?: string;
  processed?: boolean;
  status?: string;
  type?: string;
  creationDate: string;
  file?: string;
  resolveDate?: string;
}

export class IssueModel {
  private json: IssueJson;
  private _isDirty: boolean = false;

  constructor(json: IssueJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      feature: "",
      author: "",
      title: "",
      creationDate: "",
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get feature(): string {
    return this.json.feature;
  }

  set feature(newValue: string) {
    this._isDirty = true;
    this.json.feature = newValue;
  }

  /// field: string
  get author(): string {
    return this.json.author;
  }

  set author(newValue: string) {
    this._isDirty = true;
    this.json.author = newValue;
  }

  /// field: string
  get title(): string {
    return this.json.title;
  }

  set title(newValue: string) {
    this._isDirty = true;
    this.json.title = newValue;
  }

  /// field: string
  get description(): string | undefined {
    return this.json.description;
  }

  set description(newValue: string | undefined) {
    this._isDirty = true;
    this.json.description = newValue;
  }

  /// field: string
  get attributions(): string | undefined {
    return this.json.attributions;
  }

  set attributions(newValue: string | undefined) {
    this._isDirty = true;
    this.json.attributions = newValue;
  }

  /// field: string
  get other(): string | undefined {
    return this.json.other;
  }

  set other(newValue: string | undefined) {
    this._isDirty = true;
    this.json.other = newValue;
  }

  /// field: string
  get assignee(): string | undefined {
    return this.json.assignee;
  }

  set assignee(newValue: string | undefined) {
    this._isDirty = true;
    this.json.assignee = newValue;
  }

  /// field: bool
  get processed(): boolean | undefined {
    return this.json.processed;
  }

  set processed(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.processed = newValue;
  }

  /// field: string
  get status(): string | undefined {
    return this.json.status;
  }

  set status(newValue: string | undefined) {
    this._isDirty = true;
    this.json.status = newValue;
  }

  /// field: string
  get type(): string | undefined {
    return this.json.type;
  }

  set type(newValue: string | undefined) {
    this._isDirty = true;
    this.json.type = newValue;
  }

  /// object: SysTime
  get creationDate(): DateTime {
    return DateTime.fromISO(this.json.creationDate);
  }

  set creationDate(newValue: DateTime) {
    this._isDirty = true;
    this.json.creationDate = newValue.toISO();
  }

  /// object: string
  get file(): string | undefined {
    return this.json.file;
  }

  set file(newValue: string | undefined) {
    this._isDirty = true;
    this.json.file = newValue;
  }

  /// object: SysTime
  get resolveDate(): DateTime | undefined {
    if (!this.json.resolveDate) {
      return undefined;
    }
    return DateTime.fromISO(this.json.resolveDate);
  }

  set resolveDate(newValue: DateTime | undefined) {
    this._isDirty = true;
    this.json.resolveDate = newValue?.toISO() ?? undefined;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.file?.isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<IssueModel> {
    let result: IssueModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createIssue(this);
    } else {
      result = await Store.instance.saveIssue(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): IssueJson {
    return this.json;
  }
}
