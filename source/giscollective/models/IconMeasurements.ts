import { Store } from "./store";

export interface IconMeasurements {
  width?: number;
  height?: number;
  content?: number[];
  stretchX?: number[][];
  stretchY?: number[][];
}

export interface IconMeasurementsJson {
  width?: number;
  height?: number;
  content?: number[];
  stretchX?: number[][];
  stretchY?: number[][];
}

export class IconMeasurementsModel {
  private json: IconMeasurementsJson;
  private _isDirty: boolean = false;

  constructor(json: IconMeasurementsJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: uint
  get width(): number | undefined {
    return this.json.width;
  }

  set width(newValue: number | undefined) {
    this._isDirty = true;
    this.json.width = newValue;
  }

  /// field: uint
  get height(): number | undefined {
    return this.json.height;
  }

  set height(newValue: number | undefined) {
    this._isDirty = true;
    this.json.height = newValue;
  }

  /// field: int
  get content(): number[] | undefined {
    return this.json.content;
  }

  set content(newValue: number[] | undefined) {
    this._isDirty = true;
    this.json.content = newValue;
  }

  /// field: int
  get stretchX(): number[][] | undefined {
    return this.json.stretchX;
  }

  set stretchX(newValue: number[][] | undefined) {
    this._isDirty = true;
    this.json.stretchX = newValue;
  }

  /// field: int
  get stretchY(): number[][] | undefined {
    return this.json.stretchY;
  }

  set stretchY(newValue: number[][] | undefined) {
    this._isDirty = true;
    this.json.stretchY = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): IconMeasurementsJson {
    return this.json;
  }
}
