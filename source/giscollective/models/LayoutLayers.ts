import { Store } from "./store";

export interface LayoutLayers {
  count: number;
  showContentAfter: number;
  effect: string;
}

export interface LayoutLayersJson {
  count: number;
  showContentAfter: number;
  effect: string;
}

export class LayoutLayersModel {
  private json: LayoutLayersJson;
  private _isDirty: boolean = false;

  constructor(json: LayoutLayersJson | null | undefined) {
    this.json = json ?? { count: 0, showContentAfter: 0, effect: "" };
  }

  /// field: uint
  get count(): number {
    return this.json.count;
  }

  set count(newValue: number) {
    this._isDirty = true;
    this.json.count = newValue;
  }

  /// field: uint
  get showContentAfter(): number {
    return this.json.showContentAfter;
  }

  set showContentAfter(newValue: number) {
    this._isDirty = true;
    this.json.showContentAfter = newValue;
  }

  /// field: string
  get effect(): string {
    return this.json.effect;
  }

  set effect(newValue: string) {
    this._isDirty = true;
    this.json.effect = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): LayoutLayersJson {
    return this.json;
  }
}
