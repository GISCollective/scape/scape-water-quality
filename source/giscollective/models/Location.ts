import {
  DetailedLocation,
  DetailedLocationJson,
  DetailedLocationModel,
} from "./DetailedLocation";
import { Store } from "./store";

export interface Location {
  isDetailed: boolean;
  simple: string;
  detailedLocation?: DetailedLocation;
}

export interface LocationJson {
  isDetailed: boolean;
  simple: string;
  detailedLocation?: DetailedLocationJson;
}

export class LocationModel {
  private json: LocationJson;
  private _isDirty: boolean = false;

  constructor(json: LocationJson | null | undefined) {
    this.json = json ?? { isDetailed: false, simple: "" };
  }

  /// field: bool
  get isDetailed(): boolean {
    return this.json.isDetailed;
  }

  set isDetailed(newValue: boolean) {
    this._isDirty = true;
    this.json.isDetailed = newValue;
  }

  /// field: string
  get simple(): string {
    return this.json.simple;
  }

  set simple(newValue: string) {
    this._isDirty = true;
    this.json.simple = newValue;
  }

  /// object: DetailedLocation
  get detailedLocation(): DetailedLocationModel {
    return new DetailedLocationModel(this.json.detailedLocation);
  }

  set detailedLocation(newValue: DetailedLocationModel | undefined) {
    this._isDirty = true;

    this.json.detailedLocation = newValue?.toJSON();
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.detailedLocation?.isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): LocationJson {
    return this.json;
  }
}
