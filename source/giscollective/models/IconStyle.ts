import {
  LabelStyleProperties,
  LabelStylePropertiesJson,
  LabelStylePropertiesModel,
} from "./LabelStyleProperties";
import {
  LineStyleProperties,
  LineStylePropertiesJson,
  LineStylePropertiesModel,
} from "./LineStyleProperties";
import {
  PolygonStyleProperties,
  PolygonStylePropertiesJson,
  PolygonStylePropertiesModel,
} from "./PolygonStyleProperties";
import {
  PointStyleProperties,
  PointStylePropertiesJson,
  PointStylePropertiesModel,
} from "./PointStyleProperties";
import { Store } from "./store";

export interface IconStyle {
  site?: PointStyleProperties;
  siteLabel?: LabelStyleProperties;
  line?: LineStyleProperties;
  lineMarker?: PointStyleProperties;
  lineLabel?: LabelStyleProperties;
  polygon?: PolygonStyleProperties;
  polygonMarker?: PointStyleProperties;
  polygonLabel?: LabelStyleProperties;
}

export interface IconStyleJson {
  site?: PointStylePropertiesJson;
  siteLabel?: LabelStylePropertiesJson;
  line?: LineStylePropertiesJson;
  lineMarker?: PointStylePropertiesJson;
  lineLabel?: LabelStylePropertiesJson;
  polygon?: PolygonStylePropertiesJson;
  polygonMarker?: PointStylePropertiesJson;
  polygonLabel?: LabelStylePropertiesJson;
}

export class IconStyleModel {
  private json: IconStyleJson;
  private _isDirty: boolean = false;

  constructor(json: IconStyleJson | null | undefined) {
    this.json = json ?? {};
  }

  /// object: PointStyleProperties
  get site(): PointStylePropertiesModel {
    return new PointStylePropertiesModel(this.json.site);
  }

  set site(newValue: PointStylePropertiesModel | undefined) {
    this._isDirty = true;

    this.json.site = newValue?.toJSON();
  }

  /// object: LabelStyleProperties
  get siteLabel(): LabelStylePropertiesModel {
    return new LabelStylePropertiesModel(this.json.siteLabel);
  }

  set siteLabel(newValue: LabelStylePropertiesModel | undefined) {
    this._isDirty = true;

    this.json.siteLabel = newValue?.toJSON();
  }

  /// object: LineStyleProperties
  get line(): LineStylePropertiesModel {
    return new LineStylePropertiesModel(this.json.line);
  }

  set line(newValue: LineStylePropertiesModel | undefined) {
    this._isDirty = true;

    this.json.line = newValue?.toJSON();
  }

  /// object: PointStyleProperties
  get lineMarker(): PointStylePropertiesModel {
    return new PointStylePropertiesModel(this.json.lineMarker);
  }

  set lineMarker(newValue: PointStylePropertiesModel | undefined) {
    this._isDirty = true;

    this.json.lineMarker = newValue?.toJSON();
  }

  /// object: LabelStyleProperties
  get lineLabel(): LabelStylePropertiesModel {
    return new LabelStylePropertiesModel(this.json.lineLabel);
  }

  set lineLabel(newValue: LabelStylePropertiesModel | undefined) {
    this._isDirty = true;

    this.json.lineLabel = newValue?.toJSON();
  }

  /// object: PolygonStyleProperties
  get polygon(): PolygonStylePropertiesModel {
    return new PolygonStylePropertiesModel(this.json.polygon);
  }

  set polygon(newValue: PolygonStylePropertiesModel | undefined) {
    this._isDirty = true;

    this.json.polygon = newValue?.toJSON();
  }

  /// object: PointStyleProperties
  get polygonMarker(): PointStylePropertiesModel {
    return new PointStylePropertiesModel(this.json.polygonMarker);
  }

  set polygonMarker(newValue: PointStylePropertiesModel | undefined) {
    this._isDirty = true;

    this.json.polygonMarker = newValue?.toJSON();
  }

  /// object: LabelStyleProperties
  get polygonLabel(): LabelStylePropertiesModel {
    return new LabelStylePropertiesModel(this.json.polygonLabel);
  }

  set polygonLabel(newValue: LabelStylePropertiesModel | undefined) {
    this._isDirty = true;

    this.json.polygonLabel = newValue?.toJSON();
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.site?.isDirty) {
      return true;
    }

    if (this.siteLabel?.isDirty) {
      return true;
    }

    if (this.line?.isDirty) {
      return true;
    }

    if (this.lineMarker?.isDirty) {
      return true;
    }

    if (this.lineLabel?.isDirty) {
      return true;
    }

    if (this.polygon?.isDirty) {
      return true;
    }

    if (this.polygonMarker?.isDirty) {
      return true;
    }

    if (this.polygonLabel?.isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): IconStyleJson {
    return this.json;
  }
}
