import { Store } from "./store";

export interface PolygonStyleProperties {
  hideBackgroundOnZoom?: boolean;
  showAsLineAfterZoom?: number;
  borderColor?: string;
  backgroundColor?: string;
  borderWidth?: number;
  lineDash?: number[];
}

export interface PolygonStylePropertiesJson {
  hideBackgroundOnZoom?: boolean;
  showAsLineAfterZoom?: number;
  borderColor?: string;
  backgroundColor?: string;
  borderWidth?: number;
  lineDash?: number[];
}

export class PolygonStylePropertiesModel {
  private json: PolygonStylePropertiesJson;
  private _isDirty: boolean = false;

  constructor(json: PolygonStylePropertiesJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: bool
  get hideBackgroundOnZoom(): boolean | undefined {
    return this.json.hideBackgroundOnZoom;
  }

  set hideBackgroundOnZoom(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.hideBackgroundOnZoom = newValue;
  }

  /// field: uint
  get showAsLineAfterZoom(): number | undefined {
    return this.json.showAsLineAfterZoom;
  }

  set showAsLineAfterZoom(newValue: number | undefined) {
    this._isDirty = true;
    this.json.showAsLineAfterZoom = newValue;
  }

  /// field: string
  get borderColor(): string | undefined {
    return this.json.borderColor;
  }

  set borderColor(newValue: string | undefined) {
    this._isDirty = true;
    this.json.borderColor = newValue;
  }

  /// field: string
  get backgroundColor(): string | undefined {
    return this.json.backgroundColor;
  }

  set backgroundColor(newValue: string | undefined) {
    this._isDirty = true;
    this.json.backgroundColor = newValue;
  }

  /// field: uint
  get borderWidth(): number | undefined {
    return this.json.borderWidth;
  }

  set borderWidth(newValue: number | undefined) {
    this._isDirty = true;
    this.json.borderWidth = newValue;
  }

  /// field: uint
  get lineDash(): number[] | undefined {
    return this.json.lineDash;
  }

  set lineDash(newValue: number[] | undefined) {
    this._isDirty = true;
    this.json.lineDash = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): PolygonStylePropertiesJson {
    return this.json;
  }
}
