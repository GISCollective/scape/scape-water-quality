export type ArticleBody = ArticleBodyJson | string;

export interface ArticleBodyJson {
  blocks: ArticleBlock[];
}

export interface ArticleBlock {
  type: string;
  data?: Record<string, any>;
}
