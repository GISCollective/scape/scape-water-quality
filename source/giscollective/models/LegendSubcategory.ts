import { LegendIcon, LegendIconJson, LegendIconModel } from "./LegendIcon";
import { Store } from "./store";

export interface LegendSubcategory {
  name: string;
  uid: string;
  count: number;
  icons: LegendIcon[];
}

export interface LegendSubcategoryJson {
  name: string;
  uid: string;
  count: number;
  icons: LegendIconJson[];
}

export class LegendSubcategoryModel {
  private json: LegendSubcategoryJson;
  private _isDirty: boolean = false;

  constructor(json: LegendSubcategoryJson | null | undefined) {
    this.json = json ?? { name: "", uid: "", count: 0, icons: [] };
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: string
  get uid(): string {
    return this.json.uid;
  }

  set uid(newValue: string) {
    this._isDirty = true;
    this.json.uid = newValue;
  }

  /// field: ulong
  get count(): number {
    return this.json.count;
  }

  set count(newValue: number) {
    this._isDirty = true;
    this.json.count = newValue;
  }

  /// object: LegendIcon
  get icons(): LegendIconModel[] {
    return this.json.icons?.map((a) => new LegendIconModel(a)) ?? [];
  }

  set icons(newValue: LegendIconModel[]) {
    this._isDirty = true;

    this.json.icons = newValue?.map((a) => a.toJSON()) ?? [];
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    for (let level0 of this.icons) {
      if (level0?.isDirty) {
        return true;
      }
    }

    return false;
  }

  toJSON(): LegendSubcategoryJson {
    return this.json;
  }
}
