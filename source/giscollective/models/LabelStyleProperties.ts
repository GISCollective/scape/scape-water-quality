import { Store } from "./store";

export interface LabelStyleProperties {
  isVisible?: boolean;
  text?: string;
  align?: string;
  baseline?: string;
  weight?: string;
  color?: string;
  borderColor?: string;
  borderWidth?: number;
  size?: number;
  lineHeight?: number;
  offsetX?: number;
  offsetY?: number;
}

export interface LabelStylePropertiesJson {
  isVisible?: boolean;
  text?: string;
  align?: string;
  baseline?: string;
  weight?: string;
  color?: string;
  borderColor?: string;
  borderWidth?: number;
  size?: number;
  lineHeight?: number;
  offsetX?: number;
  offsetY?: number;
}

export class LabelStylePropertiesModel {
  private json: LabelStylePropertiesJson;
  private _isDirty: boolean = false;

  constructor(json: LabelStylePropertiesJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: bool
  get isVisible(): boolean | undefined {
    return this.json.isVisible;
  }

  set isVisible(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.isVisible = newValue;
  }

  /// field: string
  get text(): string | undefined {
    return this.json.text;
  }

  set text(newValue: string | undefined) {
    this._isDirty = true;
    this.json.text = newValue;
  }

  /// field: string
  get align(): string | undefined {
    return this.json.align;
  }

  set align(newValue: string | undefined) {
    this._isDirty = true;
    this.json.align = newValue;
  }

  /// field: string
  get baseline(): string | undefined {
    return this.json.baseline;
  }

  set baseline(newValue: string | undefined) {
    this._isDirty = true;
    this.json.baseline = newValue;
  }

  /// field: string
  get weight(): string | undefined {
    return this.json.weight;
  }

  set weight(newValue: string | undefined) {
    this._isDirty = true;
    this.json.weight = newValue;
  }

  /// field: string
  get color(): string | undefined {
    return this.json.color;
  }

  set color(newValue: string | undefined) {
    this._isDirty = true;
    this.json.color = newValue;
  }

  /// field: string
  get borderColor(): string | undefined {
    return this.json.borderColor;
  }

  set borderColor(newValue: string | undefined) {
    this._isDirty = true;
    this.json.borderColor = newValue;
  }

  /// field: int
  get borderWidth(): number | undefined {
    return this.json.borderWidth;
  }

  set borderWidth(newValue: number | undefined) {
    this._isDirty = true;
    this.json.borderWidth = newValue;
  }

  /// field: int
  get size(): number | undefined {
    return this.json.size;
  }

  set size(newValue: number | undefined) {
    this._isDirty = true;
    this.json.size = newValue;
  }

  /// field: int
  get lineHeight(): number | undefined {
    return this.json.lineHeight;
  }

  set lineHeight(newValue: number | undefined) {
    this._isDirty = true;
    this.json.lineHeight = newValue;
  }

  /// field: int
  get offsetX(): number | undefined {
    return this.json.offsetX;
  }

  set offsetX(newValue: number | undefined) {
    this._isDirty = true;
    this.json.offsetX = newValue;
  }

  /// field: int
  get offsetY(): number | undefined {
    return this.json.offsetY;
  }

  set offsetY(newValue: number | undefined) {
    this._isDirty = true;
    this.json.offsetY = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): LabelStylePropertiesJson {
    return this.json;
  }
}
