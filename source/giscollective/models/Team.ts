import { Picture, PictureModel } from "./Picture";
import {
  SubscriptionDetails,
  SubscriptionDetailsJson,
  SubscriptionDetailsModel,
} from "./SubscriptionDetails";
import { ArticleBody } from "./ArticleBody";
import { Invitation, InvitationJson, InvitationModel } from "./Invitation";
import {
  SocialMediaLinks,
  SocialMediaLinksJson,
  SocialMediaLinksModel,
} from "./SocialMediaLinks";
import { Store } from "./store";

export interface Team {
  _id: string;
  name: string;
  about: ArticleBody;
  owners: string[];
  leaders: string[];
  members: string[];
  guests: string[];
  isPublic: boolean;
  isDefault?: boolean;
  isPublisher?: boolean;
  allowCustomDataBindings?: boolean;
  allowNewsletters?: boolean;
  allowEvents?: boolean;
  allowReports?: boolean;
  categories?: string[];
  contactEmail?: string;
  subscription?: SubscriptionDetails;
  socialMediaLinks?: SocialMediaLinks;
  invitations?: Invitation[];
  logo?: Picture;
  pictures?: Picture[];
}

export interface TeamResponse {
  team: TeamJson;
}

export interface TeamsResponse {
  teams: TeamJson[];
}

export interface TeamJson {
  _id: string;
  name: string;
  about: ArticleBody;
  owners: string[];
  leaders: string[];
  members: string[];
  guests: string[];
  isPublic: boolean;
  isDefault?: boolean;
  isPublisher?: boolean;
  allowCustomDataBindings?: boolean;
  allowNewsletters?: boolean;
  allowEvents?: boolean;
  allowReports?: boolean;
  categories?: string[];
  contactEmail?: string;
  subscription?: SubscriptionDetailsJson;
  socialMediaLinks?: SocialMediaLinksJson;
  invitations?: InvitationJson[];
  logo?: string;
  pictures?: string[];
}

export class TeamModel {
  private json: TeamJson;
  private _isDirty: boolean = false;

  constructor(json: TeamJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      name: "",
      about: { blocks: [] },
      owners: [],
      leaders: [],
      members: [],
      guests: [],
      isPublic: false,
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: Json
  get about(): ArticleBody {
    return this.json.about;
  }

  set about(newValue: ArticleBody) {
    this._isDirty = true;
    this.json.about = newValue;
  }

  /// field: string
  get owners(): string[] {
    return this.json.owners;
  }

  set owners(newValue: string[]) {
    this._isDirty = true;
    this.json.owners = newValue;
  }

  /// field: string
  get leaders(): string[] {
    return this.json.leaders;
  }

  set leaders(newValue: string[]) {
    this._isDirty = true;
    this.json.leaders = newValue;
  }

  /// field: string
  get members(): string[] {
    return this.json.members;
  }

  set members(newValue: string[]) {
    this._isDirty = true;
    this.json.members = newValue;
  }

  /// field: string
  get guests(): string[] {
    return this.json.guests;
  }

  set guests(newValue: string[]) {
    this._isDirty = true;
    this.json.guests = newValue;
  }

  /// field: bool
  get isPublic(): boolean {
    return this.json.isPublic;
  }

  set isPublic(newValue: boolean) {
    this._isDirty = true;
    this.json.isPublic = newValue;
  }

  /// field: bool
  get isDefault(): boolean | undefined {
    return this.json.isDefault;
  }

  set isDefault(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.isDefault = newValue;
  }

  /// field: bool
  get isPublisher(): boolean | undefined {
    return this.json.isPublisher;
  }

  set isPublisher(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.isPublisher = newValue;
  }

  /// field: bool
  get allowCustomDataBindings(): boolean | undefined {
    return this.json.allowCustomDataBindings;
  }

  set allowCustomDataBindings(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.allowCustomDataBindings = newValue;
  }

  /// field: bool
  get allowNewsletters(): boolean | undefined {
    return this.json.allowNewsletters;
  }

  set allowNewsletters(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.allowNewsletters = newValue;
  }

  /// field: bool
  get allowEvents(): boolean | undefined {
    return this.json.allowEvents;
  }

  set allowEvents(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.allowEvents = newValue;
  }

  /// field: bool
  get allowReports(): boolean | undefined {
    return this.json.allowReports;
  }

  set allowReports(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.allowReports = newValue;
  }

  /// field: string
  get categories(): string[] | undefined {
    return this.json.categories;
  }

  set categories(newValue: string[] | undefined) {
    this._isDirty = true;
    this.json.categories = newValue;
  }

  /// field: string
  get contactEmail(): string | undefined {
    return this.json.contactEmail;
  }

  set contactEmail(newValue: string | undefined) {
    this._isDirty = true;
    this.json.contactEmail = newValue;
  }

  /// object: SubscriptionDetails
  get subscription(): SubscriptionDetailsModel {
    return new SubscriptionDetailsModel(this.json.subscription);
  }

  set subscription(newValue: SubscriptionDetailsModel | undefined) {
    this._isDirty = true;

    this.json.subscription = newValue?.toJSON();
  }

  /// object: SocialMediaLinks
  get socialMediaLinks(): SocialMediaLinksModel {
    return new SocialMediaLinksModel(this.json.socialMediaLinks);
  }

  set socialMediaLinks(newValue: SocialMediaLinksModel | undefined) {
    this._isDirty = true;

    this.json.socialMediaLinks = newValue?.toJSON();
  }

  /// object: Invitation
  get invitations(): InvitationModel[] {
    return this.json.invitations?.map((a) => new InvitationModel(a)) ?? [];
  }

  set invitations(newValue: InvitationModel[] | undefined) {
    this._isDirty = true;

    this.json.invitations = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// model: Picture
  get logo(): Promise<PictureModel | undefined> {
    if (!this.json.logo) {
      return Promise.resolve(undefined);
    }
    return Store.instance.findPicture(this.json.logo);
  }

  set logo(newValue: PictureModel | undefined) {
    this._isDirty = true;
    this.json.logo = newValue?._id;
  }

  /// model: Picture
  get pictures(): Promise<PictureModel[] | undefined> {
    const promises =
      this.json.pictures?.map((a) => Store.instance.findPicture(a)) ?? [];

    return Promise.all(promises);
  }

  set pictures(newValue: PictureModel[] | undefined) {
    this._isDirty = true;
    this.json.pictures = newValue?.map((a) => a._id) ?? [];
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.subscription?.isDirty) {
      return true;
    }

    if (this.socialMediaLinks?.isDirty) {
      return true;
    }

    for (let level0 of this.invitations) {
      if (level0?.isDirty) {
        return true;
      }
    }

    return false;
  }

  async save(): Promise<TeamModel> {
    let result: TeamModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createTeam(this);
    } else {
      result = await Store.instance.saveTeam(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): TeamJson {
    return this.json;
  }
}
