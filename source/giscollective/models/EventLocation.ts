import { Store } from "./store";

export interface EventLocation {
  type: string;
  value: string;
}

export interface EventLocationJson {
  type: string;
  value: string;
}

export class EventLocationModel {
  private json: EventLocationJson;
  private _isDirty: boolean = false;

  constructor(json: EventLocationJson | null | undefined) {
    this.json = json ?? { type: "", value: "" };
  }

  /// field: string
  get type(): string {
    return this.json.type;
  }

  set type(newValue: string) {
    this._isDirty = true;
    this.json.type = newValue;
  }

  /// field: string
  get value(): string {
    return this.json.value;
  }

  set value(newValue: string) {
    this._isDirty = true;
    this.json.value = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): EventLocationJson {
    return this.json;
  }
}
