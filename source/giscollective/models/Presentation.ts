import { Picture, PictureModel } from "./Picture";
import { ModelInfo, ModelInfoJson, ModelInfoModel } from "./ModelInfo";
import { Visibility, VisibilityJson, VisibilityModel } from "./Visibility";
import { PageCol, PageColJson, PageColModel } from "./PageCol";
import { Store } from "./store";

export interface Presentation {
  _id: string;
  name: string;
  slug: string;
  hasIntro?: boolean;
  hasEnd?: boolean;
  introTimeout?: number;
  endSlideOptions?: any;
  info: ModelInfo;
  visibility: Visibility;
  cols: PageCol[];
  cover?: Picture;
}

export interface PresentationResponse {
  presentation: PresentationJson;
}

export interface PresentationsResponse {
  presentations: PresentationJson[];
}

export interface PresentationJson {
  _id: string;
  name: string;
  slug: string;
  hasIntro?: boolean;
  hasEnd?: boolean;
  introTimeout?: number;
  endSlideOptions?: any;
  info: ModelInfoJson;
  visibility: VisibilityJson;
  cols: PageColJson[];
  cover?: string;
}

export class PresentationModel {
  private json: PresentationJson;
  private _isDirty: boolean = false;

  constructor(json: PresentationJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      name: "",
      slug: "",
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      visibility: { isPublic: false, team: "" },
      cols: [],
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: string
  get slug(): string {
    return this.json.slug;
  }

  set slug(newValue: string) {
    this._isDirty = true;
    this.json.slug = newValue;
  }

  /// field: bool
  get hasIntro(): boolean | undefined {
    return this.json.hasIntro;
  }

  set hasIntro(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.hasIntro = newValue;
  }

  /// field: bool
  get hasEnd(): boolean | undefined {
    return this.json.hasEnd;
  }

  set hasEnd(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.hasEnd = newValue;
  }

  /// field: int
  get introTimeout(): number | undefined {
    return this.json.introTimeout;
  }

  set introTimeout(newValue: number | undefined) {
    this._isDirty = true;
    this.json.introTimeout = newValue;
  }

  /// field: Json
  get endSlideOptions(): any | undefined {
    return this.json.endSlideOptions;
  }

  set endSlideOptions(newValue: any | undefined) {
    this._isDirty = true;
    this.json.endSlideOptions = newValue;
  }

  /// object: ModelInfo
  get info(): ModelInfoModel {
    return new ModelInfoModel(this.json.info);
  }

  set info(newValue: ModelInfoModel) {
    this._isDirty = true;

    this.json.info = newValue?.toJSON();
  }

  /// object: Visibility
  get visibility(): VisibilityModel {
    return new VisibilityModel(this.json.visibility);
  }

  set visibility(newValue: VisibilityModel) {
    this._isDirty = true;

    this.json.visibility = newValue?.toJSON();
  }

  /// object: PageCol
  get cols(): PageColModel[] {
    return this.json.cols?.map((a) => new PageColModel(a)) ?? [];
  }

  set cols(newValue: PageColModel[]) {
    this._isDirty = true;

    this.json.cols = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// model: Picture
  get cover(): Promise<PictureModel | undefined> {
    if (!this.json.cover) {
      return Promise.resolve(undefined);
    }
    return Store.instance.findPicture(this.json.cover);
  }

  set cover(newValue: PictureModel | undefined) {
    this._isDirty = true;
    this.json.cover = newValue?._id;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.info?.isDirty) {
      return true;
    }

    if (this.visibility?.isDirty) {
      return true;
    }

    for (let level0 of this.cols) {
      if (level0?.isDirty) {
        return true;
      }
    }

    return false;
  }

  async save(): Promise<PresentationModel> {
    let result: PresentationModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createPresentation(this);
    } else {
      result = await Store.instance.savePresentation(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): PresentationJson {
    return this.json;
  }
}
