import { DateTime } from "luxon";
import { Store } from "./store";

export interface CalendarEntry {
  repetition: string;
  timezone: string;
  begin: DateTime;
  end: DateTime;
  intervalEnd: DateTime;
}

export interface CalendarEntryJson {
  repetition: string;
  timezone: string;
  begin: string;
  end: string;
  intervalEnd: string;
}

export class CalendarEntryModel {
  private json: CalendarEntryJson;
  private _isDirty: boolean = false;

  constructor(json: CalendarEntryJson | null | undefined) {
    this.json = json ?? {
      repetition: "",
      timezone: "",
      begin: "",
      end: "",
      intervalEnd: "",
    };
  }

  /// field: string
  get repetition(): string {
    return this.json.repetition;
  }

  set repetition(newValue: string) {
    this._isDirty = true;
    this.json.repetition = newValue;
  }

  /// field: string
  get timezone(): string {
    return this.json.timezone;
  }

  set timezone(newValue: string) {
    this._isDirty = true;
    this.json.timezone = newValue;
  }

  /// object: SysTime
  get begin(): DateTime {
    return DateTime.fromISO(this.json.begin);
  }

  set begin(newValue: DateTime) {
    this._isDirty = true;
    this.json.begin = newValue.toISO();
  }

  /// object: SysTime
  get end(): DateTime {
    return DateTime.fromISO(this.json.end);
  }

  set end(newValue: DateTime) {
    this._isDirty = true;
    this.json.end = newValue.toISO();
  }

  /// object: SysTime
  get intervalEnd(): DateTime {
    return DateTime.fromISO(this.json.intervalEnd);
  }

  set intervalEnd(newValue: DateTime) {
    this._isDirty = true;
    this.json.intervalEnd = newValue.toISO();
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): CalendarEntryJson {
    return this.json;
  }
}
