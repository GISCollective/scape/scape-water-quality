import { Store } from "./store";

export interface CampaignLocationQuestion {
  customQuestion?: boolean;
  label?: string;
  allowGps?: boolean;
  allowManual?: boolean;
  allowAddress?: boolean;
  allowExistingFeature?: boolean;
}

export interface CampaignLocationQuestionJson {
  customQuestion?: boolean;
  label?: string;
  allowGps?: boolean;
  allowManual?: boolean;
  allowAddress?: boolean;
  allowExistingFeature?: boolean;
}

export class CampaignLocationQuestionModel {
  private json: CampaignLocationQuestionJson;
  private _isDirty: boolean = false;

  constructor(json: CampaignLocationQuestionJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: bool
  get customQuestion(): boolean | undefined {
    return this.json.customQuestion;
  }

  set customQuestion(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.customQuestion = newValue;
  }

  /// field: string
  get label(): string | undefined {
    return this.json.label;
  }

  set label(newValue: string | undefined) {
    this._isDirty = true;
    this.json.label = newValue;
  }

  /// field: bool
  get allowGps(): boolean | undefined {
    return this.json.allowGps;
  }

  set allowGps(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.allowGps = newValue;
  }

  /// field: bool
  get allowManual(): boolean | undefined {
    return this.json.allowManual;
  }

  set allowManual(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.allowManual = newValue;
  }

  /// field: bool
  get allowAddress(): boolean | undefined {
    return this.json.allowAddress;
  }

  set allowAddress(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.allowAddress = newValue;
  }

  /// field: bool
  get allowExistingFeature(): boolean | undefined {
    return this.json.allowExistingFeature;
  }

  set allowExistingFeature(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.allowExistingFeature = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): CampaignLocationQuestionJson {
    return this.json;
  }
}
