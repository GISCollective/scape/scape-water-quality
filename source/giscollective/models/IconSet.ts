import { IconStyle, IconStyleJson, IconStyleModel } from "./IconStyle";
import { Source, SourceJson, SourceModel } from "./Source";
import { Visibility, VisibilityJson, VisibilityModel } from "./Visibility";
import { Store } from "./store";
import { IconSprite, IconSpriteJson, IconSpriteModel } from "./IconSprite";
import { ArticleBody } from "./ArticleBody";
import { Picture, PictureModel } from "./Picture";

export interface IconSet {
  _id: string;
  name: string;
  description?: ArticleBody;
  ver?: number;
  visibility: Visibility;
  styles?: IconStyle;
  source?: Source;
  sprites?: IconSprite;
  cover?: Picture;
}

export interface IconSetResponse {
  iconSet: IconSetJson;
}

export interface IconSetsResponse {
  iconSets: IconSetJson[];
}

export interface IconSetJson {
  _id: string;
  name: string;
  description?: ArticleBody;
  ver?: number;
  visibility: VisibilityJson;
  styles?: IconStyleJson;
  source?: SourceJson;
  sprites?: IconSpriteJson;
  cover?: string;
}

export class IconSetModel {
  private json: IconSetJson;
  private _isDirty: boolean = false;

  constructor(json: IconSetJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      name: "",
      visibility: { isPublic: false, team: "" },
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: Json
  get description(): ArticleBody | undefined {
    return this.json.description;
  }

  set description(newValue: ArticleBody | undefined) {
    this._isDirty = true;
    this.json.description = newValue;
  }

  /// field: ulong
  get ver(): number | undefined {
    return this.json.ver;
  }

  set ver(newValue: number | undefined) {
    this._isDirty = true;
    this.json.ver = newValue;
  }

  /// object: Visibility
  get visibility(): VisibilityModel {
    return new VisibilityModel(this.json.visibility);
  }

  set visibility(newValue: VisibilityModel) {
    this._isDirty = true;

    this.json.visibility = newValue?.toJSON();
  }

  /// object: IconStyle
  get styles(): IconStyleModel {
    return new IconStyleModel(this.json.styles);
  }

  set styles(newValue: IconStyleModel | undefined) {
    this._isDirty = true;

    this.json.styles = newValue?.toJSON();
  }

  /// object: Source
  get source(): SourceModel {
    return new SourceModel(this.json.source);
  }

  set source(newValue: SourceModel | undefined) {
    this._isDirty = true;

    this.json.source = newValue?.toJSON();
  }

  /// object: IconSprite
  get sprites(): IconSpriteModel {
    return new IconSpriteModel(this.json.sprites);
  }

  set sprites(newValue: IconSpriteModel | undefined) {
    this._isDirty = true;

    this.json.sprites = newValue?.toJSON();
  }

  /// model: Picture
  get cover(): Promise<PictureModel | undefined> {
    if (!this.json.cover) {
      return Promise.resolve(undefined);
    }
    return Store.instance.findPicture(this.json.cover);
  }

  set cover(newValue: PictureModel | undefined) {
    this._isDirty = true;
    this.json.cover = newValue?._id;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.visibility?.isDirty) {
      return true;
    }

    if (this.styles?.isDirty) {
      return true;
    }

    if (this.source?.isDirty) {
      return true;
    }

    if (this.sprites?.isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<IconSetModel> {
    let result: IconSetModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createIconSet(this);
    } else {
      result = await Store.instance.saveIconSet(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): IconSetJson {
    return this.json;
  }
}
