import { Store } from "./store";

export interface OptionalIconImage {
  useParent?: boolean;
  value?: string;
}

export interface OptionalIconImageJson {
  useParent?: boolean;
  value?: string;
}

export class OptionalIconImageModel {
  private json: OptionalIconImageJson;
  private _isDirty: boolean = false;

  constructor(json: OptionalIconImageJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: bool
  get useParent(): boolean | undefined {
    return this.json.useParent;
  }

  set useParent(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.useParent = newValue;
  }

  /// object: string
  get value(): string | undefined {
    return this.json.value;
  }

  set value(newValue: string | undefined) {
    this._isDirty = true;
    this.json.value = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.value?.isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): OptionalIconImageJson {
    return this.json;
  }
}
