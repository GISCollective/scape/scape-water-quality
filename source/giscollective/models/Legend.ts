import {
  LegendSubcategory,
  LegendSubcategoryJson,
  LegendSubcategoryModel,
} from "./LegendSubcategory";
import { Store } from "./store";

export interface Legend {
  _id: number;
  name: string;
  count: number;
  subcategories: LegendSubcategory[];
}

export interface LegendResponse {
  legend: LegendJson;
}

export interface LegendsResponse {
  legends: LegendJson[];
}

export interface LegendJson {
  _id: number;
  name: string;
  count: number;
  subcategories: LegendSubcategoryJson[];
}

export class LegendModel {
  private json: LegendJson;
  private _isDirty: boolean = false;

  constructor(json: LegendJson | null | undefined) {
    this.json = json ?? { _id: 0, name: "", count: 0, subcategories: [] };
  }

  /// field: ulong
  get _id(): number {
    return this.json._id;
  }

  set _id(newValue: number) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: ulong
  get count(): number {
    return this.json.count;
  }

  set count(newValue: number) {
    this._isDirty = true;
    this.json.count = newValue;
  }

  /// object: LegendSubcategory
  get subcategories(): LegendSubcategoryModel[] {
    return (
      this.json.subcategories?.map((a) => new LegendSubcategoryModel(a)) ?? []
    );
  }

  set subcategories(newValue: LegendSubcategoryModel[]) {
    this._isDirty = true;

    this.json.subcategories = newValue?.map((a) => a.toJSON()) ?? [];
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    for (let level0 of this.subcategories) {
      if (level0?.isDirty) {
        return true;
      }
    }

    return false;
  }

  async save(): Promise<LegendModel> {
    let result: LegendModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createLegend(this);
    } else {
      result = await Store.instance.saveLegend(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): LegendJson {
    return this.json;
  }
}
