import { DateTime } from "luxon";
import { Store } from "./store";

export interface Stat {
  _id: string;
  name: string;
  value: number;
  lastUpdate?: DateTime;
}

export interface StatResponse {
  stat: StatJson;
}

export interface StatsResponse {
  stats: StatJson[];
}

export interface StatJson {
  _id: string;
  name: string;
  value: number;
  lastUpdate?: string;
}

export class StatModel {
  private json: StatJson;
  private _isDirty: boolean = false;

  constructor(json: StatJson | null | undefined) {
    this.json = json ?? { _id: "", name: "", value: 0 };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: double
  get value(): number {
    return this.json.value;
  }

  set value(newValue: number) {
    this._isDirty = true;
    this.json.value = newValue;
  }

  /// object: SysTime
  get lastUpdate(): DateTime | undefined {
    if (!this.json.lastUpdate) {
      return undefined;
    }
    return DateTime.fromISO(this.json.lastUpdate);
  }

  set lastUpdate(newValue: DateTime | undefined) {
    this._isDirty = true;
    this.json.lastUpdate = newValue?.toISO() ?? undefined;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<StatModel> {
    let result: StatModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createStat(this);
    } else {
      result = await Store.instance.saveStat(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): StatJson {
    return this.json;
  }
}
