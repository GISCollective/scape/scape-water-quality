import { Store } from "./store";

export interface FieldMapping {
  key?: string;
  destination?: string;
  preview?: string[];
}

export interface FieldMappingJson {
  key?: string;
  destination?: string;
  preview?: string[];
}

export class FieldMappingModel {
  private json: FieldMappingJson;
  private _isDirty: boolean = false;

  constructor(json: FieldMappingJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: string
  get key(): string | undefined {
    return this.json.key;
  }

  set key(newValue: string | undefined) {
    this._isDirty = true;
    this.json.key = newValue;
  }

  /// field: string
  get destination(): string | undefined {
    return this.json.destination;
  }

  set destination(newValue: string | undefined) {
    this._isDirty = true;
    this.json.destination = newValue;
  }

  /// field: string
  get preview(): string[] | undefined {
    return this.json.preview;
  }

  set preview(newValue: string[] | undefined) {
    this._isDirty = true;
    this.json.preview = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): FieldMappingJson {
    return this.json;
  }
}
