import { Store } from "./store";

export interface CampaignLabelQuestion {
  customQuestion?: boolean;
  label?: string;
}

export interface CampaignLabelQuestionJson {
  customQuestion?: boolean;
  label?: string;
}

export class CampaignLabelQuestionModel {
  private json: CampaignLabelQuestionJson;
  private _isDirty: boolean = false;

  constructor(json: CampaignLabelQuestionJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: bool
  get customQuestion(): boolean | undefined {
    return this.json.customQuestion;
  }

  set customQuestion(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.customQuestion = newValue;
  }

  /// field: string
  get label(): string | undefined {
    return this.json.label;
  }

  set label(newValue: string | undefined) {
    this._isDirty = true;
    this.json.label = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): CampaignLabelQuestionJson {
    return this.json;
  }
}
