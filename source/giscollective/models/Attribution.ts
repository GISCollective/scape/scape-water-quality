import { Store } from "./store";

export interface Attribution {
  name?: string;
  url?: string;
}

export interface AttributionJson {
  name?: string;
  url?: string;
}

export class AttributionModel {
  private json: AttributionJson;
  private _isDirty: boolean = false;

  constructor(json: AttributionJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: string
  get name(): string | undefined {
    return this.json.name;
  }

  set name(newValue: string | undefined) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: string
  get url(): string | undefined {
    return this.json.url;
  }

  set url(newValue: string | undefined) {
    this._isDirty = true;
    this.json.url = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): AttributionJson {
    return this.json;
  }
}
