import { Visibility, VisibilityJson, VisibilityModel } from "./Visibility";
import { Space, SpaceModel } from "./Space";
import {
  LayoutContainer,
  LayoutContainerJson,
  LayoutContainerModel,
} from "./LayoutContainer";
import {
  PageContainer,
  PageContainerJson,
  PageContainerModel,
} from "./PageContainer";
import { Store } from "./store";
import { ModelInfo, ModelInfoJson, ModelInfoModel } from "./ModelInfo";
import { PageCol, PageColJson, PageColModel } from "./PageCol";
import { Picture, PictureModel } from "./Picture";

export interface Page {
  _id: string;
  name: string;
  slug: string;
  description?: string;
  categories?: string[];
  info: ModelInfo;
  visibility: Visibility;
  cols: PageCol[];
  containers?: PageContainer[];
  layoutContainers?: LayoutContainer[];
  space?: Space;
  cover?: Picture;
}

export interface PageResponse {
  page: PageJson;
}

export interface PagesResponse {
  pages: PageJson[];
}

export interface PageJson {
  _id: string;
  name: string;
  slug: string;
  description?: string;
  categories?: string[];
  info: ModelInfoJson;
  visibility: VisibilityJson;
  cols: PageColJson[];
  containers?: PageContainerJson[];
  layoutContainers?: LayoutContainerJson[];
  space?: string;
  cover?: string;
}

export class PageModel {
  private json: PageJson;
  private _isDirty: boolean = false;

  constructor(json: PageJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      name: "",
      slug: "",
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      visibility: { isPublic: false, team: "" },
      cols: [],
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: string
  get slug(): string {
    return this.json.slug;
  }

  set slug(newValue: string) {
    this._isDirty = true;
    this.json.slug = newValue;
  }

  /// field: string
  get description(): string | undefined {
    return this.json.description;
  }

  set description(newValue: string | undefined) {
    this._isDirty = true;
    this.json.description = newValue;
  }

  /// field: string
  get categories(): string[] | undefined {
    return this.json.categories;
  }

  set categories(newValue: string[] | undefined) {
    this._isDirty = true;
    this.json.categories = newValue;
  }

  /// object: ModelInfo
  get info(): ModelInfoModel {
    return new ModelInfoModel(this.json.info);
  }

  set info(newValue: ModelInfoModel) {
    this._isDirty = true;

    this.json.info = newValue?.toJSON();
  }

  /// object: Visibility
  get visibility(): VisibilityModel {
    return new VisibilityModel(this.json.visibility);
  }

  set visibility(newValue: VisibilityModel) {
    this._isDirty = true;

    this.json.visibility = newValue?.toJSON();
  }

  /// object: PageCol
  get cols(): PageColModel[] {
    return this.json.cols?.map((a) => new PageColModel(a)) ?? [];
  }

  set cols(newValue: PageColModel[]) {
    this._isDirty = true;

    this.json.cols = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// object: PageContainer
  get containers(): PageContainerModel[] {
    return this.json.containers?.map((a) => new PageContainerModel(a)) ?? [];
  }

  set containers(newValue: PageContainerModel[] | undefined) {
    this._isDirty = true;

    this.json.containers = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// object: LayoutContainer
  get layoutContainers(): LayoutContainerModel[] {
    return (
      this.json.layoutContainers?.map((a) => new LayoutContainerModel(a)) ?? []
    );
  }

  set layoutContainers(newValue: LayoutContainerModel[] | undefined) {
    this._isDirty = true;

    this.json.layoutContainers = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// model: Space
  get space(): Promise<SpaceModel | undefined> {
    if (!this.json.space) {
      return Promise.resolve(undefined);
    }
    return Store.instance.findSpace(this.json.space);
  }

  set space(newValue: SpaceModel | undefined) {
    this._isDirty = true;
    this.json.space = newValue?._id;
  }

  /// model: Picture
  get cover(): Promise<PictureModel | undefined> {
    if (!this.json.cover) {
      return Promise.resolve(undefined);
    }
    return Store.instance.findPicture(this.json.cover);
  }

  set cover(newValue: PictureModel | undefined) {
    this._isDirty = true;
    this.json.cover = newValue?._id;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.info?.isDirty) {
      return true;
    }

    if (this.visibility?.isDirty) {
      return true;
    }

    for (let level0 of this.cols) {
      if (level0?.isDirty) {
        return true;
      }
    }

    for (let level0 of this.containers) {
      if (level0?.isDirty) {
        return true;
      }
    }

    for (let level0 of this.layoutContainers) {
      if (level0?.isDirty) {
        return true;
      }
    }

    return false;
  }

  async save(): Promise<PageModel> {
    let result: PageModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createPage(this);
    } else {
      result = await Store.instance.savePage(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): PageJson {
    return this.json;
  }
}
