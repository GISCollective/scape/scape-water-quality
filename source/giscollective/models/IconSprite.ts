import { Picture, PictureModel } from "./Picture";
import { Store } from "./store";

export interface IconSprite {
  small?: Picture;
  large?: Picture;
}

export interface IconSpriteJson {
  small?: string;
  large?: string;
}

export class IconSpriteModel {
  private json: IconSpriteJson;
  private _isDirty: boolean = false;

  constructor(json: IconSpriteJson | null | undefined) {
    this.json = json ?? {};
  }

  /// model: Picture
  get small(): Promise<PictureModel | undefined> {
    if (!this.json.small) {
      return Promise.resolve(undefined);
    }
    return Store.instance.findPicture(this.json.small);
  }

  set small(newValue: PictureModel | undefined) {
    this._isDirty = true;
    this.json.small = newValue?._id;
  }

  /// model: Picture
  get large(): Promise<PictureModel | undefined> {
    if (!this.json.large) {
      return Promise.resolve(undefined);
    }
    return Store.instance.findPicture(this.json.large);
  }

  set large(newValue: PictureModel | undefined) {
    this._isDirty = true;
    this.json.large = newValue?._id;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): IconSpriteJson {
    return this.json;
  }
}
