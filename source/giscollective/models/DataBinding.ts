import {
  DataBindingDestination,
  DataBindingDestinationJson,
  DataBindingDestinationModel,
} from "./DataBindingDestination";
import { Visibility, VisibilityJson, VisibilityModel } from "./Visibility";
import {
  FieldMapping,
  FieldMappingJson,
  FieldMappingModel,
} from "./FieldMapping";
import { Connection, ConnectionJson, ConnectionModel } from "./Connection";
import { Store } from "./store";
import { ModelInfo, ModelInfoJson, ModelInfoModel } from "./ModelInfo";
import { DateTime } from "luxon";

export interface DataBinding {
  _id: string;
  name: string;
  descriptionTemplate: any;
  extraIcons: string[];
  updateBy: string;
  crs: string;
  destination: DataBindingDestination;
  fields: FieldMapping[];
  analyzedAt?: DateTime;
  connection: Connection;
  visibility: Visibility;
  info: ModelInfo;
}

export interface DataBindingResponse {
  dataBinding: DataBindingJson;
}

export interface DataBindingsResponse {
  dataBindings: DataBindingJson[];
}

export interface DataBindingJson {
  _id: string;
  name: string;
  descriptionTemplate: any;
  extraIcons: string[];
  updateBy: string;
  crs: string;
  destination: DataBindingDestinationJson;
  fields: FieldMappingJson[];
  analyzedAt?: string;
  connection: ConnectionJson;
  visibility: VisibilityJson;
  info: ModelInfoJson;
}

export class DataBindingModel {
  private json: DataBindingJson;
  private _isDirty: boolean = false;

  constructor(json: DataBindingJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      name: "",
      descriptionTemplate: null,
      extraIcons: [],
      updateBy: "",
      crs: "",
      destination: { type: "", modelId: "", deleteNonSyncedRecords: false },
      fields: [],
      connection: { type: "", config: null },
      visibility: { isPublic: false, team: "" },
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: Json
  get descriptionTemplate(): any {
    return this.json.descriptionTemplate;
  }

  set descriptionTemplate(newValue: any) {
    this._isDirty = true;
    this.json.descriptionTemplate = newValue;
  }

  /// field: string
  get extraIcons(): string[] {
    return this.json.extraIcons;
  }

  set extraIcons(newValue: string[]) {
    this._isDirty = true;
    this.json.extraIcons = newValue;
  }

  /// field: string
  get updateBy(): string {
    return this.json.updateBy;
  }

  set updateBy(newValue: string) {
    this._isDirty = true;
    this.json.updateBy = newValue;
  }

  /// field: string
  get crs(): string {
    return this.json.crs;
  }

  set crs(newValue: string) {
    this._isDirty = true;
    this.json.crs = newValue;
  }

  /// object: DataBindingDestination
  get destination(): DataBindingDestinationModel {
    return new DataBindingDestinationModel(this.json.destination);
  }

  set destination(newValue: DataBindingDestinationModel) {
    this._isDirty = true;

    this.json.destination = newValue?.toJSON();
  }

  /// object: FieldMapping
  get fields(): FieldMappingModel[] {
    return this.json.fields?.map((a) => new FieldMappingModel(a)) ?? [];
  }

  set fields(newValue: FieldMappingModel[]) {
    this._isDirty = true;

    this.json.fields = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// object: SysTime
  get analyzedAt(): DateTime | undefined {
    if (!this.json.analyzedAt) {
      return undefined;
    }
    return DateTime.fromISO(this.json.analyzedAt);
  }

  set analyzedAt(newValue: DateTime | undefined) {
    this._isDirty = true;
    this.json.analyzedAt = newValue?.toISO() ?? undefined;
  }

  /// object: Connection
  get connection(): ConnectionModel {
    return new ConnectionModel(this.json.connection);
  }

  set connection(newValue: ConnectionModel) {
    this._isDirty = true;

    this.json.connection = newValue?.toJSON();
  }

  /// object: Visibility
  get visibility(): VisibilityModel {
    return new VisibilityModel(this.json.visibility);
  }

  set visibility(newValue: VisibilityModel) {
    this._isDirty = true;

    this.json.visibility = newValue?.toJSON();
  }

  /// object: ModelInfo
  get info(): ModelInfoModel {
    return new ModelInfoModel(this.json.info);
  }

  set info(newValue: ModelInfoModel) {
    this._isDirty = true;

    this.json.info = newValue?.toJSON();
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.destination?.isDirty) {
      return true;
    }

    for (let level0 of this.fields) {
      if (level0?.isDirty) {
        return true;
      }
    }

    if (this.connection?.isDirty) {
      return true;
    }

    if (this.visibility?.isDirty) {
      return true;
    }

    if (this.info?.isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<DataBindingModel> {
    let result: DataBindingModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createDataBinding(this);
    } else {
      result = await Store.instance.saveDataBinding(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): DataBindingJson {
    return this.json;
  }
}
