import { ModelInfo, ModelInfoJson, ModelInfoModel } from "./ModelInfo";
import { Article, ArticleModel } from "./Article";
import { Visibility, VisibilityJson, VisibilityModel } from "./Visibility";
import { Store } from "./store";

export interface Newsletter {
  _id: string;
  name: string;
  description: string;
  info: ModelInfo;
  visibility: Visibility;
  welcomeMessage?: Article;
}

export interface NewsletterResponse {
  newsletter: NewsletterJson;
}

export interface NewslettersResponse {
  newsletters: NewsletterJson[];
}

export interface NewsletterJson {
  _id: string;
  name: string;
  description: string;
  info: ModelInfoJson;
  visibility: VisibilityJson;
  welcomeMessage?: string;
}

export class NewsletterModel {
  private json: NewsletterJson;
  private _isDirty: boolean = false;

  constructor(json: NewsletterJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      name: "",
      description: "",
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      visibility: { isPublic: false, team: "" },
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: string
  get description(): string {
    return this.json.description;
  }

  set description(newValue: string) {
    this._isDirty = true;
    this.json.description = newValue;
  }

  /// object: ModelInfo
  get info(): ModelInfoModel {
    return new ModelInfoModel(this.json.info);
  }

  set info(newValue: ModelInfoModel) {
    this._isDirty = true;

    this.json.info = newValue?.toJSON();
  }

  /// object: Visibility
  get visibility(): VisibilityModel {
    return new VisibilityModel(this.json.visibility);
  }

  set visibility(newValue: VisibilityModel) {
    this._isDirty = true;

    this.json.visibility = newValue?.toJSON();
  }

  /// model: Article
  get welcomeMessage(): Promise<ArticleModel | undefined> {
    if (!this.json.welcomeMessage) {
      return Promise.resolve(undefined);
    }
    return Store.instance.findArticle(this.json.welcomeMessage);
  }

  set welcomeMessage(newValue: ArticleModel | undefined) {
    this._isDirty = true;
    this.json.welcomeMessage = newValue?._id;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.info?.isDirty) {
      return true;
    }

    if (this.visibility?.isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<NewsletterModel> {
    let result: NewsletterModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createNewsletter(this);
    } else {
      result = await Store.instance.saveNewsletter(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): NewsletterJson {
    return this.json;
  }
}
