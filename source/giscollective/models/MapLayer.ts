import { Store } from "./store";

export interface MapLayer {
  type: string;
  options?: any;
}

export interface MapLayerJson {
  type: string;
  options?: any;
}

export class MapLayerModel {
  private json: MapLayerJson;
  private _isDirty: boolean = false;

  constructor(json: MapLayerJson | null | undefined) {
    this.json = json ?? { type: "" };
  }

  /// field: string
  get type(): string {
    return this.json.type;
  }

  set type(newValue: string) {
    this._isDirty = true;
    this.json.type = newValue;
  }

  /// field: Json
  get options(): any | undefined {
    return this.json.options;
  }

  set options(newValue: any | undefined) {
    this._isDirty = true;
    this.json.options = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): MapLayerJson {
    return this.json;
  }
}
