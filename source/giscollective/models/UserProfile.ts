import { Picture, PictureModel } from "./Picture";
import { DateTime } from "luxon";
import { Location, LocationJson, LocationModel } from "./Location";
import { Store } from "./store";

export interface UserProfile {
  _id: string;
  statusEmoji: string;
  statusMessage: string;
  salutation?: string;
  title?: string;
  firstName: string;
  lastName: string;
  skype: string;
  linkedin: string;
  twitter: string;
  website: string;
  jobTitle: string;
  organization: string;
  bio: string;
  showPrivateContributions: boolean;
  showCalendarContributions?: boolean;
  showWelcomePresentation?: boolean;
  location: Location;
  joinedTime?: DateTime;
  picture?: Picture;
}

export interface UserProfileResponse {
  userProfile: UserProfileJson;
}

export interface UserProfilesResponse {
  userProfiles: UserProfileJson[];
}

export interface UserProfileJson {
  _id: string;
  statusEmoji: string;
  statusMessage: string;
  salutation?: string;
  title?: string;
  firstName: string;
  lastName: string;
  skype: string;
  linkedin: string;
  twitter: string;
  website: string;
  jobTitle: string;
  organization: string;
  bio: string;
  showPrivateContributions: boolean;
  showCalendarContributions?: boolean;
  showWelcomePresentation?: boolean;
  location: LocationJson;
  joinedTime?: string;
  picture?: string;
}

export class UserProfileModel {
  private json: UserProfileJson;
  private _isDirty: boolean = false;

  constructor(json: UserProfileJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      statusEmoji: "",
      statusMessage: "",
      firstName: "",
      lastName: "",
      skype: "",
      linkedin: "",
      twitter: "",
      website: "",
      jobTitle: "",
      organization: "",
      bio: "",
      showPrivateContributions: false,
      location: { isDetailed: false, simple: "" },
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get statusEmoji(): string {
    return this.json.statusEmoji;
  }

  set statusEmoji(newValue: string) {
    this._isDirty = true;
    this.json.statusEmoji = newValue;
  }

  /// field: string
  get statusMessage(): string {
    return this.json.statusMessage;
  }

  set statusMessage(newValue: string) {
    this._isDirty = true;
    this.json.statusMessage = newValue;
  }

  /// field: string
  get salutation(): string | undefined {
    return this.json.salutation;
  }

  set salutation(newValue: string | undefined) {
    this._isDirty = true;
    this.json.salutation = newValue;
  }

  /// field: string
  get title(): string | undefined {
    return this.json.title;
  }

  set title(newValue: string | undefined) {
    this._isDirty = true;
    this.json.title = newValue;
  }

  /// field: string
  get firstName(): string {
    return this.json.firstName;
  }

  set firstName(newValue: string) {
    this._isDirty = true;
    this.json.firstName = newValue;
  }

  /// field: string
  get lastName(): string {
    return this.json.lastName;
  }

  set lastName(newValue: string) {
    this._isDirty = true;
    this.json.lastName = newValue;
  }

  /// field: string
  get skype(): string {
    return this.json.skype;
  }

  set skype(newValue: string) {
    this._isDirty = true;
    this.json.skype = newValue;
  }

  /// field: string
  get linkedin(): string {
    return this.json.linkedin;
  }

  set linkedin(newValue: string) {
    this._isDirty = true;
    this.json.linkedin = newValue;
  }

  /// field: string
  get twitter(): string {
    return this.json.twitter;
  }

  set twitter(newValue: string) {
    this._isDirty = true;
    this.json.twitter = newValue;
  }

  /// field: string
  get website(): string {
    return this.json.website;
  }

  set website(newValue: string) {
    this._isDirty = true;
    this.json.website = newValue;
  }

  /// field: string
  get jobTitle(): string {
    return this.json.jobTitle;
  }

  set jobTitle(newValue: string) {
    this._isDirty = true;
    this.json.jobTitle = newValue;
  }

  /// field: string
  get organization(): string {
    return this.json.organization;
  }

  set organization(newValue: string) {
    this._isDirty = true;
    this.json.organization = newValue;
  }

  /// field: string
  get bio(): string {
    return this.json.bio;
  }

  set bio(newValue: string) {
    this._isDirty = true;
    this.json.bio = newValue;
  }

  /// field: bool
  get showPrivateContributions(): boolean {
    return this.json.showPrivateContributions;
  }

  set showPrivateContributions(newValue: boolean) {
    this._isDirty = true;
    this.json.showPrivateContributions = newValue;
  }

  /// field: bool
  get showCalendarContributions(): boolean | undefined {
    return this.json.showCalendarContributions;
  }

  set showCalendarContributions(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.showCalendarContributions = newValue;
  }

  /// field: bool
  get showWelcomePresentation(): boolean | undefined {
    return this.json.showWelcomePresentation;
  }

  set showWelcomePresentation(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.showWelcomePresentation = newValue;
  }

  /// object: Location
  get location(): LocationModel {
    return new LocationModel(this.json.location);
  }

  set location(newValue: LocationModel) {
    this._isDirty = true;

    this.json.location = newValue?.toJSON();
  }

  /// object: SysTime
  get joinedTime(): DateTime | undefined {
    if (!this.json.joinedTime) {
      return undefined;
    }
    return DateTime.fromISO(this.json.joinedTime);
  }

  set joinedTime(newValue: DateTime | undefined) {
    this._isDirty = true;
    this.json.joinedTime = newValue?.toISO() ?? undefined;
  }

  /// model: Picture
  get picture(): Promise<PictureModel | undefined> {
    if (!this.json.picture) {
      return Promise.resolve(undefined);
    }
    return Store.instance.findPicture(this.json.picture);
  }

  set picture(newValue: PictureModel | undefined) {
    this._isDirty = true;
    this.json.picture = newValue?._id;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.location?.isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<UserProfileModel> {
    let result: UserProfileModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createUserProfile(this);
    } else {
      result = await Store.instance.saveUserProfile(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): UserProfileJson {
    return this.json;
  }
}
