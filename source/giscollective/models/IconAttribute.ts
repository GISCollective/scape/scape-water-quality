import { Store } from "./store";

export interface IconAttribute {
  name: string;
  type: string;
  options?: string;
  isPrivate?: boolean;
  isRequired?: boolean;
  displayName?: string;
  help?: string;
  isInherited?: boolean;
  otherNames?: string[];
}

export interface IconAttributeJson {
  name: string;
  type: string;
  options?: string;
  isPrivate?: boolean;
  isRequired?: boolean;
  displayName?: string;
  help?: string;
  isInherited?: boolean;
  otherNames?: string[];
}

export class IconAttributeModel {
  private json: IconAttributeJson;
  private _isDirty: boolean = false;

  constructor(json: IconAttributeJson | null | undefined) {
    this.json = json ?? { name: "", type: "" };
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: string
  get type(): string {
    return this.json.type;
  }

  set type(newValue: string) {
    this._isDirty = true;
    this.json.type = newValue;
  }

  /// field: string
  get options(): string | undefined {
    return this.json.options;
  }

  set options(newValue: string | undefined) {
    this._isDirty = true;
    this.json.options = newValue;
  }

  /// field: bool
  get isPrivate(): boolean | undefined {
    return this.json.isPrivate;
  }

  set isPrivate(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.isPrivate = newValue;
  }

  /// field: bool
  get isRequired(): boolean | undefined {
    return this.json.isRequired;
  }

  set isRequired(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.isRequired = newValue;
  }

  /// field: string
  get displayName(): string | undefined {
    return this.json.displayName;
  }

  set displayName(newValue: string | undefined) {
    this._isDirty = true;
    this.json.displayName = newValue;
  }

  /// field: string
  get help(): string | undefined {
    return this.json.help;
  }

  set help(newValue: string | undefined) {
    this._isDirty = true;
    this.json.help = newValue;
  }

  /// field: bool
  get isInherited(): boolean | undefined {
    return this.json.isInherited;
  }

  set isInherited(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.isInherited = newValue;
  }

  /// field: string
  get otherNames(): string[] | undefined {
    return this.json.otherNames;
  }

  set otherNames(newValue: string[] | undefined) {
    this._isDirty = true;
    this.json.otherNames = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): IconAttributeJson {
    return this.json;
  }
}
