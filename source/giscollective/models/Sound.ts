import { ModelInfo, ModelInfoJson, ModelInfoModel } from "./ModelInfo";
import {
  VisibilityOptional,
  VisibilityOptionalJson,
  VisibilityOptionalModel,
} from "./VisibilityOptional";
import { Store } from "./store";

export interface Sound {
  _id: string;
  name: string;
  feature?: string;
  campaignAnswer?: string;
  sourceUrl?: string;
  attributions?: string;
  visibility: VisibilityOptional;
  info: ModelInfo;
  sound: string;
}

export interface SoundResponse {
  sound: SoundJson;
}

export interface SoundsResponse {
  sounds: SoundJson[];
}

export interface SoundJson {
  _id: string;
  name: string;
  feature?: string;
  campaignAnswer?: string;
  sourceUrl?: string;
  attributions?: string;
  visibility: VisibilityOptionalJson;
  info: ModelInfoJson;
  sound: string;
}

export class SoundModel {
  private json: SoundJson;
  private _isDirty: boolean = false;

  constructor(json: SoundJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      name: "",
      visibility: {},
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      sound: "",
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: string
  get feature(): string | undefined {
    return this.json.feature;
  }

  set feature(newValue: string | undefined) {
    this._isDirty = true;
    this.json.feature = newValue;
  }

  /// field: string
  get campaignAnswer(): string | undefined {
    return this.json.campaignAnswer;
  }

  set campaignAnswer(newValue: string | undefined) {
    this._isDirty = true;
    this.json.campaignAnswer = newValue;
  }

  /// field: string
  get sourceUrl(): string | undefined {
    return this.json.sourceUrl;
  }

  set sourceUrl(newValue: string | undefined) {
    this._isDirty = true;
    this.json.sourceUrl = newValue;
  }

  /// field: string
  get attributions(): string | undefined {
    return this.json.attributions;
  }

  set attributions(newValue: string | undefined) {
    this._isDirty = true;
    this.json.attributions = newValue;
  }

  /// object: VisibilityOptional
  get visibility(): VisibilityOptionalModel {
    return new VisibilityOptionalModel(this.json.visibility);
  }

  set visibility(newValue: VisibilityOptionalModel) {
    this._isDirty = true;

    this.json.visibility = newValue?.toJSON();
  }

  /// object: ModelInfo
  get info(): ModelInfoModel {
    return new ModelInfoModel(this.json.info);
  }

  set info(newValue: ModelInfoModel) {
    this._isDirty = true;

    this.json.info = newValue?.toJSON();
  }

  /// object: string
  get sound(): string {
    return this.json.sound;
  }

  set sound(newValue: string) {
    this._isDirty = true;
    this.json.sound = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.visibility?.isDirty) {
      return true;
    }

    if (this.info?.isDirty) {
      return true;
    }

    if (this.sound?.isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<SoundModel> {
    let result: SoundModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createSound(this);
    } else {
      result = await Store.instance.saveSound(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): SoundJson {
    return this.json;
  }
}
