import { Store } from "./store";

export interface ArticleLink {
  _id: string;
  url: string;
  articleId: string;
}

export interface ArticleLinkResponse {
  articleLink: ArticleLinkJson;
}

export interface ArticleLinksResponse {
  articleLinks: ArticleLinkJson[];
}

export interface ArticleLinkJson {
  _id: string;
  url: string;
  articleId: string;
}

export class ArticleLinkModel {
  private json: ArticleLinkJson;
  private _isDirty: boolean = false;

  constructor(json: ArticleLinkJson | null | undefined) {
    this.json = json ?? { _id: "", url: "", articleId: "" };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get url(): string {
    return this.json.url;
  }

  set url(newValue: string) {
    this._isDirty = true;
    this.json.url = newValue;
  }

  /// field: string
  get articleId(): string {
    return this.json.articleId;
  }

  set articleId(newValue: string) {
    this._isDirty = true;
    this.json.articleId = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<ArticleLinkModel> {
    let result: ArticleLinkModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createArticleLink(this);
    } else {
      result = await Store.instance.saveArticleLink(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): ArticleLinkJson {
    return this.json;
  }
}
