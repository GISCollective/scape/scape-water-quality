import { Store } from "./store";

export interface DetailedLocation {
  country?: string;
  province?: string;
  city?: string;
  postalCode?: string;
}

export interface DetailedLocationJson {
  country?: string;
  province?: string;
  city?: string;
  postalCode?: string;
}

export class DetailedLocationModel {
  private json: DetailedLocationJson;
  private _isDirty: boolean = false;

  constructor(json: DetailedLocationJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: string
  get country(): string | undefined {
    return this.json.country;
  }

  set country(newValue: string | undefined) {
    this._isDirty = true;
    this.json.country = newValue;
  }

  /// field: string
  get province(): string | undefined {
    return this.json.province;
  }

  set province(newValue: string | undefined) {
    this._isDirty = true;
    this.json.province = newValue;
  }

  /// field: string
  get city(): string | undefined {
    return this.json.city;
  }

  set city(newValue: string | undefined) {
    this._isDirty = true;
    this.json.city = newValue;
  }

  /// field: string
  get postalCode(): string | undefined {
    return this.json.postalCode;
  }

  set postalCode(newValue: string | undefined) {
    this._isDirty = true;
    this.json.postalCode = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): DetailedLocationJson {
    return this.json;
  }
}
