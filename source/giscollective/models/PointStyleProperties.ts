import { Store } from "./store";

export interface PointStyleProperties {
  isVisible?: boolean;
  shape?: string;
  borderColor?: string;
  backgroundColor?: string;
  borderWidth?: number;
  size?: number;
}

export interface PointStylePropertiesJson {
  isVisible?: boolean;
  shape?: string;
  borderColor?: string;
  backgroundColor?: string;
  borderWidth?: number;
  size?: number;
}

export class PointStylePropertiesModel {
  private json: PointStylePropertiesJson;
  private _isDirty: boolean = false;

  constructor(json: PointStylePropertiesJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: bool
  get isVisible(): boolean | undefined {
    return this.json.isVisible;
  }

  set isVisible(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.isVisible = newValue;
  }

  /// field: string
  get shape(): string | undefined {
    return this.json.shape;
  }

  set shape(newValue: string | undefined) {
    this._isDirty = true;
    this.json.shape = newValue;
  }

  /// field: string
  get borderColor(): string | undefined {
    return this.json.borderColor;
  }

  set borderColor(newValue: string | undefined) {
    this._isDirty = true;
    this.json.borderColor = newValue;
  }

  /// field: string
  get backgroundColor(): string | undefined {
    return this.json.backgroundColor;
  }

  set backgroundColor(newValue: string | undefined) {
    this._isDirty = true;
    this.json.backgroundColor = newValue;
  }

  /// field: uint
  get borderWidth(): number | undefined {
    return this.json.borderWidth;
  }

  set borderWidth(newValue: number | undefined) {
    this._isDirty = true;
    this.json.borderWidth = newValue;
  }

  /// field: uint
  get size(): number | undefined {
    return this.json.size;
  }

  set size(newValue: number | undefined) {
    this._isDirty = true;
    this.json.size = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): PointStylePropertiesJson {
    return this.json;
  }
}
