import { Store } from "./store";

export interface SocialMediaLinks {
  facebook?: string;
  youtube?: string;
  xTwitter?: string;
  instagram?: string;
  whatsApp?: string;
  tikTok?: string;
  linkedin?: string;
}

export interface SocialMediaLinksJson {
  facebook?: string;
  youtube?: string;
  xTwitter?: string;
  instagram?: string;
  whatsApp?: string;
  tikTok?: string;
  linkedin?: string;
}

export class SocialMediaLinksModel {
  private json: SocialMediaLinksJson;
  private _isDirty: boolean = false;

  constructor(json: SocialMediaLinksJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: string
  get facebook(): string | undefined {
    return this.json.facebook;
  }

  set facebook(newValue: string | undefined) {
    this._isDirty = true;
    this.json.facebook = newValue;
  }

  /// field: string
  get youtube(): string | undefined {
    return this.json.youtube;
  }

  set youtube(newValue: string | undefined) {
    this._isDirty = true;
    this.json.youtube = newValue;
  }

  /// field: string
  get xTwitter(): string | undefined {
    return this.json.xTwitter;
  }

  set xTwitter(newValue: string | undefined) {
    this._isDirty = true;
    this.json.xTwitter = newValue;
  }

  /// field: string
  get instagram(): string | undefined {
    return this.json.instagram;
  }

  set instagram(newValue: string | undefined) {
    this._isDirty = true;
    this.json.instagram = newValue;
  }

  /// field: string
  get whatsApp(): string | undefined {
    return this.json.whatsApp;
  }

  set whatsApp(newValue: string | undefined) {
    this._isDirty = true;
    this.json.whatsApp = newValue;
  }

  /// field: string
  get tikTok(): string | undefined {
    return this.json.tikTok;
  }

  set tikTok(newValue: string | undefined) {
    this._isDirty = true;
    this.json.tikTok = newValue;
  }

  /// field: string
  get linkedin(): string | undefined {
    return this.json.linkedin;
  }

  set linkedin(newValue: string | undefined) {
    this._isDirty = true;
    this.json.linkedin = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): SocialMediaLinksJson {
    return this.json;
  }
}
