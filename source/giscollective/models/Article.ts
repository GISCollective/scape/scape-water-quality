import { DateTime } from "luxon";
import { Visibility, VisibilityJson, VisibilityModel } from "./Visibility";
import { Event, EventModel } from "./Event";
import { Store } from "./store";
import { ModelInfo, ModelInfoJson, ModelInfoModel } from "./ModelInfo";
import { ArticleBody } from "./ArticleBody";
import { Picture, PictureModel } from "./Picture";

export interface Article {
  _id: string;
  slug?: string;
  title: string;
  content: ArticleBody;
  categories?: string[];
  type?: string;
  relatedId?: string;
  status?: string;
  subject?: string;
  order?: number;
  releaseDate?: DateTime;
  info: ModelInfo;
  visibility: Visibility;
  cover?: Picture;
  pictures?: Picture[];
  events?: Event[];
}

export interface ArticleResponse {
  article: ArticleJson;
}

export interface ArticlesResponse {
  articles: ArticleJson[];
}

export interface ArticleJson {
  _id: string;
  slug?: string;
  title: string;
  content: ArticleBody;
  categories?: string[];
  type?: string;
  relatedId?: string;
  status?: string;
  subject?: string;
  order?: number;
  releaseDate?: string;
  info: ModelInfoJson;
  visibility: VisibilityJson;
  cover?: string;
  pictures?: string[];
  events?: string[];
}

export class ArticleModel {
  private json: ArticleJson;
  private _isDirty: boolean = false;

  constructor(json: ArticleJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      title: "",
      content: { blocks: [] },
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      visibility: { isPublic: false, team: "" },
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get slug(): string | undefined {
    return this.json.slug;
  }

  set slug(newValue: string | undefined) {
    this._isDirty = true;
    this.json.slug = newValue;
  }

  /// field: string
  get title(): string {
    return this.json.title;
  }

  set title(newValue: string) {
    this._isDirty = true;
    this.json.title = newValue;
  }

  /// field: Json
  get content(): ArticleBody {
    return this.json.content;
  }

  set content(newValue: ArticleBody) {
    this._isDirty = true;
    this.json.content = newValue;
  }

  /// field: string
  get categories(): string[] | undefined {
    return this.json.categories;
  }

  set categories(newValue: string[] | undefined) {
    this._isDirty = true;
    this.json.categories = newValue;
  }

  /// field: string
  get type(): string | undefined {
    return this.json.type;
  }

  set type(newValue: string | undefined) {
    this._isDirty = true;
    this.json.type = newValue;
  }

  /// field: string
  get relatedId(): string | undefined {
    return this.json.relatedId;
  }

  set relatedId(newValue: string | undefined) {
    this._isDirty = true;
    this.json.relatedId = newValue;
  }

  /// field: string
  get status(): string | undefined {
    return this.json.status;
  }

  set status(newValue: string | undefined) {
    this._isDirty = true;
    this.json.status = newValue;
  }

  /// field: string
  get subject(): string | undefined {
    return this.json.subject;
  }

  set subject(newValue: string | undefined) {
    this._isDirty = true;
    this.json.subject = newValue;
  }

  /// field: long
  get order(): number | undefined {
    return this.json.order;
  }

  set order(newValue: number | undefined) {
    this._isDirty = true;
    this.json.order = newValue;
  }

  /// object: SysTime
  get releaseDate(): DateTime | undefined {
    if (!this.json.releaseDate) {
      return undefined;
    }
    return DateTime.fromISO(this.json.releaseDate);
  }

  set releaseDate(newValue: DateTime | undefined) {
    this._isDirty = true;
    this.json.releaseDate = newValue?.toISO() ?? undefined;
  }

  /// object: ModelInfo
  get info(): ModelInfoModel {
    return new ModelInfoModel(this.json.info);
  }

  set info(newValue: ModelInfoModel) {
    this._isDirty = true;

    this.json.info = newValue?.toJSON();
  }

  /// object: Visibility
  get visibility(): VisibilityModel {
    return new VisibilityModel(this.json.visibility);
  }

  set visibility(newValue: VisibilityModel) {
    this._isDirty = true;

    this.json.visibility = newValue?.toJSON();
  }

  /// model: Picture
  get cover(): Promise<PictureModel | undefined> {
    if (!this.json.cover) {
      return Promise.resolve(undefined);
    }
    return Store.instance.findPicture(this.json.cover);
  }

  set cover(newValue: PictureModel | undefined) {
    this._isDirty = true;
    this.json.cover = newValue?._id;
  }

  /// model: Picture
  get pictures(): Promise<PictureModel[] | undefined> {
    const promises =
      this.json.pictures?.map((a) => Store.instance.findPicture(a)) ?? [];

    return Promise.all(promises);
  }

  set pictures(newValue: PictureModel[] | undefined) {
    this._isDirty = true;
    this.json.pictures = newValue?.map((a) => a._id) ?? [];
  }

  /// model: Event
  get events(): Promise<EventModel[] | undefined> {
    const promises =
      this.json.events?.map((a) => Store.instance.findEvent(a)) ?? [];

    return Promise.all(promises);
  }

  set events(newValue: EventModel[] | undefined) {
    this._isDirty = true;
    this.json.events = newValue?.map((a) => a._id) ?? [];
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.info?.isDirty) {
      return true;
    }

    if (this.visibility?.isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<ArticleModel> {
    let result: ArticleModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createArticle(this);
    } else {
      result = await Store.instance.saveArticle(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): ArticleJson {
    return this.json;
  }
}
