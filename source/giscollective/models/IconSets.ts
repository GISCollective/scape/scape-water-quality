import { IconSet, IconSetModel } from "./IconSet";
import { Store } from "./store";

export interface IconSets {
  useCustomList: boolean;
  list: IconSet[];
}

export interface IconSetsJson {
  useCustomList: boolean;
  list: string[];
}

export class IconSetsModel {
  private json: IconSetsJson;
  private _isDirty: boolean = false;

  constructor(json: IconSetsJson | null | undefined) {
    this.json = json ?? { useCustomList: false, list: [] };
  }

  /// field: bool
  get useCustomList(): boolean {
    return this.json.useCustomList;
  }

  set useCustomList(newValue: boolean) {
    this._isDirty = true;
    this.json.useCustomList = newValue;
  }

  /// model: IconSet
  get list(): Promise<IconSetModel[]> {
    const promises =
      this.json.list?.map((a) => Store.instance.findIconSet(a)) ?? [];

    return Promise.all(promises);
  }

  set list(newValue: IconSetModel[]) {
    this._isDirty = true;
    this.json.list = newValue?.map((a) => a._id) ?? [];
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): IconSetsJson {
    return this.json;
  }
}
