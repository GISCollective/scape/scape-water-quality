import { GeoJsonGeometry } from "./GeoJsonGeometry";
import { Store } from "./store";

export interface FeatureFields {
  isGeoCoding?: boolean;
  area?: number;
  distance?: number;
  maps?: string[];
  centroid?: GeoJsonGeometry;
}

export interface FeatureFieldsJson {
  isGeoCoding?: boolean;
  area?: number;
  distance?: number;
  maps?: string[];
  centroid?: GeoJsonGeometry;
}

export class FeatureFieldsModel {
  private json: FeatureFieldsJson;
  private _isDirty: boolean = false;

  constructor(json: FeatureFieldsJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: bool
  get isGeoCoding(): boolean | undefined {
    return this.json.isGeoCoding;
  }

  set isGeoCoding(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.isGeoCoding = newValue;
  }

  /// field: ulong
  get area(): number | undefined {
    return this.json.area;
  }

  set area(newValue: number | undefined) {
    this._isDirty = true;
    this.json.area = newValue;
  }

  /// field: ulong
  get distance(): number | undefined {
    return this.json.distance;
  }

  set distance(newValue: number | undefined) {
    this._isDirty = true;
    this.json.distance = newValue;
  }

  /// field: string
  get maps(): string[] | undefined {
    return this.json.maps;
  }

  set maps(newValue: string[] | undefined) {
    this._isDirty = true;
    this.json.maps = newValue;
  }

  /// object: GeoJsonGeometry
  get centroid(): GeoJsonGeometry {
    return this.json.centroid;
  }

  set centroid(newValue: GeoJsonGeometry | undefined) {
    this._isDirty = true;

    this.json.centroid = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): FeatureFieldsJson {
    return this.json;
  }
}
