import { Store } from "./store";

export interface ColorPalette {
  blue: string;
  indigo: string;
  purple: string;
  pink: string;
  red: string;
  orange: string;
  yellow: string;
  green: string;
  teal: string;
  cyan: string;
}

export interface ColorPaletteJson {
  blue: string;
  indigo: string;
  purple: string;
  pink: string;
  red: string;
  orange: string;
  yellow: string;
  green: string;
  teal: string;
  cyan: string;
}

export class ColorPaletteModel {
  private json: ColorPaletteJson;
  private _isDirty: boolean = false;

  constructor(json: ColorPaletteJson | null | undefined) {
    this.json = json ?? {
      blue: "",
      indigo: "",
      purple: "",
      pink: "",
      red: "",
      orange: "",
      yellow: "",
      green: "",
      teal: "",
      cyan: "",
    };
  }

  /// field: string
  get blue(): string {
    return this.json.blue;
  }

  set blue(newValue: string) {
    this._isDirty = true;
    this.json.blue = newValue;
  }

  /// field: string
  get indigo(): string {
    return this.json.indigo;
  }

  set indigo(newValue: string) {
    this._isDirty = true;
    this.json.indigo = newValue;
  }

  /// field: string
  get purple(): string {
    return this.json.purple;
  }

  set purple(newValue: string) {
    this._isDirty = true;
    this.json.purple = newValue;
  }

  /// field: string
  get pink(): string {
    return this.json.pink;
  }

  set pink(newValue: string) {
    this._isDirty = true;
    this.json.pink = newValue;
  }

  /// field: string
  get red(): string {
    return this.json.red;
  }

  set red(newValue: string) {
    this._isDirty = true;
    this.json.red = newValue;
  }

  /// field: string
  get orange(): string {
    return this.json.orange;
  }

  set orange(newValue: string) {
    this._isDirty = true;
    this.json.orange = newValue;
  }

  /// field: string
  get yellow(): string {
    return this.json.yellow;
  }

  set yellow(newValue: string) {
    this._isDirty = true;
    this.json.yellow = newValue;
  }

  /// field: string
  get green(): string {
    return this.json.green;
  }

  set green(newValue: string) {
    this._isDirty = true;
    this.json.green = newValue;
  }

  /// field: string
  get teal(): string {
    return this.json.teal;
  }

  set teal(newValue: string) {
    this._isDirty = true;
    this.json.teal = newValue;
  }

  /// field: string
  get cyan(): string {
    return this.json.cyan;
  }

  set cyan(newValue: string) {
    this._isDirty = true;
    this.json.cyan = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): ColorPaletteJson {
    return this.json;
  }
}
