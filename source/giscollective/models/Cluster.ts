import { Store } from "./store";

export interface Cluster {
  mode: number;
  map?: string;
}

export interface ClusterJson {
  mode: number;
  map?: string;
}

export class ClusterModel {
  private json: ClusterJson;
  private _isDirty: boolean = false;

  constructor(json: ClusterJson | null | undefined) {
    this.json = json ?? { mode: 0 };
  }

  /// field: int
  get mode(): number {
    return this.json.mode;
  }

  set mode(newValue: number) {
    this._isDirty = true;
    this.json.mode = newValue;
  }

  /// field: string
  get map(): string | undefined {
    return this.json.map;
  }

  set map(newValue: string | undefined) {
    this._isDirty = true;
    this.json.map = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): ClusterJson {
    return this.json;
  }
}
