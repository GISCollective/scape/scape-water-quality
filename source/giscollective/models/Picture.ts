import { PictureMeta, PictureMetaJson, PictureMetaModel } from "./PictureMeta";
import { Store } from "./store";

export interface Picture {
  _id: string;
  name: string;
  owner?: string;
  hash?: string;
  sourceUrl?: string;
  picture: string;
  meta?: PictureMeta;
}

export interface PictureResponse {
  picture: PictureJson;
}

export interface PicturesResponse {
  pictures: PictureJson[];
}

export interface PictureJson {
  _id: string;
  name: string;
  owner?: string;
  hash?: string;
  sourceUrl?: string;
  picture: string;
  meta?: PictureMetaJson;
}

export class PictureModel {
  private json: PictureJson;
  private _isDirty: boolean = false;

  constructor(json: PictureJson | null | undefined) {
    this.json = json ?? { _id: "", name: "", picture: "" };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: string
  get owner(): string | undefined {
    return this.json.owner;
  }

  set owner(newValue: string | undefined) {
    this._isDirty = true;
    this.json.owner = newValue;
  }

  /// field: string
  get hash(): string | undefined {
    return this.json.hash;
  }

  set hash(newValue: string | undefined) {
    this._isDirty = true;
    this.json.hash = newValue;
  }

  /// field: string
  get sourceUrl(): string | undefined {
    return this.json.sourceUrl;
  }

  set sourceUrl(newValue: string | undefined) {
    this._isDirty = true;
    this.json.sourceUrl = newValue;
  }

  /// object: string
  get picture(): string {
    return this.json.picture;
  }

  set picture(newValue: string) {
    this._isDirty = true;
    this.json.picture = newValue;
  }

  /// object: PictureMeta
  get meta(): PictureMetaModel {
    return new PictureMetaModel(this.json.meta);
  }

  set meta(newValue: PictureMetaModel | undefined) {
    this._isDirty = true;

    this.json.meta = newValue?.toJSON();
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.picture?.isDirty) {
      return true;
    }

    if (this.meta?.isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<PictureModel> {
    let result: PictureModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createPicture(this);
    } else {
      result = await Store.instance.savePicture(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): PictureJson {
    return this.json;
  }
}
