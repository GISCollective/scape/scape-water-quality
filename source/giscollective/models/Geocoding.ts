import { License, LicenseJson, LicenseModel } from "./License";
import { GeoJsonGeometry } from "./GeoJsonGeometry";
import { Store } from "./store";

export interface Geocoding {
  _id: string;
  name: string;
  icons: string[];
  score: number;
  geometry: GeoJsonGeometry;
  license: License;
}

export interface GeocodingResponse {
  geocoding: GeocodingJson;
}

export interface GeocodingsResponse {
  geocodings: GeocodingJson[];
}

export interface GeocodingJson {
  _id: string;
  name: string;
  icons: string[];
  score: number;
  geometry: GeoJsonGeometry;
  license: LicenseJson;
}

export class GeocodingModel {
  private json: GeocodingJson;
  private _isDirty: boolean = false;

  constructor(json: GeocodingJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      name: "",
      icons: [],
      score: 0,
      geometry: { type: "Point", coordinates: [0, 0] },
      license: {},
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: string
  get icons(): string[] {
    return this.json.icons;
  }

  set icons(newValue: string[]) {
    this._isDirty = true;
    this.json.icons = newValue;
  }

  /// field: double
  get score(): number {
    return this.json.score;
  }

  set score(newValue: number) {
    this._isDirty = true;
    this.json.score = newValue;
  }

  /// object: GeoJsonGeometry
  get geometry(): GeoJsonGeometry {
    return this.json.geometry;
  }

  set geometry(newValue: GeoJsonGeometry) {
    this._isDirty = true;

    this.json.geometry = newValue;
  }

  /// object: License
  get license(): LicenseModel {
    return new LicenseModel(this.json.license);
  }

  set license(newValue: LicenseModel) {
    this._isDirty = true;

    this.json.license = newValue?.toJSON();
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.license?.isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<GeocodingModel> {
    let result: GeocodingModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createGeocoding(this);
    } else {
      result = await Store.instance.saveGeocoding(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): GeocodingJson {
    return this.json;
  }
}
