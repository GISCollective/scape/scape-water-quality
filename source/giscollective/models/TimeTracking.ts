import { DateTime } from "luxon";
import { Store } from "./store";

export interface TimeTracking {
  hours: number;
  details: string;
  date: DateTime;
}

export interface TimeTrackingJson {
  hours: number;
  details: string;
  date: string;
}

export class TimeTrackingModel {
  private json: TimeTrackingJson;
  private _isDirty: boolean = false;

  constructor(json: TimeTrackingJson | null | undefined) {
    this.json = json ?? { hours: 0, details: "", date: "" };
  }

  /// field: double
  get hours(): number {
    return this.json.hours;
  }

  set hours(newValue: number) {
    this._isDirty = true;
    this.json.hours = newValue;
  }

  /// field: string
  get details(): string {
    return this.json.details;
  }

  set details(newValue: string) {
    this._isDirty = true;
    this.json.details = newValue;
  }

  /// object: SysTime
  get date(): DateTime {
    return DateTime.fromISO(this.json.date);
  }

  set date(newValue: DateTime) {
    this._isDirty = true;
    this.json.date = newValue.toISO();
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): TimeTrackingJson {
    return this.json;
  }
}
