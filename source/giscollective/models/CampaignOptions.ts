import {
  CampaignPicturesQuestion,
  CampaignPicturesQuestionJson,
  CampaignPicturesQuestionModel,
} from "./CampaignPicturesQuestion";
import {
  CampaignLabelQuestion,
  CampaignLabelQuestionJson,
  CampaignLabelQuestionModel,
} from "./CampaignLabelQuestion";
import {
  CampaignLocationQuestion,
  CampaignLocationQuestionJson,
  CampaignLocationQuestionModel,
} from "./CampaignLocationQuestion";
import { Store } from "./store";

export interface CampaignOptions {
  featureNamePrefix: string;
  showNameQuestion?: boolean;
  nameLabel?: string;
  showDescriptionQuestion?: boolean;
  descriptionLabel?: string;
  iconsLabel?: string;
  registrationMandatory: boolean;
  name?: CampaignLabelQuestion;
  description?: CampaignLabelQuestion;
  icons?: CampaignLabelQuestion;
  location?: CampaignLocationQuestion;
  pictures?: CampaignPicturesQuestion;
}

export interface CampaignOptionsJson {
  featureNamePrefix: string;
  showNameQuestion?: boolean;
  nameLabel?: string;
  showDescriptionQuestion?: boolean;
  descriptionLabel?: string;
  iconsLabel?: string;
  registrationMandatory: boolean;
  name?: CampaignLabelQuestionJson;
  description?: CampaignLabelQuestionJson;
  icons?: CampaignLabelQuestionJson;
  location?: CampaignLocationQuestionJson;
  pictures?: CampaignPicturesQuestionJson;
}

export class CampaignOptionsModel {
  private json: CampaignOptionsJson;
  private _isDirty: boolean = false;

  constructor(json: CampaignOptionsJson | null | undefined) {
    this.json = json ?? { featureNamePrefix: "", registrationMandatory: false };
  }

  /// field: string
  get featureNamePrefix(): string {
    return this.json.featureNamePrefix;
  }

  set featureNamePrefix(newValue: string) {
    this._isDirty = true;
    this.json.featureNamePrefix = newValue;
  }

  /// field: bool
  get showNameQuestion(): boolean | undefined {
    return this.json.showNameQuestion;
  }

  set showNameQuestion(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.showNameQuestion = newValue;
  }

  /// field: string
  get nameLabel(): string | undefined {
    return this.json.nameLabel;
  }

  set nameLabel(newValue: string | undefined) {
    this._isDirty = true;
    this.json.nameLabel = newValue;
  }

  /// field: bool
  get showDescriptionQuestion(): boolean | undefined {
    return this.json.showDescriptionQuestion;
  }

  set showDescriptionQuestion(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.showDescriptionQuestion = newValue;
  }

  /// field: string
  get descriptionLabel(): string | undefined {
    return this.json.descriptionLabel;
  }

  set descriptionLabel(newValue: string | undefined) {
    this._isDirty = true;
    this.json.descriptionLabel = newValue;
  }

  /// field: string
  get iconsLabel(): string | undefined {
    return this.json.iconsLabel;
  }

  set iconsLabel(newValue: string | undefined) {
    this._isDirty = true;
    this.json.iconsLabel = newValue;
  }

  /// field: bool
  get registrationMandatory(): boolean {
    return this.json.registrationMandatory;
  }

  set registrationMandatory(newValue: boolean) {
    this._isDirty = true;
    this.json.registrationMandatory = newValue;
  }

  /// object: CampaignLabelQuestion
  get name(): CampaignLabelQuestionModel {
    return new CampaignLabelQuestionModel(this.json.name);
  }

  set name(newValue: CampaignLabelQuestionModel | undefined) {
    this._isDirty = true;

    this.json.name = newValue?.toJSON();
  }

  /// object: CampaignLabelQuestion
  get description(): CampaignLabelQuestionModel {
    return new CampaignLabelQuestionModel(this.json.description);
  }

  set description(newValue: CampaignLabelQuestionModel | undefined) {
    this._isDirty = true;

    this.json.description = newValue?.toJSON();
  }

  /// object: CampaignLabelQuestion
  get icons(): CampaignLabelQuestionModel {
    return new CampaignLabelQuestionModel(this.json.icons);
  }

  set icons(newValue: CampaignLabelQuestionModel | undefined) {
    this._isDirty = true;

    this.json.icons = newValue?.toJSON();
  }

  /// object: CampaignLocationQuestion
  get location(): CampaignLocationQuestionModel {
    return new CampaignLocationQuestionModel(this.json.location);
  }

  set location(newValue: CampaignLocationQuestionModel | undefined) {
    this._isDirty = true;

    this.json.location = newValue?.toJSON();
  }

  /// object: CampaignPicturesQuestion
  get pictures(): CampaignPicturesQuestionModel {
    return new CampaignPicturesQuestionModel(this.json.pictures);
  }

  set pictures(newValue: CampaignPicturesQuestionModel | undefined) {
    this._isDirty = true;

    this.json.pictures = newValue?.toJSON();
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.name?.isDirty) {
      return true;
    }

    if (this.description?.isDirty) {
      return true;
    }

    if (this.icons?.isDirty) {
      return true;
    }

    if (this.location?.isDirty) {
      return true;
    }

    if (this.pictures?.isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): CampaignOptionsJson {
    return this.json;
  }
}
