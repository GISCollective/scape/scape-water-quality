import { DateTime } from "luxon";
import {
  TimeTracking,
  TimeTrackingJson,
  TimeTrackingModel,
} from "./TimeTracking";
import { Store } from "./store";

export interface SubscriptionDetails {
  name: string;
  domains: string[];
  comment: string;
  details?: string;
  monthlySupportHours?: number;
  expire: DateTime;
  support?: TimeTracking[];
  invoices?: TimeTracking[];
}

export interface SubscriptionDetailsJson {
  name: string;
  domains: string[];
  comment: string;
  details?: string;
  monthlySupportHours?: number;
  expire: string;
  support?: TimeTrackingJson[];
  invoices?: TimeTrackingJson[];
}

export class SubscriptionDetailsModel {
  private json: SubscriptionDetailsJson;
  private _isDirty: boolean = false;

  constructor(json: SubscriptionDetailsJson | null | undefined) {
    this.json = json ?? { name: "", domains: [], comment: "", expire: "" };
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: string
  get domains(): string[] {
    return this.json.domains;
  }

  set domains(newValue: string[]) {
    this._isDirty = true;
    this.json.domains = newValue;
  }

  /// field: string
  get comment(): string {
    return this.json.comment;
  }

  set comment(newValue: string) {
    this._isDirty = true;
    this.json.comment = newValue;
  }

  /// field: string
  get details(): string | undefined {
    return this.json.details;
  }

  set details(newValue: string | undefined) {
    this._isDirty = true;
    this.json.details = newValue;
  }

  /// field: double
  get monthlySupportHours(): number | undefined {
    return this.json.monthlySupportHours;
  }

  set monthlySupportHours(newValue: number | undefined) {
    this._isDirty = true;
    this.json.monthlySupportHours = newValue;
  }

  /// object: SysTime
  get expire(): DateTime {
    return DateTime.fromISO(this.json.expire);
  }

  set expire(newValue: DateTime) {
    this._isDirty = true;
    this.json.expire = newValue.toISO();
  }

  /// object: TimeTracking
  get support(): TimeTrackingModel[] {
    return this.json.support?.map((a) => new TimeTrackingModel(a)) ?? [];
  }

  set support(newValue: TimeTrackingModel[] | undefined) {
    this._isDirty = true;

    this.json.support = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// object: TimeTracking
  get invoices(): TimeTrackingModel[] {
    return this.json.invoices?.map((a) => new TimeTrackingModel(a)) ?? [];
  }

  set invoices(newValue: TimeTrackingModel[] | undefined) {
    this._isDirty = true;

    this.json.invoices = newValue?.map((a) => a.toJSON()) ?? [];
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    for (let level0 of this.support) {
      if (level0?.isDirty) {
        return true;
      }
    }

    for (let level0 of this.invoices) {
      if (level0?.isDirty) {
        return true;
      }
    }

    return false;
  }

  toJSON(): SubscriptionDetailsJson {
    return this.json;
  }
}
