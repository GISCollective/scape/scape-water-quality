import { Store } from "./store";

export interface CampaignPicturesQuestion {
  customQuestion?: boolean;
  label?: string;
  isMandatory?: boolean;
}

export interface CampaignPicturesQuestionJson {
  customQuestion?: boolean;
  label?: string;
  isMandatory?: boolean;
}

export class CampaignPicturesQuestionModel {
  private json: CampaignPicturesQuestionJson;
  private _isDirty: boolean = false;

  constructor(json: CampaignPicturesQuestionJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: bool
  get customQuestion(): boolean | undefined {
    return this.json.customQuestion;
  }

  set customQuestion(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.customQuestion = newValue;
  }

  /// field: string
  get label(): string | undefined {
    return this.json.label;
  }

  set label(newValue: string | undefined) {
    this._isDirty = true;
    this.json.label = newValue;
  }

  /// field: bool
  get isMandatory(): boolean | undefined {
    return this.json.isMandatory;
  }

  set isMandatory(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.isMandatory = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): CampaignPicturesQuestionJson {
    return this.json;
  }
}
