import { DateTime } from "luxon";
import { Store } from "./store";

export interface User {
  _id: string;
  salutation?: string;
  title?: string;
  firstName?: string;
  lastName?: string;
  username: string;
  email: string;
  isActive: boolean;
  lastActivity?: number;
  createdAt?: DateTime;
}

export interface UserResponse {
  user: UserJson;
}

export interface UsersResponse {
  users: UserJson[];
}

export interface UserJson {
  _id: string;
  salutation?: string;
  title?: string;
  firstName?: string;
  lastName?: string;
  username: string;
  email: string;
  isActive: boolean;
  lastActivity?: number;
  createdAt?: string;
}

export class UserModel {
  private json: UserJson;
  private _isDirty: boolean = false;

  constructor(json: UserJson | null | undefined) {
    this.json = json ?? { _id: "", username: "", email: "", isActive: false };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get salutation(): string | undefined {
    return this.json.salutation;
  }

  set salutation(newValue: string | undefined) {
    this._isDirty = true;
    this.json.salutation = newValue;
  }

  /// field: string
  get title(): string | undefined {
    return this.json.title;
  }

  set title(newValue: string | undefined) {
    this._isDirty = true;
    this.json.title = newValue;
  }

  /// field: string
  get firstName(): string | undefined {
    return this.json.firstName;
  }

  set firstName(newValue: string | undefined) {
    this._isDirty = true;
    this.json.firstName = newValue;
  }

  /// field: string
  get lastName(): string | undefined {
    return this.json.lastName;
  }

  set lastName(newValue: string | undefined) {
    this._isDirty = true;
    this.json.lastName = newValue;
  }

  /// field: string
  get username(): string {
    return this.json.username;
  }

  set username(newValue: string) {
    this._isDirty = true;
    this.json.username = newValue;
  }

  /// field: string
  get email(): string {
    return this.json.email;
  }

  set email(newValue: string) {
    this._isDirty = true;
    this.json.email = newValue;
  }

  /// field: bool
  get isActive(): boolean {
    return this.json.isActive;
  }

  set isActive(newValue: boolean) {
    this._isDirty = true;
    this.json.isActive = newValue;
  }

  /// field: ulong
  get lastActivity(): number | undefined {
    return this.json.lastActivity;
  }

  set lastActivity(newValue: number | undefined) {
    this._isDirty = true;
    this.json.lastActivity = newValue;
  }

  /// object: SysTime
  get createdAt(): DateTime | undefined {
    if (!this.json.createdAt) {
      return undefined;
    }
    return DateTime.fromISO(this.json.createdAt);
  }

  set createdAt(newValue: DateTime | undefined) {
    this._isDirty = true;
    this.json.createdAt = newValue?.toISO() ?? undefined;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<UserModel> {
    let result: UserModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createUser(this);
    } else {
      result = await Store.instance.saveUser(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): UserJson {
    return this.json;
  }
}
