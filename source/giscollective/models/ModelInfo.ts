import { DateTime } from "luxon";
import { Store } from "./store";

export interface ModelInfo {
  changeIndex: number;
  author: string;
  originalAuthor?: string;
  createdOn: DateTime;
  lastChangeOn: DateTime;
}

export interface ModelInfoJson {
  changeIndex: number;
  author: string;
  originalAuthor?: string;
  createdOn: string;
  lastChangeOn: string;
}

export class ModelInfoModel {
  private json: ModelInfoJson;
  private _isDirty: boolean = false;

  constructor(json: ModelInfoJson | null | undefined) {
    this.json = json ?? {
      changeIndex: 0,
      author: "",
      createdOn: "",
      lastChangeOn: "",
    };
  }

  /// field: ulong
  get changeIndex(): number {
    return this.json.changeIndex;
  }

  set changeIndex(newValue: number) {
    this._isDirty = true;
    this.json.changeIndex = newValue;
  }

  /// field: string
  get author(): string {
    return this.json.author;
  }

  set author(newValue: string) {
    this._isDirty = true;
    this.json.author = newValue;
  }

  /// field: string
  get originalAuthor(): string | undefined {
    return this.json.originalAuthor;
  }

  set originalAuthor(newValue: string | undefined) {
    this._isDirty = true;
    this.json.originalAuthor = newValue;
  }

  /// object: SysTime
  get createdOn(): DateTime {
    return DateTime.fromISO(this.json.createdOn);
  }

  set createdOn(newValue: DateTime) {
    this._isDirty = true;
    this.json.createdOn = newValue.toISO();
  }

  /// object: SysTime
  get lastChangeOn(): DateTime {
    return DateTime.fromISO(this.json.lastChangeOn);
  }

  set lastChangeOn(newValue: DateTime) {
    this._isDirty = true;
    this.json.lastChangeOn = newValue.toISO();
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): ModelInfoJson {
    return this.json;
  }
}
