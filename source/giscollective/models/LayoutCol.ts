import { Store } from "./store";

export interface LayoutCol {
  type?: string;
  data?: any;
  options?: string[];
  componentCount?: number;
  name?: string;
}

export interface LayoutColJson {
  type?: string;
  data?: any;
  options?: string[];
  componentCount?: number;
  name?: string;
}

export class LayoutColModel {
  private json: LayoutColJson;
  private _isDirty: boolean = false;

  constructor(json: LayoutColJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: string
  get type(): string | undefined {
    return this.json.type;
  }

  set type(newValue: string | undefined) {
    this._isDirty = true;
    this.json.type = newValue;
  }

  /// field: Json
  get data(): any | undefined {
    return this.json.data;
  }

  set data(newValue: any | undefined) {
    this._isDirty = true;
    this.json.data = newValue;
  }

  /// field: string
  get options(): string[] | undefined {
    return this.json.options;
  }

  set options(newValue: string[] | undefined) {
    this._isDirty = true;
    this.json.options = newValue;
  }

  /// field: int
  get componentCount(): number | undefined {
    return this.json.componentCount;
  }

  set componentCount(newValue: number | undefined) {
    this._isDirty = true;
    this.json.componentCount = newValue;
  }

  /// field: string
  get name(): string | undefined {
    return this.json.name;
  }

  set name(newValue: string | undefined) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): LayoutColJson {
    return this.json;
  }
}
