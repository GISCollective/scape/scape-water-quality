import { Team, TeamModel } from "./Team";
import { Store } from "./store";

export interface VisibilityOptional {
  isPublic?: boolean;
  isDefault?: boolean;
  team?: Team;
}

export interface VisibilityOptionalJson {
  isPublic?: boolean;
  isDefault?: boolean;
  team?: string;
}

export class VisibilityOptionalModel {
  private json: VisibilityOptionalJson;
  private _isDirty: boolean = false;

  constructor(json: VisibilityOptionalJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: bool
  get isPublic(): boolean | undefined {
    return this.json.isPublic;
  }

  set isPublic(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.isPublic = newValue;
  }

  /// field: bool
  get isDefault(): boolean | undefined {
    return this.json.isDefault;
  }

  set isDefault(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.isDefault = newValue;
  }

  /// model: Team
  get team(): Promise<TeamModel | undefined> {
    if (!this.json.team) {
      return Promise.resolve(undefined);
    }
    return Store.instance.findTeam(this.json.team);
  }

  set team(newValue: TeamModel | undefined) {
    this._isDirty = true;
    this.json.team = newValue?._id;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): VisibilityOptionalJson {
    return this.json;
  }
}
