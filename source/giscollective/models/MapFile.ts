import {
  MapFileOptions,
  MapFileOptionsJson,
  MapFileOptionsModel,
} from "./MapFileOptions";
import { Map, MapModel } from "./Map";
import { Store } from "./store";

export interface MapFile {
  _id: string;
  file: string;
  options?: MapFileOptions;
  map: Map;
}

export interface MapFileResponse {
  mapFile: MapFileJson;
}

export interface MapFilesResponse {
  mapFiles: MapFileJson[];
}

export interface MapFileJson {
  _id: string;
  file: string;
  options?: MapFileOptionsJson;
  map: string;
}

export class MapFileModel {
  private json: MapFileJson;
  private _isDirty: boolean = false;

  constructor(json: MapFileJson | null | undefined) {
    this.json = json ?? { _id: "", file: "", map: "" };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// object: string
  get file(): string {
    return this.json.file;
  }

  set file(newValue: string) {
    this._isDirty = true;
    this.json.file = newValue;
  }

  /// object: MapFileOptions
  get options(): MapFileOptionsModel {
    return new MapFileOptionsModel(this.json.options);
  }

  set options(newValue: MapFileOptionsModel | undefined) {
    this._isDirty = true;

    this.json.options = newValue?.toJSON();
  }

  /// model: Map
  get map(): Promise<MapModel> {
    return Store.instance.findMap(this.json.map);
  }

  set map(newValue: MapModel) {
    this._isDirty = true;
    this.json.map = newValue?._id;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.file?.isDirty) {
      return true;
    }

    if (this.options?.isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<MapFileModel> {
    let result: MapFileModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createMapFile(this);
    } else {
      result = await Store.instance.saveMapFile(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): MapFileJson {
    return this.json;
  }
}
