import { Store } from "./store";

export interface DataBindingDestination {
  type: string;
  modelId: string;
  deleteNonSyncedRecords: boolean;
}

export interface DataBindingDestinationJson {
  type: string;
  modelId: string;
  deleteNonSyncedRecords: boolean;
}

export class DataBindingDestinationModel {
  private json: DataBindingDestinationJson;
  private _isDirty: boolean = false;

  constructor(json: DataBindingDestinationJson | null | undefined) {
    this.json = json ?? {
      type: "",
      modelId: "",
      deleteNonSyncedRecords: false,
    };
  }

  /// field: string
  get type(): string {
    return this.json.type;
  }

  set type(newValue: string) {
    this._isDirty = true;
    this.json.type = newValue;
  }

  /// field: string
  get modelId(): string {
    return this.json.modelId;
  }

  set modelId(newValue: string) {
    this._isDirty = true;
    this.json.modelId = newValue;
  }

  /// field: bool
  get deleteNonSyncedRecords(): boolean {
    return this.json.deleteNonSyncedRecords;
  }

  set deleteNonSyncedRecords(newValue: boolean) {
    this._isDirty = true;
    this.json.deleteNonSyncedRecords = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): DataBindingDestinationJson {
    return this.json;
  }
}
