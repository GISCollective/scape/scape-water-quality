import { Store } from "./store";

export interface LineStyleProperties {
  borderColor?: string;
  backgroundColor?: string;
  borderWidth?: number;
  lineDash?: number[];
  markerInterval?: number;
}

export interface LineStylePropertiesJson {
  borderColor?: string;
  backgroundColor?: string;
  borderWidth?: number;
  lineDash?: number[];
  markerInterval?: number;
}

export class LineStylePropertiesModel {
  private json: LineStylePropertiesJson;
  private _isDirty: boolean = false;

  constructor(json: LineStylePropertiesJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: string
  get borderColor(): string | undefined {
    return this.json.borderColor;
  }

  set borderColor(newValue: string | undefined) {
    this._isDirty = true;
    this.json.borderColor = newValue;
  }

  /// field: string
  get backgroundColor(): string | undefined {
    return this.json.backgroundColor;
  }

  set backgroundColor(newValue: string | undefined) {
    this._isDirty = true;
    this.json.backgroundColor = newValue;
  }

  /// field: uint
  get borderWidth(): number | undefined {
    return this.json.borderWidth;
  }

  set borderWidth(newValue: number | undefined) {
    this._isDirty = true;
    this.json.borderWidth = newValue;
  }

  /// field: uint
  get lineDash(): number[] | undefined {
    return this.json.lineDash;
  }

  set lineDash(newValue: number[] | undefined) {
    this._isDirty = true;
    this.json.lineDash = newValue;
  }

  /// field: uint
  get markerInterval(): number | undefined {
    return this.json.markerInterval;
  }

  set markerInterval(newValue: number | undefined) {
    this._isDirty = true;
    this.json.markerInterval = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): LineStylePropertiesJson {
    return this.json;
  }
}
