import { Picture, PictureModel } from "./Picture";
import { Icon, IconModel } from "./Icon";
import { DateTime } from "luxon";
import {
  FeatureFields,
  FeatureFieldsJson,
  FeatureFieldsModel,
} from "./FeatureFields";
import { Visibility, VisibilityJson, VisibilityModel } from "./Visibility";
import { Store } from "./store";

export interface SearchMeta {
  _id: string;
  title: string;
  description: string;
  relatedId: string;
  relatedModel: string;
  keywords: string[];
  categories?: string[];
  visibility: Visibility;
  feature?: FeatureFields;
  lastChangeOn?: DateTime;
  lastDuplicateCheckOn?: DateTime;
  icons?: Icon[];
  cover?: Picture;
}

export interface SearchMetaResponse {
  searchMeta: SearchMetaJson;
}

export interface SearchMetasResponse {
  searchMetas: SearchMetaJson[];
}

export interface SearchMetaJson {
  _id: string;
  title: string;
  description: string;
  relatedId: string;
  relatedModel: string;
  keywords: string[];
  categories?: string[];
  visibility: VisibilityJson;
  feature?: FeatureFieldsJson;
  lastChangeOn?: string;
  lastDuplicateCheckOn?: string;
  icons?: string[];
  cover?: string;
}

export class SearchMetaModel {
  private json: SearchMetaJson;
  private _isDirty: boolean = false;

  constructor(json: SearchMetaJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      title: "",
      description: "",
      relatedId: "",
      relatedModel: "",
      keywords: [],
      visibility: { isPublic: false, team: "" },
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get title(): string {
    return this.json.title;
  }

  set title(newValue: string) {
    this._isDirty = true;
    this.json.title = newValue;
  }

  /// field: string
  get description(): string {
    return this.json.description;
  }

  set description(newValue: string) {
    this._isDirty = true;
    this.json.description = newValue;
  }

  /// field: string
  get relatedId(): string {
    return this.json.relatedId;
  }

  set relatedId(newValue: string) {
    this._isDirty = true;
    this.json.relatedId = newValue;
  }

  /// field: string
  get relatedModel(): string {
    return this.json.relatedModel;
  }

  set relatedModel(newValue: string) {
    this._isDirty = true;
    this.json.relatedModel = newValue;
  }

  /// field: string
  get keywords(): string[] {
    return this.json.keywords;
  }

  set keywords(newValue: string[]) {
    this._isDirty = true;
    this.json.keywords = newValue;
  }

  /// field: string
  get categories(): string[] | undefined {
    return this.json.categories;
  }

  set categories(newValue: string[] | undefined) {
    this._isDirty = true;
    this.json.categories = newValue;
  }

  /// object: Visibility
  get visibility(): VisibilityModel {
    return new VisibilityModel(this.json.visibility);
  }

  set visibility(newValue: VisibilityModel) {
    this._isDirty = true;

    this.json.visibility = newValue?.toJSON();
  }

  /// object: FeatureFields
  get feature(): FeatureFieldsModel {
    return new FeatureFieldsModel(this.json.feature);
  }

  set feature(newValue: FeatureFieldsModel | undefined) {
    this._isDirty = true;

    this.json.feature = newValue?.toJSON();
  }

  /// object: SysTime
  get lastChangeOn(): DateTime | undefined {
    if (!this.json.lastChangeOn) {
      return undefined;
    }
    return DateTime.fromISO(this.json.lastChangeOn);
  }

  set lastChangeOn(newValue: DateTime | undefined) {
    this._isDirty = true;
    this.json.lastChangeOn = newValue?.toISO() ?? undefined;
  }

  /// object: SysTime
  get lastDuplicateCheckOn(): DateTime | undefined {
    if (!this.json.lastDuplicateCheckOn) {
      return undefined;
    }
    return DateTime.fromISO(this.json.lastDuplicateCheckOn);
  }

  set lastDuplicateCheckOn(newValue: DateTime | undefined) {
    this._isDirty = true;
    this.json.lastDuplicateCheckOn = newValue?.toISO() ?? undefined;
  }

  /// model: Icon
  get icons(): Promise<IconModel[] | undefined> {
    const promises =
      this.json.icons?.map((a) => Store.instance.findIcon(a)) ?? [];

    return Promise.all(promises);
  }

  set icons(newValue: IconModel[] | undefined) {
    this._isDirty = true;
    this.json.icons = newValue?.map((a) => a._id) ?? [];
  }

  /// model: Picture
  get cover(): Promise<PictureModel | undefined> {
    if (!this.json.cover) {
      return Promise.resolve(undefined);
    }
    return Store.instance.findPicture(this.json.cover);
  }

  set cover(newValue: PictureModel | undefined) {
    this._isDirty = true;
    this.json.cover = newValue?._id;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.visibility?.isDirty) {
      return true;
    }

    if (this.feature?.isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<SearchMetaModel> {
    let result: SearchMetaModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createSearchMeta(this);
    } else {
      result = await Store.instance.saveSearchMeta(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): SearchMetaJson {
    return this.json;
  }
}
