import { DateTime } from "luxon";
import { Question, QuestionJson, QuestionModel } from "./Question";
import { Visibility, VisibilityJson, VisibilityModel } from "./Visibility";
import { Icon, IconModel } from "./Icon";
import {
  CampaignOptions,
  CampaignOptionsJson,
  CampaignOptionsModel,
} from "./CampaignOptions";
import { Store } from "./store";
import { ModelInfo, ModelInfoJson, ModelInfoModel } from "./ModelInfo";
import { ArticleBody } from "./ArticleBody";
import { OptionalMap, OptionalMapJson, OptionalMapModel } from "./OptionalMap";
import { Picture, PictureModel } from "./Picture";

export interface Campaign {
  _id: string;
  name: string;
  article: ArticleBody;
  visibility: Visibility;
  info: ModelInfo;
  map?: OptionalMap;
  startDate?: DateTime;
  endDate?: DateTime;
  options?: CampaignOptions;
  questions?: Question[];
  contributorQuestions?: Question[];
  cover: Picture;
  icons: Icon[];
  optionalIcons?: Icon[];
}

export interface CampaignResponse {
  campaign: CampaignJson;
}

export interface CampaignsResponse {
  campaigns: CampaignJson[];
}

export interface CampaignJson {
  _id: string;
  name: string;
  article: ArticleBody;
  visibility: VisibilityJson;
  info: ModelInfoJson;
  map?: OptionalMapJson;
  startDate?: string;
  endDate?: string;
  options?: CampaignOptionsJson;
  questions?: QuestionJson[];
  contributorQuestions?: QuestionJson[];
  cover: string;
  icons: string[];
  optionalIcons?: string[];
}

export class CampaignModel {
  private json: CampaignJson;
  private _isDirty: boolean = false;

  constructor(json: CampaignJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      name: "",
      article: { blocks: [] },
      visibility: { isPublic: false, team: "" },
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      cover: "",
      icons: [],
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: Json
  get article(): ArticleBody {
    return this.json.article;
  }

  set article(newValue: ArticleBody) {
    this._isDirty = true;
    this.json.article = newValue;
  }

  /// object: Visibility
  get visibility(): VisibilityModel {
    return new VisibilityModel(this.json.visibility);
  }

  set visibility(newValue: VisibilityModel) {
    this._isDirty = true;

    this.json.visibility = newValue?.toJSON();
  }

  /// object: ModelInfo
  get info(): ModelInfoModel {
    return new ModelInfoModel(this.json.info);
  }

  set info(newValue: ModelInfoModel) {
    this._isDirty = true;

    this.json.info = newValue?.toJSON();
  }

  /// object: OptionalMap
  get map(): OptionalMapModel {
    return new OptionalMapModel(this.json.map);
  }

  set map(newValue: OptionalMapModel | undefined) {
    this._isDirty = true;

    this.json.map = newValue?.toJSON();
  }

  /// object: SysTime
  get startDate(): DateTime | undefined {
    if (!this.json.startDate) {
      return undefined;
    }
    return DateTime.fromISO(this.json.startDate);
  }

  set startDate(newValue: DateTime | undefined) {
    this._isDirty = true;
    this.json.startDate = newValue?.toISO() ?? undefined;
  }

  /// object: SysTime
  get endDate(): DateTime | undefined {
    if (!this.json.endDate) {
      return undefined;
    }
    return DateTime.fromISO(this.json.endDate);
  }

  set endDate(newValue: DateTime | undefined) {
    this._isDirty = true;
    this.json.endDate = newValue?.toISO() ?? undefined;
  }

  /// object: CampaignOptions
  get options(): CampaignOptionsModel {
    return new CampaignOptionsModel(this.json.options);
  }

  set options(newValue: CampaignOptionsModel | undefined) {
    this._isDirty = true;

    this.json.options = newValue?.toJSON();
  }

  /// object: Question
  get questions(): QuestionModel[] {
    return this.json.questions?.map((a) => new QuestionModel(a)) ?? [];
  }

  set questions(newValue: QuestionModel[] | undefined) {
    this._isDirty = true;

    this.json.questions = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// object: Question
  get contributorQuestions(): QuestionModel[] {
    return (
      this.json.contributorQuestions?.map((a) => new QuestionModel(a)) ?? []
    );
  }

  set contributorQuestions(newValue: QuestionModel[] | undefined) {
    this._isDirty = true;

    this.json.contributorQuestions = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// model: Picture
  get cover(): Promise<PictureModel> {
    return Store.instance.findPicture(this.json.cover);
  }

  set cover(newValue: PictureModel) {
    this._isDirty = true;
    this.json.cover = newValue?._id;
  }

  /// model: Icon
  get icons(): Promise<IconModel[]> {
    const promises =
      this.json.icons?.map((a) => Store.instance.findIcon(a)) ?? [];

    return Promise.all(promises);
  }

  set icons(newValue: IconModel[]) {
    this._isDirty = true;
    this.json.icons = newValue?.map((a) => a._id) ?? [];
  }

  /// model: Icon
  get optionalIcons(): Promise<IconModel[] | undefined> {
    const promises =
      this.json.optionalIcons?.map((a) => Store.instance.findIcon(a)) ?? [];

    return Promise.all(promises);
  }

  set optionalIcons(newValue: IconModel[] | undefined) {
    this._isDirty = true;
    this.json.optionalIcons = newValue?.map((a) => a._id) ?? [];
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.visibility?.isDirty) {
      return true;
    }

    if (this.info?.isDirty) {
      return true;
    }

    if (this.map?.isDirty) {
      return true;
    }

    if (this.options?.isDirty) {
      return true;
    }

    for (let level0 of this.questions) {
      if (level0?.isDirty) {
        return true;
      }
    }

    for (let level0 of this.contributorQuestions) {
      if (level0?.isDirty) {
        return true;
      }
    }

    return false;
  }

  async save(): Promise<CampaignModel> {
    let result: CampaignModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createCampaign(this);
    } else {
      result = await Store.instance.saveCampaign(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): CampaignJson {
    return this.json;
  }
}
