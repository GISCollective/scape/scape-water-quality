import { DateTime } from "luxon";
import { ArticleBody } from "./ArticleBody";
import { License, LicenseJson, LicenseModel } from "./License";
import { BaseMaps, BaseMapsJson, BaseMapsModel } from "./BaseMaps";
import { Visibility, VisibilityJson, VisibilityModel } from "./Visibility";
import { IconSets, IconSetsJson, IconSetsModel } from "./IconSets";
import { Mask, MaskJson, MaskModel } from "./Mask";
import { Store } from "./store";
import { ModelInfo, ModelInfoJson, ModelInfoModel } from "./ModelInfo";
import { IconSprite, IconSpriteJson, IconSpriteModel } from "./IconSprite";
import { Cluster, ClusterJson, ClusterModel } from "./Cluster";
import { GeoJsonGeometry } from "./GeoJsonGeometry";
import { Picture, PictureModel } from "./Picture";

export interface Map {
  _id: string;
  name: string;
  description: ArticleBody;
  tagLine?: string;
  isIndex?: boolean;
  hideOnMainMap?: boolean;
  showPublicDownloadLinks?: boolean;
  addFeaturesAsPending?: boolean;
  visibility: Visibility;
  area?: GeoJsonGeometry;
  iconSets: IconSets;
  baseMaps?: BaseMaps;
  startDate?: DateTime;
  endDate?: DateTime;
  info?: ModelInfo;
  mask?: Mask;
  license?: License;
  cluster?: Cluster;
  sprites?: IconSprite;
  cover: Picture;
  squareCover?: Picture;
}

export interface MapResponse {
  map: MapJson;
}

export interface MapsResponse {
  maps: MapJson[];
}

export interface MapJson {
  _id: string;
  name: string;
  description: ArticleBody;
  tagLine?: string;
  isIndex?: boolean;
  hideOnMainMap?: boolean;
  showPublicDownloadLinks?: boolean;
  addFeaturesAsPending?: boolean;
  visibility: VisibilityJson;
  area?: GeoJsonGeometryJson;
  iconSets: IconSetsJson;
  baseMaps?: BaseMapsJson;
  startDate?: string;
  endDate?: string;
  info?: ModelInfoJson;
  mask?: MaskJson;
  license?: LicenseJson;
  cluster?: ClusterJson;
  sprites?: IconSpriteJson;
  cover: string;
  squareCover?: string;
}

export class MapModel {
  private json: MapJson;
  private _isDirty: boolean = false;

  constructor(json: MapJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      name: "",
      description: { blocks: [] },
      visibility: { isPublic: false, team: "" },
      iconSets: { useCustomList: false, list: [] },
      cover: "",
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: Json
  get description(): ArticleBody {
    return this.json.description;
  }

  set description(newValue: ArticleBody) {
    this._isDirty = true;
    this.json.description = newValue;
  }

  /// field: string
  get tagLine(): string | undefined {
    return this.json.tagLine;
  }

  set tagLine(newValue: string | undefined) {
    this._isDirty = true;
    this.json.tagLine = newValue;
  }

  /// field: bool
  get isIndex(): boolean | undefined {
    return this.json.isIndex;
  }

  set isIndex(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.isIndex = newValue;
  }

  /// field: bool
  get hideOnMainMap(): boolean | undefined {
    return this.json.hideOnMainMap;
  }

  set hideOnMainMap(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.hideOnMainMap = newValue;
  }

  /// field: bool
  get showPublicDownloadLinks(): boolean | undefined {
    return this.json.showPublicDownloadLinks;
  }

  set showPublicDownloadLinks(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.showPublicDownloadLinks = newValue;
  }

  /// field: bool
  get addFeaturesAsPending(): boolean | undefined {
    return this.json.addFeaturesAsPending;
  }

  set addFeaturesAsPending(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.addFeaturesAsPending = newValue;
  }

  /// object: Visibility
  get visibility(): VisibilityModel {
    return new VisibilityModel(this.json.visibility);
  }

  set visibility(newValue: VisibilityModel) {
    this._isDirty = true;

    this.json.visibility = newValue?.toJSON();
  }

  /// object: Polygon
  get area(): GeoJsonGeometry {
    return this.json.area;
  }

  set area(newValue: GeoJsonGeometry | undefined) {
    this._isDirty = true;

    this.json.area = newValue;
  }

  /// object: IconSets
  get iconSets(): IconSetsModel {
    return new IconSetsModel(this.json.iconSets);
  }

  set iconSets(newValue: IconSetsModel) {
    this._isDirty = true;

    this.json.iconSets = newValue?.toJSON();
  }

  /// object: BaseMaps
  get baseMaps(): BaseMapsModel {
    return new BaseMapsModel(this.json.baseMaps);
  }

  set baseMaps(newValue: BaseMapsModel | undefined) {
    this._isDirty = true;

    this.json.baseMaps = newValue?.toJSON();
  }

  /// object: SysTime
  get startDate(): DateTime | undefined {
    if (!this.json.startDate) {
      return undefined;
    }
    return DateTime.fromISO(this.json.startDate);
  }

  set startDate(newValue: DateTime | undefined) {
    this._isDirty = true;
    this.json.startDate = newValue?.toISO() ?? undefined;
  }

  /// object: SysTime
  get endDate(): DateTime | undefined {
    if (!this.json.endDate) {
      return undefined;
    }
    return DateTime.fromISO(this.json.endDate);
  }

  set endDate(newValue: DateTime | undefined) {
    this._isDirty = true;
    this.json.endDate = newValue?.toISO() ?? undefined;
  }

  /// object: ModelInfo
  get info(): ModelInfoModel {
    return new ModelInfoModel(this.json.info);
  }

  set info(newValue: ModelInfoModel | undefined) {
    this._isDirty = true;

    this.json.info = newValue?.toJSON();
  }

  /// object: Mask
  get mask(): MaskModel {
    return new MaskModel(this.json.mask);
  }

  set mask(newValue: MaskModel | undefined) {
    this._isDirty = true;

    this.json.mask = newValue?.toJSON();
  }

  /// object: License
  get license(): LicenseModel {
    return new LicenseModel(this.json.license);
  }

  set license(newValue: LicenseModel | undefined) {
    this._isDirty = true;

    this.json.license = newValue?.toJSON();
  }

  /// object: Cluster
  get cluster(): ClusterModel {
    return new ClusterModel(this.json.cluster);
  }

  set cluster(newValue: ClusterModel | undefined) {
    this._isDirty = true;

    this.json.cluster = newValue?.toJSON();
  }

  /// object: IconSprite
  get sprites(): IconSpriteModel {
    return new IconSpriteModel(this.json.sprites);
  }

  set sprites(newValue: IconSpriteModel | undefined) {
    this._isDirty = true;

    this.json.sprites = newValue?.toJSON();
  }

  /// model: Picture
  get cover(): Promise<PictureModel> {
    return Store.instance.findPicture(this.json.cover);
  }

  set cover(newValue: PictureModel) {
    this._isDirty = true;
    this.json.cover = newValue?._id;
  }

  /// model: Picture
  get squareCover(): Promise<PictureModel | undefined> {
    if (!this.json.squareCover) {
      return Promise.resolve(undefined);
    }
    return Store.instance.findPicture(this.json.squareCover);
  }

  set squareCover(newValue: PictureModel | undefined) {
    this._isDirty = true;
    this.json.squareCover = newValue?._id;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.visibility?.isDirty) {
      return true;
    }

    if (this.area?.isDirty) {
      return true;
    }

    if (this.iconSets?.isDirty) {
      return true;
    }

    if (this.baseMaps?.isDirty) {
      return true;
    }

    if (this.info?.isDirty) {
      return true;
    }

    if (this.mask?.isDirty) {
      return true;
    }

    if (this.license?.isDirty) {
      return true;
    }

    if (this.cluster?.isDirty) {
      return true;
    }

    if (this.sprites?.isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<MapModel> {
    let result: MapModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createMap(this);
    } else {
      result = await Store.instance.saveMap(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): MapJson {
    return this.json;
  }
}
