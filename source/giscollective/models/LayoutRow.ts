import { LayoutCol, LayoutColJson, LayoutColModel } from "./LayoutCol";
import { Store } from "./store";

export interface LayoutRow {
  options?: string[];
  data?: any;
  cols?: LayoutCol[];
}

export interface LayoutRowJson {
  options?: string[];
  data?: any;
  cols?: LayoutColJson[];
}

export class LayoutRowModel {
  private json: LayoutRowJson;
  private _isDirty: boolean = false;

  constructor(json: LayoutRowJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: string
  get options(): string[] | undefined {
    return this.json.options;
  }

  set options(newValue: string[] | undefined) {
    this._isDirty = true;
    this.json.options = newValue;
  }

  /// field: Json
  get data(): any | undefined {
    return this.json.data;
  }

  set data(newValue: any | undefined) {
    this._isDirty = true;
    this.json.data = newValue;
  }

  /// object: LayoutCol
  get cols(): LayoutColModel[] {
    return this.json.cols?.map((a) => new LayoutColModel(a)) ?? [];
  }

  set cols(newValue: LayoutColModel[] | undefined) {
    this._isDirty = true;

    this.json.cols = newValue?.map((a) => a.toJSON()) ?? [];
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    for (let level0 of this.cols) {
      if (level0?.isDirty) {
        return true;
      }
    }

    return false;
  }

  toJSON(): LayoutRowJson {
    return this.json;
  }
}
