import { Store } from "./store";

export interface DefaultModels {
  map?: string;
  campaign?: string;
  calendar?: string;
  newsletter?: string;
}

export interface DefaultModelsJson {
  map?: string;
  campaign?: string;
  calendar?: string;
  newsletter?: string;
}

export class DefaultModelsModel {
  private json: DefaultModelsJson;
  private _isDirty: boolean = false;

  constructor(json: DefaultModelsJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: string
  get map(): string | undefined {
    return this.json.map;
  }

  set map(newValue: string | undefined) {
    this._isDirty = true;
    this.json.map = newValue;
  }

  /// field: string
  get campaign(): string | undefined {
    return this.json.campaign;
  }

  set campaign(newValue: string | undefined) {
    this._isDirty = true;
    this.json.campaign = newValue;
  }

  /// field: string
  get calendar(): string | undefined {
    return this.json.calendar;
  }

  set calendar(newValue: string | undefined) {
    this._isDirty = true;
    this.json.calendar = newValue;
  }

  /// field: string
  get newsletter(): string | undefined {
    return this.json.newsletter;
  }

  set newsletter(newValue: string | undefined) {
    this._isDirty = true;
    this.json.newsletter = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): DefaultModelsJson {
    return this.json;
  }
}
