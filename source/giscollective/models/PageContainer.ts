import { Store } from "./store";

export interface PageContainer {
  backgroundColor: string;
}

export interface PageContainerJson {
  backgroundColor: string;
}

export class PageContainerModel {
  private json: PageContainerJson;
  private _isDirty: boolean = false;

  constructor(json: PageContainerJson | null | undefined) {
    this.json = json ?? { backgroundColor: "" };
  }

  /// field: string
  get backgroundColor(): string {
    return this.json.backgroundColor;
  }

  set backgroundColor(newValue: string) {
    this._isDirty = true;
    this.json.backgroundColor = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): PageContainerJson {
    return this.json;
  }
}
