import { Store } from "./store";

export interface FeatureDecorators {
  useDefault?: boolean;
  center?: number[];
  showAsLineAfterZoom?: number;
  showLineMarkers?: boolean;
  keepWhenSmall?: boolean;
  changeIndex?: number;
  minZoom?: number;
  maxZoom?: number;
}

export interface FeatureDecoratorsJson {
  useDefault?: boolean;
  center?: number[];
  showAsLineAfterZoom?: number;
  showLineMarkers?: boolean;
  keepWhenSmall?: boolean;
  changeIndex?: number;
  minZoom?: number;
  maxZoom?: number;
}

export class FeatureDecoratorsModel {
  private json: FeatureDecoratorsJson;
  private _isDirty: boolean = false;

  constructor(json: FeatureDecoratorsJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: bool
  get useDefault(): boolean | undefined {
    return this.json.useDefault;
  }

  set useDefault(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.useDefault = newValue;
  }

  /// field: double
  get center(): number[] | undefined {
    return this.json.center;
  }

  set center(newValue: number[] | undefined) {
    this._isDirty = true;
    this.json.center = newValue;
  }

  /// field: int
  get showAsLineAfterZoom(): number | undefined {
    return this.json.showAsLineAfterZoom;
  }

  set showAsLineAfterZoom(newValue: number | undefined) {
    this._isDirty = true;
    this.json.showAsLineAfterZoom = newValue;
  }

  /// field: bool
  get showLineMarkers(): boolean | undefined {
    return this.json.showLineMarkers;
  }

  set showLineMarkers(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.showLineMarkers = newValue;
  }

  /// field: bool
  get keepWhenSmall(): boolean | undefined {
    return this.json.keepWhenSmall;
  }

  set keepWhenSmall(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.keepWhenSmall = newValue;
  }

  /// field: ulong
  get changeIndex(): number | undefined {
    return this.json.changeIndex;
  }

  set changeIndex(newValue: number | undefined) {
    this._isDirty = true;
    this.json.changeIndex = newValue;
  }

  /// field: uint
  get minZoom(): number | undefined {
    return this.json.minZoom;
  }

  set minZoom(newValue: number | undefined) {
    this._isDirty = true;
    this.json.minZoom = newValue;
  }

  /// field: uint
  get maxZoom(): number | undefined {
    return this.json.maxZoom;
  }

  set maxZoom(newValue: number | undefined) {
    this._isDirty = true;
    this.json.maxZoom = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): FeatureDecoratorsJson {
    return this.json;
  }
}
