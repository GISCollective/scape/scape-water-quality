import { DateTime } from "luxon";
import { Store } from "./store";

export interface Metric {
  _id: string;
  name: string;
  type: string;
  reporter: string;
  value: number;
  labels?: Record<string, string>;
  time?: DateTime;
}

export interface MetricResponse {
  metric: MetricJson;
}

export interface MetricsResponse {
  metrics: MetricJson[];
}

export interface MetricJson {
  _id: string;
  name: string;
  type: string;
  reporter: string;
  value: number;
  labels?: Record<string, string>;
  time?: string;
}

export class MetricModel {
  private json: MetricJson;
  private _isDirty: boolean = false;

  constructor(json: MetricJson | null | undefined) {
    this.json = json ?? { _id: "", name: "", type: "", reporter: "", value: 0 };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: string
  get type(): string {
    return this.json.type;
  }

  set type(newValue: string) {
    this._isDirty = true;
    this.json.type = newValue;
  }

  /// field: string
  get reporter(): string {
    return this.json.reporter;
  }

  set reporter(newValue: string) {
    this._isDirty = true;
    this.json.reporter = newValue;
  }

  /// field: double
  get value(): number {
    return this.json.value;
  }

  set value(newValue: number) {
    this._isDirty = true;
    this.json.value = newValue;
  }

  /// field: string
  get labels(): Record<string, string> | undefined {
    return this.json.labels;
  }

  set labels(newValue: Record<string, string> | undefined) {
    this._isDirty = true;
    this.json.labels = newValue;
  }

  /// object: SysTime
  get time(): DateTime | undefined {
    if (!this.json.time) {
      return undefined;
    }
    return DateTime.fromISO(this.json.time);
  }

  set time(newValue: DateTime | undefined) {
    this._isDirty = true;
    this.json.time = newValue?.toISO() ?? undefined;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<MetricModel> {
    let result: MetricModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createMetric(this);
    } else {
      result = await Store.instance.saveMetric(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): MetricJson {
    return this.json;
  }
}
