import { DateTime } from "luxon";
import { Store } from "./store";

export interface TimeInterval {
  dayOfWeek?: number;
  begin: DateTime;
  end: DateTime;
}

export interface TimeIntervalJson {
  dayOfWeek?: number;
  begin: string;
  end: string;
}

export class TimeIntervalModel {
  private json: TimeIntervalJson;
  private _isDirty: boolean = false;

  constructor(json: TimeIntervalJson | null | undefined) {
    this.json = json ?? { begin: "", end: "" };
  }

  /// field: int
  get dayOfWeek(): number | undefined {
    return this.json.dayOfWeek;
  }

  set dayOfWeek(newValue: number | undefined) {
    this._isDirty = true;
    this.json.dayOfWeek = newValue;
  }

  /// object: SysTime
  get begin(): DateTime {
    return DateTime.fromISO(this.json.begin);
  }

  set begin(newValue: DateTime) {
    this._isDirty = true;
    this.json.begin = newValue.toISO();
  }

  /// object: SysTime
  get end(): DateTime {
    return DateTime.fromISO(this.json.end);
  }

  set end(newValue: DateTime) {
    this._isDirty = true;
    this.json.end = newValue.toISO();
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): TimeIntervalJson {
    return this.json;
  }
}
