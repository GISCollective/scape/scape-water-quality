import { DateTime } from "luxon";
import { Store } from "./store";

export interface Review {
  user: string;
  reviewedOn: DateTime;
}

export interface ReviewJson {
  user: string;
  reviewedOn: string;
}

export class ReviewModel {
  private json: ReviewJson;
  private _isDirty: boolean = false;

  constructor(json: ReviewJson | null | undefined) {
    this.json = json ?? { user: "", reviewedOn: "" };
  }

  /// field: string
  get user(): string {
    return this.json.user;
  }

  set user(newValue: string) {
    this._isDirty = true;
    this.json.user = newValue;
  }

  /// object: SysTime
  get reviewedOn(): DateTime {
    return DateTime.fromISO(this.json.reviewedOn);
  }

  set reviewedOn(newValue: DateTime) {
    this._isDirty = true;
    this.json.reviewedOn = newValue.toISO();
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): ReviewJson {
    return this.json;
  }
}
