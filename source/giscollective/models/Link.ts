import { Store } from "./store";

export interface Link {
  route?: string;
  model?: string;
  path?: string;
  url?: string;
  anchor?: string;
}

export interface LinkJson {
  route?: string;
  model?: string;
  path?: string;
  url?: string;
  anchor?: string;
}

export class LinkModel {
  private json: LinkJson;
  private _isDirty: boolean = false;

  constructor(json: LinkJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: string
  get route(): string | undefined {
    return this.json.route;
  }

  set route(newValue: string | undefined) {
    this._isDirty = true;
    this.json.route = newValue;
  }

  /// field: string
  get model(): string | undefined {
    return this.json.model;
  }

  set model(newValue: string | undefined) {
    this._isDirty = true;
    this.json.model = newValue;
  }

  /// field: string
  get path(): string | undefined {
    return this.json.path;
  }

  set path(newValue: string | undefined) {
    this._isDirty = true;
    this.json.path = newValue;
  }

  /// field: string
  get url(): string | undefined {
    return this.json.url;
  }

  set url(newValue: string | undefined) {
    this._isDirty = true;
    this.json.url = newValue;
  }

  /// field: string
  get anchor(): string | undefined {
    return this.json.anchor;
  }

  set anchor(newValue: string | undefined) {
    this._isDirty = true;
    this.json.anchor = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): LinkJson {
    return this.json;
  }
}
