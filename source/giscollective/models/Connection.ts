import { Store } from "./store";

export interface Connection {
  type: string;
  config: any;
}

export interface ConnectionJson {
  type: string;
  config: any;
}

export class ConnectionModel {
  private json: ConnectionJson;
  private _isDirty: boolean = false;

  constructor(json: ConnectionJson | null | undefined) {
    this.json = json ?? { type: "", config: null };
  }

  /// field: string
  get type(): string {
    return this.json.type;
  }

  set type(newValue: string) {
    this._isDirty = true;
    this.json.type = newValue;
  }

  /// field: Json
  get config(): any {
    return this.json.config;
  }

  set config(newValue: any) {
    this._isDirty = true;
    this.json.config = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): ConnectionJson {
    return this.json;
  }
}
