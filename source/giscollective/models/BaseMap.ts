import { Picture, PictureModel } from "./Picture";
import { MapLayer, MapLayerJson, MapLayerModel } from "./MapLayer";
import { Visibility, VisibilityJson, VisibilityModel } from "./Visibility";
import { Attribution, AttributionJson, AttributionModel } from "./Attribution";
import { Store } from "./store";

export interface BaseMap {
  _id: string;
  name: string;
  icon: string;
  defaultOrder?: number;
  layers: MapLayer[];
  visibility: Visibility;
  attributions?: Attribution[];
  cover?: Picture;
}

export interface BaseMapResponse {
  baseMap: BaseMapJson;
}

export interface BaseMapsResponse {
  baseMaps: BaseMapJson[];
}

export interface BaseMapJson {
  _id: string;
  name: string;
  icon: string;
  defaultOrder?: number;
  layers: MapLayerJson[];
  visibility: VisibilityJson;
  attributions?: AttributionJson[];
  cover?: string;
}

export class BaseMapModel {
  private json: BaseMapJson;
  private _isDirty: boolean = false;

  constructor(json: BaseMapJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      name: "",
      icon: "",
      layers: [],
      visibility: { isPublic: false, team: "" },
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: string
  get icon(): string {
    return this.json.icon;
  }

  set icon(newValue: string) {
    this._isDirty = true;
    this.json.icon = newValue;
  }

  /// field: int
  get defaultOrder(): number | undefined {
    return this.json.defaultOrder;
  }

  set defaultOrder(newValue: number | undefined) {
    this._isDirty = true;
    this.json.defaultOrder = newValue;
  }

  /// object: MapLayer
  get layers(): MapLayerModel[] {
    return this.json.layers?.map((a) => new MapLayerModel(a)) ?? [];
  }

  set layers(newValue: MapLayerModel[]) {
    this._isDirty = true;

    this.json.layers = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// object: Visibility
  get visibility(): VisibilityModel {
    return new VisibilityModel(this.json.visibility);
  }

  set visibility(newValue: VisibilityModel) {
    this._isDirty = true;

    this.json.visibility = newValue?.toJSON();
  }

  /// object: Attribution
  get attributions(): AttributionModel[] {
    return this.json.attributions?.map((a) => new AttributionModel(a)) ?? [];
  }

  set attributions(newValue: AttributionModel[] | undefined) {
    this._isDirty = true;

    this.json.attributions = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// model: Picture
  get cover(): Promise<PictureModel | undefined> {
    if (!this.json.cover) {
      return Promise.resolve(undefined);
    }
    return Store.instance.findPicture(this.json.cover);
  }

  set cover(newValue: PictureModel | undefined) {
    this._isDirty = true;
    this.json.cover = newValue?._id;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    for (let level0 of this.layers) {
      if (level0?.isDirty) {
        return true;
      }
    }

    if (this.visibility?.isDirty) {
      return true;
    }

    for (let level0 of this.attributions) {
      if (level0?.isDirty) {
        return true;
      }
    }

    return false;
  }

  async save(): Promise<BaseMapModel> {
    let result: BaseMapModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createBaseMap(this);
    } else {
      result = await Store.instance.saveBaseMap(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): BaseMapJson {
    return this.json;
  }
}
