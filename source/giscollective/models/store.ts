import {
  IconSetModel,
  IconSetJson,
  IconSetResponse,
  IconSetsResponse,
} from "./IconSet";
import { StatModel, StatJson, StatResponse, StatsResponse } from "./Stat";
import { SpaceModel, SpaceJson, SpaceResponse, SpacesResponse } from "./Space";
import {
  CalendarModel,
  CalendarJson,
  CalendarResponse,
  CalendarsResponse,
} from "./Calendar";
import {
  DataBindingModel,
  DataBindingJson,
  DataBindingResponse,
  DataBindingsResponse,
} from "./DataBinding";
import {
  PreferenceModel,
  PreferenceJson,
  PreferenceResponse,
  PreferencesResponse,
} from "./Preference";
import { TeamModel, TeamJson, TeamResponse, TeamsResponse } from "./Team";
import {
  ArticleLinkModel,
  ArticleLinkJson,
  ArticleLinkResponse,
  ArticleLinksResponse,
} from "./ArticleLink";
import { EventModel, EventJson, EventResponse, EventsResponse } from "./Event";
import { IconModel, IconJson, IconResponse, IconsResponse } from "./Icon";
import {
  SearchMetaModel,
  SearchMetaJson,
  SearchMetaResponse,
  SearchMetasResponse,
} from "./SearchMeta";
import {
  FeatureModel,
  FeatureJson,
  FeatureResponse,
  FeaturesResponse,
} from "./Feature";
import {
  ArticleModel,
  ArticleJson,
  ArticleResponse,
  ArticlesResponse,
} from "./Article";
import { PageModel, PageJson, PageResponse, PagesResponse } from "./Page";
import { UserModel, UserJson, UserResponse, UsersResponse } from "./User";
import {
  GeocodingModel,
  GeocodingJson,
  GeocodingResponse,
  GeocodingsResponse,
} from "./Geocoding";
import {
  CampaignAnswerModel,
  CampaignAnswerJson,
  CampaignAnswerResponse,
  CampaignAnswersResponse,
} from "./CampaignAnswer";
import {
  MapFileModel,
  MapFileJson,
  MapFileResponse,
  MapFilesResponse,
} from "./MapFile";
import {
  LegendModel,
  LegendJson,
  LegendResponse,
  LegendsResponse,
} from "./Legend";
import {
  MetricModel,
  MetricJson,
  MetricResponse,
  MetricsResponse,
} from "./Metric";
import {
  TranslationModel,
  TranslationJson,
  TranslationResponse,
  TranslationsResponse,
} from "./Translation";
import {
  BaseMapModel,
  BaseMapJson,
  BaseMapResponse,
  BaseMapsResponse,
} from "./BaseMap";
import {
  CampaignModel,
  CampaignJson,
  CampaignResponse,
  CampaignsResponse,
} from "./Campaign";
import {
  UserProfileModel,
  UserProfileJson,
  UserProfileResponse,
  UserProfilesResponse,
} from "./UserProfile";
import { MapModel, MapJson, MapResponse, MapsResponse } from "./Map";
import { SoundModel, SoundJson, SoundResponse, SoundsResponse } from "./Sound";
import { IssueModel, IssueJson, IssueResponse, IssuesResponse } from "./Issue";
import {
  PresentationModel,
  PresentationJson,
  PresentationResponse,
  PresentationsResponse,
} from "./Presentation";
import {
  NewsletterModel,
  NewsletterJson,
  NewsletterResponse,
  NewslettersResponse,
} from "./Newsletter";
import { ModelModel, ModelJson, ModelResponse, ModelsResponse } from "./Model";
import {
  PictureModel,
  PictureJson,
  PictureResponse,
  PicturesResponse,
} from "./Picture";

export type GenericFetch = <T>(
  method: string,
  url: string,
  headers: Record<string, string>,
  body?: Record<string, any>,
) => Promise<T | ErrorResponse | ErrorListResponse>;

function toQueryParams(params: Record<string, string>) {
  const searchParams = new URLSearchParams();

  Object.keys(params).forEach((key) => searchParams.append(key, params[key]));

  return searchParams.toString();
}

export interface Cache {
  upsert(id: string | number, type: string, value: any): Promise<void>;
  get(id: string | number, type: string): Promise<null | string>;
  getPending(type: string): Promise<string[]>;
  delete(id: string | number, type: string): Promise<void>;
  clear(): Promise<void>;
}

export interface Authenticator {
  authenticate(
    headers: Record<string, string>,
  ): Promise<Record<string, string>>;
}

export interface ErrorResponse {
  error: string;
}

export interface DetailedError {
  description: string;
  status: number;
  title: string;
}

export interface ErrorListResponse {
  errors: DetailedError[];
}

function handleErrorResponse(response: ErrorResponse | ErrorListResponse) {
  if ("error" in response) {
    throw new Error(response.error);
  }

  if (response.errors?.length) {
    throw new Error(
      response.errors[0].title + ": " + response.errors[0].description,
    );
  }
}

export type QueryListFunc = <T>(query: Record<string, string>) => Promise<T[]>;

export class LazyList<T> {
  buckets: T[][] = [];
  isLoading: boolean = false;
  lastError: string | null = null;

  itemsPerPage: number = 24;

  constructor(
    private readonly query: Record<string, string>,
    private readonly queryList: QueryListFunc,
  ) {}

  get loadedAny() {
    return this.skip;
  }

  get isEmpty() {
    return this.skip;
  }

  get lastBucket(): T[] {
    const len = this.buckets?.length;

    if (!len) {
      return [];
    }

    return this.buckets[len - 1];
  }

  get canLoadMore() {
    if (this.lastError) {
      return false;
    }

    if (this.query?.limit) {
      return this.buckets.length == 0;
    }

    if (this.buckets?.length == 0) {
      return true;
    }

    return this.lastBucket.length == this.itemsPerPage;
  }

  get items() {
    return this.buckets.flatMap((a) => a);
  }

  get skip() {
    return this.buckets.map((a) => a.length).reduce((a, b) => a + b, 0);
  }

  buildParams(): Record<string, string> {
    let limit = this.itemsPerPage;
    let skip = this.skip;

    if (this.query.limit) {
      limit = parseInt(this.query.limit);
    }

    return {
      ...this.query,
      limit: `${limit}`,
      skip: `${skip}`,
    };
  }

  async loadNext() {
    if (!this.canLoadMore) {
      return;
    }

    this.isLoading = true;

    try {
      let results = (await this.queryList<T>(this.buildParams())) ?? [];

      this.buckets.push(results);
    } catch (err) {
      console.error(err);
      this.lastError = err;
    }

    this.isLoading = false;
  }
}

export type AnyGISCollectiveModel =
  | LazyList<IconModel>
  | LazyList<CampaignModel>
  | MapModel[]
  | IconSetModel
  | IconModel[]
  | LazyList<TeamModel>
  | NewsletterModel[]
  | PictureModel[]
  | GeocodingModel[]
  | FeatureModel[]
  | SpaceModel
  | StatModel
  | LazyList<CalendarModel>
  | BaseMapModel[]
  | CalendarModel
  | LazyList<MapModel>
  | PreferenceModel[]
  | PreferenceModel
  | LegendModel[]
  | DataBindingModel
  | LazyList<FeatureModel>
  | LazyList<SearchMetaModel>
  | LazyList<ModelModel>
  | LazyList<DataBindingModel>
  | TranslationModel[]
  | TeamModel
  | ArticleLinkModel
  | SpaceModel[]
  | SoundModel[]
  | EventModel
  | IconModel
  | LazyList<TranslationModel>
  | IssueModel[]
  | EventModel[]
  | LazyList<BaseMapModel>
  | LazyList<MetricModel>
  | MapFileModel[]
  | LazyList<SpaceModel>
  | SearchMetaModel
  | FeatureModel
  | ArticleModel
  | CampaignAnswerModel[]
  | LazyList<LegendModel>
  | LazyList<UserModel>
  | SearchMetaModel[]
  | PageModel
  | UserModel
  | LazyList<MapFileModel>
  | LazyList<PresentationModel>
  | CampaignAnswerModel
  | GeocodingModel
  | LazyList<PageModel>
  | MapFileModel
  | PageModel[]
  | LegendModel
  | LazyList<StatModel>
  | ModelModel[]
  | MetricModel
  | PresentationModel[]
  | MetricModel[]
  | TranslationModel
  | LazyList<ArticleLinkModel>
  | ArticleLinkModel[]
  | ArticleModel[]
  | LazyList<EventModel>
  | BaseMapModel
  | CampaignModel
  | CalendarModel[]
  | MapModel
  | UserProfileModel
  | LazyList<IssueModel>
  | SoundModel
  | LazyList<GeocodingModel>
  | LazyList<UserProfileModel>
  | UserProfileModel[]
  | LazyList<PreferenceModel>
  | LazyList<IconSetModel>
  | CampaignModel[]
  | StatModel[]
  | UserModel[]
  | LazyList<SoundModel>
  | IssueModel
  | LazyList<ArticleModel>
  | LazyList<NewsletterModel>
  | PresentationModel
  | LazyList<CampaignAnswerModel>
  | NewsletterModel
  | DataBindingModel[]
  | LazyList<PictureModel>
  | TeamModel[]
  | ModelModel
  | IconSetModel[]
  | PictureModel;

export type GISCollectiveModel = Record<string, AnyGISCollectiveModel>;

export class Store {
  private static _instance: Store | undefined;

  static set instance(value: Store) {
    this._instance = value;
  }

  static get instance(): Store {
    if (!this._instance) {
      throw new Error("The store instance is not set up.");
    }

    return this._instance;
  }

  constructor(
    private readonly baseUrl: string,
    private readonly fetch: GenericFetch,
    public readonly cache: Cache,
    public readonly authenticator: Authenticator,
  ) {}
  /// IconSet
  async findIconSet(id: string): Promise<IconSetModel> {
    let cachedRecord: string | IconSetJson = (await this.cache.get(
      id,
      "IconSet",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as IconSetJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<IconSetResponse>(
        "GET",
        this.baseUrl + "/iconsets" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("iconSet" in result) {
        await this.cache.upsert(id, "IconSet", result.iconSet);
        cachedRecord = result.iconSet;
      }
    }

    return new IconSetModel(cachedRecord);
  }

  async queryIconSets(query: Record<string, string>): Promise<IconSetModel[]> {
    const headers = {};

    const result = await this.fetch<IconSetsResponse>(
      "GET",
      this.baseUrl + "/iconsets" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("iconSets" in result) {
      const items = result.iconSets;
      await Promise.all(
        items.map((a: IconSetJson) => this.cache.upsert(a._id, "IconSet", a)),
      );

      return items.map((a: IconSetJson) => new IconSetModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteIconSet(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "IconSet");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/iconsets" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createIconSet(input: IconSetModel): Promise<IconSetModel> {
    const headers = {};
    const body = {
      iconSet: input.toJSON(),
    };

    const result = await this.fetch<IconSetResponse>(
      "POST",
      this.baseUrl + "/iconsets",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("iconSet" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.iconSet._id, "IconSet");
    }

    if ("iconSet" in result) {
      await this.cache.upsert(result.iconSet._id, "IconSet", result.iconSet);

      return new IconSetModel(result.iconSet);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveIconSet(input: IconSetModel): Promise<IconSetModel> {
    const headers = {};
    const body = {
      iconSet: input.toJSON(),
    };

    const result = await this.fetch<IconSetResponse>(
      "PUT",
      this.baseUrl + "/iconsets" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("iconSet" in result) {
      await this.cache.upsert(result.iconSet._id, "IconSet", result.iconSet);

      return new IconSetModel(result.iconSet);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Stat
  async findStat(id: string): Promise<StatModel> {
    let cachedRecord: string | StatJson = (await this.cache.get(
      id,
      "Stat",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as StatJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<StatResponse>(
        "GET",
        this.baseUrl + "/stats" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("stat" in result) {
        await this.cache.upsert(id, "IconSet", result.stat);
        cachedRecord = result.stat;
      }
    }

    return new StatModel(cachedRecord);
  }

  async queryStats(query: Record<string, string>): Promise<StatModel[]> {
    const headers = {};

    const result = await this.fetch<StatsResponse>(
      "GET",
      this.baseUrl + "/stats" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("stats" in result) {
      const items = result.stats;
      await Promise.all(
        items.map((a: StatJson) => this.cache.upsert(a._id, "Stat", a)),
      );

      return items.map((a: StatJson) => new StatModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteStat(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Stat");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/stats" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createStat(input: StatModel): Promise<StatModel> {
    const headers = {};
    const body = {
      stat: input.toJSON(),
    };

    const result = await this.fetch<StatResponse>(
      "POST",
      this.baseUrl + "/stats",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("stat" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.stat._id, "Stat");
    }

    if ("stat" in result) {
      await this.cache.upsert(result.stat._id, "Stat", result.stat);

      return new StatModel(result.stat);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveStat(input: StatModel): Promise<StatModel> {
    const headers = {};
    const body = {
      stat: input.toJSON(),
    };

    const result = await this.fetch<StatResponse>(
      "PUT",
      this.baseUrl + "/stats" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("stat" in result) {
      await this.cache.upsert(result.stat._id, "Stat", result.stat);

      return new StatModel(result.stat);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Space
  async findSpace(id: string): Promise<SpaceModel> {
    let cachedRecord: string | SpaceJson = (await this.cache.get(
      id,
      "Space",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as SpaceJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<SpaceResponse>(
        "GET",
        this.baseUrl + "/spaces" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("space" in result) {
        await this.cache.upsert(id, "IconSet", result.space);
        cachedRecord = result.space;
      }
    }

    return new SpaceModel(cachedRecord);
  }

  async querySpaces(query: Record<string, string>): Promise<SpaceModel[]> {
    const headers = {};

    const result = await this.fetch<SpacesResponse>(
      "GET",
      this.baseUrl + "/spaces" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("spaces" in result) {
      const items = result.spaces;
      await Promise.all(
        items.map((a: SpaceJson) => this.cache.upsert(a._id, "Space", a)),
      );

      return items.map((a: SpaceJson) => new SpaceModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteSpace(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Space");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/spaces" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createSpace(input: SpaceModel): Promise<SpaceModel> {
    const headers = {};
    const body = {
      space: input.toJSON(),
    };

    const result = await this.fetch<SpaceResponse>(
      "POST",
      this.baseUrl + "/spaces",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("space" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.space._id, "Space");
    }

    if ("space" in result) {
      await this.cache.upsert(result.space._id, "Space", result.space);

      return new SpaceModel(result.space);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveSpace(input: SpaceModel): Promise<SpaceModel> {
    const headers = {};
    const body = {
      space: input.toJSON(),
    };

    const result = await this.fetch<SpaceResponse>(
      "PUT",
      this.baseUrl + "/spaces" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("space" in result) {
      await this.cache.upsert(result.space._id, "Space", result.space);

      return new SpaceModel(result.space);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Calendar
  async findCalendar(id: string): Promise<CalendarModel> {
    let cachedRecord: string | CalendarJson = (await this.cache.get(
      id,
      "Calendar",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as CalendarJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<CalendarResponse>(
        "GET",
        this.baseUrl + "/calendars" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("calendar" in result) {
        await this.cache.upsert(id, "IconSet", result.calendar);
        cachedRecord = result.calendar;
      }
    }

    return new CalendarModel(cachedRecord);
  }

  async queryCalendars(
    query: Record<string, string>,
  ): Promise<CalendarModel[]> {
    const headers = {};

    const result = await this.fetch<CalendarsResponse>(
      "GET",
      this.baseUrl + "/calendars" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("calendars" in result) {
      const items = result.calendars;
      await Promise.all(
        items.map((a: CalendarJson) => this.cache.upsert(a._id, "Calendar", a)),
      );

      return items.map((a: CalendarJson) => new CalendarModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteCalendar(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Calendar");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/calendars" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createCalendar(input: CalendarModel): Promise<CalendarModel> {
    const headers = {};
    const body = {
      calendar: input.toJSON(),
    };

    const result = await this.fetch<CalendarResponse>(
      "POST",
      this.baseUrl + "/calendars",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("calendar" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.calendar._id, "Calendar");
    }

    if ("calendar" in result) {
      await this.cache.upsert(result.calendar._id, "Calendar", result.calendar);

      return new CalendarModel(result.calendar);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveCalendar(input: CalendarModel): Promise<CalendarModel> {
    const headers = {};
    const body = {
      calendar: input.toJSON(),
    };

    const result = await this.fetch<CalendarResponse>(
      "PUT",
      this.baseUrl + "/calendars" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("calendar" in result) {
      await this.cache.upsert(result.calendar._id, "Calendar", result.calendar);

      return new CalendarModel(result.calendar);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// DataBinding
  async findDataBinding(id: string): Promise<DataBindingModel> {
    let cachedRecord: string | DataBindingJson = (await this.cache.get(
      id,
      "DataBinding",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as DataBindingJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<DataBindingResponse>(
        "GET",
        this.baseUrl + "/databindings" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("dataBinding" in result) {
        await this.cache.upsert(id, "IconSet", result.dataBinding);
        cachedRecord = result.dataBinding;
      }
    }

    return new DataBindingModel(cachedRecord);
  }

  async queryDataBindings(
    query: Record<string, string>,
  ): Promise<DataBindingModel[]> {
    const headers = {};

    const result = await this.fetch<DataBindingsResponse>(
      "GET",
      this.baseUrl + "/databindings" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("dataBindings" in result) {
      const items = result.dataBindings;
      await Promise.all(
        items.map((a: DataBindingJson) =>
          this.cache.upsert(a._id, "DataBinding", a),
        ),
      );

      return items.map((a: DataBindingJson) => new DataBindingModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteDataBinding(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "DataBinding");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/databindings" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createDataBinding(input: DataBindingModel): Promise<DataBindingModel> {
    const headers = {};
    const body = {
      dataBinding: input.toJSON(),
    };

    const result = await this.fetch<DataBindingResponse>(
      "POST",
      this.baseUrl + "/databindings",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("dataBinding" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.dataBinding._id, "DataBinding");
    }

    if ("dataBinding" in result) {
      await this.cache.upsert(
        result.dataBinding._id,
        "DataBinding",
        result.dataBinding,
      );

      return new DataBindingModel(result.dataBinding);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveDataBinding(input: DataBindingModel): Promise<DataBindingModel> {
    const headers = {};
    const body = {
      dataBinding: input.toJSON(),
    };

    const result = await this.fetch<DataBindingResponse>(
      "PUT",
      this.baseUrl + "/databindings" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("dataBinding" in result) {
      await this.cache.upsert(
        result.dataBinding._id,
        "DataBinding",
        result.dataBinding,
      );

      return new DataBindingModel(result.dataBinding);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Preference
  async findPreference(id: string): Promise<PreferenceModel> {
    let cachedRecord: string | PreferenceJson = (await this.cache.get(
      id,
      "Preference",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as PreferenceJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<PreferenceResponse>(
        "GET",
        this.baseUrl + "/preferences" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("preference" in result) {
        await this.cache.upsert(id, "IconSet", result.preference);
        cachedRecord = result.preference;
      }
    }

    return new PreferenceModel(cachedRecord);
  }

  async queryPreferences(
    query: Record<string, string>,
  ): Promise<PreferenceModel[]> {
    const headers = {};

    const result = await this.fetch<PreferencesResponse>(
      "GET",
      this.baseUrl + "/preferences" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("preferences" in result) {
      const items = result.preferences;
      await Promise.all(
        items.map((a: PreferenceJson) =>
          this.cache.upsert(a._id, "Preference", a),
        ),
      );

      return items.map((a: PreferenceJson) => new PreferenceModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deletePreference(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Preference");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/preferences" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createPreference(input: PreferenceModel): Promise<PreferenceModel> {
    const headers = {};
    const body = {
      preference: input.toJSON(),
    };

    const result = await this.fetch<PreferenceResponse>(
      "POST",
      this.baseUrl + "/preferences",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("preference" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.preference._id, "Preference");
    }

    if ("preference" in result) {
      await this.cache.upsert(
        result.preference._id,
        "Preference",
        result.preference,
      );

      return new PreferenceModel(result.preference);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async savePreference(input: PreferenceModel): Promise<PreferenceModel> {
    const headers = {};
    const body = {
      preference: input.toJSON(),
    };

    const result = await this.fetch<PreferenceResponse>(
      "PUT",
      this.baseUrl + "/preferences" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("preference" in result) {
      await this.cache.upsert(
        result.preference._id,
        "Preference",
        result.preference,
      );

      return new PreferenceModel(result.preference);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Team
  async findTeam(id: string): Promise<TeamModel> {
    let cachedRecord: string | TeamJson = (await this.cache.get(
      id,
      "Team",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as TeamJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<TeamResponse>(
        "GET",
        this.baseUrl + "/teams" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("team" in result) {
        await this.cache.upsert(id, "IconSet", result.team);
        cachedRecord = result.team;
      }
    }

    return new TeamModel(cachedRecord);
  }

  async queryTeams(query: Record<string, string>): Promise<TeamModel[]> {
    const headers = {};

    const result = await this.fetch<TeamsResponse>(
      "GET",
      this.baseUrl + "/teams" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("teams" in result) {
      const items = result.teams;
      await Promise.all(
        items.map((a: TeamJson) => this.cache.upsert(a._id, "Team", a)),
      );

      return items.map((a: TeamJson) => new TeamModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteTeam(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Team");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/teams" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createTeam(input: TeamModel): Promise<TeamModel> {
    const headers = {};
    const body = {
      team: input.toJSON(),
    };

    const result = await this.fetch<TeamResponse>(
      "POST",
      this.baseUrl + "/teams",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("team" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.team._id, "Team");
    }

    if ("team" in result) {
      await this.cache.upsert(result.team._id, "Team", result.team);

      return new TeamModel(result.team);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveTeam(input: TeamModel): Promise<TeamModel> {
    const headers = {};
    const body = {
      team: input.toJSON(),
    };

    const result = await this.fetch<TeamResponse>(
      "PUT",
      this.baseUrl + "/teams" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("team" in result) {
      await this.cache.upsert(result.team._id, "Team", result.team);

      return new TeamModel(result.team);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// ArticleLink
  async findArticleLink(id: string): Promise<ArticleLinkModel> {
    let cachedRecord: string | ArticleLinkJson = (await this.cache.get(
      id,
      "ArticleLink",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as ArticleLinkJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<ArticleLinkResponse>(
        "GET",
        this.baseUrl + "/articlelinks" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("articleLink" in result) {
        await this.cache.upsert(id, "IconSet", result.articleLink);
        cachedRecord = result.articleLink;
      }
    }

    return new ArticleLinkModel(cachedRecord);
  }

  async queryArticleLinks(
    query: Record<string, string>,
  ): Promise<ArticleLinkModel[]> {
    const headers = {};

    const result = await this.fetch<ArticleLinksResponse>(
      "GET",
      this.baseUrl + "/articlelinks" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("articleLinks" in result) {
      const items = result.articleLinks;
      await Promise.all(
        items.map((a: ArticleLinkJson) =>
          this.cache.upsert(a._id, "ArticleLink", a),
        ),
      );

      return items.map((a: ArticleLinkJson) => new ArticleLinkModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteArticleLink(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "ArticleLink");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/articlelinks" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createArticleLink(input: ArticleLinkModel): Promise<ArticleLinkModel> {
    const headers = {};
    const body = {
      articleLink: input.toJSON(),
    };

    const result = await this.fetch<ArticleLinkResponse>(
      "POST",
      this.baseUrl + "/articlelinks",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("articleLink" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.articleLink._id, "ArticleLink");
    }

    if ("articleLink" in result) {
      await this.cache.upsert(
        result.articleLink._id,
        "ArticleLink",
        result.articleLink,
      );

      return new ArticleLinkModel(result.articleLink);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveArticleLink(input: ArticleLinkModel): Promise<ArticleLinkModel> {
    const headers = {};
    const body = {
      articleLink: input.toJSON(),
    };

    const result = await this.fetch<ArticleLinkResponse>(
      "PUT",
      this.baseUrl + "/articlelinks" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("articleLink" in result) {
      await this.cache.upsert(
        result.articleLink._id,
        "ArticleLink",
        result.articleLink,
      );

      return new ArticleLinkModel(result.articleLink);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Event
  async findEvent(id: string): Promise<EventModel> {
    let cachedRecord: string | EventJson = (await this.cache.get(
      id,
      "Event",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as EventJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<EventResponse>(
        "GET",
        this.baseUrl + "/events" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("event" in result) {
        await this.cache.upsert(id, "IconSet", result.event);
        cachedRecord = result.event;
      }
    }

    return new EventModel(cachedRecord);
  }

  async queryEvents(query: Record<string, string>): Promise<EventModel[]> {
    const headers = {};

    const result = await this.fetch<EventsResponse>(
      "GET",
      this.baseUrl + "/events" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("events" in result) {
      const items = result.events;
      await Promise.all(
        items.map((a: EventJson) => this.cache.upsert(a._id, "Event", a)),
      );

      return items.map((a: EventJson) => new EventModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteEvent(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Event");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/events" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createEvent(input: EventModel): Promise<EventModel> {
    const headers = {};
    const body = {
      event: input.toJSON(),
    };

    const result = await this.fetch<EventResponse>(
      "POST",
      this.baseUrl + "/events",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("event" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.event._id, "Event");
    }

    if ("event" in result) {
      await this.cache.upsert(result.event._id, "Event", result.event);

      return new EventModel(result.event);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveEvent(input: EventModel): Promise<EventModel> {
    const headers = {};
    const body = {
      event: input.toJSON(),
    };

    const result = await this.fetch<EventResponse>(
      "PUT",
      this.baseUrl + "/events" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("event" in result) {
      await this.cache.upsert(result.event._id, "Event", result.event);

      return new EventModel(result.event);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Icon
  async findIcon(id: string): Promise<IconModel> {
    let cachedRecord: string | IconJson = (await this.cache.get(
      id,
      "Icon",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as IconJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<IconResponse>(
        "GET",
        this.baseUrl + "/icons" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("icon" in result) {
        await this.cache.upsert(id, "IconSet", result.icon);
        cachedRecord = result.icon;
      }
    }

    return new IconModel(cachedRecord);
  }

  async queryIcons(query: Record<string, string>): Promise<IconModel[]> {
    const headers = {};

    const result = await this.fetch<IconsResponse>(
      "GET",
      this.baseUrl + "/icons" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("icons" in result) {
      const items = result.icons;
      await Promise.all(
        items.map((a: IconJson) => this.cache.upsert(a._id, "Icon", a)),
      );

      return items.map((a: IconJson) => new IconModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteIcon(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Icon");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/icons" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createIcon(input: IconModel): Promise<IconModel> {
    const headers = {};
    const body = {
      icon: input.toJSON(),
    };

    const result = await this.fetch<IconResponse>(
      "POST",
      this.baseUrl + "/icons",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("icon" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.icon._id, "Icon");
    }

    if ("icon" in result) {
      await this.cache.upsert(result.icon._id, "Icon", result.icon);

      return new IconModel(result.icon);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveIcon(input: IconModel): Promise<IconModel> {
    const headers = {};
    const body = {
      icon: input.toJSON(),
    };

    const result = await this.fetch<IconResponse>(
      "PUT",
      this.baseUrl + "/icons" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("icon" in result) {
      await this.cache.upsert(result.icon._id, "Icon", result.icon);

      return new IconModel(result.icon);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// SearchMeta
  async findSearchMeta(id: string): Promise<SearchMetaModel> {
    let cachedRecord: string | SearchMetaJson = (await this.cache.get(
      id,
      "SearchMeta",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as SearchMetaJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<SearchMetaResponse>(
        "GET",
        this.baseUrl + "/searchmetas" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("searchMeta" in result) {
        await this.cache.upsert(id, "IconSet", result.searchMeta);
        cachedRecord = result.searchMeta;
      }
    }

    return new SearchMetaModel(cachedRecord);
  }

  async querySearchMetas(
    query: Record<string, string>,
  ): Promise<SearchMetaModel[]> {
    const headers = {};

    const result = await this.fetch<SearchMetasResponse>(
      "GET",
      this.baseUrl + "/searchmetas" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("searchMetas" in result) {
      const items = result.searchMetas;
      await Promise.all(
        items.map((a: SearchMetaJson) =>
          this.cache.upsert(a._id, "SearchMeta", a),
        ),
      );

      return items.map((a: SearchMetaJson) => new SearchMetaModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteSearchMeta(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "SearchMeta");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/searchmetas" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createSearchMeta(input: SearchMetaModel): Promise<SearchMetaModel> {
    const headers = {};
    const body = {
      searchMeta: input.toJSON(),
    };

    const result = await this.fetch<SearchMetaResponse>(
      "POST",
      this.baseUrl + "/searchmetas",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("searchMeta" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.searchMeta._id, "SearchMeta");
    }

    if ("searchMeta" in result) {
      await this.cache.upsert(
        result.searchMeta._id,
        "SearchMeta",
        result.searchMeta,
      );

      return new SearchMetaModel(result.searchMeta);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveSearchMeta(input: SearchMetaModel): Promise<SearchMetaModel> {
    const headers = {};
    const body = {
      searchMeta: input.toJSON(),
    };

    const result = await this.fetch<SearchMetaResponse>(
      "PUT",
      this.baseUrl + "/searchmetas" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("searchMeta" in result) {
      await this.cache.upsert(
        result.searchMeta._id,
        "SearchMeta",
        result.searchMeta,
      );

      return new SearchMetaModel(result.searchMeta);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Feature
  async findFeature(id: string): Promise<FeatureModel> {
    let cachedRecord: string | FeatureJson = (await this.cache.get(
      id,
      "Feature",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as FeatureJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<FeatureResponse>(
        "GET",
        this.baseUrl + "/features" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("feature" in result) {
        await this.cache.upsert(id, "IconSet", result.feature);
        cachedRecord = result.feature;
      }
    }

    return new FeatureModel(cachedRecord);
  }

  async queryFeatures(query: Record<string, string>): Promise<FeatureModel[]> {
    const headers = {};

    const result = await this.fetch<FeaturesResponse>(
      "GET",
      this.baseUrl + "/features" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("features" in result) {
      const items = result.features;
      await Promise.all(
        items.map((a: FeatureJson) => this.cache.upsert(a._id, "Feature", a)),
      );

      return items.map((a: FeatureJson) => new FeatureModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteFeature(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Feature");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/features" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createFeature(input: FeatureModel): Promise<FeatureModel> {
    const headers = {};
    const body = {
      feature: input.toJSON(),
    };

    const result = await this.fetch<FeatureResponse>(
      "POST",
      this.baseUrl + "/features",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("feature" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.feature._id, "Feature");
    }

    if ("feature" in result) {
      await this.cache.upsert(result.feature._id, "Feature", result.feature);

      return new FeatureModel(result.feature);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveFeature(input: FeatureModel): Promise<FeatureModel> {
    const headers = {};
    const body = {
      feature: input.toJSON(),
    };

    const result = await this.fetch<FeatureResponse>(
      "PUT",
      this.baseUrl + "/features" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("feature" in result) {
      await this.cache.upsert(result.feature._id, "Feature", result.feature);

      return new FeatureModel(result.feature);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Article
  async findArticle(id: string): Promise<ArticleModel> {
    let cachedRecord: string | ArticleJson = (await this.cache.get(
      id,
      "Article",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as ArticleJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<ArticleResponse>(
        "GET",
        this.baseUrl + "/articles" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("article" in result) {
        await this.cache.upsert(id, "IconSet", result.article);
        cachedRecord = result.article;
      }
    }

    return new ArticleModel(cachedRecord);
  }

  async queryArticles(query: Record<string, string>): Promise<ArticleModel[]> {
    const headers = {};

    const result = await this.fetch<ArticlesResponse>(
      "GET",
      this.baseUrl + "/articles" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("articles" in result) {
      const items = result.articles;
      await Promise.all(
        items.map((a: ArticleJson) => this.cache.upsert(a._id, "Article", a)),
      );

      return items.map((a: ArticleJson) => new ArticleModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteArticle(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Article");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/articles" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createArticle(input: ArticleModel): Promise<ArticleModel> {
    const headers = {};
    const body = {
      article: input.toJSON(),
    };

    const result = await this.fetch<ArticleResponse>(
      "POST",
      this.baseUrl + "/articles",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("article" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.article._id, "Article");
    }

    if ("article" in result) {
      await this.cache.upsert(result.article._id, "Article", result.article);

      return new ArticleModel(result.article);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveArticle(input: ArticleModel): Promise<ArticleModel> {
    const headers = {};
    const body = {
      article: input.toJSON(),
    };

    const result = await this.fetch<ArticleResponse>(
      "PUT",
      this.baseUrl + "/articles" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("article" in result) {
      await this.cache.upsert(result.article._id, "Article", result.article);

      return new ArticleModel(result.article);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Page
  async findPage(id: string): Promise<PageModel> {
    let cachedRecord: string | PageJson = (await this.cache.get(
      id,
      "Page",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as PageJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<PageResponse>(
        "GET",
        this.baseUrl + "/pages" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("page" in result) {
        await this.cache.upsert(id, "IconSet", result.page);
        cachedRecord = result.page;
      }
    }

    return new PageModel(cachedRecord);
  }

  async queryPages(query: Record<string, string>): Promise<PageModel[]> {
    const headers = {};

    const result = await this.fetch<PagesResponse>(
      "GET",
      this.baseUrl + "/pages" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("pages" in result) {
      const items = result.pages;
      await Promise.all(
        items.map((a: PageJson) => this.cache.upsert(a._id, "Page", a)),
      );

      return items.map((a: PageJson) => new PageModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deletePage(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Page");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/pages" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createPage(input: PageModel): Promise<PageModel> {
    const headers = {};
    const body = {
      page: input.toJSON(),
    };

    const result = await this.fetch<PageResponse>(
      "POST",
      this.baseUrl + "/pages",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("page" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.page._id, "Page");
    }

    if ("page" in result) {
      await this.cache.upsert(result.page._id, "Page", result.page);

      return new PageModel(result.page);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async savePage(input: PageModel): Promise<PageModel> {
    const headers = {};
    const body = {
      page: input.toJSON(),
    };

    const result = await this.fetch<PageResponse>(
      "PUT",
      this.baseUrl + "/pages" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("page" in result) {
      await this.cache.upsert(result.page._id, "Page", result.page);

      return new PageModel(result.page);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// User
  async findUser(id: string): Promise<UserModel> {
    let cachedRecord: string | UserJson = (await this.cache.get(
      id,
      "User",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as UserJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<UserResponse>(
        "GET",
        this.baseUrl + "/users" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("user" in result) {
        await this.cache.upsert(id, "IconSet", result.user);
        cachedRecord = result.user;
      }
    }

    return new UserModel(cachedRecord);
  }

  async queryUsers(query: Record<string, string>): Promise<UserModel[]> {
    const headers = {};

    const result = await this.fetch<UsersResponse>(
      "GET",
      this.baseUrl + "/users" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("users" in result) {
      const items = result.users;
      await Promise.all(
        items.map((a: UserJson) => this.cache.upsert(a._id, "User", a)),
      );

      return items.map((a: UserJson) => new UserModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteUser(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "User");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/users" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createUser(input: UserModel): Promise<UserModel> {
    const headers = {};
    const body = {
      user: input.toJSON(),
    };

    const result = await this.fetch<UserResponse>(
      "POST",
      this.baseUrl + "/users",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("user" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.user._id, "User");
    }

    if ("user" in result) {
      await this.cache.upsert(result.user._id, "User", result.user);

      return new UserModel(result.user);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveUser(input: UserModel): Promise<UserModel> {
    const headers = {};
    const body = {
      user: input.toJSON(),
    };

    const result = await this.fetch<UserResponse>(
      "PUT",
      this.baseUrl + "/users" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("user" in result) {
      await this.cache.upsert(result.user._id, "User", result.user);

      return new UserModel(result.user);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Geocoding
  async findGeocoding(id: string): Promise<GeocodingModel> {
    let cachedRecord: string | GeocodingJson = (await this.cache.get(
      id,
      "Geocoding",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as GeocodingJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<GeocodingResponse>(
        "GET",
        this.baseUrl + "/geocodings" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("geocoding" in result) {
        await this.cache.upsert(id, "IconSet", result.geocoding);
        cachedRecord = result.geocoding;
      }
    }

    return new GeocodingModel(cachedRecord);
  }

  async queryGeocodings(
    query: Record<string, string>,
  ): Promise<GeocodingModel[]> {
    const headers = {};

    const result = await this.fetch<GeocodingsResponse>(
      "GET",
      this.baseUrl + "/geocodings" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("geocodings" in result) {
      const items = result.geocodings;
      await Promise.all(
        items.map((a: GeocodingJson) =>
          this.cache.upsert(a._id, "Geocoding", a),
        ),
      );

      return items.map((a: GeocodingJson) => new GeocodingModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteGeocoding(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Geocoding");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/geocodings" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createGeocoding(input: GeocodingModel): Promise<GeocodingModel> {
    const headers = {};
    const body = {
      geocoding: input.toJSON(),
    };

    const result = await this.fetch<GeocodingResponse>(
      "POST",
      this.baseUrl + "/geocodings",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("geocoding" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.geocoding._id, "Geocoding");
    }

    if ("geocoding" in result) {
      await this.cache.upsert(
        result.geocoding._id,
        "Geocoding",
        result.geocoding,
      );

      return new GeocodingModel(result.geocoding);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveGeocoding(input: GeocodingModel): Promise<GeocodingModel> {
    const headers = {};
    const body = {
      geocoding: input.toJSON(),
    };

    const result = await this.fetch<GeocodingResponse>(
      "PUT",
      this.baseUrl + "/geocodings" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("geocoding" in result) {
      await this.cache.upsert(
        result.geocoding._id,
        "Geocoding",
        result.geocoding,
      );

      return new GeocodingModel(result.geocoding);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// CampaignAnswer
  async findCampaignAnswer(id: string): Promise<CampaignAnswerModel> {
    let cachedRecord: string | CampaignAnswerJson = (await this.cache.get(
      id,
      "CampaignAnswer",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as CampaignAnswerJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<CampaignAnswerResponse>(
        "GET",
        this.baseUrl + "/campaignanswers" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("campaignAnswer" in result) {
        await this.cache.upsert(id, "IconSet", result.campaignAnswer);
        cachedRecord = result.campaignAnswer;
      }
    }

    return new CampaignAnswerModel(cachedRecord);
  }

  async queryCampaignAnswers(
    query: Record<string, string>,
  ): Promise<CampaignAnswerModel[]> {
    const headers = {};

    const result = await this.fetch<CampaignAnswersResponse>(
      "GET",
      this.baseUrl + "/campaignanswers" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("campaignAnswers" in result) {
      const items = result.campaignAnswers;
      await Promise.all(
        items.map((a: CampaignAnswerJson) =>
          this.cache.upsert(a._id, "CampaignAnswer", a),
        ),
      );

      return (
        items.map((a: CampaignAnswerJson) => new CampaignAnswerModel(a)) ?? []
      );
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteCampaignAnswer(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "CampaignAnswer");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/campaignanswers" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createCampaignAnswer(
    input: CampaignAnswerModel,
  ): Promise<CampaignAnswerModel> {
    const headers = {};
    const body = {
      campaignAnswer: input.toJSON(),
    };

    const initialId = input._id;

    const result = await this.fetch<CampaignAnswerResponse>(
      "POST",
      this.baseUrl + "/campaignanswers",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("campaignAnswer" in result && initialId.indexOf("pending") == 0) {
      await this.cache.delete(initialId, "CampaignAnswer");
    }

    if ("campaignAnswer" in result) {
      await this.cache.upsert(
        result.campaignAnswer._id,
        "CampaignAnswer",
        result.campaignAnswer,
      );

      return new CampaignAnswerModel(result.campaignAnswer);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveCampaignAnswer(
    input: CampaignAnswerModel,
  ): Promise<CampaignAnswerModel> {
    const headers = {};
    const body = {
      campaignAnswer: input.toJSON(),
    };

    const result = await this.fetch<CampaignAnswerResponse>(
      "PUT",
      this.baseUrl + "/campaignanswers" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("campaignAnswer" in result) {
      await this.cache.upsert(
        result.campaignAnswer._id,
        "CampaignAnswer",
        result.campaignAnswer,
      );

      return new CampaignAnswerModel(result.campaignAnswer);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// MapFile
  async findMapFile(id: string): Promise<MapFileModel> {
    let cachedRecord: string | MapFileJson = (await this.cache.get(
      id,
      "MapFile",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as MapFileJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<MapFileResponse>(
        "GET",
        this.baseUrl + "/mapfiles" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("mapFile" in result) {
        await this.cache.upsert(id, "IconSet", result.mapFile);
        cachedRecord = result.mapFile;
      }
    }

    return new MapFileModel(cachedRecord);
  }

  async queryMapFiles(query: Record<string, string>): Promise<MapFileModel[]> {
    const headers = {};

    const result = await this.fetch<MapFilesResponse>(
      "GET",
      this.baseUrl + "/mapfiles" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("mapFiles" in result) {
      const items = result.mapFiles;
      await Promise.all(
        items.map((a: MapFileJson) => this.cache.upsert(a._id, "MapFile", a)),
      );

      return items.map((a: MapFileJson) => new MapFileModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteMapFile(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "MapFile");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/mapfiles" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createMapFile(input: MapFileModel): Promise<MapFileModel> {
    const headers = {};
    const body = {
      mapFile: input.toJSON(),
    };

    const result = await this.fetch<MapFileResponse>(
      "POST",
      this.baseUrl + "/mapfiles",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("mapFile" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.mapFile._id, "MapFile");
    }

    if ("mapFile" in result) {
      await this.cache.upsert(result.mapFile._id, "MapFile", result.mapFile);

      return new MapFileModel(result.mapFile);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveMapFile(input: MapFileModel): Promise<MapFileModel> {
    const headers = {};
    const body = {
      mapFile: input.toJSON(),
    };

    const result = await this.fetch<MapFileResponse>(
      "PUT",
      this.baseUrl + "/mapfiles" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("mapFile" in result) {
      await this.cache.upsert(result.mapFile._id, "MapFile", result.mapFile);

      return new MapFileModel(result.mapFile);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Legend
  async findLegend(id: string): Promise<LegendModel> {
    let cachedRecord: string | LegendJson = (await this.cache.get(
      id,
      "Legend",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as LegendJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<LegendResponse>(
        "GET",
        this.baseUrl + "/legends" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("legend" in result) {
        await this.cache.upsert(id, "IconSet", result.legend);
        cachedRecord = result.legend;
      }
    }

    return new LegendModel(cachedRecord);
  }

  async queryLegends(query: Record<string, string>): Promise<LegendModel[]> {
    const headers = {};

    const result = await this.fetch<LegendsResponse>(
      "GET",
      this.baseUrl + "/legends" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("legends" in result) {
      const items = result.legends;
      await Promise.all(
        items.map((a: LegendJson) => this.cache.upsert(a._id, "Legend", a)),
      );

      return items.map((a: LegendJson) => new LegendModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteLegend(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Legend");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/legends" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createLegend(input: LegendModel): Promise<LegendModel> {
    const headers = {};
    const body = {
      legend: input.toJSON(),
    };

    const result = await this.fetch<LegendResponse>(
      "POST",
      this.baseUrl + "/legends",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("legend" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.legend._id, "Legend");
    }

    if ("legend" in result) {
      await this.cache.upsert(result.legend._id, "Legend", result.legend);

      return new LegendModel(result.legend);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveLegend(input: LegendModel): Promise<LegendModel> {
    const headers = {};
    const body = {
      legend: input.toJSON(),
    };

    const result = await this.fetch<LegendResponse>(
      "PUT",
      this.baseUrl + "/legends" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("legend" in result) {
      await this.cache.upsert(result.legend._id, "Legend", result.legend);

      return new LegendModel(result.legend);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Metric
  async findMetric(id: string): Promise<MetricModel> {
    let cachedRecord: string | MetricJson = (await this.cache.get(
      id,
      "Metric",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as MetricJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<MetricResponse>(
        "GET",
        this.baseUrl + "/metrics" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("metric" in result) {
        await this.cache.upsert(id, "IconSet", result.metric);
        cachedRecord = result.metric;
      }
    }

    return new MetricModel(cachedRecord);
  }

  async queryMetrics(query: Record<string, string>): Promise<MetricModel[]> {
    const headers = {};

    const result = await this.fetch<MetricsResponse>(
      "GET",
      this.baseUrl + "/metrics" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("metrics" in result) {
      const items = result.metrics;
      await Promise.all(
        items.map((a: MetricJson) => this.cache.upsert(a._id, "Metric", a)),
      );

      return items.map((a: MetricJson) => new MetricModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteMetric(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Metric");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/metrics" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createMetric(input: MetricModel): Promise<MetricModel> {
    const headers = {};
    const body = {
      metric: input.toJSON(),
    };

    const result = await this.fetch<MetricResponse>(
      "POST",
      this.baseUrl + "/metrics",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("metric" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.metric._id, "Metric");
    }

    if ("metric" in result) {
      await this.cache.upsert(result.metric._id, "Metric", result.metric);

      return new MetricModel(result.metric);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveMetric(input: MetricModel): Promise<MetricModel> {
    const headers = {};
    const body = {
      metric: input.toJSON(),
    };

    const result = await this.fetch<MetricResponse>(
      "PUT",
      this.baseUrl + "/metrics" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("metric" in result) {
      await this.cache.upsert(result.metric._id, "Metric", result.metric);

      return new MetricModel(result.metric);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Translation
  async findTranslation(id: string): Promise<TranslationModel> {
    let cachedRecord: string | TranslationJson = (await this.cache.get(
      id,
      "Translation",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as TranslationJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<TranslationResponse>(
        "GET",
        this.baseUrl + "/translations" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("translation" in result) {
        await this.cache.upsert(id, "IconSet", result.translation);
        cachedRecord = result.translation;
      }
    }

    return new TranslationModel(cachedRecord);
  }

  async queryTranslations(
    query: Record<string, string>,
  ): Promise<TranslationModel[]> {
    const headers = {};

    const result = await this.fetch<TranslationsResponse>(
      "GET",
      this.baseUrl + "/translations" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("translations" in result) {
      const items = result.translations;
      await Promise.all(
        items.map((a: TranslationJson) =>
          this.cache.upsert(a._id, "Translation", a),
        ),
      );

      return items.map((a: TranslationJson) => new TranslationModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteTranslation(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Translation");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/translations" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createTranslation(input: TranslationModel): Promise<TranslationModel> {
    const headers = {};
    const body = {
      translation: input.toJSON(),
    };

    const result = await this.fetch<TranslationResponse>(
      "POST",
      this.baseUrl + "/translations",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("translation" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.translation._id, "Translation");
    }

    if ("translation" in result) {
      await this.cache.upsert(
        result.translation._id,
        "Translation",
        result.translation,
      );

      return new TranslationModel(result.translation);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveTranslation(input: TranslationModel): Promise<TranslationModel> {
    const headers = {};
    const body = {
      translation: input.toJSON(),
    };

    const result = await this.fetch<TranslationResponse>(
      "PUT",
      this.baseUrl + "/translations" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("translation" in result) {
      await this.cache.upsert(
        result.translation._id,
        "Translation",
        result.translation,
      );

      return new TranslationModel(result.translation);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// BaseMap
  async findBaseMap(id: string): Promise<BaseMapModel> {
    let cachedRecord: string | BaseMapJson = (await this.cache.get(
      id,
      "BaseMap",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as BaseMapJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<BaseMapResponse>(
        "GET",
        this.baseUrl + "/basemaps" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("baseMap" in result) {
        await this.cache.upsert(id, "IconSet", result.baseMap);
        cachedRecord = result.baseMap;
      }
    }

    return new BaseMapModel(cachedRecord);
  }

  async queryBaseMaps(query: Record<string, string>): Promise<BaseMapModel[]> {
    const headers = {};

    const result = await this.fetch<BaseMapsResponse>(
      "GET",
      this.baseUrl + "/basemaps" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("baseMaps" in result) {
      const items = result.baseMaps;
      await Promise.all(
        items.map((a: BaseMapJson) => this.cache.upsert(a._id, "BaseMap", a)),
      );

      return items.map((a: BaseMapJson) => new BaseMapModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteBaseMap(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "BaseMap");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/basemaps" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createBaseMap(input: BaseMapModel): Promise<BaseMapModel> {
    const headers = {};
    const body = {
      baseMap: input.toJSON(),
    };

    const result = await this.fetch<BaseMapResponse>(
      "POST",
      this.baseUrl + "/basemaps",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("baseMap" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.baseMap._id, "BaseMap");
    }

    if ("baseMap" in result) {
      await this.cache.upsert(result.baseMap._id, "BaseMap", result.baseMap);

      return new BaseMapModel(result.baseMap);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveBaseMap(input: BaseMapModel): Promise<BaseMapModel> {
    const headers = {};
    const body = {
      baseMap: input.toJSON(),
    };

    const result = await this.fetch<BaseMapResponse>(
      "PUT",
      this.baseUrl + "/basemaps" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("baseMap" in result) {
      await this.cache.upsert(result.baseMap._id, "BaseMap", result.baseMap);

      return new BaseMapModel(result.baseMap);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Campaign
  async findCampaign(id: string): Promise<CampaignModel> {
    let cachedRecord: string | CampaignJson = (await this.cache.get(
      id,
      "Campaign",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as CampaignJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<CampaignResponse>(
        "GET",
        this.baseUrl + "/campaigns" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("campaign" in result) {
        await this.cache.upsert(id, "IconSet", result.campaign);
        cachedRecord = result.campaign;
      }
    }

    return new CampaignModel(cachedRecord);
  }

  async queryCampaigns(
    query: Record<string, string>,
  ): Promise<CampaignModel[]> {
    const headers = {};

    const result = await this.fetch<CampaignsResponse>(
      "GET",
      this.baseUrl + "/campaigns" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("campaigns" in result) {
      const items = result.campaigns;
      await Promise.all(
        items.map((a: CampaignJson) => this.cache.upsert(a._id, "Campaign", a)),
      );

      return items.map((a: CampaignJson) => new CampaignModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteCampaign(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Campaign");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/campaigns" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createCampaign(input: CampaignModel): Promise<CampaignModel> {
    const headers = {};
    const body = {
      campaign: input.toJSON(),
    };

    const result = await this.fetch<CampaignResponse>(
      "POST",
      this.baseUrl + "/campaigns",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("campaign" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.campaign._id, "Campaign");
    }

    if ("campaign" in result) {
      await this.cache.upsert(result.campaign._id, "Campaign", result.campaign);

      return new CampaignModel(result.campaign);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveCampaign(input: CampaignModel): Promise<CampaignModel> {
    const headers = {};
    const body = {
      campaign: input.toJSON(),
    };

    const result = await this.fetch<CampaignResponse>(
      "PUT",
      this.baseUrl + "/campaigns" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("campaign" in result) {
      await this.cache.upsert(result.campaign._id, "Campaign", result.campaign);

      return new CampaignModel(result.campaign);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// UserProfile
  async findUserProfile(id: string): Promise<UserProfileModel> {
    let cachedRecord: string | UserProfileJson = (await this.cache.get(
      id,
      "UserProfile",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as UserProfileJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<UserProfileResponse>(
        "GET",
        this.baseUrl + "/userprofiles" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("userProfile" in result) {
        await this.cache.upsert(id, "IconSet", result.userProfile);
        cachedRecord = result.userProfile;
      }
    }

    return new UserProfileModel(cachedRecord);
  }

  async queryUserProfiles(
    query: Record<string, string>,
  ): Promise<UserProfileModel[]> {
    const headers = {};

    const result = await this.fetch<UserProfilesResponse>(
      "GET",
      this.baseUrl + "/userprofiles" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("userProfiles" in result) {
      const items = result.userProfiles;
      await Promise.all(
        items.map((a: UserProfileJson) =>
          this.cache.upsert(a._id, "UserProfile", a),
        ),
      );

      return items.map((a: UserProfileJson) => new UserProfileModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteUserProfile(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "UserProfile");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/userprofiles" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createUserProfile(input: UserProfileModel): Promise<UserProfileModel> {
    const headers = {};
    const body = {
      userProfile: input.toJSON(),
    };

    const result = await this.fetch<UserProfileResponse>(
      "POST",
      this.baseUrl + "/userprofiles",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("userProfile" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.userProfile._id, "UserProfile");
    }

    if ("userProfile" in result) {
      await this.cache.upsert(
        result.userProfile._id,
        "UserProfile",
        result.userProfile,
      );

      return new UserProfileModel(result.userProfile);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveUserProfile(input: UserProfileModel): Promise<UserProfileModel> {
    const headers = {};
    const body = {
      userProfile: input.toJSON(),
    };

    const result = await this.fetch<UserProfileResponse>(
      "PUT",
      this.baseUrl + "/userprofiles" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("userProfile" in result) {
      await this.cache.upsert(
        result.userProfile._id,
        "UserProfile",
        result.userProfile,
      );

      return new UserProfileModel(result.userProfile);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Map
  async findMap(id: string): Promise<MapModel> {
    let cachedRecord: string | MapJson = (await this.cache.get(
      id,
      "Map",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as MapJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<MapResponse>(
        "GET",
        this.baseUrl + "/maps" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("map" in result) {
        await this.cache.upsert(id, "IconSet", result.map);
        cachedRecord = result.map;
      }
    }

    return new MapModel(cachedRecord);
  }

  async queryMaps(query: Record<string, string>): Promise<MapModel[]> {
    const headers = {};

    const result = await this.fetch<MapsResponse>(
      "GET",
      this.baseUrl + "/maps" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("maps" in result) {
      const items = result.maps;
      await Promise.all(
        items.map((a: MapJson) => this.cache.upsert(a._id, "Map", a)),
      );

      return items.map((a: MapJson) => new MapModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteMap(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Map");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/maps" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createMap(input: MapModel): Promise<MapModel> {
    const headers = {};
    const body = {
      map: input.toJSON(),
    };

    const result = await this.fetch<MapResponse>(
      "POST",
      this.baseUrl + "/maps",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("map" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.map._id, "Map");
    }

    if ("map" in result) {
      await this.cache.upsert(result.map._id, "Map", result.map);

      return new MapModel(result.map);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveMap(input: MapModel): Promise<MapModel> {
    const headers = {};
    const body = {
      map: input.toJSON(),
    };

    const result = await this.fetch<MapResponse>(
      "PUT",
      this.baseUrl + "/maps" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("map" in result) {
      await this.cache.upsert(result.map._id, "Map", result.map);

      return new MapModel(result.map);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Sound
  async findSound(id: string): Promise<SoundModel> {
    let cachedRecord: string | SoundJson = (await this.cache.get(
      id,
      "Sound",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as SoundJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<SoundResponse>(
        "GET",
        this.baseUrl + "/sounds" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("sound" in result) {
        await this.cache.upsert(id, "IconSet", result.sound);
        cachedRecord = result.sound;
      }
    }

    return new SoundModel(cachedRecord);
  }

  async querySounds(query: Record<string, string>): Promise<SoundModel[]> {
    const headers = {};

    const result = await this.fetch<SoundsResponse>(
      "GET",
      this.baseUrl + "/sounds" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("sounds" in result) {
      const items = result.sounds;
      await Promise.all(
        items.map((a: SoundJson) => this.cache.upsert(a._id, "Sound", a)),
      );

      return items.map((a: SoundJson) => new SoundModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteSound(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Sound");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/sounds" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createSound(input: SoundModel): Promise<SoundModel> {
    const headers = {};
    const body = {
      sound: input.toJSON(),
    };

    const result = await this.fetch<SoundResponse>(
      "POST",
      this.baseUrl + "/sounds",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("sound" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.sound._id, "Sound");
    }

    if ("sound" in result) {
      await this.cache.upsert(result.sound._id, "Sound", result.sound);

      return new SoundModel(result.sound);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveSound(input: SoundModel): Promise<SoundModel> {
    const headers = {};
    const body = {
      sound: input.toJSON(),
    };

    const result = await this.fetch<SoundResponse>(
      "PUT",
      this.baseUrl + "/sounds" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("sound" in result) {
      await this.cache.upsert(result.sound._id, "Sound", result.sound);

      return new SoundModel(result.sound);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Issue
  async findIssue(id: string): Promise<IssueModel> {
    let cachedRecord: string | IssueJson = (await this.cache.get(
      id,
      "Issue",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as IssueJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<IssueResponse>(
        "GET",
        this.baseUrl + "/issues" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("issue" in result) {
        await this.cache.upsert(id, "IconSet", result.issue);
        cachedRecord = result.issue;
      }
    }

    return new IssueModel(cachedRecord);
  }

  async queryIssues(query: Record<string, string>): Promise<IssueModel[]> {
    const headers = {};

    const result = await this.fetch<IssuesResponse>(
      "GET",
      this.baseUrl + "/issues" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("issues" in result) {
      const items = result.issues;
      await Promise.all(
        items.map((a: IssueJson) => this.cache.upsert(a._id, "Issue", a)),
      );

      return items.map((a: IssueJson) => new IssueModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteIssue(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Issue");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/issues" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createIssue(input: IssueModel): Promise<IssueModel> {
    const headers = {};
    const body = {
      issue: input.toJSON(),
    };

    const result = await this.fetch<IssueResponse>(
      "POST",
      this.baseUrl + "/issues",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("issue" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.issue._id, "Issue");
    }

    if ("issue" in result) {
      await this.cache.upsert(result.issue._id, "Issue", result.issue);

      return new IssueModel(result.issue);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveIssue(input: IssueModel): Promise<IssueModel> {
    const headers = {};
    const body = {
      issue: input.toJSON(),
    };

    const result = await this.fetch<IssueResponse>(
      "PUT",
      this.baseUrl + "/issues" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("issue" in result) {
      await this.cache.upsert(result.issue._id, "Issue", result.issue);

      return new IssueModel(result.issue);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Presentation
  async findPresentation(id: string): Promise<PresentationModel> {
    let cachedRecord: string | PresentationJson = (await this.cache.get(
      id,
      "Presentation",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as PresentationJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<PresentationResponse>(
        "GET",
        this.baseUrl + "/presentations" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("presentation" in result) {
        await this.cache.upsert(id, "IconSet", result.presentation);
        cachedRecord = result.presentation;
      }
    }

    return new PresentationModel(cachedRecord);
  }

  async queryPresentations(
    query: Record<string, string>,
  ): Promise<PresentationModel[]> {
    const headers = {};

    const result = await this.fetch<PresentationsResponse>(
      "GET",
      this.baseUrl + "/presentations" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("presentations" in result) {
      const items = result.presentations;
      await Promise.all(
        items.map((a: PresentationJson) =>
          this.cache.upsert(a._id, "Presentation", a),
        ),
      );

      return items.map((a: PresentationJson) => new PresentationModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deletePresentation(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Presentation");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/presentations" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createPresentation(
    input: PresentationModel,
  ): Promise<PresentationModel> {
    const headers = {};
    const body = {
      presentation: input.toJSON(),
    };

    const result = await this.fetch<PresentationResponse>(
      "POST",
      this.baseUrl + "/presentations",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("presentation" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.presentation._id, "Presentation");
    }

    if ("presentation" in result) {
      await this.cache.upsert(
        result.presentation._id,
        "Presentation",
        result.presentation,
      );

      return new PresentationModel(result.presentation);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async savePresentation(input: PresentationModel): Promise<PresentationModel> {
    const headers = {};
    const body = {
      presentation: input.toJSON(),
    };

    const result = await this.fetch<PresentationResponse>(
      "PUT",
      this.baseUrl + "/presentations" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("presentation" in result) {
      await this.cache.upsert(
        result.presentation._id,
        "Presentation",
        result.presentation,
      );

      return new PresentationModel(result.presentation);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Newsletter
  async findNewsletter(id: string): Promise<NewsletterModel> {
    let cachedRecord: string | NewsletterJson = (await this.cache.get(
      id,
      "Newsletter",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as NewsletterJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<NewsletterResponse>(
        "GET",
        this.baseUrl + "/newsletters" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("newsletter" in result) {
        await this.cache.upsert(id, "IconSet", result.newsletter);
        cachedRecord = result.newsletter;
      }
    }

    return new NewsletterModel(cachedRecord);
  }

  async queryNewsletters(
    query: Record<string, string>,
  ): Promise<NewsletterModel[]> {
    const headers = {};

    const result = await this.fetch<NewslettersResponse>(
      "GET",
      this.baseUrl + "/newsletters" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("newsletters" in result) {
      const items = result.newsletters;
      await Promise.all(
        items.map((a: NewsletterJson) =>
          this.cache.upsert(a._id, "Newsletter", a),
        ),
      );

      return items.map((a: NewsletterJson) => new NewsletterModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteNewsletter(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Newsletter");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/newsletters" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createNewsletter(input: NewsletterModel): Promise<NewsletterModel> {
    const headers = {};
    const body = {
      newsletter: input.toJSON(),
    };

    const result = await this.fetch<NewsletterResponse>(
      "POST",
      this.baseUrl + "/newsletters",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("newsletter" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.newsletter._id, "Newsletter");
    }

    if ("newsletter" in result) {
      await this.cache.upsert(
        result.newsletter._id,
        "Newsletter",
        result.newsletter,
      );

      return new NewsletterModel(result.newsletter);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveNewsletter(input: NewsletterModel): Promise<NewsletterModel> {
    const headers = {};
    const body = {
      newsletter: input.toJSON(),
    };

    const result = await this.fetch<NewsletterResponse>(
      "PUT",
      this.baseUrl + "/newsletters" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("newsletter" in result) {
      await this.cache.upsert(
        result.newsletter._id,
        "Newsletter",
        result.newsletter,
      );

      return new NewsletterModel(result.newsletter);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Model
  async findModel(id: string): Promise<ModelModel> {
    let cachedRecord: string | ModelJson = (await this.cache.get(
      id,
      "Model",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as ModelJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<ModelResponse>(
        "GET",
        this.baseUrl + "/models" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("model" in result) {
        await this.cache.upsert(id, "IconSet", result.model);
        cachedRecord = result.model;
      }
    }

    return new ModelModel(cachedRecord);
  }

  async queryModels(query: Record<string, string>): Promise<ModelModel[]> {
    const headers = {};

    const result = await this.fetch<ModelsResponse>(
      "GET",
      this.baseUrl + "/models" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("models" in result) {
      const items = result.models;
      await Promise.all(
        items.map((a: ModelJson) => this.cache.upsert(a._id, "Model", a)),
      );

      return items.map((a: ModelJson) => new ModelModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deleteModel(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Model");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/models" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createModel(input: ModelModel): Promise<ModelModel> {
    const headers = {};
    const body = {
      model: input.toJSON(),
    };

    const result = await this.fetch<ModelResponse>(
      "POST",
      this.baseUrl + "/models",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("model" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.model._id, "Model");
    }

    if ("model" in result) {
      await this.cache.upsert(result.model._id, "Model", result.model);

      return new ModelModel(result.model);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async saveModel(input: ModelModel): Promise<ModelModel> {
    const headers = {};
    const body = {
      model: input.toJSON(),
    };

    const result = await this.fetch<ModelResponse>(
      "PUT",
      this.baseUrl + "/models" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("model" in result) {
      await this.cache.upsert(result.model._id, "Model", result.model);

      return new ModelModel(result.model);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  /// Picture
  async findPicture(id: string): Promise<PictureModel> {
    let cachedRecord: string | PictureJson = (await this.cache.get(
      id,
      "Picture",
    )) as unknown as string;

    if (typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord) as unknown as PictureJson;
    }

    const headers = {};

    if (!cachedRecord) {
      const result = await this.fetch<PictureResponse>(
        "GET",
        this.baseUrl + "/pictures" + "/" + id,
        headers,
      );

      if ("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if ("picture" in result) {
        await this.cache.upsert(id, "IconSet", result.picture);
        cachedRecord = result.picture;
      }
    }

    return new PictureModel(cachedRecord);
  }

  async queryPictures(query: Record<string, string>): Promise<PictureModel[]> {
    const headers = {};

    const result = await this.fetch<PicturesResponse>(
      "GET",
      this.baseUrl + "/pictures" + "?" + toQueryParams(query),
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("pictures" in result) {
      const items = result.pictures;
      await Promise.all(
        items.map((a: PictureJson) => this.cache.upsert(a._id, "Picture", a)),
      );

      return items.map((a: PictureJson) => new PictureModel(a)) ?? [];
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async deletePicture(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "Picture");

    const result = await this.fetch<{}>(
      "DELETE",
      this.baseUrl + "/pictures" + "/" + id,
      headers,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async createPicture(input: PictureModel): Promise<PictureModel> {
    const headers = {};
    const body = {
      picture: input.toJSON(),
    };

    const result = await this.fetch<PictureResponse>(
      "POST",
      this.baseUrl + "/pictures",
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("picture" in result && input._id.indexOf("pending") == 0) {
      await this.cache.delete(result.picture._id, "Picture");
    }

    if ("picture" in result) {
      await this.cache.upsert(result.picture._id, "Picture", result.picture);

      return new PictureModel(result.picture);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async savePicture(input: PictureModel): Promise<PictureModel> {
    const headers = {};
    const body = {
      picture: input.toJSON(),
    };

    const result = await this.fetch<PictureResponse>(
      "PUT",
      this.baseUrl + "/pictures" + "/" + input._id,
      headers,
      body,
    );

    if ("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if ("picture" in result) {
      await this.cache.upsert(result.picture._id, "Picture", result.picture);

      return new PictureModel(result.picture);
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }
}
