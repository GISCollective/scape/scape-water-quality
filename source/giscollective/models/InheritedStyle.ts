import { IconStyle, IconStyleJson, IconStyleModel } from "./IconStyle";
import { Store } from "./store";

export interface InheritedStyle {
  hasCustomStyle: boolean;
  types?: IconStyle;
}

export interface InheritedStyleJson {
  hasCustomStyle: boolean;
  types?: IconStyleJson;
}

export class InheritedStyleModel {
  private json: InheritedStyleJson;
  private _isDirty: boolean = false;

  constructor(json: InheritedStyleJson | null | undefined) {
    this.json = json ?? { hasCustomStyle: false };
  }

  /// field: bool
  get hasCustomStyle(): boolean {
    return this.json.hasCustomStyle;
  }

  set hasCustomStyle(newValue: boolean) {
    this._isDirty = true;
    this.json.hasCustomStyle = newValue;
  }

  /// object: IconStyle
  get types(): IconStyleModel {
    return new IconStyleModel(this.json.types);
  }

  set types(newValue: IconStyleModel | undefined) {
    this._isDirty = true;

    this.json.types = newValue?.toJSON();
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.types?.isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): InheritedStyleJson {
    return this.json;
  }
}
