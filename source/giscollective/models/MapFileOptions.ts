import { DateTime } from "luxon";
import {
  FieldMapping,
  FieldMappingJson,
  FieldMappingModel,
} from "./FieldMapping";
import { Store } from "./store";

export interface MapFileOptions {
  uuid?: string;
  destinationMap?: string;
  updateBy?: string;
  crs?: string;
  extraIcons?: string[];
  analyzedAt?: DateTime;
  fields?: FieldMapping[];
}

export interface MapFileOptionsJson {
  uuid?: string;
  destinationMap?: string;
  updateBy?: string;
  crs?: string;
  extraIcons?: string[];
  analyzedAt?: string;
  fields?: FieldMappingJson[];
}

export class MapFileOptionsModel {
  private json: MapFileOptionsJson;
  private _isDirty: boolean = false;

  constructor(json: MapFileOptionsJson | null | undefined) {
    this.json = json ?? {};
  }

  /// field: string
  get uuid(): string | undefined {
    return this.json.uuid;
  }

  set uuid(newValue: string | undefined) {
    this._isDirty = true;
    this.json.uuid = newValue;
  }

  /// field: string
  get destinationMap(): string | undefined {
    return this.json.destinationMap;
  }

  set destinationMap(newValue: string | undefined) {
    this._isDirty = true;
    this.json.destinationMap = newValue;
  }

  /// field: string
  get updateBy(): string | undefined {
    return this.json.updateBy;
  }

  set updateBy(newValue: string | undefined) {
    this._isDirty = true;
    this.json.updateBy = newValue;
  }

  /// field: string
  get crs(): string | undefined {
    return this.json.crs;
  }

  set crs(newValue: string | undefined) {
    this._isDirty = true;
    this.json.crs = newValue;
  }

  /// field: string
  get extraIcons(): string[] | undefined {
    return this.json.extraIcons;
  }

  set extraIcons(newValue: string[] | undefined) {
    this._isDirty = true;
    this.json.extraIcons = newValue;
  }

  /// object: SysTime
  get analyzedAt(): DateTime | undefined {
    if (!this.json.analyzedAt) {
      return undefined;
    }
    return DateTime.fromISO(this.json.analyzedAt);
  }

  set analyzedAt(newValue: DateTime | undefined) {
    this._isDirty = true;
    this.json.analyzedAt = newValue?.toISO() ?? undefined;
  }

  /// object: FieldMapping
  get fields(): FieldMappingModel[] {
    return this.json.fields?.map((a) => new FieldMappingModel(a)) ?? [];
  }

  set fields(newValue: FieldMappingModel[] | undefined) {
    this._isDirty = true;

    this.json.fields = newValue?.map((a) => a.toJSON()) ?? [];
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    for (let level0 of this.fields) {
      if (level0?.isDirty) {
        return true;
      }
    }

    return false;
  }

  toJSON(): MapFileOptionsJson {
    return this.json;
  }
}
