import { Attribution, AttributionJson, AttributionModel } from "./Attribution";
import { Link, LinkJson, LinkModel } from "./Link";
import { FontStyles, FontStylesJson, FontStylesModel } from "./FontStyles";
import {
  DefaultModels,
  DefaultModelsJson,
  DefaultModelsModel,
} from "./DefaultModels";
import { Visibility, VisibilityJson, VisibilityModel } from "./Visibility";
import {
  SpaceSearchOptions,
  SpaceSearchOptionsJson,
  SpaceSearchOptionsModel,
} from "./SpaceSearchOptions";
import {
  LayoutContainer,
  LayoutContainerJson,
  LayoutContainerModel,
} from "./LayoutContainer";
import {
  ColorPalette,
  ColorPaletteJson,
  ColorPaletteModel,
} from "./ColorPalette";
import { Store } from "./store";
import { ModelInfo, ModelInfoJson, ModelInfoModel } from "./ModelInfo";
import { PageCol, PageColJson, PageColModel } from "./PageCol";
import { Picture, PictureModel } from "./Picture";

export interface Space {
  _id: string;
  name: string;
  description?: string;
  landingPageId?: string;
  globalMapPageId?: string;
  domain?: string;
  domainValidationKey?: string;
  allowDomainChange?: boolean;
  hasDomainValidated?: boolean;
  domainStatus?: string;
  matomoUrl?: string;
  matomoSiteId?: string;
  locale?: string;
  isTemplate?: boolean;
  info: ModelInfo;
  visibility: Visibility;
  colorPalette?: ColorPalette;
  fontStyles?: FontStyles;
  attributions?: Attribution[];
  cols?: Record<string, PageCol>;
  layoutContainers?: Record<string, LayoutContainer>;
  redirects?: Record<string, Link>;
  searchOptions?: SpaceSearchOptions;
  defaultModels?: DefaultModels;
  logo?: Picture;
  logoSquare?: Picture;
  cover?: Picture;
}

export interface SpaceResponse {
  space: SpaceJson;
}

export interface SpacesResponse {
  spaces: SpaceJson[];
}

export interface SpaceJson {
  _id: string;
  name: string;
  description?: string;
  landingPageId?: string;
  globalMapPageId?: string;
  domain?: string;
  domainValidationKey?: string;
  allowDomainChange?: boolean;
  hasDomainValidated?: boolean;
  domainStatus?: string;
  matomoUrl?: string;
  matomoSiteId?: string;
  locale?: string;
  isTemplate?: boolean;
  info: ModelInfoJson;
  visibility: VisibilityJson;
  colorPalette?: ColorPaletteJson;
  fontStyles?: FontStylesJson;
  attributions?: AttributionJson[];
  cols?: Record<string, PageColJson>;
  layoutContainers?: Record<string, LayoutContainerJson>;
  redirects?: Record<string, LinkJson>;
  searchOptions?: SpaceSearchOptionsJson;
  defaultModels?: DefaultModelsJson;
  logo?: string;
  logoSquare?: string;
  cover?: string;
}

export class SpaceModel {
  private json: SpaceJson;
  private _isDirty: boolean = false;

  constructor(json: SpaceJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      name: "",
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      visibility: { isPublic: false, team: "" },
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: string
  get description(): string | undefined {
    return this.json.description;
  }

  set description(newValue: string | undefined) {
    this._isDirty = true;
    this.json.description = newValue;
  }

  /// field: string
  get landingPageId(): string | undefined {
    return this.json.landingPageId;
  }

  set landingPageId(newValue: string | undefined) {
    this._isDirty = true;
    this.json.landingPageId = newValue;
  }

  /// field: string
  get globalMapPageId(): string | undefined {
    return this.json.globalMapPageId;
  }

  set globalMapPageId(newValue: string | undefined) {
    this._isDirty = true;
    this.json.globalMapPageId = newValue;
  }

  /// field: string
  get domain(): string | undefined {
    return this.json.domain;
  }

  set domain(newValue: string | undefined) {
    this._isDirty = true;
    this.json.domain = newValue;
  }

  /// field: string
  get domainValidationKey(): string | undefined {
    return this.json.domainValidationKey;
  }

  set domainValidationKey(newValue: string | undefined) {
    this._isDirty = true;
    this.json.domainValidationKey = newValue;
  }

  /// field: bool
  get allowDomainChange(): boolean | undefined {
    return this.json.allowDomainChange;
  }

  set allowDomainChange(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.allowDomainChange = newValue;
  }

  /// field: bool
  get hasDomainValidated(): boolean | undefined {
    return this.json.hasDomainValidated;
  }

  set hasDomainValidated(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.hasDomainValidated = newValue;
  }

  /// field: string
  get domainStatus(): string | undefined {
    return this.json.domainStatus;
  }

  set domainStatus(newValue: string | undefined) {
    this._isDirty = true;
    this.json.domainStatus = newValue;
  }

  /// field: string
  get matomoUrl(): string | undefined {
    return this.json.matomoUrl;
  }

  set matomoUrl(newValue: string | undefined) {
    this._isDirty = true;
    this.json.matomoUrl = newValue;
  }

  /// field: string
  get matomoSiteId(): string | undefined {
    return this.json.matomoSiteId;
  }

  set matomoSiteId(newValue: string | undefined) {
    this._isDirty = true;
    this.json.matomoSiteId = newValue;
  }

  /// field: string
  get locale(): string | undefined {
    return this.json.locale;
  }

  set locale(newValue: string | undefined) {
    this._isDirty = true;
    this.json.locale = newValue;
  }

  /// field: bool
  get isTemplate(): boolean | undefined {
    return this.json.isTemplate;
  }

  set isTemplate(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.isTemplate = newValue;
  }

  /// object: ModelInfo
  get info(): ModelInfoModel {
    return new ModelInfoModel(this.json.info);
  }

  set info(newValue: ModelInfoModel) {
    this._isDirty = true;

    this.json.info = newValue?.toJSON();
  }

  /// object: Visibility
  get visibility(): VisibilityModel {
    return new VisibilityModel(this.json.visibility);
  }

  set visibility(newValue: VisibilityModel) {
    this._isDirty = true;

    this.json.visibility = newValue?.toJSON();
  }

  /// object: ColorPalette
  get colorPalette(): ColorPaletteModel {
    return new ColorPaletteModel(this.json.colorPalette);
  }

  set colorPalette(newValue: ColorPaletteModel | undefined) {
    this._isDirty = true;

    this.json.colorPalette = newValue?.toJSON();
  }

  /// object: FontStyles
  get fontStyles(): FontStylesModel {
    return new FontStylesModel(this.json.fontStyles);
  }

  set fontStyles(newValue: FontStylesModel | undefined) {
    this._isDirty = true;

    this.json.fontStyles = newValue?.toJSON();
  }

  /// object: Attribution
  get attributions(): AttributionModel[] {
    return this.json.attributions?.map((a) => new AttributionModel(a)) ?? [];
  }

  set attributions(newValue: AttributionModel[] | undefined) {
    this._isDirty = true;

    this.json.attributions = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// object: PageCol
  get cols(): Record<string, PageColModel> {
    return this.json.cols?.map((a) => new PageColModel(a)) ?? [];
  }

  set cols(newValue: Record<string, PageColModel> | undefined) {
    this._isDirty = true;

    this.json.cols = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// object: LayoutContainer
  get layoutContainers(): Record<string, LayoutContainerModel> {
    return (
      this.json.layoutContainers?.map((a) => new LayoutContainerModel(a)) ?? []
    );
  }

  set layoutContainers(
    newValue: Record<string, LayoutContainerModel> | undefined,
  ) {
    this._isDirty = true;

    this.json.layoutContainers = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// object: Link
  get redirects(): Record<string, LinkModel> {
    return this.json.redirects?.map((a) => new LinkModel(a)) ?? [];
  }

  set redirects(newValue: Record<string, LinkModel> | undefined) {
    this._isDirty = true;

    this.json.redirects = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// object: SpaceSearchOptions
  get searchOptions(): SpaceSearchOptionsModel {
    return new SpaceSearchOptionsModel(this.json.searchOptions);
  }

  set searchOptions(newValue: SpaceSearchOptionsModel | undefined) {
    this._isDirty = true;

    this.json.searchOptions = newValue?.toJSON();
  }

  /// object: DefaultModels
  get defaultModels(): DefaultModelsModel {
    return new DefaultModelsModel(this.json.defaultModels);
  }

  set defaultModels(newValue: DefaultModelsModel | undefined) {
    this._isDirty = true;

    this.json.defaultModels = newValue?.toJSON();
  }

  /// model: Picture
  get logo(): Promise<PictureModel | undefined> {
    if (!this.json.logo) {
      return Promise.resolve(undefined);
    }
    return Store.instance.findPicture(this.json.logo);
  }

  set logo(newValue: PictureModel | undefined) {
    this._isDirty = true;
    this.json.logo = newValue?._id;
  }

  /// model: Picture
  get logoSquare(): Promise<PictureModel | undefined> {
    if (!this.json.logoSquare) {
      return Promise.resolve(undefined);
    }
    return Store.instance.findPicture(this.json.logoSquare);
  }

  set logoSquare(newValue: PictureModel | undefined) {
    this._isDirty = true;
    this.json.logoSquare = newValue?._id;
  }

  /// model: Picture
  get cover(): Promise<PictureModel | undefined> {
    if (!this.json.cover) {
      return Promise.resolve(undefined);
    }
    return Store.instance.findPicture(this.json.cover);
  }

  set cover(newValue: PictureModel | undefined) {
    this._isDirty = true;
    this.json.cover = newValue?._id;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.info?.isDirty) {
      return true;
    }

    if (this.visibility?.isDirty) {
      return true;
    }

    if (this.colorPalette?.isDirty) {
      return true;
    }

    if (this.fontStyles?.isDirty) {
      return true;
    }

    for (let level0 of this.attributions) {
      if (level0?.isDirty) {
        return true;
      }
    }

    for (let level0 of this.cols) {
      if (level0?.isDirty) {
        return true;
      }
    }

    for (let level0 of this.layoutContainers) {
      if (level0?.isDirty) {
        return true;
      }
    }

    for (let level0 of this.redirects) {
      if (level0?.isDirty) {
        return true;
      }
    }

    if (this.searchOptions?.isDirty) {
      return true;
    }

    if (this.defaultModels?.isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<SpaceModel> {
    let result: SpaceModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createSpace(this);
    } else {
      result = await Store.instance.saveSpace(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): SpaceJson {
    return this.json;
  }
}
