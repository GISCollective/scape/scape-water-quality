import { Team, TeamModel } from "./Team";
import { Store } from "./store";

export interface Visibility {
  isPublic: boolean;
  isDefault?: boolean;
  team: Team;
}

export interface VisibilityJson {
  isPublic: boolean;
  isDefault?: boolean;
  team: string;
}

export class VisibilityModel {
  private json: VisibilityJson;
  private _isDirty: boolean = false;

  constructor(json: VisibilityJson | null | undefined) {
    this.json = json ?? { isPublic: false, team: "" };
  }

  /// field: bool
  get isPublic(): boolean {
    return this.json.isPublic;
  }

  set isPublic(newValue: boolean) {
    this._isDirty = true;
    this.json.isPublic = newValue;
  }

  /// field: bool
  get isDefault(): boolean | undefined {
    return this.json.isDefault;
  }

  set isDefault(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.isDefault = newValue;
  }

  /// model: Team
  get team(): Promise<TeamModel> {
    return Store.instance.findTeam(this.json.team);
  }

  set team(newValue: TeamModel) {
    this._isDirty = true;
    this.json.team = newValue?._id;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  toJSON(): VisibilityJson {
    return this.json;
  }
}
