import {
  TimeInterval,
  TimeIntervalJson,
  TimeIntervalModel,
} from "./TimeInterval";
import { Visibility, VisibilityJson, VisibilityModel } from "./Visibility";
import { Icon, IconModel } from "./Icon";
import { Store } from "./store";
import { Calendar, CalendarModel } from "./Calendar";
import { ModelInfo, ModelInfoJson, ModelInfoModel } from "./ModelInfo";
import {
  CalendarEntry,
  CalendarEntryJson,
  CalendarEntryModel,
} from "./CalendarEntry";
import {
  EventLocation,
  EventLocationJson,
  EventLocationModel,
} from "./EventLocation";
import { ArticleBody } from "./ArticleBody";
import { Picture, PictureModel } from "./Picture";

export interface Event {
  _id: string;
  name: string;
  article: ArticleBody;
  attributes?: any;
  info: ModelInfo;
  visibility: Visibility;
  entries: CalendarEntry[];
  location: EventLocation;
  allOccurrences?: TimeInterval[];
  nextOccurrence?: TimeInterval;
  calendar: Calendar;
  icons: Icon[];
  cover?: Picture;
}

export interface EventResponse {
  event: EventJson;
}

export interface EventsResponse {
  events: EventJson[];
}

export interface EventJson {
  _id: string;
  name: string;
  article: ArticleBody;
  attributes?: any;
  info: ModelInfoJson;
  visibility: VisibilityJson;
  entries: CalendarEntryJson[];
  location: EventLocationJson;
  allOccurrences?: TimeIntervalJson[];
  nextOccurrence?: TimeIntervalJson;
  calendar: string;
  icons: string[];
  cover?: string;
}

export class EventModel {
  private json: EventJson;
  private _isDirty: boolean = false;

  constructor(json: EventJson | null | undefined) {
    this.json = json ?? {
      _id: "",
      name: "",
      article: { blocks: [] },
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      visibility: { isPublic: false, team: "" },
      entries: [],
      location: { type: "", value: "" },
      calendar: "",
      icons: [],
    };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: Json
  get article(): ArticleBody {
    return this.json.article;
  }

  set article(newValue: ArticleBody) {
    this._isDirty = true;
    this.json.article = newValue;
  }

  /// field: Json
  get attributes(): any | undefined {
    return this.json.attributes;
  }

  set attributes(newValue: any | undefined) {
    this._isDirty = true;
    this.json.attributes = newValue;
  }

  /// object: ModelInfo
  get info(): ModelInfoModel {
    return new ModelInfoModel(this.json.info);
  }

  set info(newValue: ModelInfoModel) {
    this._isDirty = true;

    this.json.info = newValue?.toJSON();
  }

  /// object: Visibility
  get visibility(): VisibilityModel {
    return new VisibilityModel(this.json.visibility);
  }

  set visibility(newValue: VisibilityModel) {
    this._isDirty = true;

    this.json.visibility = newValue?.toJSON();
  }

  /// object: CalendarEntry
  get entries(): CalendarEntryModel[] {
    return this.json.entries?.map((a) => new CalendarEntryModel(a)) ?? [];
  }

  set entries(newValue: CalendarEntryModel[]) {
    this._isDirty = true;

    this.json.entries = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// object: EventLocation
  get location(): EventLocationModel {
    return new EventLocationModel(this.json.location);
  }

  set location(newValue: EventLocationModel) {
    this._isDirty = true;

    this.json.location = newValue?.toJSON();
  }

  /// object: TimeInterval
  get allOccurrences(): TimeIntervalModel[] {
    return this.json.allOccurrences?.map((a) => new TimeIntervalModel(a)) ?? [];
  }

  set allOccurrences(newValue: TimeIntervalModel[] | undefined) {
    this._isDirty = true;

    this.json.allOccurrences = newValue?.map((a) => a.toJSON()) ?? [];
  }

  /// object: TimeInterval
  get nextOccurrence(): TimeIntervalModel {
    return new TimeIntervalModel(this.json.nextOccurrence);
  }

  set nextOccurrence(newValue: TimeIntervalModel | undefined) {
    this._isDirty = true;

    this.json.nextOccurrence = newValue?.toJSON();
  }

  /// model: Calendar
  get calendar(): Promise<CalendarModel> {
    return Store.instance.findCalendar(this.json.calendar);
  }

  set calendar(newValue: CalendarModel) {
    this._isDirty = true;
    this.json.calendar = newValue?._id;
  }

  /// model: Icon
  get icons(): Promise<IconModel[]> {
    const promises =
      this.json.icons?.map((a) => Store.instance.findIcon(a)) ?? [];

    return Promise.all(promises);
  }

  set icons(newValue: IconModel[]) {
    this._isDirty = true;
    this.json.icons = newValue?.map((a) => a._id) ?? [];
  }

  /// model: Picture
  get cover(): Promise<PictureModel | undefined> {
    if (!this.json.cover) {
      return Promise.resolve(undefined);
    }
    return Store.instance.findPicture(this.json.cover);
  }

  set cover(newValue: PictureModel | undefined) {
    this._isDirty = true;
    this.json.cover = newValue?._id;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    if (this.info?.isDirty) {
      return true;
    }

    if (this.visibility?.isDirty) {
      return true;
    }

    for (let level0 of this.entries) {
      if (level0?.isDirty) {
        return true;
      }
    }

    if (this.location?.isDirty) {
      return true;
    }

    for (let level0 of this.allOccurrences) {
      if (level0?.isDirty) {
        return true;
      }
    }

    if (this.nextOccurrence?.isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<EventModel> {
    let result: EventModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createEvent(this);
    } else {
      result = await Store.instance.saveEvent(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): EventJson {
    return this.json;
  }
}
