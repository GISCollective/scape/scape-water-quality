import { Store } from "./store";

export interface Preference {
  _id: string;
  name: string;
  value: any;
  isSecret?: boolean;
}

export interface PreferenceResponse {
  preference: PreferenceJson;
}

export interface PreferencesResponse {
  preferences: PreferenceJson[];
}

export interface PreferenceJson {
  _id: string;
  name: string;
  value: any;
  isSecret?: boolean;
}

export class PreferenceModel {
  private json: PreferenceJson;
  private _isDirty: boolean = false;

  constructor(json: PreferenceJson | null | undefined) {
    this.json = json ?? { _id: "", name: "", value: null };
  }

  /// field: string
  get _id(): string {
    return this.json._id;
  }

  set _id(newValue: string) {
    this._isDirty = true;
    this.json._id = newValue;
  }

  /// field: string
  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }

  /// field: Json
  get value(): any {
    return this.json.value;
  }

  set value(newValue: any) {
    this._isDirty = true;
    this.json.value = newValue;
  }

  /// field: bool
  get isSecret(): boolean | undefined {
    return this.json.isSecret;
  }

  set isSecret(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.isSecret = newValue;
  }

  get isDirty(): boolean {
    if (this._isDirty) {
      return true;
    }

    return false;
  }

  async save(): Promise<PreferenceModel> {
    let result: PreferenceModel;

    if (!this._id || this._id?.indexOf("pending") == 0) {
      result = await Store.instance.createPreference(this);
    } else {
      result = await Store.instance.savePreference(this);
    }

    this.json = result.toJSON();

    return this;
  }
  toJSON(): PreferenceJson {
    return this.json;
  }
}
