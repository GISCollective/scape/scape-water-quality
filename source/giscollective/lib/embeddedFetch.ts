import { Store } from "../models/store";
import { genericBinaryFetch } from "./fetch";

function getPlural(name: string) {
  return `${name}s`;
}

export function getListKey(name: string) {
  const plural = getPlural(name);

  return plural.substring(0, 1).toLowerCase() + plural.substring(1);
}

export interface StoreFetchInfo {
  id: null | string;
  modelName: string;
  requestType: string;
  query: Record<string, string> | undefined;
}

type StoreOp = keyof Store;

export function getAction(info: StoreFetchInfo): StoreOp {
  return `${info.requestType}${getPlural(info.modelName)}` as StoreOp;
}

export async function storeFetch(
  info: StoreFetchInfo,
  type: string,
  options: Record<string, any>,
): Promise<any> {
  const action = getAction(info);

  const func = action in Store.instance ? Store.instance?.[action] : null;

  if (typeof func !== "function") {
    return null;
  }

  const result = await func.call(Store.instance, info.query);

  if (Array.isArray(result)) {
    const key = getListKey(info.modelName);

    return btoa(
      `{ "${key}": [${result
        .map((a) => `${JSON.stringify(a.toJSON())}`)
        .join(",")}] }`,
    );
  }

  if (typeof result?.toJSON == "function") {
    const key = "";
    return { [key]: result.toJSON() };
  }

  return result;
}

export interface WebviewFetchOptions {
  url: string;
  method: string;
  headers: Record<string, string>;
  body: string | null;
}

export async function webviewFetch(options: WebviewFetchOptions) {
  const result = await genericBinaryFetch(
    options.method,
    options.url,
    options.headers,
    options.body,
    1,
  );

  return btoa(
    JSON.stringify({
      body: result.data,
      options: {
        status: result.status,
        statusText: result.statusText,
        headers: result.headers,
      },
    }),
  );
}
