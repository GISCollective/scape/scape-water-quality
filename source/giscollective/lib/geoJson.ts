/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import GeoJSON from "ol/format/GeoJSON";
import { getCenter } from "ol/extent";
import { transformExtent } from "ol/proj";

export class GeoJson {
  type: string = "Point";
  coordinates: number[] | number[][] | number[][][] = [];

  constructor(deserialized) {
    if (!deserialized || typeof deserialized != "object") {
      deserialized = {};
    }

    if (deserialized.type) {
      this.type = deserialized.type;
    }

    if (deserialized.coordinates) {
      this.coordinates = deserialized.coordinates.slice();
    }
  }

  get center() {
    if (this.type == "Point") {
      return this;
    }

    if (this.type == "MultiPoint") {
      return new GeoJson({
        type: "Point",
        coordinates: this.coordinates[0],
      });
    }

    if (this.type == "LineString") {
      const middle = Math.round(this.coordinates.length / 2);
      return new GeoJson({
        type: "Point",
        coordinates: this.coordinates[middle],
      });
    }

    if (this.type == "LineString") {
      const middle = Math.round(this.coordinates.length / 2);
      return new GeoJson({
        type: "Point",
        coordinates: this.coordinates[middle],
      });
    }

    if (this.type == "MultiLineString" && Array.isArray(this.coordinates[0])) {
      const middle = Math.round(this.coordinates[0]?.length / 2);
      return new GeoJson({
        type: "Point",
        coordinates: this.coordinates[0][middle],
      });
    }

    if (this.type == "Polygon") {
      return new GeoJson({
        type: "Point",
        coordinates: getCenter(this.toOlFeature().getGeometry().getExtent()),
      });
    }

    if (this.type == "MultiPolygon") {
      return new GeoJson({
        type: "Point",
        coordinates: getCenter(this.toOlFeature().getGeometry().getExtent()),
      });
    }

    return new GeoJson({
      type: "Point",
      coordinates: [0, 0],
    });
  }

  toExtent(projection) {
    if (this.type == "Point") {
      const center = this.center.coordinates;
      const delta = 0.005;
      return transformExtent(
        [
          center[0] - delta,
          center[1] - delta,
          center[0] + delta,
          center[1] + delta,
        ],
        "EPSG:4326",
        projection,
      );
    }

    return this.toOlFeature(projection).getGeometry().getExtent();
  }

  toOlFeature(projection) {
    const geoJson = new GeoJSON();

    const feature = geoJson.readFeature(this.toJSON());

    if (projection) {
      feature.getGeometry().transform("EPSG:4326", projection);
    }

    return feature;
  }

  toJSON() {
    const result = {};

    if (this.type) {
      result["type"] = this.type;
    }

    if (this.coordinates) {
      result["coordinates"] = this.coordinates;
    }

    return result;
  }
}
