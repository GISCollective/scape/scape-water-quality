import { Calendar } from "react-native-paper-dates";
import { SQLiteExecutor } from "../../../__tests__/sqlite/SQLiteExecutor";
import { LocalStorage } from "./localStorage";

describe("LocalStorage", () => {
  let db: any;
  let instance: LocalStorage;

  beforeEach(async () => {
    db = SQLiteExecutor.openDatabase(":memory:");
    instance = new LocalStorage(db);
    await instance.setup();

    Calendar.instance = { sqlNow: `2024-08-19 12:34:36` };
  });

  afterEach(async () => {
    await db.close();
  });

  describe("setItem", () => {
    it("stores a new record", async () => {
      await instance.setItem("test123", "test");

      const value = await instance.getItem("test123");

      expect(value).toEqual("test");
    });

    it("updates a record when it is called twice", async () => {
      const instance = new LocalStorage(db);
      await instance.setup();

      await instance.setItem("test123", "test");
      await instance.setItem("test123", "test-new");

      const value = await instance.getItem("test123");

      expect(value).toEqual("test-new");
    });
  });

  describe("removeItem", () => {
    it("does nothing when the key is not set", async () => {
      await instance.removeItem("test123");

      const value = await instance.getItem("test123");

      expect(value).toEqual("");
    });

    it("removes an existing item", async () => {
      const instance = new LocalStorage(db);
      await instance.setup();

      await instance.setItem("test123", "test");
      await instance.removeItem("test123");

      const value = await instance.getItem("test123");

      expect(value).toEqual("");
    });
  });

  describe("clear", () => {
    it("deletes a stored record", async () => {
      await instance.setItem("test123", "test");
      await instance.clear();
      const value = await instance.getItem("test123");

      expect(value).toEqual("");
    });
  });
});
