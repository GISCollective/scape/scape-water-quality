import { CampaignModel } from "../models/Campaign";
import { CampaignAnswerModel } from "../models/CampaignAnswer";
import { GeoJsonGeometry } from "../models/GeoJsonGeometry";
import { IconModel } from "../models/Icon";
import { PictureModel } from "../models/Picture";
import { SoundModel } from "../models/Sound";
import { Store } from "../models/store";
import { LocalStorage } from "./localStorage";
import { uuidv4 } from "./uuid";

function onlyUnique<T>(value: T, index: number, array: T[]) {
  return array.indexOf(value) === index;
}

export interface NewAnswerResponse {
  answer: CampaignAnswerModel;
  area: GeoJsonGeometry;
  defaultIcons: IconModel[];
  isRestored: boolean;
}

export async function getPendingAnswers() {
  let pendingRecords = await LocalStorage.instance.getItem("pending-answers");

  let parsedRecords: Record<string, string> = {};

  try {
    parsedRecords = JSON.parse(pendingRecords);
  } catch (err) {
    console.log(err);
  }

  return parsedRecords;
}

export async function setPendingAnswer(surveyId: string, answerId: string) {
  let parsedRecords = await getPendingAnswers();
  parsedRecords[surveyId] = answerId;

  return LocalStorage.instance.setItem(
    `pending-answers`,
    JSON.stringify(parsedRecords),
  );
}

export async function createNewAnswer(
  survey: CampaignModel,
): Promise<NewAnswerResponse> {
  const pendingAnswers = await getPendingAnswers();
  const answerId = pendingAnswers[survey._id];
  const storedAnswer = await Store.instance.cache.get(
    answerId,
    "CampaignAnswer",
  );

  const isRestored = !!storedAnswer;

  const answer = new CampaignAnswerModel(
    await parseStoredAnswer(storedAnswer ?? "", survey._id),
  );

  let area: GeoJsonGeometry | undefined;

  if (survey?.map.isEnabled && survey?.map.map) {
    const map = await Store.instance.findMap(survey?.map.map);
    area = map.area;
  }

  const defaultIcons = await survey.icons;
  const defaultIconsId = defaultIcons.map((a) => a._id);

  const icons = (await answer.icons) ?? [];
  const iconIds = icons.map((a) => a._id);

  const selectedIconIds = iconIds.filter((a) => !defaultIconsId.includes(a));

  const selectedIcons = selectedIconIds
    .filter((value, index, self) => self.indexOf(value) === index)
    .map((a) => icons.find((b) => b._id == a)) as IconModel[];

  answer.icons = [...defaultIcons, ...selectedIcons];

  if (!area) {
    area = {
      type: "Polygon",
      coordinates: [],
    };
  }

  return {
    answer,
    area,
    defaultIcons,
    isRestored,
  };
}

export async function extractGenericAnswers(answer: CampaignAnswerModel) {
  const survey = await answer.campaign;
  const defaultIcons = await survey.icons;
  let icons: IconModel[] = defaultIcons;

  try {
    icons = (await answer.icons) ?? [];
  } catch (err) {
    console.error(err);
  }

  let pictures: PictureModel[] = [];
  try {
    pictures = (await answer.pictures) ?? [];
  } catch (err) {
    console.error(err);
  }

  let sounds: SoundModel[] = [];
  try {
    sounds = (await answer.sounds) ?? [];
  } catch (err) {
    console.error(err);
  }

  const result: Record<string, any> = {};

  if (answer.attributes?.["about"]?.["name"]) {
    result.name = answer.attributes?.["about"]?.["name"];
  }

  if (answer.attributes?.["about"]?.["description"]) {
    result.description = answer.attributes?.["about"]?.["description"];
  }

  if (answer.position?.type) {
    result.position = answer.position;
  }

  if (answer.attributes?.["position details"]) {
    result["position details"] = answer.attributes?.["position details"];
  }

  if (icons?.length) {
    const defaultIds = defaultIcons?.map((a) => a._id) ?? [];
    result.icons = icons.filter((a) => !defaultIds.includes(a._id));
  } else {
    result.icons = [];
  }

  result.sounds = sounds ?? [];
  result.pictures = pictures ?? [];

  for (const key in answer.attributes?.["other"] ?? {}) {
    result[key] = answer.attributes["other"][key];
  }

  return result;
}

export function parseStoredAnswer(value: string, surveyId: string) {
  if (value) {
    try {
      return JSON.parse(value);
    } catch (err) {}
  }

  return {
    _id: `pending-${uuidv4()}`,
    attributes: {
      about: {
        name: "",
        description: "",
      },
      "position details": {},
    },
    campaign: surveyId,
    info: { author: "", changeIndex: 0, createdOn: "", lastChangeOn: "" },
  };
}

export async function fillAnswer(
  answer: CampaignAnswerModel,
  values: Record<string, any>,
  defaultIcons: IconModel[] = [],
) {
  const knownAttributes = [
    "pictures",
    "sounds",
    "icons",
    "positionDetails",
    "position",
    "name",
    "description",
  ];

  const newIcons = [...defaultIcons, ...(values?.["icons"] ?? [])];

  answer.pictures = values["pictures"];
  answer.sounds = values["sounds"];
  answer.icons = newIcons
    .map((a) => a._id)
    .filter(onlyUnique)
    .map((id) => newIcons.find((a) => a._id == id));
  answer.attributes["position details"] = values["positionDetails"] ?? {};
  answer.position = values["position"] ?? {};

  if (!answer.attributes.about) {
    answer.attributes.about = {
      name: "",
      description: "",
    };
  }

  if (!answer.attributes.other) {
    answer.attributes.other = {};
  }

  answer.attributes.about.name = values["name"] ?? "";
  answer.attributes.about.description = values["description"] ?? "";

  for (const key in values) {
    if (knownAttributes.includes(key)) {
      continue;
    }

    answer.attributes.other[key] = values[key];
  }
}

export async function hasPendingAnswer(surveyId: string) {
  const pendingAnswers = await getPendingAnswers();

  return !!pendingAnswers[surveyId];
}

export async function needsUnitsQuestion(
  survey: CampaignModel,
): Promise<boolean> {
  const measurements = ["Temperature"];

  let result = survey.questions
    .filter((a) => a.isVisible)
    .map((a) => a.options?.measurement ?? "")
    .filter((a) => measurements.includes(a));

  if (result.length > 0) {
    return true;
  }

  const icons = await survey.icons;
  const attributes = icons.flatMap((a) => a.attributes).map((a) => a.options);

  result = attributes
    .map((a) => a?.measurement ?? "")
    .filter((a) => measurements.includes(a));

  if (result.length > 0) {
    return true;
  }

  return false;
}
