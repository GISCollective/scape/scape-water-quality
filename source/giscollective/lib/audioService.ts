import AudioRecorderPlayer, {
  PlayBackType,
} from "react-native-audio-recorder-player";

export interface PlayBackEvent {
  playTime: string;
  duration: string;
  isPlaying: boolean;
  file: string;
}

export class AudioService {
  static _instance?: AudioService;
  private audioRecorderPlayer: AudioRecorderPlayer;
  private lastFile: string = "";
  playBackState: PlayBackEvent = {
    playTime: "",
    duration: "",
    isPlaying: false,
    file: "",
  };

  static get instance() {
    if (!AudioService._instance) {
      AudioService._instance = new AudioService();
    }

    return AudioService._instance;
  }

  constructor() {
    this.audioRecorderPlayer = new AudioRecorderPlayer();
    this.audioRecorderPlayer.addPlayBackListener((e: PlayBackType) => {
      const isPlaying = !e.isFinished && e.currentPosition != e.duration;

      if (!isPlaying) {
        this.playBackState = {
          playTime: "",
          duration: "",
          isPlaying: false,
          file: "",
        };

        return;
      }

      this.playBackState = {
        playTime: this.audioRecorderPlayer.mmssss(
          Math.floor(e.currentPosition),
        ),
        duration: this.audioRecorderPlayer.mmssss(Math.floor(e.duration)),
        isPlaying,
        file: this.lastFile,
      };
    });
  }

  play(file: string) {
    this.audioRecorderPlayer.stopPlayer();
    this.audioRecorderPlayer.startPlayer(file);
    this.lastFile = file;
    this.playBackState = {
      playTime: "",
      duration: "",
      isPlaying: true,
      file,
    };
  }

  stop() {
    this.audioRecorderPlayer.stopPlayer();
    this.playBackState = {
      playTime: "",
      duration: "",
      isPlaying: false,
      file: "",
    };
  }
}
