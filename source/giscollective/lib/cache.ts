import { Calendar } from "./calendar";
import { Cache } from "../models/store";
import SQLite, { SQLiteDatabase } from "react-native-sqlite-storage";

SQLite.enablePromise(true);

export class SQLiteCache implements Cache {
  constructor(private readonly db: SQLiteDatabase) {}

  async setup() {
    await this.db.executeSql(
      `CREATE TABLE IF NOT EXISTS store (
          id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
          record text,
          type VARCHAR(32),
          remote_id VARCHAR(64),
          last_sync DATETIME,
          UNIQUE(type, remote_id) ON CONFLICT REPLACE
        );`,
    );
  }

  async upsert(id: string | number, type: string, value: any): Promise<void> {
    await this.db.transaction((tx) => {
      tx.executeSql(
        "INSERT INTO store (record, type, remote_id, last_sync) VALUES(?, ?, ?, ?);",
        [JSON.stringify(value), type, id, Calendar.instance.sqlNow],
      );
    });
  }

  async get(id: string | number, type: string): Promise<null | string> {
    return await new Promise((resolve) =>
      this.db.transaction((tx: any) => {
        tx.executeSql(
          `SELECT * FROM store WHERE remote_id = "${id}" AND type = "${type}"`,
          [],
          (tx: any, results: any) => {
            if (!results?.rows?.length) {
              return resolve(null);
            }

            resolve(results.rows.item(0).record);
          },
        );
      }),
    );
  }

  async getPending(type: string): Promise<string[]> {
    return await new Promise((resolve) =>
      this.db.transaction((tx: any) => {
        tx.executeSql(
          `SELECT * FROM store WHERE type = "${type}" AND remote_id LIKE "%pending%"`,
          [],
          (tx: any, results: any) => {
            if (!results?.rows?.length) {
              return resolve([]);
            }

            const result = [];

            for (let i = 0; i < results.rows.length; i++) {
              const item = results.rows.item(i);
              result.push(item?.["record"]);
            }

            resolve(result);
          },
        );
      }),
    );
  }

  async delete(id: string | number, type: string): Promise<void> {
    return await new Promise((resolve) =>
      this.db.transaction((tx: any) => {
        tx.executeSql(
          `DELETE FROM store WHERE remote_id = "${id}" AND type = "${type}"`,
          [],
          () => {
            resolve();
          },
        );
      }),
    );
  }

  async clear(): Promise<void> {
    return await new Promise((resolve) =>
      this.db.transaction((tx: any) => {
        tx.executeSql(`DELETE FROM store`, [], () => {
          resolve();
        });
      }),
    );
  }

  async getAll() {
    return await new Promise((resolve) =>
      this.db.transaction((tx: any) => {
        tx.executeSql("SELECT * FROM store", [], (tx: any, results: any) => {
          const rows: string[] = [];

          for (let i = 0; i < results.rows.length; i++) {
            rows.push(results.rows.item(i));
          }

          resolve(rows);
        });
      }),
    );
  }
}

export class MemoryCache implements Cache {
  store: Record<string, string> = {};

  async upsert(id: string | number, type: string, value: any): Promise<void> {
    const key = `${id}-${type}`;
    this.store[key] = JSON.stringify(value);
  }

  async get(id: string | number, type: string): Promise<null | string> {
    const key = `${id}-${type}`;

    return this.store[key];
  }

  async getPending(type: string): Promise<string[]> {
    return Object.keys(this.store)
      .filter((a) => a.includes(`-${type}`))
      .filter((a) => a.includes(`pending-`));
  }

  async delete(id: string | number, type: string): Promise<void> {
    const key = `${id}-${type}`;

    delete this.store[key];
  }

  async clear(): Promise<void> {
    this.store = {};
  }

  async getAll() {
    return Object.values(this.store);
  }
}
