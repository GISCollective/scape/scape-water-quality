import { extractCssVariables } from "./css";

describe("extractCssVariables", () => {
  it("returns an empty map when there is no input", () => {
    const result = extractCssVariables("");

    expect(result).toEqual({});
  });

  it("creates a key for a bs-theme", () => {
    const result = extractCssVariables(
      "[data-bs-theme=light-63a1e76696f0db010077e7ee] {}",
    );

    expect(Object.keys(result)).toEqual(["light-63a1e76696f0db010077e7ee"]);
  });

  it("creates two keys for each bs-theme", () => {
    const result = extractCssVariables(`
      [data-bs-theme=light-63a1e76696f0db010077e7ee] {}
      [data-bs-theme=dark-63a1e76696f0db010077e7ee] {}`);

    expect(Object.keys(result).sort()).toEqual([
      "dark-63a1e76696f0db010077e7ee",
      "light-63a1e76696f0db010077e7ee",
    ]);
  });

  it("extracts a variable from a theme", () => {
    const result = extractCssVariables(`
      [data-bs-theme=light-63a1e76696f0db010077e7ee] {
        --bs-teal-800-rgb: 49, 61, 72;
      }`);

    expect(result).toEqual({
      "light-63a1e76696f0db010077e7ee": {
        "teal-800-rgb": "49, 61, 72",
      },
    });
  });

  it("extracts a variables from two themes", () => {
    const result = extractCssVariables(`
      [data-bs-theme=light-63a1e76696f0db010077e7ee] {
        --bs-teal-800-rgb: 49, 61, 72;
      }

      [data-bs-theme=dark-63a1e76696f0db010077e7ee] {
        --bs-teal-800-rgb: 9, 1, 2;
      }`);

    expect(result).toEqual({
      "light-63a1e76696f0db010077e7ee": {
        "teal-800-rgb": "49, 61, 72",
      },
      "dark-63a1e76696f0db010077e7ee": {
        "teal-800-rgb": "9, 1, 2",
      },
    });
  });

  it("ignores whitespaces in bs-theme names", () => {
    const result = extractCssVariables(
      "[data-bs-theme=  light-63a1e76696f0db010077e7ee  ] {}",
    );

    expect(Object.keys(result)).toEqual(["light-63a1e76696f0db010077e7ee"]);
  });

  it("ignores theme blocks without braces", () => {
    const result = extractCssVariables(`
      [data-bs-theme=light-63a1e76696f0db010077e7ee] { --bs-teal-800-rgb: 49, 61, 72; }
      [data-bs-theme=light-63a1e76696f0db010077e7ee] p, div, ul, ol { font-family: var(--ff-poppins); font-weight: normal; font-size: 1rem }`);

    expect(result).toEqual({
      "light-63a1e76696f0db010077e7ee": {
        "teal-800-rgb": "49, 61, 72",
      },
    });
  });
});
