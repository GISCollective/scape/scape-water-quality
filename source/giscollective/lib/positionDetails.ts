import { LocationService } from "./locationService";

export interface PositionDetailsValue {
  capturedUsingGPS: boolean;
  type: null | string;
  altitude: number;
  accuracy: number;
  altitudeAccuracy: number;
  searchTerm: null | string;
  address: null | string;
  id: null | string;

  toJSON?: () => PositionDetailsValue;
}

export class PositionDetails implements PositionDetailsValue {
  capturedUsingGPS = false;
  type: null | string = null;
  altitude = 0;
  accuracy = 1000;
  altitudeAccuracy = 1000;
  searchTerm: null | string = null;
  address: null | string = null;
  id: null | string = null;

  constructor(deserialized: any) {
    if (!deserialized || typeof deserialized != "object") {
      deserialized = {};
    }

    if (deserialized.capturedUsingGPS) {
      this.capturedUsingGPS = deserialized.capturedUsingGPS;
    }

    if (deserialized.type) {
      this.type = deserialized.type;
    }

    if (deserialized.id) {
      this.id = deserialized.id;
    }

    if (deserialized.searchTerm) {
      this.searchTerm = deserialized.searchTerm;
    }

    if (deserialized.address) {
      this.address = deserialized.address;
    }

    if (deserialized.altitude) {
      this.altitude = deserialized.altitude;
    }

    if (deserialized.accuracy) {
      this.accuracy = deserialized.accuracy;
    }

    if (deserialized.altitudeAccuracy) {
      this.altitudeAccuracy = deserialized.altitudeAccuracy;
    }

    if (deserialized["altitude accuracy"]) {
      this.altitudeAccuracy = deserialized["altitude accuracy"];
    }
  }

  toJSON() {
    const result: PositionDetailsValue = {
      altitude: this.altitude,
      accuracy: this.accuracy,
      altitudeAccuracy: this.altitudeAccuracy,
      capturedUsingGPS: false,
      type: null,
      searchTerm: null,
      address: null,
      id: null,
    };

    if (this.type) {
      result.type = this.type;
    } else {
      result.capturedUsingGPS = this.capturedUsingGPS ?? false;
    }

    if (this.type) {
      result.type = this.type;
    }

    if (this.searchTerm) {
      result.searchTerm = this.searchTerm;
    }

    if (this.address) {
      result.address = this.address;
    }

    if (this.id) {
      result.id = this.id;
    }

    return result;
  }
}

export class PositionDetailsGps implements PositionDetailsValue {
  get capturedUsingGPS() {
    return true;
  }

  get type() {
    return "gps";
  }

  get altitude() {
    return LocationService.instance.lastValue?.coords.altitude ?? 0;
  }

  get accuracy() {
    return LocationService.instance.lastValue?.coords.accuracy ?? 0;
  }
  get altitudeAccuracy() {
    return LocationService.instance.lastValue?.coords.altitudeAccuracy ?? 0;
  }
  get searchTerm() {
    return null;
  }
  get address() {
    return null;
  }
  get id() {
    return null;
  }

  toJSON() {
    const result: PositionDetailsValue = {
      altitude: this.altitude,
      accuracy: this.accuracy,
      altitudeAccuracy: this.altitudeAccuracy,
      capturedUsingGPS: false,
      type: null,
      searchTerm: null,
      address: null,
      id: null,
    };

    if (this.type) {
      result.type = this.type;
    } else {
      result.capturedUsingGPS = this.capturedUsingGPS ?? false;
    }

    if (this.type) {
      result.type = this.type;
    }

    if (this.searchTerm) {
      result.searchTerm = this.searchTerm;
    }

    if (this.address) {
      result.address = this.address;
    }

    if (this.id) {
      result.id = this.id;
    }

    return result;
  }
}
