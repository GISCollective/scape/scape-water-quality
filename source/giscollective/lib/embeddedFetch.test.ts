import { getAction, getListKey } from "./embeddedFetch";

describe("getAction", () => {
  it("returns queryBaseMaps for a GET BaseMap without id", () => {
    const result = getAction({
      id: null,
      modelName: "BaseMap",
      requestType: "query",
      query: undefined,
    });

    expect(result).toEqual("queryBaseMaps");
  });
});

describe("getListKey", () => {
  it("returns baseMaps for a base map list", () => {
    const result = getListKey("BaseMap");

    expect(result).toEqual("baseMaps");
  });
});
