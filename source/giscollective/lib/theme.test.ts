import { TextStyle, ViewStyle } from "react-native";
import { Theme } from "./theme";

describe("Theme", () => {
  it("uses the default styles for the text classes", () => {
    const instance = new Theme();

    const result: TextStyle = instance.toTextStyle([
      "ff-anton",
      "lh-lg",
      "fw-bold",
      "text-center",
      "text-line-clamp-2",
      "pt-5",
      "pb-5",
      "ps-5",
      "pe-5",
      "text-size-17",
    ]);

    expect(result).toEqual({
      fontFamily: "anton",
      lineHeight: 28,
      fontWeight: "bold",
      textAlign: "center",
      fontSize: 19.833333333333336,
      paddingTop: 42,
      paddingBottom: 42,
      paddingLeft: 42,
      paddingRight: 42,
      color: "#212529",
    });
  });

  it("can get a default color", () => {
    const instance = new Theme();

    const result = instance.toColor("primary");

    expect(result).toEqual("#3961d0");
  });

  describe("the button styles", () => {
    it("can get the default button style", () => {
      const instance = new Theme();

      const result: ViewStyle = instance.toButtonStyle([], "");

      expect(result).toEqual({
        backgroundColor: undefined,
        borderColor: "#6c757d",
        borderRadius: 7,
        borderWidth: 1,
        paddingHorizontal: 14,
        opacity: 1,
        paddingVertical: 7,
      });
    });

    it("can get the primary button style", () => {
      const instance = new Theme();

      const result: ViewStyle = instance.toButtonStyle(["btn-primary"], "");

      expect(result).toEqual({
        backgroundColor: "rgb(57,97,208)",
        borderColor: "rgb(57,97,208)",
        borderRadius: 7,
        opacity: 1,
        borderWidth: 1,
        paddingHorizontal: 14,
        paddingVertical: 7,
      });
    });

    it("can get the primary text button style", () => {
      const instance = new Theme();

      const result: TextStyle = instance.toButtonTextStyle(["btn-primary"], "");

      expect(result).toEqual({
        color: "rgb(255,255,255)",
        fontFamily: "lato",
        textAlign: "center",
        fontSize: 14,
      });
    });

    it("can get the outline primary button style", () => {
      const instance = new Theme();

      const result: ViewStyle = instance.toButtonStyle(
        ["btn-outline-primary"],
        "",
      );

      expect(result).toEqual({
        backgroundColor: "rgb(255,255,255)",
        borderColor: "rgb(57,97,208)",
        borderRadius: 7,
        borderWidth: 1,
        opacity: 1,
        paddingHorizontal: 14,
        paddingVertical: 7,
      });
    });

    it("can get the outline primary active button style", () => {
      const instance = new Theme();

      const result: ViewStyle = instance.toButtonStyle(
        ["btn-outline-primary:active"],
        "",
      );

      expect(result).toEqual({
        backgroundColor: "rgb(215,223,246)",
        borderColor: "rgb(57,97,208)",
        borderRadius: 7,
        borderWidth: 1,
        opacity: 1,
        paddingHorizontal: 14,
        paddingVertical: 7,
      });
    });

    it("can get the primary active button style", () => {
      const instance = new Theme();

      const result: ViewStyle = instance.toButtonStyle(
        ["btn-primary"],
        "active",
      );

      expect(result).toEqual({
        backgroundColor: "rgb(46,78,166)",
        borderColor: "rgb(57,97,208)",
        borderRadius: 7,
        borderWidth: 1,
        opacity: 1,
        paddingHorizontal: 14,
        paddingVertical: 7,
      });
    });

    it("can get the secondary button style", () => {
      const instance = new Theme();

      const result: ViewStyle = instance.toButtonStyle(["btn-secondary"], "");

      expect(result).toEqual({
        backgroundColor: "#6c757d",
        borderColor: "#6c757d",
        borderRadius: 7,
        borderWidth: 1,
        opacity: 1,
        paddingHorizontal: 14,
        paddingVertical: 7,
      });
    });
  });
});
