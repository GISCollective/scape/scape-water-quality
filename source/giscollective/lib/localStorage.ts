import { SQLiteDatabase } from "react-native-sqlite-storage";

export interface ILocalStorage {
  setItem(key: string, value: string): Promise<void>;
  getItem(key: string): Promise<string>;
  removeItem(key: string): Promise<void>;
  clear(): Promise<void>;
}

export class LocalStorage implements ILocalStorage {
  private static _instance: ILocalStorage | undefined;

  static set instance(value: ILocalStorage) {
    this._instance = value;
  }

  static get instance(): ILocalStorage {
    if (!this._instance) {
      throw new Error("The local storage instance is not set up.");
    }

    return this._instance;
  }

  constructor(private readonly db: SQLiteDatabase) {}

  async setItem(key: string, value: string): Promise<void> {
    await this.db.transaction((tx) => {
      tx.executeSql("INSERT INTO local_store (key, value) VALUES(?, ?);", [
        key,
        value,
      ]);
    });
  }

  async getItem(key: string): Promise<string> {
    return await new Promise((resolve) =>
      this.db.transaction((tx: any) => {
        tx.executeSql(
          `SELECT * FROM local_store WHERE key = ?`,
          [key],
          (tx: any, results: any) => {
            if (!results?.rows?.length) {
              return resolve("");
            }

            resolve(results.rows.item(0).value);
          },
        );
      }),
    );
  }

  removeItem(key: string): Promise<void> {
    return new Promise((resolve) =>
      this.db.transaction((tx: any) => {
        tx.executeSql(`DELETE FROM local_store WHERE key = ?`, [key], () => {
          resolve();
        });
      }),
    );
  }

  async setup() {
    await this.db.transaction((tx) => {
      tx.executeSql(
        `CREATE TABLE IF NOT EXISTS local_store (
          id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
          key text,
          value text,
          UNIQUE(key) ON CONFLICT REPLACE
        );`,
      );
    });
  }

  clear(): Promise<void> {
    return new Promise((resolve) =>
      this.db.transaction((tx: any) => {
        tx.executeSql(`DELETE FROM local_store`, [], () => {
          resolve();
        });
      }),
    );
  }
}

export class LocalStorageMemory implements ILocalStorage {
  private _values: Record<string, string> = {};

  setItem(key: string, value: string): Promise<void> {
    this._values[key] = value;

    return Promise.resolve();
  }

  getItem(key: string): Promise<string> {
    return Promise.resolve(this._values[key] ?? "");
  }

  removeItem(key: string): Promise<void> {
    this._values[key] = "";
    return Promise.resolve();
  }

  clear(): Promise<void> {
    this._values = {};

    return Promise.resolve();
  }
}
