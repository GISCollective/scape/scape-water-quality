import { DateTime } from "luxon";
import { RequestStore } from "./requestStore";
import { MemoryCache } from "./cache";

describe("RequestStore", function () {
  let instance: RequestStore;
  let cache: MemoryCache;

  beforeEach(() => {
    cache = new MemoryCache();
    instance = new RequestStore(cache);
  });

  it("returns null when a response is not cached", async () => {
    const result = await instance.get("http://test.com");

    expect(result).toBeNull();
  });

  it("returns a CachedResponse when a response is cached", async () => {
    cache.upsert("http://test.com", "Response", {
      etag: "test",
      time: "2024-01-02T00:00:00Z",
      response: "{ a: 1 }",
    });

    const result = await instance.get("http://test.com");

    expect(result).toEqual({
      etag: "test",
      response: "{ a: 1 }",
      time: DateTime.fromISO("2024-01-02T00:00:00Z"),
      url: "http://test.com",
    });
  });
});
