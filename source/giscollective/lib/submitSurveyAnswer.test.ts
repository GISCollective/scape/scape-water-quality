import { CampaignAnswerModel } from "../models/CampaignAnswer";
import { PictureModel } from "../models/Picture";
import { SoundModel } from "../models/Sound";
import { Store } from "../models/store";
import { StaticAuthenticator } from "./authenticator";
import { MemoryCache } from "./cache";
import { submitSurveyAnswer } from "./submitSurveyAnswer";

describe("submitSurveyAnswer", () => {
  let mockFetch: jest.Mock;
  let cache: MemoryCache;

  beforeEach(() => {
    cache = new MemoryCache();
    mockFetch = jest.fn();

    Store.instance = new Store(
      "",
      mockFetch,
      cache,
      new StaticAuthenticator(""),
    );
  });

  it("sends a survey answer to the server", async () => {
    const answer = new CampaignAnswerModel({
      _id: "pending-1",
      attributes: [],
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      campaign: "1",
    });

    mockFetch.mockResolvedValue({
      campaignAnswer: answer.toJSON(),
    });

    await submitSurveyAnswer(answer);

    expect(mockFetch).toHaveBeenCalledTimes(1);
    expect(mockFetch).toHaveBeenCalledWith(
      "POST",
      "/campaignanswers",
      {},
      {
        campaignAnswer: answer.toJSON(),
      },
    );
  });

  it("sends the pictures before the survey", async () => {
    const answer = new CampaignAnswerModel({
      _id: "pending-1",
      attributes: [],
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      campaign: "1",
    });

    const picture = new PictureModel({
      _id: "pending-picture-1",
      name: "",
      picture: "",
    });

    await cache.upsert(picture._id, "Picture", picture.toJSON());
    answer.pictures = [picture];

    let i = -1;
    mockFetch.mockImplementation(() => {
      i++;
      if (i == 0) {
        return {
          picture: {
            _id: "success-1",
          },
        };
      }

      if (i == 1) {
        return {
          campaignAnswer: { ...answer.toJSON(), _id: "1" },
        };
      }
    });

    await submitSurveyAnswer(answer);

    expect(mockFetch).toHaveBeenCalledTimes(2);
    expect(mockFetch).toHaveBeenNthCalledWith(
      1,
      "POST",
      "/pictures",
      {},
      {
        picture: {
          _id: "pending-picture-1",
          name: "",
          picture: "",
        },
      },
    );
    expect(mockFetch).toHaveBeenNthCalledWith(
      2,
      "POST",
      "/campaignanswers",
      {},
      {
        campaignAnswer: {
          _id: "pending-1",
          attributes: [],
          campaign: "1",
          info: { author: "", changeIndex: 0, createdOn: "", lastChangeOn: "" },
          pictures: ["success-1"],
          sounds: [],
        },
      },
    );
  });

  it("sends the sound before the survey", async () => {
    const answer = new CampaignAnswerModel({
      _id: "pending-1",
      attributes: [],
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      campaign: "1",
    });

    const sound = new SoundModel({
      _id: "pending-sound-1",
      name: "",
      sound: "",
      visibility: {
        isPublic: false,
        team: "",
      },
      info: {
        changeIndex: 0,
        author: "",
        createdOn: "",
        lastChangeOn: "",
      },
    });

    await cache.upsert(sound._id, "Sound", sound.toJSON());
    answer.sounds = [sound];

    let i = -1;
    mockFetch.mockImplementation(() => {
      i++;
      if (i == 0) {
        return {
          sound: {
            _id: "success-1",
          },
        };
      }

      if (i == 1) {
        return {
          campaignAnswer: { ...answer.toJSON(), _id: "1" },
        };
      }
    });

    await submitSurveyAnswer(answer);

    expect(mockFetch).toHaveBeenCalledTimes(2);
    expect(mockFetch).toHaveBeenNthCalledWith(
      1,
      "POST",
      "/sounds",
      {},
      {
        sound: {
          _id: "pending-sound-1",
          name: "",
          sound: "",
          visibility: {
            isPublic: false,
            team: "",
          },
          info: {
            changeIndex: 0,
            author: "",
            createdOn: "",
            lastChangeOn: "",
          },
        },
      },
    );
    expect(mockFetch).toHaveBeenNthCalledWith(
      2,
      "POST",
      "/campaignanswers",
      {},
      {
        campaignAnswer: {
          _id: "pending-1",
          attributes: [],
          campaign: "1",
          info: { author: "", changeIndex: 0, createdOn: "", lastChangeOn: "" },
          sounds: ["success-1"],
          pictures: [],
        },
      },
    );
  });
});
