import { Authenticator } from "../giscollective/models/store";

export class StaticAuthenticator implements Authenticator {
  constructor(private readonly token: string) {}

  authenticate(
    headers: Record<string, string>,
  ): Promise<Record<string, string>> {
    headers["Authorization"] = `Bearer some-token`;

    return Promise.resolve(headers);
  }
}
