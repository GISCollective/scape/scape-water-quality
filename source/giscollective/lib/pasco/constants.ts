export const SENSOR_SERVICE_ID = 0;

export const SEND_CMD_CHAR_ID = 2;
export const RECV_CMD_CHAR_ID = 3;
export const SEND_ACK_CHAR_ID = 5;

export const GCMD_CUSTOM_CMD = 0x37;
export const GCMD_CONTROL_NODE_CMD = 0x37;
export const CTRLNODE_CMD_DETECT_DEVICES = 2;
export const GCMD_READ_ONE_SAMPLE = 0x05;
export const VALID_DATA = 0x00;
export const GCMD_XFER_BURST_RAM = 0x0e;

export const GRSP_RESULT = 0xc0;
export const GEVT_SENSOR_ID = 0x82;

export const WIRELESS_RMS_START = [0x37, 0x01, 0x00];

export const TIME_DELAY = 1.0;
