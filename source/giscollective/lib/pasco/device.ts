// @ts-nocheck

import {
  GCMD_READ_ONE_SAMPLE,
  GRSP_RESULT,
  SEND_CMD_CHAR_ID,
  SENSOR_SERVICE_ID,
  VALID_DATA,
} from "./constants";
import interfaces from "./data/interfaces.json";
import sensors from "./data/sensors.json";
import { Measurement, Sensor, SensorsDevice } from "../lib/sensor";
import { Device } from "../hooks/SensorsService";
import { EventEmitter } from "../vernier/utils";

const compatibleDeviceNames = [
  "//code.Node",
  "Accel Alt",
  "CO2",
  "Conductivity",
  "//control.Node",
  "Current",
  "Diffraction",
  "Drop Counter",
  "Force Accel",
  "Light",
  "Load Cell",
  "Mag Field",
  "Motion",
  "O2",
  "Optical DO",
  "pH",
  "Pressure",
  "Rotary Motion",
  "Smart Cart",
  "Temperature",
  "Voltage",
  "Weather",
  "Moisture",
];

function decode64(value) {
  const chr = value.charCodeAt(0);

  if (chr >= "0".charCodeAt(0) && chr <= "9".charCodeAt(0)) {
    return chr - "0".charCodeAt(0);
  }

  if (chr >= "K".charCodeAt(0) && chr <= "Z".charCodeAt(0)) {
    return chr - "A".charCodeAt(0);
  }

  if (chr >= "A".charCodeAt(0) && chr <= "J".charCodeAt(0)) {
    return chr - "A".charCodeAt(0) + 26;
  }

  if (chr >= "a".charCodeAt(0) && chr <= "z".charCodeAt(0)) {
    return chr - "a".charCodeAt(0) + 36;
  }

  if (chr == "*".charCodeAt(0)) {
    return 62;
  }

  if (chr == "#".charCodeAt(0)) {
    return 63;
  }

  return -1;
}

export function isPascoDevice(device) {
  const name = device.name ?? "";

  for (const prefix of compatibleDeviceNames) {
    if (name.indexOf(`${prefix} `) == 0 && name.indexOf("-") > 0) {
      return true;
    }
  }

  return false;
}

export function getDeviceParams(name) {
  const pieces = name.split(" ");
  const type = pieces[0];
  const serialId = pieces[1].substring(0, 7);
  const interfaceId = 1024 + decode64(pieces[1][8]);
  const interface_ = interfaces.find((a) => a.id == `${interfaceId}`);

  const sensors_ =
    interface_?.channel.map((a) => {
      const sensor = sensors.find((b) => b.id == a.SensorID);
      const sizes =
        sensor?.measurement?.map?.((a) =>
          isNaN(a.dataSize) ? 0 : parseInt(a.dataSize),
        ) ?? [];

      return {
        totalDataSize: sizes.reduce((a, b) => a + b, 0),
        ...a,
        ...sensor,
      };
    }) ?? [];

  const result = {
    type,
    serialId,
    name: `${type} ${serialId}`,
    interfaceId,
    interface: interface_,
    sensors: sensors_,
  };

  return result;
}

export function getServiceIds(characteristics) {
  const result = {};

  for (const characteristic of characteristics) {
    if (characteristic.characteristic.length < 8) {
      continue;
    }

    const ch = characteristic.characteristic[7];

    if (isNaN(ch)) {
      continue;
    }

    result[characteristic.characteristic] = ch;
  }

  return result;
}

function calc4Params(raw, x1, y1, x2, y2) {
  const b = (x1 * y2 - x2 * y1) / (x1 - x2);
  let m = 0;

  if (x1 != 0) {
    m = (y1 - b) / x1;
  }

  if (x2 != 0) {
    m = (y2 - b) / x2;
  }

  return m * raw + b;
}

function calcLinearParams(raw, m, b) {
  return m * raw + b;
}

export function decodeSensorMeasurement(message, sensor) {
  const result: Measurement[] = [];

  let rawValue: number = 0;
  const measurements = sensor.measurement
    .map((a) => ({
      ...a,
      id: parseInt(a.id),
      inputs: isNaN(a.inputs) ? -1 : parseInt(a.inputs),
    }))
    .sort((a, b) => a.inputs - b.inputs);

  for (const measurement of measurements) {
    let value: number | null =
      result.find((a) => a.id == measurement.inputs)?.value ?? null;

    if (measurement.type == "RawDigital") {
      rawValue = 0;

      let i = 0;
      for (const value of message) {
        rawValue += value * Math.pow(2, 8 * i);
        i++;
      }

      value = rawValue;
    }

    const params =
      measurement.params?.split?.(",")?.map?.((a) => parseFloat(a)) ?? [];

    if (measurement.type == "UserCal") {
      value = calc4Params(value, params[0], params[1], params[2], params[3]);
    }

    if (measurement.type == "LinearConv") {
      value = calcLinearParams(value, params[0], params[1]);
    }

    const name = `${measurement.measType}`;
    const unit = measurement.unitType == "Unitless" ? "" : measurement.unitType;
    const isReliable = !!measurement.visible;

    const isPrimary = `${sensor.tag}`
      .toLowerCase()
      .includes(name.toLowerCase());

    result.push({
      id: measurement.id,
      value,
      name,
      unit,
      isReliable,
      isPrimary,
    });
  }

  return result;
}

export class PascoSensor extends EventEmitter implements Sensor {
  device: Device;
  lastMeasurements: Measurement[] = [];
  index: number;
  sensorInfo: any;
  characteristic: string;
  service: string;
  updateCounter: number = 0;

  constructor(device: Device, sensorInfo: any, index: number | string) {
    super();

    this.device = device;
    this.sensorInfo = sensorInfo;
    this.index = parseInt(index as string);

    const prefix = `4a5c000${this.index + 1}`;
    this.characteristic = `${prefix}-000${SEND_CMD_CHAR_ID}-0000-0000-5c1e741f1c00`;
    this.service = `${prefix}-0000-0000-0000-5c1e741f1c00`;
  }

  get enabled() {
    return true;
  }

  handleResponse(notification) {
    const message: number[] = [];

    for (let i = 0; i < notification.buffer.byteLength; i++) {
      const word = notification.getUint8(i) as number;
      message.push(word);
    }

    if (
      message[0] == GRSP_RESULT &&
      message[1] == VALID_DATA &&
      message[2] == GCMD_READ_ONE_SAMPLE
    ) {
      const value = message.slice(3);
      this.lastMeasurements = decodeSensorMeasurement(value, this.sensorInfo);
      this.updateCounter++;

      this.emit("value-changed", this);
    }
  }

  async getMeasurements(): Promise<Measurement[]> {
    await this.device.rawWrite(this.service, this.characteristic, [
      GCMD_READ_ONE_SAMPLE,
      this.sensorInfo.totalDataSize,
    ]);

    const counter = this.updateCounter;
    let interval;
    let index = 0;

    return new Promise<Measurement[]>((resolve, reject) => {
      interval = setInterval(() => {
        if (this.updateCounter > counter) {
          clearInterval(interval);
          return resolve(this.lastMeasurements);
        }

        if (index > 1000) {
          clearInterval(interval);
          return reject(new Error("Pasco sensor read timeout"));
        }

        index++;
      }, 50);
    });
  }
}

export class PascoSensorsDevice implements SensorsDevice {
  sensors: Sensor[];
  device: Device;
  deviceInfo: any;
  interval: any;

  constructor(device, characteristics) {
    this.device = device;
    this.device.setCharacteristics(this.getCharacteristics(characteristics));
  }

  getCharacteristics(characteristics) {
    const pascoServices = characteristics.filter(
      (a) => a.service.indexOf("-0000-5c1e741f1c00") > 0,
    );

    const allNotifications = pascoServices.filter(
      (a) => a?.properties?.Notify == "Notify",
    );

    const write = characteristics.find((a) => a?.properties?.Write == "Write");
    const read = characteristics.find((a) => a?.properties?.Read == "Read");

    return {
      notifications: allNotifications[0],
      write,
      read,
      allNotifications,
    };
  }

  _onClosed() {
    clearInterval(this.interval);
  }

  get enabled(): boolean {
    return true;
  }

  async readAll() {
    for (const sensor of this.sensors) {
      await sensor.getMeasurements();
    }
  }

  async open(): Promise<void> {
    this.device.setup({
      onClosed: () => this._onClosed(),
      onResponse: (data, ch) => this._handleResponse(data, ch),
    });

    this.deviceInfo = getDeviceParams(this.device.name);

    await this.keepAlive();

    this.sensors = this.deviceInfo.sensors.map(
      (sensor, index) => new PascoSensor(this.device, sensor, index),
    );

    await this.readAll();

    this.interval = setInterval(async () => {
      await this.readAll();
    }, 900);
  }

  _handleResponse(notification, ch): void {
    const sensorIndex = 0; // it should be the 7th char of "ch"

    if (!this.sensors[sensorIndex]) {
      return;
    }

    this.sensors[sensorIndex].handleResponse(notification);
  }

  getMeasurement(sensorIndex) {
    const characteristic = `4a5c000${
      parseInt(sensorIndex) + 1
    }-000${SEND_CMD_CHAR_ID}-0000-0000-5c1e741f1c00`;
    const service = `4a5c000${
      parseInt(sensorIndex) + 1
    }-0000-0000-0000-5c1e741f1c00`;

    const sensor = this.deviceInfo.sensors[sensorIndex];

    return this.device.rawWrite(service, characteristic, [
      GCMD_READ_ONE_SAMPLE,
      sensor.totalDataSize,
    ]);
  }

  keepAlive() {
    const characteristic = `4a5c000${SENSOR_SERVICE_ID}-000${SEND_CMD_CHAR_ID}-0000-0000-5c1e741f1c00`;
    const service = this.device.write.service;

    return this.device.rawWrite(service, characteristic, [0x00]);
  }
}
