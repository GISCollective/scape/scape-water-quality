import {
  isPascoDevice,
  getDeviceParams,
  getServiceIds,
  decodeSensorMeasurement,
} from "./device";

const characteristics = [
  {
    service: "4a5c0000-0000-0000-0000-5c1e741f1c00",
    characteristic: "4a5c0000-0001-0000-0000-5c1e741f1c00",
    isNotifying: false,
    properties: {
      Read: "Read",
    },
  },
  {
    service: "4a5c0000-0000-0000-0000-5c1e741f1c00",
    characteristic: "4a5c0000-0002-0000-0000-5c1e741f1c00",
    isNotifying: false,
    properties: {
      WriteWithoutResponse: "WriteWithoutResponse",
    },
  },
  {
    service: "4a5c0000-0000-0000-0000-5c1e741f1c00",
    characteristic: "4a5c0000-0003-0000-0000-5c1e741f1c00",
    properties: {
      Write: "Write",
      Notify: "Notify",
      Indicate: "Indicate",
    },
    isNotifying: false,
    descriptors: [
      {
        uuid: "2902",
      },
    ],
  },
  {
    service: "4a5c0001-0000-0000-0000-5c1e741f1c00",
    characteristic: "4a5c0001-0002-0000-0000-5c1e741f1c00",
    isNotifying: false,
    properties: {
      WriteWithoutResponse: "WriteWithoutResponse",
    },
  },
  {
    service: "4a5c0001-0000-0000-0000-5c1e741f1c00",
    characteristic: "4a5c0001-0003-0000-0000-5c1e741f1c00",
    properties: {
      Write: "Write",
      Notify: "Notify",
      Indicate: "Indicate",
    },
    isNotifying: false,
    descriptors: [
      {
        uuid: "2902",
      },
    ],
  },
  {
    service: "4a5c0001-0000-0000-0000-5c1e741f1c00",
    characteristic: "4a5c0001-0004-0000-0000-5c1e741f1c00",
    properties: {
      Notify: "Notify",
      Read: "Read",
    },
    isNotifying: false,
    descriptors: [
      {
        uuid: "2902",
      },
    ],
  },
  {
    service: "4a5c0001-0000-0000-0000-5c1e741f1c00",
    characteristic: "4a5c0001-0005-0000-0000-5c1e741f1c00",
    isNotifying: false,
    properties: {
      WriteWithoutResponse: "WriteWithoutResponse",
    },
  },
];

describe("getDeviceParams", () => {
  it("can get the params from a temperature sensor name", () => {
    const result = getDeviceParams("Temperature 971-669>11");

    expect(result).toEqual({
      interfaceId: 1025,
      name: "Temperature 971-669",
      serialId: "971-669",
      type: "Temperature",
      interface: {
        AdvertisingName: "Temperature",
        DiscoveryImage: "Icon1025.png",
        SupportsLogging: "1",
        channel: [
          {
            HitRect: "10,100,10,10",
            SensorID: "2020",
            Type: "Pasport",
            id: "0",
            name: "",
          },
        ],
        id: "1025",
        name: "WirelessTemperature",
      },
      sensors: [
        {
          HitRect: "10,100,10,10",
          SensorID: "2020",
          Type: "Pasport",
          defaultRate: "2Hz",
          iconId: "136",
          id: "2020",
          maxRate: "10Hz",
          totalDataSize: 2,
          measurement: [
            {
              dataSize: "2",
              id: "0",
              internal: "1",
              nameTag: "RawTemperature",
              type: "RawDigital",
            },
            {
              accuracy: "0.5",
              calFlags: "1,1,1,1",
              id: "1",
              inputs: "2",
              maximum: "135",
              measType: "Temperature",
              minimum: "-35",
              nameTag: "Temperature",
              params: "0,0,100,100",
              precision: "1",
              shortNameTag: "Temp",
              symbolTag: "T",
              type: "UserCal",
              typicalMax: "30",
              typicalMin: "15",
              unitType: "DegC",
              visible: "1",
            },
            {
              id: "2",
              inputs: "0",
              internal: "1",
              measType: "Temperature",
              nameTag: "UncalTemperature",
              params: "0.00268127,-46.85",
              type: "LinearConv",
              unitType: "DegC",
            },
          ],
          model: "PS-3201",
          name: "",
          tag: "WirelessTemperatureSensor",
        },
      ],
    });
  });
});

describe("getServiceIds", () => {
  it("can get the service ids from a characteristics list", () => {
    const result = getServiceIds(characteristics);

    expect(result).toEqual({
      "4a5c0000-0001-0000-0000-5c1e741f1c00": "0",
      "4a5c0000-0002-0000-0000-5c1e741f1c00": "0",
      "4a5c0000-0003-0000-0000-5c1e741f1c00": "0",
      "4a5c0001-0002-0000-0000-5c1e741f1c00": "1",
      "4a5c0001-0003-0000-0000-5c1e741f1c00": "1",
      "4a5c0001-0004-0000-0000-5c1e741f1c00": "1",
      "4a5c0001-0005-0000-0000-5c1e741f1c00": "1",
    });
  });
});

describe("isPascoDevice", () => {
  it("returns true for a temperature sensor", () => {
    const result = isPascoDevice({
      advertising: {
        isConnectable: 1,
        kCBAdvDataRxPrimaryPHY: 1,
        kCBAdvDataRxSecondaryPHY: 0,
        kCBAdvDataTimestamp: 721912482.11382,
        localName: "Temperature 971-669>11",
        txPowerLevel: 0,
      },
      id: "1e30871f-fe32-918b-cff4-58d0d4e77c71",
      name: "Temperature 971-669>11",
      rssi: -67,
    });

    expect(result).toBe(true);
  });

  it("returns false for a temperature sensor without ids", () => {
    const result = isPascoDevice({
      advertising: {
        isConnectable: 1,
        kCBAdvDataRxPrimaryPHY: 1,
        kCBAdvDataRxSecondaryPHY: 0,
        kCBAdvDataTimestamp: 721912482.11382,
        localName: "Temperature",
        txPowerLevel: 0,
      },
      id: "1e30871f-fe32-918b-cff4-58d0d4e77c71",
      name: "Temperature",
      rssi: -67,
    });

    expect(result).toBe(false);
  });

  it("returns false for an unknown device", () => {
    const result = isPascoDevice({
      advertising: {
        isConnectable: 1,
        kCBAdvDataRxPrimaryPHY: 1,
        kCBAdvDataRxSecondaryPHY: 0,
        kCBAdvDataTimestamp: 721912482.11382,
        localName: "unknown",
        txPowerLevel: 0,
      },
      id: "1e30871f-fe32-918b-cff4-58d0d4e77c71",
      name: "unknown",
      rssi: -67,
    });

    expect(result).toBe(false);
  });
});

describe("decode", () => {
  let device: any;

  beforeAll(() => {
    device = getDeviceParams("Temperature 971-669>11");
  });

  it("can decode the [48, 107] message", () => {
    const result = decodeSensorMeasurement([48, 107], device.sensors[0]);

    expect(result).toEqual([
      {
        id: 0,
        isPrimary: false,
        isReliable: false,
        name: "undefined",
        unit: undefined,
        value: 27440,
      },
      {
        id: 2,
        isPrimary: true,
        isReliable: false,
        name: "Temperature",
        unit: "DegC",
        value: 26.7240488,
      },
      {
        id: 1,
        isPrimary: true,
        isReliable: true,
        name: "Temperature",
        unit: "DegC",
        value: 26.7240488,
      },
    ]);
  });
});
