import { SQLiteExecutor } from "../../../__tests__/sqlite/SQLiteExecutor";
import { SQLiteCache } from "./cache";
import { Calendar } from "./calendar";

describe("SQLiteCache", () => {
  let db: any;
  let instance: SQLiteCache;

  beforeEach(async () => {
    db = SQLiteExecutor.openDatabase(":memory:");
    instance = new SQLiteCache(db);
    await instance.setup();

    Calendar.instance = { sqlNow: `2024-08-19 12:34:36` };
  });

  afterEach(async () => {
    await db.close();
  });

  describe("upsert", () => {
    it("stores a new record", async () => {
      await instance.upsert("test123", "test", { id: 1 });

      const records = await instance.getAll();

      expect(records).toEqual([
        {
          id: 1,
          record: '{"id":1}',
          type: "test",
          remote_id: "test123",
          last_sync: "2024-08-19 12:34:36",
        },
      ]);
    });

    it("updates a record when it is called twice", async () => {
      const instance = new SQLiteCache(db);
      await instance.setup();

      await instance.upsert("test123", "test", { id: 1 });
      await instance.upsert("test123", "test", { id: 1, name: "updated" });

      const records = await instance.getAll();

      expect(records).toEqual([
        {
          id: 2,
          record: '{"id":1,"name":"updated"}',
          type: "test",
          remote_id: "test123",
          last_sync: "2024-08-19 12:34:36",
        },
      ]);
    });
  });

  describe("get", () => {
    it("returns null when the record is not stored", async () => {
      const result = await instance.get("1", "test");

      expect(result).toBe(null);
    });

    it("returns a stored record when there is one stored", async () => {
      await instance.upsert("test123", "test", { id: 1 });

      const result = await instance.get("test123", "test");

      expect(result).toBe(`{"id":1}`);
    });

    it("returns the right record when it was previously stored", async () => {
      await instance.upsert("right-record", "test", {
        id: 1,
        name: "i am the right one",
      });
      await instance.upsert("test123", "test", { id: 1 });

      const result = await instance.get("right-record", "test");

      expect(result).toBe(`{"id":1,"name":"i am the right one"}`);
    });
  });

  describe("getPending", () => {
    it("returns an empty list when the record is not stored", async () => {
      const result = await instance.getPending("test");

      expect(result).toEqual([]);
    });

    it("returns a stored record when there is one pending record stored stored", async () => {
      await instance.upsert("pending-test123", "test", { id: 1 });

      const result = await instance.getPending("test");

      expect(result).toEqual([`{"id":1}`]);
    });

    it("returns an empty list when there is one record stored stored", async () => {
      await instance.upsert("test123", "test", { id: 1 });

      const result = await instance.getPending("test");

      expect(result).toEqual([]);
    });
  });

  describe("delete", () => {
    it("does nothing when nothing is stored", async () => {
      await instance.delete("1", "test");
      const result = await instance.getAll();

      expect(result).toEqual([]);
    });

    it("deletes a stored record when it is found by id", async () => {
      await instance.upsert("test123", "test", { id: 1 });
      await instance.delete("test123", "test");

      const result = await instance.getAll();

      expect(result).toEqual([]);
    });

    it("does not delete a record when it does not match the id", async () => {
      await instance.upsert("test123", "test", { id: 1 });
      await instance.delete("other", "test");

      const result = await instance.getAll();

      expect(result).toEqual([
        {
          id: 1,
          last_sync: "2024-08-19 12:34:36",
          record: '{"id":1}',
          remote_id: "test123",
          type: "test",
        },
      ]);
    });
  });

  describe("clear", () => {
    it("does nothing when nothing is stored", async () => {
      await instance.clear();
      const result = await instance.getAll();

      expect(result).toEqual([]);
    });

    it("deletes a stored record", async () => {
      await instance.upsert("test123", "test", { id: 1 });
      await instance.clear();
      const result = await instance.getAll();

      expect(result).toEqual([]);
    });
  });
});
