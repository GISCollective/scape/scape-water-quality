import { Cache } from "../models/store";
import { DateTime } from "luxon";

export interface CachedResponseRecord {
  url: string;
  etag: string;
  time: string;
  type: string;
  response: string;
}

export interface CachedResponse {
  url: string;
  etag: string;
  time: DateTime;
  type: string;
  response: string;
}

export type ConnectionState = "online" | "slow" | "offline";

export class RequestStore {
  private static _instance: RequestStore | undefined;
  private localCache: Record<string, CachedResponse> = {};

  static set instance(value: RequestStore) {
    this._instance = value;
  }

  static get instance(): RequestStore {
    if (!this._instance) {
      throw new Error("The store instance is not set up.");
    }

    return this._instance;
  }

  constructor(public cache: Cache) {}

  async get(url: string): Promise<CachedResponse | null> {
    if (this.localCache[url]) {
      return this.localCache[url];
    }

    const cachedResult = await this.cache.get(url, "Response");

    if (!cachedResult) {
      return null;
    }

    const parsedResult = JSON.parse(cachedResult) as CachedResponseRecord;

    const time = DateTime.fromISO(parsedResult.time);

    const result = {
      ...parsedResult,
      url,
      time,
    };

    this.localCache[url] = result;

    return result;
  }

  async upsert(url: string, etag: string, type: string, response: string) {
    const record = {
      url,
      time: DateTime.now(),
      etag,
      type,
      response,
    };

    this.localCache[url] = record;
    this.cache.upsert(url, "Response", {
      ...record,
      time: record.time.toISO(),
    });
  }
}
