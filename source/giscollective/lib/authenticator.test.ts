import { StaticAuthenticator } from "./authenticator";

describe("StaticAuthenticator", () => {
  it("returns an object with the Auth header", async () => {
    const instance = new StaticAuthenticator("some-token");

    const response = await instance.authenticate({});

    expect(response).toEqual({
      Authorization: `Bearer some-token`,
    });
  });
});
