import axios, { AxiosProgressEvent, AxiosResponse } from "axios";
import { RequestStore } from "./requestStore";
import { NetInfoState } from "@react-native-community/netinfo";
import { DateTime } from "luxon";
import {
  GenericFetch,
  ErrorResponse,
  ErrorListResponse,
} from "../models/store";

const oneMinute = 60 * 1000;
const oneHour = oneMinute * 60;
const oneDay = oneHour * 24;

function blobToBase64(blob: Blob): Promise<string> {
  return new Promise((resolve, _) => {
    const reader = new FileReader();
    reader.onloadend = () => resolve(reader.result);
    reader.readAsDataURL(blob);
  });
}

export class FetchPolicy {
  static instance: FetchPolicy = new FetchPolicy();
  netinfo: NetInfoState | undefined;

  get isConnectionExpensive() {
    return this.netinfo?.details?.isConnectionExpensive ?? false;
  }

  highPriority(_diff: number): boolean {
    return true;
  }

  mediumPriority(diff: number): boolean {
    if (!this.isConnectionExpensive && diff > oneMinute * -1) {
      return false;
    }

    if (this.isConnectionExpensive && diff > oneDay * -10) {
      return false;
    }

    return true;
  }

  lowPriority(diff: number): boolean {
    if (!this.isConnectionExpensive && diff > oneDay * -365) {
      return false;
    }

    if (this.isConnectionExpensive) {
      return false;
    }

    return true;
  }

  shouldFetch(lastFetch: DateTime | undefined, priority: number): boolean {
    const diff = lastFetch?.diffNow?.()?.toMillis?.();

    if (this.netinfo?.isInternetReachable) {
      return false;
    }

    if (diff === undefined) {
      return true;
    }

    if (priority === 0) {
      return this.highPriority(diff);
    }

    if (priority === 1) {
      return this.mediumPriority(diff);
    }

    return this.lowPriority(diff);
  }
}

export type GenericJsonResponse = Record<string, any>;
export interface GenericBinaryResponse {
  data: string;
  type: string;
  status: number;
  statusText: string;
  headers: Record<string, string>;
}

export const genericFetch: GenericFetch = async <GenericJsonResponse>(
  method: string,
  baseURL: string,
  headers: Record<string, string>,
  data?: Record<string, any>,
  priority?: number,
): Promise<GenericJsonResponse | ErrorResponse | ErrorListResponse> => {
  const result = await genericBinaryFetch(
    method,
    baseURL,
    headers,
    data,
    priority ?? 1,
  );

  if (!result.type.toLowerCase().includes("json")) {
    throw new Error("Unknown response.");
  }

  return JSON.parse(atob(result.data));
};

export const genericBinaryFetch = async (
  method: string,
  baseURL: string,
  headers: Record<string, string>,
  data?: Record<string, any> | string | null,
  priority?: number,
): Promise<GenericBinaryResponse> => {
  if (typeof data == "object") {
    headers["Content-Type"] = "application/json";
  }

  const cachedResponse =
    method == "GET" ? await RequestStore.instance.get(baseURL) : null;

  const hasValidCache = cachedResponse?.response && cachedResponse?.type;

  if (
    hasValidCache &&
    FetchPolicy.instance.shouldFetch(cachedResponse?.time, priority ?? 1)
  ) {
    return {
      status: 200,
      statusText: "OK",
      headers: {},
      data: cachedResponse.response,
      type: cachedResponse.type,
    };
  }

  if (cachedResponse?.etag) {
    headers["If-None-Match"] = cachedResponse.etag;
  }

  let requestData = undefined;

  if (data && typeof data == "object") {
    requestData = JSON.stringify(data);
  }

  if (data && typeof data == "string") {
    requestData = data;
  }

  console.log(method, baseURL);
  let response: AxiosResponse | undefined;

  try {
    response = await axios.request({
      method,
      baseURL,
      headers,
      data: requestData,
      onUploadProgress: function (progressEvent: AxiosProgressEvent) {
        if (!progressEvent.total) {
          return;
        }
      },
      validateStatus: function (status) {
        return true; //(status >= 200 && status < 300) || status == 304;
      },
      timeout: 60 * 1000 * 5,
      maxBodyLength: 1024 * 16,
      responseType: "blob",
    });
  } catch (err) {
    console.error(err);
  }

  if (!response && cachedResponse) {
    return {
      status: 200,
      statusText: "OK",
      headers: {},
      data: cachedResponse.response,
      type: cachedResponse.type,
    };
  }

  if (response?.data?.errors?.[0]?.description) {
    throw new Error(response.data.errors[0].description);
  }

  if (response?.status == 304) {
    return {
      status: 200,
      statusText: "OK",
      headers: {},
      data: cachedResponse?.response ?? "",
      type: response.headers["content-type"] as string,
    };
  }

  if (!response) {
    throw new Error("Can't connect to the server.");
  }

  let responseData: string = await blobToBase64(response?.data);

  const base64Index = responseData?.indexOf("base64,");

  if (base64Index != -1) {
    responseData = responseData.substring(base64Index + 7);
  }

  await RequestStore.instance.upsert(
    baseURL,
    response?.headers["etag"] ?? "",
    response?.headers["content-type"] as string,
    responseData,
  );

  return {
    status: response.status,
    statusText: response.statusText,
    headers: response.headers as Record<string, string>,
    data: responseData,
    type: response.headers["content-type"] as string,
  };
};
