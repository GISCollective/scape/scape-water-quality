export interface Sensor {
  handleResponse(notification: any): unknown;
  lastMeasurements: Measurement[];
  getMeasurements(): Promise<Measurement[]>;
}

export interface SensorsDevice {
  open(): Promise<void>;
  sensors: Sensor[];
  enabled: boolean;
}

export interface Measurement {
  id: number | string;
  value: number | null;
  name: string;
  unit: string;
  isReliable: boolean;
  isPrimary?: boolean;
}
