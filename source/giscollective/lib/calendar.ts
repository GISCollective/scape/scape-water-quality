import { DateTime } from "luxon";

export class Calendar {
  static instance: Calendar = new Calendar();

  get sqlNow(): string {
    return DateTime.now().toFormat("yyyy-MM-dd TT");
  }
}
