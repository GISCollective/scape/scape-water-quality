import { CampaignAnswerModel } from "../models/CampaignAnswer";
import { Store } from "../models/store";

export class SurveyAnswerUpload {
  public static readonly instance: SurveyAnswerUpload =
    new SurveyAnswerUpload();
  private _promise: Promise<void> | undefined;

  upload(answers: CampaignAnswerModel[]): Promise<void> {
    if (this._promise) {
      return this._promise;
    }

    this._promise = new Promise(async (resolve, reject) => {
      const promises = answers.map((a) => a.save());

      try {
        await Promise.all(promises);
      } catch (err) {
        reject(err);
      }

      resolve();
    });

    return this._promise;
  }
}

export async function submitSurveyAnswer(answer: CampaignAnswerModel) {
  try {
    console.log("submitSurveyAnswer");

    console.log("store the last answer version to the store");
    await Store.instance.cache.upsert(
      answer._id,
      "CampaignAnswer",
      answer.toJSON(),
    );

    console.log("loading pictures");
    const pictures = (await answer.pictures) ?? [];

    console.log("loading sounds");
    const sounds = (await answer.sounds) ?? [];

    console.log("sending pictures");
    answer.pictures = await Promise.all(pictures?.map((a) => a.save()) ?? []);

    console.log("sending sounds");
    answer.sounds = await Promise.all(sounds?.map((a) => a.save()) ?? []);

    console.log("sending answer");
    await answer.save();
  } catch (err) {
    console.error(err);
  }
}
