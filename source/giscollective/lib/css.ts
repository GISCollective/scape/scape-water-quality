export function extractThemeName(line: string): string {
  const index1 = line.indexOf("data-bs-theme=") + 14;
  const index2 = line.indexOf("]");

  if (index1 == -1 || index2 == -1) {
    return "";
  }

  return line.substring(index1, index2).trim();
}

export function fillCssVariable(result: Record<string, string>, line: string) {
  const [key, value] = line.split(":").map((a) => a.trim());

  if (key.indexOf("--bs-") != 0) {
    return;
  }

  const niceKey = key.replace("--bs-", "");
  const niceValue = value.replace(";", "").trim();

  result[niceKey] = niceValue;
}

export function extractCssVariables(
  input: string,
): Record<string, Record<string, string>> {
  const result: Record<string, Record<string, string>> = {};

  const lines = input
    .replace(/{/g, "{\n")
    .replace(/}/g, "}\n")
    .replace(/;/g, ";\n")
    .split("\n");

  let lastThemeName;
  for (let line of lines) {
    if (line.indexOf("data-bs-") != -1) {
      lastThemeName = extractThemeName(line);
    }

    if (!lastThemeName) {
      continue;
    }

    if (!result[lastThemeName]) {
      result[lastThemeName] = {};
    }

    fillCssVariable(result[lastThemeName], line);
  }

  return result;
}
