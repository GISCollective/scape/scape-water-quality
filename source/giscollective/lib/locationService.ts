import Geolocation, {
  GeolocationResponse,
} from "@react-native-community/geolocation";

export class LocationService {
  static instance: LocationService = new LocationService();

  lastValue?: GeolocationResponse;

  getCurrentPosition(): Promise<GeolocationResponse> {
    return new Promise((resolve, reject) => {
      Geolocation.getCurrentPosition(
        (value) => {
          this.lastValue = value;
          resolve(value);
        },
        reject,
        {
          enableHighAccuracy: true,
          timeout: 20000,
        },
      );
    });
  }
}
