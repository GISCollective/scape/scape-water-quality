import {
  AnimatableNumericValue,
  ColorValue,
  TextStyle,
  ViewStyle,
} from "react-native";
import { SpaceModel } from "../models/Space";

function splitClass(cls: string): { key: string; value: string } {
  const pieces = cls.split("-");
  const value = pieces.pop() ?? "default";
  const key = pieces.join("-");

  return {
    key,
    value,
  };
}

const textSize = 14;
const textSizeFraction = textSize / 12;
const defaultBorderRadius = textSize * 0.5;

const spacer = 14;
const spacers = {
  0: 0,
  1: spacer * 0.25,
  2: spacer * 0.5,
  3: spacer,
  4: spacer * 1.5,
  5: spacer * 3,
};

const grays: Record<string, Record<string, string>> = {
  light: {
    white: "#ffffff",
    black: "#000000",
    gray: "#6c757d",
    dark: "#343a40",
    secondary: "#6c757d",
    "black-rgb": "0,0,0",
    "white-rgb": "255,255,255",
    "gray-dark": "#343a40",
    "gray-100": "#f8f9fa",
    "gray-200": "#e9ecef",
    "gray-300": "#dee2e6",
    "gray-400": "#ced4da",
    "gray-500": "#adb5bd",
    "gray-600": "#6c757d",
    "gray-700": "#495057",
    "gray-800": "#343a40",
    "gray-900": "#212529",
  },
  dark: {
    white: "#000000",
    black: "#ffffff",
    gray: "#ced4da",
    dark: "#e9ecef",
    secondary: "#dee2e6",
    "white-rgb": "0,0,0",
    "black-rgb": "255,255,255",
    "gray-900": "#f8f9fa",
    "gray-800": "#e9ecef",
    "gray-700": "#dee2e6",
    "gray-600": "#ced4da",
    "gray-500": "#adb5bd",
    "gray-400": "#6c757d",
    "gray-300": "#495057",
    "gray-200": "#343a40",
    "gray-100": "#212529",
  },
};

const defaultStyle: Record<string, Record<string, StyleValue>> = {
  ff: {
    default: "lato",
    monospace: "monospace",
    lato: "lato",
    poppins: "poppins",
    "league-gothic": "league-gothic",
    "zen-kaku-gothic-new": "zen-kaku-gothic-new",
    "paytone-one": "paytone-one",
    "fira-sans-condensed": "fira-sans-condensed",
    anton: "anton",
    oswald: "oswald",
    "fjalla-one": "fjalla-one",
    "crimson-pro": "crimson-pro",
    "fredoka-one": "fredoka-one",
  },
  lh: {
    default: textSize * 1.5,
    "1": textSize * 1,
    sm: textSize * 1.25,
    base: textSize * 1.5,
    lg: textSize * 2,
  },
  fw: {
    default: "normal",
    bold: "bold",
    normal: "normal",
    light: "light",
  },
  text: {
    default: "left",
    start: "left",
    center: "center",
    end: "right",
  },
  "text-decoration": {
    default: "none",
    none: "none",
    underline: "underline",
    "line-through": "line-through",
  },
  "text-size": {
    default: 12,
    "0": 0,
    "1": textSizeFraction,
    "2": textSizeFraction * 2,
    "3": textSizeFraction * 3,
    "4": textSizeFraction * 4,
    "5": textSizeFraction * 5,
    "6": textSizeFraction * 6,
    "7": textSizeFraction * 7,
    "8": textSizeFraction * 8,
    "9": textSizeFraction * 9,
    "10": textSizeFraction * 10,
    "11": textSizeFraction * 11,
    "12": textSize,
    "13": textSizeFraction * 13,
    "14": textSizeFraction * 14,
    "15": textSizeFraction * 15,
    "16": textSizeFraction * 16,
    "17": textSizeFraction * 17,
    "18": textSizeFraction * 18,
    "20": textSizeFraction * 20,
    "21": textSizeFraction * 21,
    "22": textSizeFraction * 22,
    "23": textSizeFraction * 23,
    "24": textSizeFraction * 24,
    "25": textSizeFraction * 25,
    "26": textSizeFraction * 26,
    "27": textSizeFraction * 27,
    "28": textSizeFraction * 28,
    "29": textSizeFraction * 29,
  },
  color: {
    blue: "#3961d0",
    indigo: "#2a1a68",
    purple: "#663eff",
    pink: "#ff3e76",
    red: "#e50620",
    orange: "#e55b06",
    yellow: "#e5ca06",
    green: "#107353",
    teal: "#5d7388",
    cyan: "#159ea7",
    black: "0,0,0",
    white: "255,255,255",
    gray: "#6c757d",
    "gray-dark": "#343a40",
    "gray-100": "#f8f9fa",
    "gray-200": "#e9ecef",
    "gray-300": "#dee2e6",
    "gray-400": "#ced4da",
    "gray-500": "#adb5bd",
    "gray-600": "#6c757d",
    "gray-700": "#495057",
    "gray-800": "#343a40",
    "gray-900": "#212529",
    primary: "#3961d0",
    secondary: "#6c757d",
    success: "#107353",
    info: "#159ea7",
    warning: "#e5ca06",
    danger: "#e50620",
    light: "#f8f9fa",
    dark: "#212529",
    "primary-rgb": "57,97,208",
    "secondary-rgb": "108,117,125",
    "success-rgb": "16,115,83",
    "info-rgb": "21,158,167",
    "warning-rgb": "229,202,6",
    "danger-rgb": "229,6,32",
    "light-rgb": "248,249,250",
    "dark-rgb": "33,37,41",
    "black-rgb": "0,0,0",
    "white-rgb": "255,255,255",
    "blue-rgb": "57,97,208",
    "indigo-rgb": "42,26,104",
    "purple-rgb": "102,62,255",
    "pink-rgb": "255,62,118",
    "red-rgb": "229,6,32",
    "orange-rgb": "229,91,6",
    "yellow-rgb": "229,202,6",
    "green-rgb": "16,115,83",
    "teal-rgb": "93,115,136",
    "cyan-rgb": "21,158,167",
    "gray-rgb": "173,181,189",
    "blue-100-rgb": "215,223,246",
    "blue-200-rgb": "176,192,236",
    "blue-300-rgb": "136,160,227",
    "blue-400-rgb": "97,129,217",
    "blue-500-rgb": "57,97,208",
    "blue-600-rgb": "46,78,166",
    "blue-700-rgb": "34,58,125",
    "blue-800-rgb": "23,39,83",
    "blue-900-rgb": "11,19,42",
    "indigo-100-rgb": "212,209,225",
    "indigo-200-rgb": "170,163,195",
    "indigo-300-rgb": "127,118,164",
    "indigo-400-rgb": "85,72,134",
    "indigo-500-rgb": "42,26,104",
    "indigo-600-rgb": "34,21,83",
    "indigo-700-rgb": "25,16,62",
    "indigo-800-rgb": "17,10,42",
    "indigo-900-rgb": "8,5,21",
    "purple-100-rgb": "224,216,255",
    "purple-200-rgb": "194,178,255",
    "purple-300-rgb": "163,139,255",
    "purple-400-rgb": "133,101,255",
    "purple-500-rgb": "102,62,255",
    "purple-600-rgb": "82,50,204",
    "purple-700-rgb": "61,37,153",
    "purple-800-rgb": "41,25,102",
    "purple-900-rgb": "20,12,51",
    "pink-100-rgb": "255,216,228",
    "pink-200-rgb": "255,178,200",
    "pink-300-rgb": "255,139,173",
    "pink-400-rgb": "255,101,145",
    "pink-500-rgb": "255,62,118",
    "pink-600-rgb": "204,50,94",
    "pink-700-rgb": "153,37,71",
    "pink-800-rgb": "102,25,47",
    "pink-900-rgb": "51,12,24",
    "red-100-rgb": "250,205,210",
    "red-200-rgb": "245,155,166",
    "red-300-rgb": "239,106,121",
    "red-400-rgb": "234,56,77",
    "red-500-rgb": "229,6,32",
    "red-600-rgb": "183,5,26",
    "red-700-rgb": "137,4,19",
    "red-800-rgb": "92,2,13",
    "red-900-rgb": "46,1,6",
    "orange-100-rgb": "250,222,205",
    "orange-200-rgb": "245,189,155",
    "orange-300-rgb": "239,157,106",
    "orange-400-rgb": "234,124,56",
    "orange-500-rgb": "229,91,6",
    "orange-600-rgb": "183,73,5",
    "orange-700-rgb": "137,55,4",
    "orange-800-rgb": "92,36,2",
    "orange-900-rgb": "46,18,1",
    "yellow-100-rgb": "250,244,205",
    "yellow-200-rgb": "245,234,155",
    "yellow-300-rgb": "239,223,106",
    "yellow-400-rgb": "234,213,56",
    "yellow-500-rgb": "229,202,6",
    "yellow-600-rgb": "183,162,5",
    "yellow-700-rgb": "137,121,4",
    "yellow-800-rgb": "92,81,2",
    "yellow-900-rgb": "46,40,1",
    "green-100-rgb": "207,227,221",
    "green-200-rgb": "159,199,186",
    "green-300-rgb": "112,171,152",
    "green-400-rgb": "64,143,117",
    "green-500-rgb": "16,115,83",
    "green-600-rgb": "13,92,66",
    "green-700-rgb": "10,69,50",
    "green-800-rgb": "6,46,33",
    "green-900-rgb": "3,23,17",
    "teal-100-rgb": "223,227,231",
    "teal-200-rgb": "190,199,207",
    "teal-300-rgb": "158,171,184",
    "teal-400-rgb": "125,143,160",
    "teal-500-rgb": "93,115,136",
    "teal-600-rgb": "74,92,109",
    "teal-700-rgb": "56,69,82",
    "teal-800-rgb": "37,46,54",
    "teal-900-rgb": "19,23,27",
    "cyan-100-rgb": "208,236,237",
    "cyan-200-rgb": "161,216,220",
    "cyan-300-rgb": "115,197,202",
    "cyan-400-rgb": "68,177,185",
    "cyan-500-rgb": "21,158,167",
    "cyan-600-rgb": "17,126,134",
    "cyan-700-rgb": "13,95,100",
    "cyan-800-rgb": "8,63,67",
    "cyan-900-rgb": "4,32,33",
    "gray-100-rgb": "248,249,250",
    "gray-200-rgb": "233,236,239",
    "gray-300-rgb": "222,226,230",
    "gray-400-rgb": "206,212,218",
    "gray-500-rgb": "173,181,189",
    "gray-600-rgb": "108,117,125",
    "gray-700-rgb": "73,80,87",
    "gray-800-rgb": "52,58,64",
    "gray-900-rgb": "33,37,41",
    "primary-text-emphasis": "#172753",
    "secondary-text-emphasis": "#2b2f32",
    "success-text-emphasis": "#062e21",
    "info-text-emphasis": "#083f43",
    "warning-text-emphasis": "#5c5102",
    "danger-text-emphasis": "#5c020d",
    "light-text-emphasis": "#495057",
    "dark-text-emphasis": "#495057",
    "primary-bg-subtle": "#d7dff6",
    "secondary-bg-subtle": "#e2e3e5",
    "success-bg-subtle": "#cfe3dd",
    "info-bg-subtle": "#d0eced",
    "warning-bg-subtle": "#faf4cd",
    "danger-bg-subtle": "#facdd2",
    "light-bg-subtle": "#fcfcfd",
    "dark-bg-subtle": "#ced4da",
    "primary-border-subtle": "#b0c0ec",
    "secondary-border-subtle": "#c4c8cb",
    "success-border-subtle": "#9fc7ba",
    "info-border-subtle": "#a1d8dc",
    "warning-border-subtle": "#f5ea9b",
    "danger-border-subtle": "#f59ba6",
    "light-border-subtle": "#e9ecef",
    "dark-border-subtle": "#adb5bd",
  },
  fontStyles: {
    h6: ["ff-poppins", "text-size-12", "fw-bold"],
    h2: ["ff-crimson-pro", "fw-bold", "text-size-20"],
    h5: ["ff-poppins", "text-size-14", "fw-bold"],
    paragraph: ["ff-poppins", "fw-normal", "text-size-12"],
    h1: ["ff-crimson-pro", "fw-bold", "text-size-23"],
    h3: ["ff-crimson-pro", "fw-bold", "text-size-18"],
    h4: ["ff-crimson-pro", "fw-bold", "text-size-16"],
  },
  "btn-link": {
    backgroundColor: "transparent",
    borderColor: "transparent",
    borderWidth: 0,
  },
  "btn-link:active": {
    backgroundColor: "blue-600-rgb",
    borderColor: "primary-rgb",
  },
  "text-btn-link": {
    fontFamily: "lato",
    fontSize: textSize,
    color: "primary-rgb",
    fontWeight: "bold",
  },
  "text-btn-link:active": {
    color: "white",
  },

  "btn-primary": {
    backgroundColor: "primary-rgb",
    borderColor: "primary-rgb",
  },
  "btn-primary:active": {
    backgroundColor: "blue-600-rgb",
    borderColor: "primary-rgb",
  },
  "text-btn-primary": {
    fontFamily: "lato",
    fontSize: textSize,
    color: "white",
  },
  "text-btn-primary:active": {
    color: "white",
  },

  "btn-success": {
    backgroundColor: "success-rgb",
    borderColor: "success-rgb",
  },
  "btn-success:active": {
    backgroundColor: "green-600-rgb",
    borderColor: "success-rgb",
  },
  "text-btn-success": {
    fontFamily: "lato",
    fontSize: textSize,
    color: "white",
  },
  "text-btn-success:active": {
    color: "white",
  },

  "btn-secondary": {
    backgroundColor: "secondary",
    borderColor: "secondary",
  },
  "btn-secondary:active": {
    backgroundColor: "gray-700-rgb",
    borderColor: "secondary",
  },
  "text-btn-secondary": {
    fontFamily: "lato",
    fontSize: textSize,
    color: "white",
  },
  "text-btn-secondary:active": {},

  "btn-danger": {
    backgroundColor: "danger-rgb",
    borderColor: "danger-rgb",
  },
  "btn-danger:active": {
    backgroundColor: "red-600-rgb",
    borderColor: "danger-rgb",
  },
  "text-btn-danger": {
    fontFamily: "lato",
    fontSize: textSize,
    color: "white",
  },
  "text-btn-danger:active": {},

  "btn-warning": {
    backgroundColor: "warning-rgb",
    borderColor: "warning-rgb",
  },
  "btn-warning:active": {
    backgroundColor: "yellow-600-rgb",
    borderColor: "warning-rgb",
  },
  "text-btn-warning": {
    fontFamily: "lato",
    fontSize: textSize,
    color: "black",
  },
  "text-btn-warning:active": {},

  "btn-info": {
    backgroundColor: "info-rgb",
    borderColor: "info-rgb",
  },
  "btn-info:active": {
    backgroundColor: "cyan-600-rgb",
    borderColor: "info-rgb",
  },
  "text-btn-info": {
    fontFamily: "lato",
    fontSize: textSize,
    color: "light",
  },
  "text-btn-info:active": {},

  "btn-light": {
    backgroundColor: "light-rgb",
    borderColor: "light-rgb",
  },
  "btn-light:active": {
    backgroundColor: "gray-600-rgb",
    borderColor: "light-rgb",
  },
  "text-btn-light": {
    fontFamily: "lato",
    fontSize: textSize,
    color: "black",
  },
  "text-btn-light:active": {},

  "btn-dark": {
    backgroundColor: "dark-rgb",
    borderColor: "dark-rgb",
  },
  "btn-dark:active": {
    backgroundColor: "gray-600-rgb",
    borderColor: "dark-rgb",
  },
  "text-btn-dark": {
    fontFamily: "lato",
    fontSize: textSize,
    color: "white",
  },
  "text-btn-dark:active": {},

  "btn-outline-primary": {
    backgroundColor: "white",
    borderColor: "primary-rgb",
  },
  "btn-outline-primary:active": {
    backgroundColor: "blue-100-rgb",
    borderColor: "primary-rgb",
  },
  "text-btn-outline-primary": {
    fontFamily: "lato",
    fontSize: textSize,
    color: "primary",
  },
  "text-btn-outline-primary:active": {},

  "btn-outline-secondary": {
    backgroundColor: "white",
    borderColor: "secondary",
  },
  "btn-outline-secondary:active": {
    backgroundColor: "gray-200-rgb",
    borderColor: "secondary",
  },
  "text-btn-outline-secondary": {
    fontFamily: "lato",
    fontSize: textSize,
    color: "secondary",
  },
  "text-btn-outline-secondary:active": {},

  "btn-outline-danger": {
    backgroundColor: "white",
    borderColor: "danger-rgb",
  },
  "btn-outline-danger:active": {
    backgroundColor: "yellow-100-rgb",
    borderColor: "danger-rgb",
  },
  "text-btn-outline-danger": {
    fontFamily: "lato",
    fontSize: textSize,
    color: "danger",
  },
  "text-btn-outline-danger:active": {},

  "btn-outline-warning": {
    backgroundColor: "white",
    borderColor: "warning-rgb",
  },
  "btn-outline-warning:active": {
    backgroundColor: "gray-200-rgb",
    borderColor: "warning-rgb",
  },
  "text-btn-outline-warning": {
    fontFamily: "lato",
    fontSize: textSize,
    color: "warning",
  },
  "text-btn-outline-warning:active": {},

  "btn-outline-info": {
    backgroundColor: "white",
    borderColor: "info-rgb",
  },
  "btn-outline-info:active": {
    backgroundColor: "info-200-rgb",
    borderColor: "info-rgb",
  },
  "text-btn-outline-info": {
    fontFamily: "lato",
    fontSize: textSize,
    color: "info",
  },
  "text-btn-outline-info:active": {},

  "btn-outline-light": {
    backgroundColor: "white",
    borderColor: "light-rgb",
  },
  "btn-outline-light:active": {
    backgroundColor: "gray-200-rgb",
    borderColor: "light-rgb",
  },
  "text-btn-outline-light": {
    fontFamily: "lato",
    fontSize: textSize,
    color: "light",
  },
  "text-btn-outline-light:active": {},

  "btn-outline-dark": {
    backgroundColor: "white",
    borderColor: "dark-rgb",
  },
  "btn-outline-dark:active": {
    backgroundColor: "gray-200-rgb",
    borderColor: "dark-rgb",
  },
  "text-btn-outline-dark": {
    fontFamily: "lato",
    fontSize: textSize,
    color: "light",
  },
  "text-btn-outline-dark:active": {},

  formControl: {
    fontFamily: "lato",
    fontSize: textSize,
    color: "dark",
    backgroundColor: "white",
    borderColor: "primary-rgb",
    borderWidth: 1,
    borderRadius: defaultBorderRadius,
    paddingHorizontal: spacer * 0.75,
    paddingVertical: spacer * 0.6,
  },
  "formControl:focus": {
    borderColor: "blue-700-rgb",
    shadowColor: "blue-300-rgb",
    shadowRadius: 5,
    elevation: 5,
  },
  "formControl:disabled": {
    borderColor: "blue-700-rgb",
    backgroundColor: "gray-300",
    color: "gray-600",
  },

  pt: spacers,
  pb: spacers,
  ps: spacers,
  pe: spacers,
  mt: spacers,
  mb: spacers,
  ms: spacers,
  me: spacers,
};

type Key = keyof TextStyle;
type StyleValue = TextStyle[keyof TextStyle] | ColorValue;

export class Theme {
  static instance: Theme = new Theme();
  private isStrict = false;

  readonly textSize = textSize;
  readonly spacers = spacers;

  private themeName = "light";
  private colors: Record<string, Record<string, string>> = {};

  readonly props: Record<string, Key> = {
    ff: "fontFamily",
    lh: "lineHeight",
    fw: "fontWeight",
    text: "textAlign",
    "text-decoration": "textDecorationLine",
    "text-size": "fontSize",
    pt: "paddingTop",
    pb: "paddingBottom",
    ps: "paddingLeft",
    pe: "paddingRight",
    mt: "marginTop",
    mb: "marginBottom",
    ms: "marginLeft",
    me: "marginRight",
  };

  style: Record<string, Record<string, StyleValue>> = JSON.parse(
    JSON.stringify(defaultStyle),
  );

  set space(space: SpaceModel) {
    for (const key in space.colorPalette) {
      defaultStyle.color[key] = space.colorPalette[key];
    }

    for (const key in space.fontStyles) {
      if (space.fontStyles[key]?.length) {
        defaultStyle.fontStyles[key] = space.fontStyles[key];
      }
    }
  }

  set name(newValue: string) {
    this.themeName = newValue;
    this.style.color = this.colors[newValue] ?? defaultStyle.color;
  }

  get name(): string {
    return this.themeName;
  }

  get iconTint() {
    if (this.themeName == "light") {
      return {};
    }

    return {
      tintColor: this.toColor("black"),
    };
  }

  loadColors(name: string, obj: Record<string, string>) {
    this.colors[name] = {
      ...(defaultStyle.color as Record<string, string>),
    };

    for (const key in obj) {
      this.colors[name][key] = obj[key];
    }

    for (const key in grays[name] ?? {}) {
      this.colors[name][key] = grays[name][key];
    }

    if (name == this.themeName) {
      this.style.color = this.colors[name];
    }
  }

  resolveClasses(classes: string[] = []) {
    const result = {};

    for (const cls of classes) {
      const { key, value } = splitClass(cls);

      const prop = this.props?.[key];
      if (!prop) {
        if (this.isStrict) console.warn("Unknown style property: ", key);
        continue;
      }

      const newVal = this.style[key]?.[value];

      if (!newVal && this.isStrict) {
        console.warn("Unknown style value: ", key, value);
      }

      // @ts-ignore
      result[prop] = newVal ?? "default";
    }

    return result;
  }

  toFontStyle(
    name: string,
    classes: string[] = [],
    style: TextStyle = {},
  ): TextStyle {
    return this.toTextStyle(
      [...(this.style.fontStyles?.[name] ?? []), ...classes],
      style,
    );
  }

  toTextStyle(classes: string[] = [], style: TextStyle = {}): TextStyle {
    const color: string = style.color ? `${String(style.color)}` : "gray-900";

    return {
      ...this.resolveClasses(classes),
      ...style,
      color: this.toColor(color) ?? color,
    };
  }

  toButtonStyle(
    classes: string[] = [],
    state: "" | "active" | "disabled" | "waiting",
    style: ViewStyle = {},
  ): ViewStyle {
    let backgroundColor: ColorValue | undefined;
    let opacity: AnimatableNumericValue = 1;

    if (state == "active") {
      backgroundColor = this.toColor("secondary");
    }

    if (state == "disabled" || state == "waiting") {
      opacity = 0.7;
    }

    let result = {
      paddingHorizontal: spacers[3],
      paddingVertical: spacers[2],
      borderWidth: 1,
      borderColor: this.toColor("secondary"),
      borderRadius: defaultBorderRadius,
      backgroundColor,
      opacity,
      ...style,
    };

    for (const cls of classes) {
      if (cls.indexOf("btn") != 0) {
        continue;
      }

      let newProps = this.style[cls];
      if (state) {
        newProps = {
          ...newProps,
          ...this.style[`${cls}:${state}`],
        };
      }

      if (!newProps && this.isStrict) {
        console.warn("Unknown button style: ", cls);
      }

      result = {
        ...result,
        ...newProps,
        backgroundColor: this.toColor(String(newProps?.backgroundColor ?? "")),
        borderColor: this.toColor(String(newProps?.borderColor ?? "")),
      };
    }

    result = {
      ...this.resolveClasses(classes),
      ...result,
    };

    return result;
  }

  toButtonTextStyle(
    classes: string[] = [],
    state: "" | "active" | "disabled" | "waiting",
    style: TextStyle = {},
  ): TextStyle {
    let result: TextStyle = {
      textAlign: "center",
      ...style,
    };

    for (const cls of classes) {
      if (cls.indexOf("btn") != 0) {
        continue;
      }

      let styleName = "text-" + cls;
      let newProps = this.style[styleName];

      if (state) {
        newProps = {
          ...newProps,
          ...this.style[`${styleName}:${state}`],
        };
      }

      if (!newProps && this.isStrict) {
        console.warn("Unknown button style: ", cls);
      }

      result = {
        ...result,
        ...newProps,
        textAlign: "center",
        color: this.toColor(String(newProps?.color ?? "")),
      };
    }

    return result;
  }

  toFormControlStyle(
    classes: string[] = [],
    state: "" | "focus" | "disabled",
    style: TextStyle = {},
  ): TextStyle {
    let result: TextStyle = {
      fontSize: textSize,
      ...this.style.formControl,
      ...style,
      ...this.resolveClasses(classes),
    };

    if (state == "focus") {
      result = {
        ...result,
        ...this.style["formControl:focus"],
      };
    }

    if (state == "disabled") {
      result = {
        ...result,
        ...this.style["formControl:disabled"],
      };
    }

    const backgroundColor =
      (result.backgroundColor as string | undefined) || "white";

    result.color = this.toColor(String(result.color ?? ""));
    result.backgroundColor = this.toColor(backgroundColor);
    result.borderColor = this.toColor(String(result.borderColor ?? "blue-300"));
    result.shadowColor = this.toColor(String(result.shadowColor ?? ""));

    return result;
  }

  toStyle(classes: string[] = [], style: ViewStyle = {}) {
    let result = {
      ...style,
      ...this.resolveClasses(classes),
    };

    if (result.backgroundColor) {
      result.backgroundColor = this.toColor(
        String(result.backgroundColor ?? ""),
      );
    }

    if (result.borderColor) {
      result.borderColor = this.toColor(String(result.borderColor ?? ""));
    }

    if (result.shadowColor) {
      result.shadowColor = this.toColor(String(result.shadowColor ?? ""));
    }

    return result;
  }

  toColor(colorName?: string): ColorValue | undefined {
    if (!colorName) {
      return undefined;
    }

    if (!this.style["color"]?.[colorName]) {
      return undefined;
    }

    const value = String(this.style["color"][colorName]);

    return value.indexOf(",") != -1 ? `rgb(${value})` : value;
  }
}
