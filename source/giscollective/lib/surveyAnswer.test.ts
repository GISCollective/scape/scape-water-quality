import { CampaignModel } from "../models/Campaign";
import { CampaignAnswerModel } from "../models/CampaignAnswer";
import { IconModel } from "../models/Icon";
import { ModelInfoJson } from "../models/ModelInfo";
import { PictureModel } from "../models/Picture";
import { QuestionModel } from "../models/Question";
import { SoundModel, SoundJson } from "../models/Sound";
import { Store } from "../models/store";
import { StaticAuthenticator } from "./authenticator";
import { MemoryCache } from "./cache";
import { PositionDetails } from "./positionDetails";
import {
  extractGenericAnswers,
  fillAnswer,
  needsUnitsQuestion,
} from "./surveyAnswer";

describe("fillAnswer", () => {
  let answer: CampaignAnswerModel;
  let cache: MemoryCache;

  beforeAll(() => {
    cache = new MemoryCache();
    Store.instance = new Store(
      "",
      jest.fn(),
      cache,
      new StaticAuthenticator(""),
    );

    cache.upsert("test", "Picture", {
      _id: "test",
      name: "name",
      picture: "test",
    });
  });

  beforeEach(() => {
    answer = new CampaignAnswerModel({
      _id: "",
      attributes: {},
      campaign: "1",
      info: {} as ModelInfoJson,
    });
  });

  it("sets the pictures when they are set in the values", async () => {
    let values = {
      pictures: [
        new PictureModel({ _id: "test", name: "name", picture: "test" }),
      ],
    };

    await fillAnswer(answer, values);

    const pictures = await answer.pictures;

    expect(pictures).toHaveLength(1);
    expect((pictures![0] as PictureModel).toJSON()).toEqual({
      _id: "test",
      name: "name",
      picture: "test",
    });
  });

  it("sets the sounds when they are set in the values", async () => {
    let sound = new SoundModel({
      _id: "test",
      name: "name",
      sound: "test",
    } as SoundJson);
    cache.upsert("test", "Sound", sound.toJSON());
    let values = {
      sounds: [sound],
    };

    await fillAnswer(answer, values);

    const sounds = await answer.sounds;

    expect(sounds).toHaveLength(1);
    expect((sounds![0] as SoundModel).toJSON()).toEqual({
      _id: "test",
      name: "name",
      sound: "test",
    });
  });

  it("sets the icons when they are set in the values", async () => {
    let icon = new IconModel({
      _id: "1",
      attributes: [],
      iconSet: "",
      image: {},
      name: "",
    });
    cache.upsert("1", "Icon", icon.toJSON());

    let values = {
      icons: [icon],
    };

    await fillAnswer(answer, values);

    const icons = await answer.icons;

    expect(icons).toHaveLength(1);
    expect((icons![0] as IconModel).toJSON()).toEqual({
      _id: "1",
      attributes: [],
      iconSet: "",
      image: {},
      name: "",
    });
  });

  it("sets the survey icons before the icon values", async () => {
    let icon1 = new IconModel({
      _id: "1",
      attributes: [],
      iconSet: "",
      image: {},
      name: "",
    });
    let icon2 = new IconModel({
      _id: "2",
      attributes: [],
      iconSet: "",
      image: {},
      name: "",
    });

    cache.upsert("1", "Icon", icon1.toJSON());
    cache.upsert("2", "Icon", icon2.toJSON());

    let values = {
      icons: [icon1],
    };

    await fillAnswer(answer, values, [icon2]);

    const icons = await answer.icons;

    expect(icons!.map((a) => a._id)).toEqual(["2", "1"]);
  });

  it("sets the name when it is set", async () => {
    let values = {
      name: "some name",
    };

    await fillAnswer(answer, values);

    expect(answer.attributes.about.name).toEqual("some name");
  });

  it("sets the description when it is set", async () => {
    let values = {
      description: "some description",
    };

    await fillAnswer(answer, values);

    expect(answer.attributes.about.description).toEqual("some description");
  });

  it("sets the positionDetails when they are set", async () => {
    let values = {
      positionDetails: new PositionDetails({ type: "test" }),
    };

    await fillAnswer(answer, values);

    expect(answer.attributes["position details"]).toEqual({
      accuracy: 1000,
      address: null,
      altitude: 0,
      altitudeAccuracy: 1000,
      capturedUsingGPS: false,
      id: null,
      searchTerm: null,
      type: "test",
    });
  });

  it("sets the position when it is set", async () => {
    let values = {
      position: {
        type: "Point",
        coordinates: [1, 2],
      },
    };

    await fillAnswer(answer, values);

    expect(answer.position).toEqual({
      type: "Point",
      coordinates: [1, 2],
    });
  });

  it("sets other attributes when set", async () => {
    let values = {
      a: 12,
      b: 13,
    };

    await fillAnswer(answer, values);

    expect(answer.attributes["other"]).toEqual({
      a: 12,
      b: 13,
    });
  });
});

describe("extractGenericAnswers", () => {
  let cache: MemoryCache;

  beforeAll(() => {
    cache = new MemoryCache();
    Store.instance = new Store(
      "",
      jest.fn(),
      cache,
      new StaticAuthenticator(""),
    );

    cache.upsert("6107b3b0ce8725010021515c", "Icon", {
      _id: "6107b3b0ce8725010021515c",
    });

    cache.upsert("pending-66454a87-698f-4331-ac31-a1cb4b5d239c", "Picture", {
      _id: "pending-66454a87-698f-4331-ac31-a1cb4b5d239c",
    });

    cache.upsert("pending-04fff71e-5f84-4360-9ea1-3f20ebae4ed8", "Sound", {
      _id: "pending-04fff71e-5f84-4360-9ea1-3f20ebae4ed8",
    });
  });

  it("returns an empty object for a new answer", async () => {
    await cache.upsert("campaignId", "Campaign", {
      _id: "campaignId",
      name: "",
      article: { blocks: [] },
      visibility: { isPublic: false, team: "" },
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      cover: "",
      icons: [],
    });

    const answer = new CampaignAnswerModel({
      _id: "test",
      attributes: {},
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      campaign: "campaignId",
    });

    const result = await extractGenericAnswers(answer);

    expect(result).toEqual({
      icons: [],
      pictures: [],
      sounds: [],
    });
  });

  it("fills in the standard questions when they are all set", async () => {
    await cache.upsert("6033fc2509977601001a959e", "Campaign", {
      _id: "6033fc2509977601001a959e",
      name: "",
      article: { blocks: [] },
      visibility: { isPublic: false, team: "" },
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      cover: "",
      icons: [],
    });

    const answer = new CampaignAnswerModel({
      _id: "pending-f644f3e4-6f6e-41bc-85d3-d1b295a10229",
      attributes: {
        about: { name: "Test", description: "Dg" },
        "position details": { a: "b" },
        other: {},
        Sound: { name: "Test", description: "Dg", Source: "birds" },
      },
      campaign: "6033fc2509977601001a959e",
      info: {
        author: "",
        changeIndex: 0,
        createdOn: "",
        lastChangeOn: "",
      },
      icons: ["6107b3b0ce8725010021515c"],
      pictures: ["pending-66454a87-698f-4331-ac31-a1cb4b5d239c"],
      sounds: ["pending-04fff71e-5f84-4360-9ea1-3f20ebae4ed8"],
      position: { type: "Point", coordinates: [-122.406417, 37.785834] },
    });

    const result = await extractGenericAnswers(answer);

    expect(result).toEqual({
      name: "Test",
      description: "Dg",
      position: { type: "Point", coordinates: [-122.406417, 37.785834] },
      "position details": { a: "b" },
      icons: [expect.objectContaining({ _id: "6107b3b0ce8725010021515c" })],
      pictures: [
        expect.objectContaining({
          _id: "pending-66454a87-698f-4331-ac31-a1cb4b5d239c",
        }),
      ],
      sounds: [
        expect.objectContaining({
          _id: "pending-04fff71e-5f84-4360-9ea1-3f20ebae4ed8",
        }),
      ],
    });
  });

  it("fills in the other questions when they exist", async () => {
    const answer = new CampaignAnswerModel({
      _id: "pending-f644f3e4-6f6e-41bc-85d3-d1b295a10229",
      attributes: {
        about: { name: "Test", description: "Dg" },
        "position details": { a: "b" },
        other: { key: "value" },
      },
      campaign: "6033fc2509977601001a959f",
      info: {
        author: "",
        changeIndex: 0,
        createdOn: "",
        lastChangeOn: "",
      },
      icons: [],
      pictures: [],
      sounds: [],
      position: { type: "Point", coordinates: [-122.406417, 37.785834] },
    });

    await cache.upsert("6033fc2509977601001a959f", "Campaign", {
      _id: "6033fc2509977601001a959f",
      name: "",
      article: { blocks: [] },
      visibility: { isPublic: false, team: "" },
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      cover: "",
      icons: [],
      contributorQuestions: [
        {
          name: "key",
        },
      ],
    });

    const result = await extractGenericAnswers(answer);

    expect(result).toEqual({
      name: "Test",
      description: "Dg",
      position: { type: "Point", coordinates: [-122.406417, 37.785834] },
      "position details": { a: "b" },
      icons: [],
      pictures: [],
      sounds: [],
      key: "value",
    });
  });
});

describe("needsUnitsQuestion", () => {
  let cache: MemoryCache;

  beforeAll(() => {
    cache = new MemoryCache();
    Store.instance = new Store(
      "",
      jest.fn(),
      cache,
      new StaticAuthenticator(""),
    );

    cache.upsert("1", "Icon", {
      _id: "1",
      attributes: [
        {
          name: "key",
          type: "decimal",
          options: {
            measurement: "Temperature",
          },
        },
      ],
    });
  });

  it("returns false when there is no question", async () => {
    const result = await needsUnitsQuestion(
      new CampaignModel({
        _id: "",
        name: "",
        article: { blocks: [] },
        visibility: { isPublic: false, team: "" },
        info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
        cover: "",
        icons: [],
      }),
    );

    expect(result).toEqual(false);
  });

  it("returns true when there is temperature generic question", async () => {
    const campaign = new CampaignModel({
      _id: "",
      name: "",
      article: { blocks: [] },
      visibility: { isPublic: false, team: "" },
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      cover: "",
      icons: [],
    });

    campaign.questions = [
      new QuestionModel({
        question: "",
        help: "",
        type: "decimal",
        name: "",
        isRequired: false,
        isVisible: true,
        options: {
          measurement: "Temperature",
        },
      }),
    ];
    const result = await needsUnitsQuestion(campaign);

    expect(result).toEqual(true);
  });

  it("returns false when there is a hidden temperature generic question", async () => {
    const campaign = new CampaignModel({
      _id: "",
      name: "",
      article: { blocks: [] },
      visibility: { isPublic: false, team: "" },
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      cover: "",
      icons: [],
    });

    campaign.questions = [
      new QuestionModel({
        question: "",
        help: "",
        type: "decimal",
        name: "",
        isRequired: false,
        isVisible: false,
        options: {
          measurement: "Temperature",
        },
      }),
    ];
    const result = await needsUnitsQuestion(campaign);

    expect(result).toEqual(false);
  });

  it("returns true when there is an icon with a temperature question", async () => {
    const campaign = new CampaignModel({
      _id: "",
      name: "",
      article: { blocks: [] },
      visibility: { isPublic: false, team: "" },
      info: { changeIndex: 0, author: "", createdOn: "", lastChangeOn: "" },
      cover: "",
      icons: ["1"],
    });

    campaign.questions = [
      new QuestionModel({
        question: "",
        help: "",
        type: "decimal",
        name: "",
        isRequired: false,
        isVisible: false,
        options: {
          measurement: "Temperature",
        },
      }),
    ];
    const result = await needsUnitsQuestion(campaign);

    expect(result).toEqual(true);
  });
});
