import { useState } from "react";
import { GeolocationResponse } from "@react-native-community/geolocation";
import { useInterval } from "usehooks-ts";
import { LocationService } from "../lib/locationService";

export default function useCurrentLocation(): GeolocationResponse {
  const [position, setPosition] = useState<GeolocationResponse>({
    coords: {
      latitude: LocationService.instance.lastValue?.coords.latitude ?? 0,
      longitude: LocationService.instance.lastValue?.coords.longitude ?? 0,
      altitude: LocationService.instance.lastValue?.coords.altitude ?? null,
      accuracy: LocationService.instance.lastValue?.coords.accuracy ?? 0,
      altitudeAccuracy:
        LocationService.instance.lastValue?.coords.altitudeAccuracy ?? null,
      heading: LocationService.instance.lastValue?.coords.heading ?? null,
      speed: LocationService.instance.lastValue?.coords.speed ?? null,
    },
    timestamp: 0,
  });

  useInterval(async () => {
    try {
      const result = await LocationService.instance.getCurrentPosition();

      if (JSON.stringify(result) == JSON.stringify(position)) {
        return;
      }

      setPosition(result);
    } catch (error) {
      console.log(JSON.stringify(error));
    }
  }, 1000);

  return position;
}
