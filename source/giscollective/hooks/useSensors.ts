import { useState, useEffect, useCallback } from "react";
import { Peripheral } from "react-native-ble-manager";
import { SensorsDevice } from "../lib/sensor";
import { SensorsService } from "./SensorsService";

export function useSensors(startScanning: boolean) {
  const [isScanning, setIsScanning] = useState(false);
  const [enabled, setEnabled] = useState<SensorsDevice[]>([]);
  const [peripherals, setPeripherals] = useState<Record<string, Peripheral>>(
    {},
  );
  const [peripheralList, setPeripheralList] = useState<any[]>([]);

  const service = SensorsService.instance;
  if (!service) {
    return {};
  }

  const update = useCallback(() => {
    const enabledSensors = service.peripheralList
      .filter((a) => a?.device)
      .flatMap((a) => a.device.sensors.filter((s) => s.enabled));

    setIsScanning(service.isScanning);
    setPeripherals(service.peripherals);
    setPeripheralList(service.peripheralList);
    setEnabled(enabledSensors);
  }, []);

  useEffect(() => {
    service.subscribe(update);

    if (startScanning) {
      service.startScan();
    }

    return () => {
      service.unsubscribe(update);
    };
  }, []);

  return {
    isScanning,
    peripherals,
    peripheralList,
    enabled,
    toggleConnection: (p) => service.toggleConnection(p),
  };
}
