import { useColorScheme } from "react-native";
import { Theme } from "../lib/theme";
import { useEffect, useState } from "react";

export function useTheme() {
  const themeName = useColorScheme() ?? "light";
  const [theme, setTheme] = useState(Theme.instance);

  theme.name = themeName;

  useEffect(() => {
    theme.name = themeName;

    setTheme(Theme.instance);
  }, [themeName]);

  return theme;
}
