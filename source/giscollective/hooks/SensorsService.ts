import { Buffer } from "buffer";

const SECONDS_TO_SCAN_FOR = 60;
const SERVICE_UUIDS: string[] = [];
const ALLOW_DUPLICATES = true;
import { Platform, PermissionsAndroid } from "react-native";

import BleManager, {
  BleDisconnectPeripheralEvent,
  Peripheral,
} from "react-native-ble-manager";
import { isPascoDevice, PascoSensorsDevice } from "../lib/pasco/device";
import VernierDevice from "../lib/vernier/device";

function sleep(ms: number) {
  return new Promise<void>((resolve) => setTimeout(resolve, ms));
}

export const handleAndroidPermissions = () => {
  if (Platform.OS === "android" && Platform.Version >= 31) {
    PermissionsAndroid.requestMultiple([
      PermissionsAndroid.PERMISSIONS.BLUETOOTH_SCAN,
      PermissionsAndroid.PERMISSIONS.BLUETOOTH_CONNECT,
    ]).then((result) => {
      if (result) {
        console.debug(
          "[handleAndroidPermissions] User accepts runtime permissions android 12+",
        );
      } else {
        console.error(
          "[handleAndroidPermissions] User refuses runtime permissions android 12+",
        );
      }
    });
  } else if (Platform.OS === "android" && Platform.Version >= 23) {
    PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    ).then((checkResult) => {
      if (checkResult) {
        console.debug(
          "[handleAndroidPermissions] runtime permission Android <12 already OK",
        );
      } else {
        PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        ).then((requestResult) => {
          if (requestResult) {
            console.debug(
              "[handleAndroidPermissions] User accepts runtime permission android <12",
            );
          } else {
            console.error(
              "[handleAndroidPermissions] User refuses runtime permission android <12",
            );
          }
        });
      }
    });
  }
};

export class Device {
  id: string;
  read: string;
  write: string;
  name: string = "hello";
  maxPacketLength: number = 200;
  notifications: any;
  listener: any;
  onClosed: any;
  allNotifications: any;

  constructor(device, name) {
    this.id = device;
    this.name = name;

    BleManager.onDidUpdateValueForCharacteristic(
      ({ value, peripheral, characteristic, service }) => {
        console.log("got data:", value, peripheral, characteristic, service);
        if (peripheral != this.id) {
          return;
        }

        const buffer = Buffer.from(value);
        const arrayBuffer = buffer.buffer.slice(
          buffer.byteOffset,
          buffer.byteOffset + buffer.byteLength,
        );

        this.onResponse(
          {
            buffer: arrayBuffer,
            getUint8: (offset) => buffer.readUInt8(offset),
            getUint16: (offset) => buffer.readUInt16LE(offset),
            getUint32: (offset) => buffer.readUInt32LE(offset),
            getFloat32: (offset) => buffer.readFloatLE(offset),
          },
          characteristic,
        );
      },
    );
  }

  setCharacteristics(characteristics) {
    this.notifications = characteristics.notifications;
    this.allNotifications = characteristics.allNotifications;
    this.read = characteristics.read;
    this.write = characteristics.write;
  }

  onResponse(
    message: {
      buffer: ArrayBuffer;
      getUint8: (offset: any) => number;
      getUint16: (offset: any) => number;
      getUint32: (offset: any) => number;
      getFloat32: (offset: any) => number;
    },
    characteristic: string,
  ) {
    throw new Error("Method not implemented.");
  }

  async setup(props) {
    this.onClosed = props.onClosed;
    this.onResponse = props.onResponse;

    return this.startNotifications();
  }

  async writeCommand(chunk) {
    try {
      await BleManager.writeWithoutResponse(
        this.id,
        this.write.service,
        this.write.characteristic,
        Array.from(chunk),
        chunk.length,
      );
    } catch (err) {
      console.log(err);
    }
  }

  async rawWrite(service, characteristic, chunk) {
    try {
      await BleManager.writeWithoutResponse(
        this.id,
        service,
        characteristic,
        Array.from(chunk),
        chunk.length,
      );
    } catch (err) {
      console.log(err);
    }
  }

  startNotifications() {
    const promises = [];

    if (!this.allNotifications) {
      return BleManager.startNotification(
        this.id,
        this.notifications.service,
        this.notifications.characteristic,
      );
    }

    for (const n of this.allNotifications) {
      promises.push(
        BleManager.startNotification(this.id, n.service, n.characteristic),
      );
    }

    return Promise.all(promises);
  }

  close() {}
}

export class SensorsService {
  static instance?: SensorsService;
  peripherals: any = {};
  listeners: any;

  _isScanning: boolean = false;
  _changes: number = 0;

  subscriptions: any[] = [];

  get peripheralList(): any[] {
    return Object.values(this.peripherals);
  }

  get isScanning() {
    return this._isScanning;
  }

  set isScanning(value) {
    this._isScanning = value;
    this.callSubscriptions();
  }

  get changes() {
    return this._changes;
  }

  set changes(value) {
    this._changes = value;
    this.callSubscriptions();
  }

  callSubscriptions() {
    for (const subscription of this.subscriptions) {
      subscription();
    }
  }

  subscribe(callback: () => void) {
    this.subscriptions.push(callback);
    callback();
  }

  unsubscribe(callback: () => void) {
    this.subscriptions = this.subscriptions.filter((a) => a != callback);
  }

  handleDiscoverPeripheral(peripheral: Peripheral) {
    console.log("handleDiscoverPeripheral:", peripheral);

    const id = peripheral.id;

    if (this.peripherals[id]) {
      return;
    }

    if (!peripheral.name) {
      peripheral.name = "Unknown";
    }

    if (
      peripheral.name.toLocaleLowerCase().indexOf("gdx-") == -1 &&
      !isPascoDevice(peripheral)
    ) {
      return;
    }

    this.upsertPeripheral(peripheral);
  }

  handleDisconnectedPeripheral(event: BleDisconnectPeripheralEvent) {
    const id = event.peripheral;

    let peripheral = this.peripherals[id];
    if (!peripheral) {
      return;
    }

    this.upsertPeripheral({
      ...peripheral,
      connecting: false,
      connected: false,
    });

    console.debug(`[handleDisconnectedPeripheral][${id}] disconnected.`);
  }

  upsertPeripheral(updatedPeripheral: Peripheral) {
    const id = updatedPeripheral.id;

    if (!this.peripherals[id]) {
      this.peripherals[id] = { changes: 0 };
    }

    for (let key of Object.keys(updatedPeripheral)) {
      this.peripherals[id][key] = updatedPeripheral[key];
    }

    this.peripherals[id].changes++;
    this.changes++;
  }

  clearPeripherals() {
    for (const item of this.peripheralList) {
      if (!item.connected) {
        delete this.peripherals[item.id];
      }
    }
  }

  async startScan() {
    if (this.isScanning) {
      return;
    }

    // reset found peripherals before scan
    this.clearPeripherals();

    console.debug("[startScan] starting scan...");
    this.isScanning = true;

    await BleManager.scan(SERVICE_UUIDS, SECONDS_TO_SCAN_FOR, ALLOW_DUPLICATES);
    console.debug("[startScan] scan promise returned successfully.");
  }

  handleStopScan() {
    console.debug("1 [handleStopScan] scan is stopped.", this.isScanning);
    this.isScanning = false;
    console.debug("2 [handleStopScan] scan is stopped.", this.isScanning);
  }

  async start() {
    BleManager.onDiscoverPeripheral((peripheral: Peripheral) => {
      console.log("=> peripheral", peripheral);
      this.handleDiscoverPeripheral(peripheral);
    });

    BleManager.onStopScan(() => this.handleStopScan());

    BleManager.onDisconnectPeripheral((peripheral: string) =>
      this.handleDisconnectedPeripheral(peripheral),
    );

    handleAndroidPermissions();

    await BleManager.start({
      verboseLogging: true,
      showAlert: false,
      restoreIdentifierKey: "scape-sensors",
    });
    console.debug("BleManager started.");
  }

  static init() {
    if (!SensorsService.instance) {
      SensorsService.instance = new SensorsService();
    }

    SensorsService.instance.start();
  }

  static destroy() {
    if (!SensorsService.instance) {
      return;
    }

    for (const listener of SensorsService.instance.listeners) {
      listener.remove();
    }

    SensorsService.instance = undefined;
  }

  async toggleConnection(p: Peripheral) {
    const id = p.id;

    let peripheral = this.peripherals[id];

    if (peripheral?.connected) {
      await BleManager.disconnect(peripheral.id);
      return;
    }

    await this.connectPeripheral(peripheral);
  }

  async connectPeripheral(peripheral: Peripheral) {
    if (!peripheral) {
      return;
    }

    const id = peripheral.id;

    this.upsertPeripheral({ id, connecting: true });

    try {
      await BleManager.connect(peripheral.id);
    } catch (err) {
      console.error(err);
      this.upsertPeripheral({
        ...peripheral,
        connecting: false,
        connected: false,
      });

      return;
    }

    this.upsertPeripheral({
      id,
      connecting: false,
      connected: true,
    });

    await sleep(900);

    const services = await BleManager.retrieveServices(peripheral.id);

    if (!services.characteristics) {
      return;
    }

    let device;
    let bleDevice = new Device(id, peripheral.name);

    if (isPascoDevice(this.peripherals[id])) {
      device = new PascoSensorsDevice(bleDevice, services.characteristics);
    }

    if (!isPascoDevice(this.peripherals[id])) {
      device = new VernierDevice(bleDevice, services.characteristics);
    }

    await device.open();

    this.upsertPeripheral({
      ...services,
      device,
      connecting: false,
      connected: true,
      sensors: {},
    });
  }
}
