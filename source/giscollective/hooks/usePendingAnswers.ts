import { useEffect, useState } from "react";
import { Store } from "../models/store";
import { CampaignAnswerModel } from "../models/CampaignAnswer";
import { getPendingAnswers } from "../lib/surveyAnswer";

export function usePendingAnswers() {
  const [answers, setAnswers] = useState<CampaignAnswerModel[]>([]);

  const reload = async () => {
    const pendingAnswers = await getPendingAnswers();
    const pendingIds = Object.values(pendingAnswers);

    const answersData = await Store.instance.cache.getPending("CampaignAnswer");
    const models = answersData
      .map((a) => JSON.parse(a))
      .filter((a) => !pendingIds.includes(a._id))
      .map((a) => new CampaignAnswerModel(a));

    setAnswers(models);
  };

  useEffect(() => {
    reload();
  }, []);

  return [answers, reload];
}
