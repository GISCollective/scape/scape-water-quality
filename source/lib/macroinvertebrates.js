import { macroInvertebratesData } from "../hooks/data";

export function bioticIndexMultiplier(name) {
  const multipliers = {
    "Sensitive.|PTI taxa Group I": 3,
    "Facultative|(Somewhat Tolerant)|PTI Taxa Group 2": 2,
    "Tolerant|PTI Taxa Group 3": 1,
  };

  let matchedKey = "";

  Object.keys(macroInvertebratesData).forEach((key) => {
    const match = macroInvertebratesData[key].find(
      (a) => a["Common Name"] == name,
    );

    if (match) {
      matchedKey = key;
    }
  });

  return multipliers[matchedKey] ?? 0;
}
