import { bioticIndexMultiplier } from "./macroinvertebrates";

describe("bioticIndexMultiplier", () => {
  it("returns 3 for Caddisfly", () => {
    const result = bioticIndexMultiplier("Caddisfly");

    expect(result).toEqual(3);
  });

  it("returns 2 for Clams", () => {
    const result = bioticIndexMultiplier("Clams");

    expect(result).toEqual(2);
  });

  it("returns 1 for Aquatic Worms", () => {
    const result = bioticIndexMultiplier("Aquatic Worms");

    expect(result).toEqual(1);
  });

  it("returns 0 for other values", () => {
    const result = bioticIndexMultiplier("other");

    expect(result).toEqual(0);
  });
});
