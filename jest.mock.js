jest.mock("react-native-permissions", () =>
  require("react-native-permissions/mock"),
);

jest.mock("react-native-ble-manager", () => ({}));

jest.mock("react-native/Libraries/Components/Switch/Switch", () => {
  const mockComponent = require("react-native/jest/mockComponent");
  return {
    default: mockComponent("react-native/Libraries/Components/Switch/Switch"),
  };
});
import "react-native-gesture-handler/jestSetup";
jest.mock("react-native/Libraries/EventEmitter/NativeEventEmitter");

import Geolocation from "@react-native-community/geolocation";

Geolocation.getCurrentPosition;

jest.mock("@react-native-community/geolocation", () => {
  return {
    getCurrentPosition: () => {},
  };
});

jest.mock("react-native-keyboard-aware-scroll-view", () => {
  const KeyboardAwareScrollView = ({ children }) => children;
  return { KeyboardAwareScrollView };
});

jest.mock("react-native-webview", () => {
  return (props) => "MapView: " + JSON.stringify(props);
});

jest.mock("react-native-fs", () => ({
  readFile: jest.fn(),
}));
