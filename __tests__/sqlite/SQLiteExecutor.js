const sqlite3 = require("sqlite3").verbose();

export class SQLiteExecutor {
  constructor() {}

  static openDatabase(name) {
    return new DatabaseExecutor(name);
  }
}

export class DatabaseExecutor {
  constructor(name) {
    this.db = new sqlite3.Database(name);
  }

  executeSql(statement, args) {
    if (statement.toLowerCase().indexOf("insert") === 0) {
      return this.executeInsert(statement, args);
    }

    return this.db.all(statement, args);
  }

  transaction(callback) {
    return new Promise((resolve, reject) => {
      const transaction = new TransactionExecutor(this.db, resolve, reject);
      this.db.serialize(() => {
        callback(transaction);
      });
    });
  }

  close() {
    this.db.close();
  }
}

export class TransactionExecutor {
  constructor(_db, _resolve, _reject) {
    this.db = _db;
    this.resolve = _resolve;
    this.reject = _reject;
    this.callsAmount = 0;
  }

  executeInsert(statement, args, callback, errorCallback) {
    this.callsAmount++;
    const _this = this;

    this.db.run(statement, args, function (err, result) {
      if (err) {
        if (errorCallback) {
          errorCallback(_this, err);
        }
        _this.reject(err);
      }

      const tmp = {
        // sqlite3 returns rows in array, so we need to imitate ResultSet object
        insertId: this.lastID,
        rowsAffected: this.changes,
      };
      if (callback) {
        callback(_this, tmp);
      }

      _this.callsAmount--;

      if (_this.callsAmount === 0) {
        _this.resolve(tmp);
      }
    });
  }

  executeSql(statement, args, callback, errorCallback) {
    if (statement.toLowerCase().indexOf("insert") === 0) {
      return this.executeInsert(statement, args, callback, errorCallback);
    }

    this.callsAmount++;

    // sqlite3 have many dedicated methods for calling sql, but 'all' is quite universal
    this.db.all(statement, args, (err, rows) => {
      if (err) {
        errorCallback?.(this, err);
        return this.reject(err);
      }

      const tmp = {
        // sqlite3 returns rows in array, so we need to imitate ResultSet object
        insertId: rows.insertId,
        rowsAffected: 0, // As you can see, unfortunately we are losing insertId and rowsAffected data, I am not sure if sqlite3 returns such a information
        rows: {
          length: rows.length,
          raw: () => rows,
          item: (index) => rows[index],
        },
      };

      callback?.(this, tmp);

      this.callsAmount--;

      if (this.callsAmount === 0) {
        this.resolve?.();
      }
    });
  }
}
