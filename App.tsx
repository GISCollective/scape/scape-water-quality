import React, { useEffect } from "react";

import { StatusBar, Image, ColorValue } from "react-native";

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import HomeScreenContainer from "./source/components/HomeScreenContainer";
import WebBrowserScreen from "./source/components/WebBrowserScreen";
import MacroScreen from "./source/components/MacroScreen";
import LocationScreen from "./source/components/LocationScreen";
import AboutScreen from "./source/components/AboutScreen";
import SensorsScreen from "./source/components/SensorsScreen";
import { SurveyScreen } from "./source/components/SurveyScreen";
import MacroDetailScreen from "./source/components/MacroDetailScreen";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { Provider as PaperProvider } from "react-native-paper";

import { faInfoCircle, faCompass } from "@fortawesome/free-solid-svg-icons";

const Stack = createNativeStackNavigator();
import { NetInfo } from "./source/giscollective/components/NetInfo";
import { SensorsService } from "./source/giscollective/hooks/SensorsService";
import { useTheme } from "./source/giscollective/hooks/useTheme";
import { AppContainer } from "./source/giscollective/AppContainer";

const AppNavigation = () => {
  const theme = useTheme();
  const isDarkMode = theme.name === "dark";

  const navigationStyle: ReactNavigation.Theme = {
    dark: isDarkMode,
    colors: {
      primary: theme.toColor("primary") as string,
      background: theme.toColor("gray-100") as string,
      card: theme.toColor("gray-100") as string,
      text: theme.toColor("gray-900") as string,
      border: theme.toColor("gray-900") as string,
      notification: theme.toColor("danger") as string,
    },
    fonts: {
      regular: {
        fontFamily: "Lato",
        fontWeight: "normal",
      },
      medium: {
        fontFamily: "Lato",
        fontWeight: "normal",
      },
      bold: {
        fontFamily: "Lato",
        fontWeight: "normal",
      },
      heavy: {
        fontFamily: "Lato",
        fontWeight: "bold",
      },
    },
  };

  const barStyle = {};

  const menuIconStyle = {
    width: 25,
    height: 25,
  };

  useEffect(() => {
    SensorsService.init();

    return () => {
      SensorsService.destroy();
    };
  }, []);

  function HomeStackScreen({}) {
    return (
      <>
        <StatusBar barStyle="default" />
        <NetInfo />
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={HomeScreenContainer}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="About"
            component={AboutScreen}
            options={{ title: "SCAPE Water Quality App" }}
          />
          <Stack.Screen
            name="Sensors"
            component={SensorsScreen}
            options={{ title: "Wireless Sensors" }}
          />
          <Stack.Screen
            name="Place"
            component={SurveyScreen}
            options={{ title: "SCAPE Place Data" }}
          />
          <Stack.Screen
            name="Browser"
            component={WebBrowserScreen}
            options={{ title: "SCAPE" }}
          />
          <Stack.Screen
            name="Macroinvertebrates"
            component={MacroScreen}
            options={{ title: "Macroinvertebrates by Sensitivity" }}
          />
          <Stack.Screen
            name="MacroinvertebratesDetails"
            component={MacroDetailScreen}
            options={{ title: "Macroinvertebrate Details" }}
          />
        </Stack.Navigator>
      </>
    );
  }

  function MacroStackScreen({}) {
    return (
      <Stack.Navigator>
        <Stack.Screen
          name="Macroinvertebrates"
          component={MacroScreen}
          options={{ title: "Macroinvertebrates by Sensitivity" }}
        />
        <Stack.Screen
          name="MacroinvertebratesDetails"
          component={MacroDetailScreen}
          options={{ title: "Macroinvert>ebrate Details" }}
        />
        <Stack.Screen
          name="Browser"
          component={WebBrowserScreen}
          options={{ title: "SCAPE" }}
        />
      </Stack.Navigator>
    );
  }

  function LocationStackScreen({}) {
    return (
      <Stack.Navigator>
        <Stack.Screen
          name="Location"
          component={LocationScreen}
          options={{ title: "My Location" }}
        />
      </Stack.Navigator>
    );
  }

  const Tab = createBottomTabNavigator();

  return (
    <PaperProvider>
      <NavigationContainer theme={navigationStyle}>
        <Tab.Navigator
          activeColor={`${String(theme.toColor("primary"))}`}
          inactiveColor={`${String(theme.toColor("dark"))}`}
          barStyle={{
            backgroundColor: theme.toColor("gray-100"),
            borderColor: theme.toColor("gray-200"),
            borderTopWidth: 1,
          }}
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color }) => {
              let icon;

              const tintColor = focused
                ? theme.toColor("primary")
                : theme.toColor("dark");

              if (route.name === "SCAPE") {
                icon = faInfoCircle;
              }

              if (route.name === "Macroinvertebrates") {
                return (
                  <Image
                    style={[menuIconStyle, { tintColor: tintColor }]}
                    source={require("./assets/invertebrate-icon.png")}
                  />
                );
              }

              if (route.name === "Location") {
                icon = faCompass;
              }

              // You can return any component that you like here!
              return (
                <FontAwesomeIcon icon={icon} size={25} color={tintColor} />
              );
            },
            headerShown: false,
          })}
        >
          <Tab.Screen name="SCAPE" component={HomeStackScreen} />
          <Tab.Screen name="Macroinvertebrates" component={MacroStackScreen} />
          <Tab.Screen name="Location" component={LocationStackScreen} />
        </Tab.Navigator>
      </NavigationContainer>
    </PaperProvider>
  );
};

export default function App() {
  /// http://192.168.1.190:9091
  return (
    <AppContainer
      spaceId="66407f165c93e50100393966"
      apiUrl="https://new.opengreenmap.org/api-v1"
      token="c1ae3e4e-47f2-c8d9-b5db-f221593c133d"
    >
      <AppNavigation />
    </AppContainer>
  );
}
