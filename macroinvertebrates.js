/* eslint-disable prettier/prettier */
const csv = require("csv-parser");
const fs = require("fs");
const { JSDOM } = require("jsdom");
const https = require("https");
const execSync = require("child_process").execSync;

function linkify(text) {
  if (!text) {
    return;
  }

  const urlRegex =
    /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
  let url = "";

  text.replace(urlRegex, function (u) {
    url = u;
  });

  if (url === "") {
    return undefined;
  }

  return url;
}

async function downloadImage(name, url, index) {
  const downloadUrl = url.split("=")[0];

  console.log("DOWNLOAD:", name, downloadUrl, index);
  const fileName = `${name}-${index}.jpg`;
  const pngFileName = `${name}-${index}.png`;
  const svgFileName = `${name}-${index}.svg`;
  const downloadPath = `./assets/photos/${fileName}`;

  if (!fs.existsSync(downloadPath)) {
    const file = fs.createWriteStream(downloadPath);

    await new Promise((resolve, reject) => {
      https
        .get(downloadUrl, function (response) {
          response.pipe(file);
        })
        .on("error", function (err) {
          reject(err);
        });

      file.on("finish", function () {
        file.close(resolve);
      });
    });
  }

  let options = "";

  if (index === 1) {
    options =
      ' -fuzz 50% -resize 800x800 -gravity center -extent 800x800 -transparent white -morphology erode square:1 -fill "#444444" +opaque none +repage';
  }

  if (index === 2) {
    options = ` -fuzz 10% -fill none -draw "matte 0,0 floodfill" `;
  }

  execSync(
    `convert ./assets/photos/${fileName} ${options} ./assets/photos/${pngFileName}`,
  );

  if (index === 1) {
    execSync(
      `convert ./assets/photos/${pngFileName} ./assets/photos/${svgFileName}`,
    );
  }

  return pngFileName;
}

async function processRecordImages(record) {
  if (!record) {
    return;
  }

  const name = record["Common Name"].toLowerCase();
  const nameKey = name
    .replace(/[^A-Za-z0-9]/g, "-")
    .split("-")
    .filter((a) => a)
    .join("-");

  if (record.icon) {
    record.icon = await downloadImage(nameKey, record.icon, 1);
  }

  if (record.drawing1) {
    record.drawing1 = await downloadImage(nameKey, record.drawing1, 2);
  }

  if (record.drawing2) {
    record.drawing2 = await downloadImage(nameKey, record.drawing2, 3);
  }

  if (record.photo_immature) {
    record.photo_immature = await downloadImage(
      nameKey,
      record.photo_immature,
      4,
    );
  }

  if (record.photo_adult) {
    record.photo_adult = await downloadImage(nameKey, record.photo_adult, 5);
  }

  return record;
}

async function processImages(results) {
  for (const category in results) {
    for (const [i, record] of results[category].entries()) {
      results[category][i] = await processRecordImages(record);
    }
  }

  return results;
}

async function main() {
  let results = {};

  await new Promise(async (resolve) => {
    let category = "";

    fs.createReadStream("./data/macroinvertebrates.csv")
      .pipe(csv())
      .on("data", (data) => {
        if (!data["Taxonomic Rank"]) {
          category = data["Common Name"]
            .split("\n")
            .map((a) => a.trim())
            .join("|");
          return;
        }

        if (!results[category]) {
          results[category] = [];
        }

        const record = {};

        Object.keys(data).forEach((key) => {
          if (!data[key]) {
            return;
          }

          record[key] = data[key]
            .replace(/(?:\r\n|\r|\n)/g, " ")
            .split(" ")
            .filter((a) => a)
            .join(" ");
        });

        record.Sources = linkify(record.Sources);
        record.Media = linkify(record.Media);

        results[category].push(record);
      })
      .on("end", () => {
        resolve();
      });
  });

  results = await processImages(results);

  fs.writeFileSync("macroinvertebrates.json", JSON.stringify(results));

  const dataFilePath = "hooks/useMacroInvertebrates.js";
  let code = `import React, {useState, useEffect, useCallback} from 'react';
  import Geolocation from '@react-native-community/geolocation';

  export default function useMacroInvertebrates() {
    const data = {
  `;

  for (let category in results) {
    code += `"${category}": [`;

    for (const record of results[category]) {
      if (!record) {
        continue;
      }

      code += "{\n";

      code += Object.keys(record)
        .map((key) => {
          if (
            [
              "icon",
              "drawing1",
              "drawing2",
              "photo_immature",
              "photo_adult",
            ].indexOf(key) !== -1 &&
            record[key]
          ) {
            return `"${key}": require("../assets/photos/${record[key]}"),`;
          }

          return `"${key}": ${JSON.stringify(record[key])},`;
        })
        .join("\n");

      code += "},\n";
    }

    code += "],\n";
  }

  code += "}; return data; }";

  fs.writeFileSync(dataFilePath, code);
}

main()
  .then(() => {
    console.log("done.");
  })
  .catch((err) => {
    console.error(err);
  });
