module.exports = {
  preset: "react-native",
  setupFiles: ["<rootDir>/jest.mock.js"],
  testMatch: ["**/?(*.)+(spec|test).(js|ts|jsx|tsx)"],
  setupFilesAfterEnv: [
    "@testing-library/jest-native/extend-expect",
    "<rootDir>/jest.setup.js",
  ],
  transformIgnorePatterns: ["node_modules/(?!@ngrx|(?!deck.gl)|ng-dynamic)"],

  reporters: ["jest-progress-bar-reporter", "summary"],
};
